var gulp           = require('gulp');
var sass           = require('gulp-sass');
var autoprefixer   = require('gulp-autoprefixer');
var nunjucksRender = require('gulp-nunjucks-render');
var plumber        = require('gulp-plumber');
var util           = require('gulp-util');
var gulpif         = require('gulp-if');
var changed        = require('gulp-changed');
var prettify       = require('gulp-prettify');
var frontMatter    = require('gulp-front-matter');

var production = util.env.production || util.env.prod || false;
var destPath = 'assets';

var config = {

    production: production,

    src: {
        templates     : 'views',
        templatesData : 'views/data',
        sass          : destPath + '/sass'
    },
    dest: {
        html          : 'html',
        css           : destPath + '/css',
    },

    setEnv: function(env) {
        if (typeof env !== 'string') return;
        this.env = env;
        this.production = env === 'production';
        process.env.NODE_ENV = env;
    },

    logEnv: function() {
        util.log(
            'Environment:',
            util.colors.white.bgRed(' ' + process.env.NODE_ENV + ' ')
        );
    },

    errorHandler: require('./gulp/util/handle-errors')
};

// sass
gulp.task('sass', function () {
    return gulp
        .src(config.src.sass + '/*.{sass,scss}')
        .pipe(sass({
            outputStyle: config.production ? 'compact' : 'expanded', // nested, expanded, compact, compressed
            precision: 5
        }))
        .on('error', config.errorHandler)
        .pipe(autoprefixer({
            browsers: ['ie 10', 'ie 11', 'last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(config.dest.css));
});

gulp.task('sass:watch', function () {
    gulp.watch([config.src.sass + '/**/*.{sass,scss}'], ['sass']);
});

// nunjucks
function renderHtml(onlyChanged) {
    nunjucksRender.nunjucks.configure({
        watch: false,
        trimBlocks: true,
        lstripBlocks: false
    });

    return gulp
        // .src([config.src.templates + '/**/[^_]*.njk'])
        .src([config.src.templates + '/pages/*.njk'])
        .pipe(plumber({
            errorHandler: config.errorHandler
        }))
        .pipe(gulpif(onlyChanged, changed(config.dest.html)))
        .pipe(frontMatter({ property: 'data' }))
        .pipe(nunjucksRender({
            PRODUCTION: config.production,
            path: [config.src.templates]
        }))
        .pipe(prettify({
            indent_size: 4,
            wrap_attributes: 'auto', // 'force'
            preserve_newlines: false,
            // unformatted: [],
            end_with_newline: true
        }))
        .pipe(gulp.dest(config.dest.html));
}

gulp.task('nunjucks', function() {
    return renderHtml();
});

gulp.task('nunjucks:changed', function() {
    return renderHtml(true);
});

gulp.task('nunjucks:watch', function() {
    gulp.watch([config.src.templates + '/**/*.njk'], ['nunjucks:changed']);
    // gulp.watch([config.src.templates + '/**/[^_]*.njk'], ['nunjucks:changed']);
    // gulp.watch([config.src.templates + '/**/_*.njk'], ['nunjucks']);
});

// watch
gulp.task('watch', [
    'sass:watch',
    'nunjucks:watch'
]);

// default
gulp.task('default', ['sass', 'nunjucks', 'watch']);
