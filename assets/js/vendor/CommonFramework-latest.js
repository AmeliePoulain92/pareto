/**
  * Infront Web toolkit version 1.2.36
  * Copyright (C) Infront 2017. All rights reserved.
  **/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*
This file only:
 Copyright
    Infront AS
    Vassilis Petroulias [DRDigit]

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
var Infront;
/*
This file only:
 Copyright
    Infront AS
    Vassilis Petroulias [DRDigit]

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
(function (Infront) {
    var Base64 = (function () {
        function Base64() {
            this.ie = /MSIE /.test(navigator.userAgent);
            this.ieo = /MSIE [67]/.test(navigator.userAgent);
            this.lookup = null;
        }
        Base64.prototype.decode = function (str) {
            if (str.length % 4) {
                throw new Error("InvalidCharacterError: 'B64.decode' failed: The string to be decoded is not correctly encoded.");
            }
            var buffer = this.fromUtf8(str), position = 0, len = buffer.length;
            var returnValue = null;
            if (this.ieo) {
                var ieResult = [];
                while (position < len) {
                    if (buffer[position] < 128)
                        ieResult.push(String.fromCharCode(buffer[position++]));
                    else if (buffer[position] > 191 && buffer[position] < 224)
                        ieResult.push(String.fromCharCode(((buffer[position++] & 31) << 6) | (buffer[position++] & 63)));
                    else
                        ieResult.push(String.fromCharCode(((buffer[position++] & 15) << 12) | ((buffer[position++] & 63) << 6) | (buffer[position++] & 63)));
                }
                returnValue = ieResult.join('');
            }
            else {
                var result = '';
                while (position < len) {
                    if (buffer[position] < 128)
                        result += String.fromCharCode(buffer[position++]);
                    else if (buffer[position] > 191 && buffer[position] < 224)
                        result += String.fromCharCode(((buffer[position++] & 31) << 6) | (buffer[position++] & 63));
                    else
                        result += String.fromCharCode(((buffer[position++] & 15) << 12) | ((buffer[position++] & 63) << 6) | (buffer[position++] & 63));
                }
                returnValue = result;
            }
            return returnValue;
        };
        Base64.prototype.encode = function (str) {
            var buffer = this.toUtf8(str), position = -1, len = buffer.length, nan0, nan1, nan2, enc = [undefined, undefined, undefined, undefined];
            var returnValue = null;
            if (this.ie) {
                var ieResult = [];
                while (++position < len) {
                    nan0 = buffer[position];
                    nan1 = buffer[++position];
                    enc[0] = nan0 >> 2;
                    enc[1] = ((nan0 & 3) << 4) | (nan1 >> 4);
                    if (isNaN(nan1))
                        enc[2] = enc[3] = 64;
                    else {
                        nan2 = buffer[++position];
                        enc[2] = ((nan1 & 15) << 2) | (nan2 >> 6);
                        enc[3] = (isNaN(nan2)) ? 64 : nan2 & 63;
                    }
                    ieResult.push(Base64.alphabet.charAt(enc[0]), Base64.alphabet.charAt(enc[1]), Base64.alphabet.charAt(enc[2]), Base64.alphabet.charAt(enc[3]));
                }
                returnValue = ieResult.join('');
            }
            else {
                var result = '';
                while (++position < len) {
                    nan0 = buffer[position];
                    nan1 = buffer[++position];
                    enc[0] = nan0 >> 2;
                    enc[1] = ((nan0 & 3) << 4) | (nan1 >> 4);
                    if (isNaN(nan1))
                        enc[2] = enc[3] = 64;
                    else {
                        nan2 = buffer[++position];
                        enc[2] = ((nan1 & 15) << 2) | (nan2 >> 6);
                        enc[3] = (isNaN(nan2)) ? 64 : nan2 & 63;
                    }
                    result += Base64.alphabet[enc[0]] + Base64.alphabet[enc[1]] + Base64.alphabet[enc[2]] + Base64.alphabet[enc[3]];
                }
                returnValue = result;
            }
            return result;
        };
        Base64.prototype.fromUtf8 = function (str) {
            var position = -1, len, buffer = [], enc = [undefined, undefined, undefined, undefined];
            if (!this.lookup) {
                len = Base64.alphabet.length;
                this.lookup = {};
                while (++position < len)
                    this.lookup[Base64.alphabet.charAt(position)] = position;
                position = -1;
            }
            len = str.length;
            while (++position < len) {
                enc[0] = this.lookup[str.charAt(position)];
                enc[1] = this.lookup[str.charAt(++position)];
                buffer.push((enc[0] << 2) | (enc[1] >> 4));
                enc[2] = this.lookup[str.charAt(++position)];
                if (enc[2] == 64)
                    break;
                buffer.push(((enc[1] & 15) << 4) | (enc[2] >> 2));
                enc[3] = this.lookup[str.charAt(++position)];
                if (enc[3] == 64)
                    break;
                buffer.push(((enc[2] & 3) << 6) | enc[3]);
            }
            return buffer;
        };
        Base64.prototype.toUtf8 = function (str) {
            var position = -1, len = str.length, chr, buffer = [];
            if (/^[\x00-\x7f]*$/.test(str))
                while (++position < len)
                    buffer.push(str.charCodeAt(position));
            else
                while (++position < len) {
                    chr = str.charCodeAt(position);
                    if (chr < 128)
                        buffer.push(chr);
                    else if (chr < 2048)
                        buffer.push((chr >> 6) | 192, (chr & 63) | 128);
                    else
                        buffer.push((chr >> 12) | 224, ((chr >> 6) & 63) | 128, (chr & 63) | 128);
                }
            return buffer;
        };
        Base64.alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        return Base64;
    }());
    Infront.Base64 = Base64;
})(Infront || (Infront = {}));
/**
 * AUTO-GENERATED CODE
 * Do not edit, your code will be overwritten!
 *
 * Library-conf-Parser version 1.1.0
 * code generated from: src/factory/library-factory.html
 * Generated at: 8/24/2017, 9:16:47 AM
 * Environment : release
 */
var Infront;
/**
 * AUTO-GENERATED CODE
 * Do not edit, your code will be overwritten!
 *
 * Library-conf-Parser version 1.1.0
 * code generated from: src/factory/library-factory.html
 * Generated at: 8/24/2017, 9:16:47 AM
 * Environment : release
 */
(function (Infront) {
    var LibraryConstants = (function () {
        function LibraryConstants() {
        }
        //Normal value-binding
        LibraryConstants.kTraceDebugLogs = "false";
        //Normal value-binding
        LibraryConstants.kDefaultClientApplication = "WEB";
        //Normal value-binding
        LibraryConstants.kBuildType = "release";
        //Normal value-binding
        LibraryConstants.kApplicationVersion = "1.2.36";
        //Normal value-binding
        LibraryConstants.kRoutingUrl = "http://mws.infrontservices.com:8068";
        //Normal value-binding
        LibraryConstants.kEnvType = "https://visual.infinancials.com/visual";
        //Normal value-binding
        LibraryConstants.kVisualConnectUrl = "" + LibraryConstants.kEnvType + "/access/connect";
        //Normal value-binding
        LibraryConstants.kComponentUrl = "" + LibraryConstants.kEnvType + "/component/";
        return LibraryConstants;
    }());
    Infront.LibraryConstants = LibraryConstants;
})(Infront || (Infront = {}));
///<reference path='LibraryConstants.ts' />
var InfrontUtil;
///<reference path='LibraryConstants.ts' />
(function (InfrontUtil) {
    var DEBUG = Infront.LibraryConstants.kTraceDebugLogs;
    var FormatSettings = (function () {
        function FormatSettings() {
            this.useBrowserFormatting = true;
            var testVal = 1.1;
            var testStr = testVal.toLocaleString();
            this.browserDecimalSeparator = testStr.substr(1, 1);
            testVal = 1000;
            testStr = testVal.toLocaleString();
            var tmp = testStr.substr(1, 1);
            this.browserThousandsSeparator = tmp == "0" ? "" : tmp;
            this.kilo = "k";
            this.mega = "m";
            this.useKiloMegaFormat = true;
        }
        return FormatSettings;
    }());
    InfrontUtil.FormatSettings = FormatSettings;
    InfrontUtil.formatSettings = new FormatSettings();
    var BrowserInfo = (function () {
        function BrowserInfo() {
            this.versionMajor = -1;
            this.versionMinor = -1;
            if (typeof (navigator) != "undefined") {
                this.uagent = navigator.userAgent.toLowerCase();
                this.opera = /mozilla/.test(this.uagent) && /applewebkit/.test(this.uagent) && /chrome/.test(this.uagent) && /safari/.test(this.uagent) && /opr/.test(this.uagent);
                this.safari = /applewebkit/.test(this.uagent) && /safari/.test(this.uagent) && !/chrome/.test(this.uagent) && !/crios/.test(this.uagent);
                this.firefox = /mozilla/.test(this.uagent) && /firefox/.test(this.uagent);
                this.chrome = /webkit/.test(this.uagent) && /chrome/.test(this.uagent);
                this.crios = /applewebkit/.test(this.uagent) && /safari/.test(this.uagent) && /crios/.test(this.uagent);
                this.msie = /msie/.test(this.uagent);
                this.versionStr = '';
                var browsers = ["opera", "safari", "firefox", "chrome", "crios", "msie"];
                for (var i = 0; i < browsers.length; i++) {
                    var x = browsers[i];
                    if (this[x]) {
                        if (x === "opera") {
                            this.versionStr = this.uagent.match(new RegExp("(opr)( |/)([0-9]+)"))[3];
                        }
                        else if (x == "safari") {
                            var match = this.uagent.match(new RegExp("(version)( |/)([0-9.]+)"));
                            this["debug"] = match;
                            this.versionStr = match[3];
                        }
                        else {
                            this.versionStr = this.uagent.match(new RegExp("(" + x + ")( |/)([0-9]+)"))[3];
                        }
                        break;
                    }
                }
                var parts = this.versionStr.split(".");
                this.versionMajor = /^\d+$/.test(parts[0]) ? parseInt(parts[0]) : -1;
                this.versionMinor = parts.length > 1 && /^\d+$/.test(parts[1]) ? parseInt(parts[1]) : -1;
            }
            else {
                this.headless = true;
                this.versionStr = "0.0";
                this.versionMajor = 0;
                this.versionMinor = 0;
            }
        }
        return BrowserInfo;
    }());
    InfrontUtil.BrowserInfo = BrowserInfo;
    InfrontUtil.browserInfo = new BrowserInfo();
    var LocalStorageCache = (function () {
        function LocalStorageCache() {
            this.prefix = LocalStorageCache.kCachePrefix;
        }
        LocalStorageCache.prototype.store = function (key, value, expires) {
            if (typeof (window.localStorage) != "undefined") {
                var useKey = this.prefix + key;
                var obj = {
                    "value": value,
                    "expires": expires
                };
                window.localStorage.setItem(useKey, JSON.stringify(obj));
            }
        };
        LocalStorageCache.prototype.get = function (key) {
            var retVal = null;
            if (typeof (window.localStorage) != "undefined") {
                var useKey = this.prefix + key;
                var obj = JSON.parse(window.localStorage.getItem(useKey));
                if (obj) {
                    var nowTs = (new Date()).getTime();
                    var expiresTs = Date.parse(obj["expires"]);
                    if (nowTs < expiresTs) {
                        retVal = obj["value"];
                    }
                    else {
                        window.localStorage.removeItem(useKey);
                    }
                }
            }
            return retVal;
        };
        LocalStorageCache.kCachePrefix = "com.goinfront.wtk.localstoragecache";
        return LocalStorageCache;
    }());
    InfrontUtil.LocalStorageCache = LocalStorageCache;
    var CallbackWithError = (function () {
        function CallbackWithError() {
        }
        return CallbackWithError;
    }());
    InfrontUtil.CallbackWithError = CallbackWithError;
    /**
     * Simple utility-class that lets you set up a callback that will be called when any number of other callbacks have completed.
     */
    var MultiCallbackHandler = (function () {
        function MultiCallbackHandler(completeCallback) {
            this.totalCallbacks = 0;
            this.returnedCallbacks = 0;
            this.completeCallback = completeCallback;
            this.log = false;
        }
        MultiCallbackHandler.prototype.setDebug = function (debug) {
            this.log = debug;
        };
        MultiCallbackHandler.prototype.addCallback = function (callback, name) {
            this.totalCallbacks++;
            if (this.log) {
                0;
            }
            return this.createCallback(callback, name);
        };
        MultiCallbackHandler.prototype.addCallbackWithError = function (callback, error, name) {
            this.totalCallbacks++;
            var retVal = new CallbackWithError();
            retVal.callback = this.createCallback(callback, name);
            retVal.error = this.createCallback(error, name);
            if (this.log) {
                0;
            }
            return retVal;
        };
        MultiCallbackHandler.prototype.createCallback = function (callback, name) {
            var _this = this;
            return function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                callback.apply(_this, args);
                _this.returnedCallbacks++;
                if (_this.log) {
                    0;
                }
                if (_this.returnedCallbacks == _this.totalCallbacks) {
                    _this.completeCallback();
                }
            };
        };
        return MultiCallbackHandler;
    }());
    InfrontUtil.MultiCallbackHandler = MultiCallbackHandler;
    var ObserverManager = (function () {
        function ObserverManager() {
            this.observers = [];
        }
        ObserverManager.prototype.addObserver = function (obs) {
            this.removeObserver(obs);
            this.observers.push(obs);
        };
        ObserverManager.prototype.removeObserver = function (obs) {
            for (var i = 0; i < this.observers.length; i++) {
                if (this.observers[i] === obs) {
                    this.observers.splice(i, 1);
                    break;
                }
            }
        };
        ObserverManager.prototype.foreach = function (func) {
            for (var i = 0; i < this.observers.length; i++) {
                func(this.observers[i]);
            }
        };
        ObserverManager.prototype.hasObservers = function () {
            return this.observers.length > 0;
        };
        return ObserverManager;
    }());
    InfrontUtil.ObserverManager = ObserverManager;
    /**
     * Utility-class used to manager targets using the InterLibraryLink
     */
    var LinkTargetManager = (function () {
        function LinkTargetManager() {
            this.targets = [];
        }
        LinkTargetManager.prototype.addTarget = function (target) {
            this.targets.push(target);
        };
        LinkTargetManager.prototype.removeTarget = function (target) {
            var idx = this.targets.indexOf(target);
            if (idx > -1) {
                this.targets.splice(idx, 1);
            }
        };
        LinkTargetManager.prototype.updateTargets = function (type, value) {
            var msg = new InterLibraryLink.Message();
            msg.type = type;
            msg.value = value;
            for (var i = 0; i < this.targets.length; i++) {
                this.targets[i].receiveMessage(msg);
            }
        };
        LinkTargetManager.prototype.isLinked = function () {
            return this.targets.length > 0;
        };
        return LinkTargetManager;
    }());
    InfrontUtil.LinkTargetManager = LinkTargetManager;
    /**
     * Utility-class for ad-hoc inline linking using the InterLibraryLink interfaces.
     */
    var InlineLinkTarget = (function () {
        function InlineLinkTarget(acceptTypes, callback) {
            this.dataTypes = acceptTypes;
            this.callback = callback;
        }
        InlineLinkTarget.prototype.accepts = function () {
            return this.dataTypes ? this.dataTypes : [];
        };
        InlineLinkTarget.prototype.receiveMessage = function (msg) {
            if (this.callback) {
                this.callback(msg);
            }
        };
        return InlineLinkTarget;
    }());
    InfrontUtil.InlineLinkTarget = InlineLinkTarget;
    /**
     * Utility-class to export values and/or names from enum
     */
    var EnumExport = (function () {
        function EnumExport() {
        }
        EnumExport.getNames = function (e) {
            return Object.keys(e).filter(function (v) { return isNaN(parseInt(v, 10)); });
        };
        EnumExport.getValues = function (e) {
            return Object.keys(e).map(function (v) { return parseInt(v, 10); }).filter(function (v) { return !isNaN(v); });
        };
        return EnumExport;
    }());
    InfrontUtil.EnumExport = EnumExport;
    /**
     * Copies object1, merges object2 into the copy and returns the copy.
     * Arrays will always be from object2 if conflicting.
     * @param object1 Defaults.
     * @param object2 Object that will be applied on the copy of object1
     * @param deepCopyObjects Whether objects that only exists on object2 and all arrays within object2 should be deepcopied or referenced
     * @returns a deep copy of object1 with object2 merged in
     */
    function mergeRecursiveCopy(object1, object2, deepCopyObjects) {
        if (deepCopyObjects === void 0) { deepCopyObjects = true; }
        var retVal = deepCopy(object1);
        mergeRecursive(retVal, object2, deepCopyObjects);
        return retVal;
    }
    InfrontUtil.mergeRecursiveCopy = mergeRecursiveCopy;
    /**
     * Takes object1, merges object2 into object1 and returns object1.
     * Arrays will always be from object2 if conflicting.
     * @param object1 Object that will be merged into and returned
     * @param object2 Object that will be applied on the copy of object1
     * @param deepCopyObjects Whether objects that only exists on object2 and all arrays within object2 should be deepcopied or referenced
     * @returns object1
     */
    function mergeRecursive(object1, object2, deepCopyObjects) {
        if (deepCopyObjects === void 0) { deepCopyObjects = true; }
        if (object1 != null && object2 != null) {
            for (var p in object2) {
                if (object2.hasOwnProperty(p)) {
                    if (object2[p] instanceof Date) {
                        object1[p] = new Date(object2[p].getTime());
                    }
                    if (object2[p] === null || isPrimitive(object2[p]) || isFunction(object2[p])) {
                        object1[p] = object2[p];
                    }
                    else if (isArray(object2[p])) {
                        object1[p] = deepCopyObjects ? arrayDeepCopy(object2[p]) : object2[p];
                    }
                    else if (isObject(object2[p])) {
                        if ('inf_clone' in object2[p] && isFunction(object2[p].inf_clone)) {
                            //var tmp = object2[p].inf_clone();
                            //object1[p] = mergeRecursive(tmp, object2[p], deepCopyObjects);
                            object1[p] = object2[p].inf_clone();
                        }
                        else if (object1[p] && isObject(object1[p])) {
                            mergeRecursive(object1[p], object2[p], deepCopyObjects);
                        }
                        else if (deepCopyObjects) {
                            object1[p] = deepCopy(object2[p]);
                        }
                        else {
                            object1[p] = object2[p];
                        }
                    }
                }
            }
        }
        return object1;
    }
    InfrontUtil.mergeRecursive = mergeRecursive;
    function deepCopy(source) {
        var retVal = null;
        if (source instanceof Date) {
            retVal = new Date(source.getTime());
        }
        else if (source === null || isPrimitive(source) || isFunction(source)) {
            retVal = source;
        }
        else if (isArray(source)) {
            retVal = arrayDeepCopy(source);
        }
        else if (isObject(source)) {
            if ('inf_clone' in source && isFunction(source.inf_clone)) {
                var tmp = source.inf_clone();
                retVal = mergeRecursive(tmp, source, true);
            }
            else {
                retVal = {};
                for (var p in source) {
                    if (source.hasOwnProperty(p)) {
                        retVal[p] = deepCopy(source[p]);
                    }
                }
            }
        }
        return retVal;
    }
    InfrontUtil.deepCopy = deepCopy;
    function arrayDeepCopy(source) {
        if (source == null) {
            return null;
        }
        var retVal = [];
        for (var i = 0; i < source.length; i++) {
            retVal[i] = deepCopy(source[i]);
        }
        return retVal;
    }
    InfrontUtil.arrayDeepCopy = arrayDeepCopy;
    function isArray(value) {
        if (Array.isArray) {
            return Array.isArray(value);
        }
        else {
            return Object.prototype.toString.call(value) === "[object Array]";
        }
    }
    InfrontUtil.isArray = isArray;
    function arraysAreEqual(array1, array2) {
        if (!array1 || !array2) {
            return false;
        }
        if (array1.length != array2.length) {
            return false;
        }
        for (var i = 0; i < array1.length; i++) {
            if (array1[i] instanceof Array && array2[i] instanceof Array) {
                if (!arraysAreEqual(array1[i], array2[i])) {
                    return false;
                }
            }
            else if (isObject(array1[i]) && isObject(array2[i])) {
                for (var key in array1[i]) {
                    if (array1[i][key] instanceof Array && array2[i][key] instanceof Array) {
                        if (!arraysAreEqual(array1[i][key], array2[i][key])) {
                            return false;
                        }
                    }
                    else if (array1[i][key] != array2[i][key] && !isFunction(array1[i][key])) {
                        return false;
                    }
                }
            }
            else if (array1[i] != array2[i]) {
                return false;
            }
        }
        return true;
    }
    InfrontUtil.arraysAreEqual = arraysAreEqual;
    //Check for supported functionality
    function isLocalStorageSupported() {
        var retVal = false;
        if (typeof (window) != "undefined" && typeof (window.localStorage) != "undefined") {
            var testKey = makeUUID();
            try {
                window.localStorage.setItem(testKey, "1");
                window.localStorage.removeItem(testKey);
                retVal = true;
            }
            catch (error) {
            }
        }
        return retVal;
    }
    InfrontUtil.isLocalStorageSupported = isLocalStorageSupported;
    //Value type checks.
    function getClassName(thing) {
        var funcNameRegex = /function (.{1,})\(/;
        var results = (funcNameRegex).exec(thing["constructor"].toString());
        return (results && results.length > 1) ? results[1] : "";
    }
    InfrontUtil.getClassName = getClassName;
    function isFunction(value) {
        return typeof (value) === "function";
    }
    InfrontUtil.isFunction = isFunction;
    function isPrimitive(value) {
        return typeof (value) === "boolean" || typeof (value) === "number" || typeof (value) === "string";
    }
    InfrontUtil.isPrimitive = isPrimitive;
    function isObject(value) {
        return value != null && typeof (value) === "object";
    }
    InfrontUtil.isObject = isObject;
    function isEmptyObject(obj) {
        for (var p in obj) {
            if (obj.hasOwnProperty(p)) {
                return false;
            }
        }
        return true;
    }
    InfrontUtil.isEmptyObject = isEmptyObject;
    function isString(value) {
        return typeof (value) === "string";
    }
    InfrontUtil.isString = isString;
    function isNumber(value) {
        return typeof (value) === "number" && !isNaN(value) && isFinite(value);
    }
    InfrontUtil.isNumber = isNumber;
    function isBoolean(value) {
        return typeof (value) === "boolean";
    }
    InfrontUtil.isBoolean = isBoolean;
    function isDate(value) {
        return value instanceof Date;
    }
    InfrontUtil.isDate = isDate;
    function isNumeric(value) {
        return /^[-]?[0-9]+([.,][0-9]+)?$/.test(value);
    }
    InfrontUtil.isNumeric = isNumeric;
    function isIntegerString(str) {
        return /^[0-9]+$/.test(str);
    }
    InfrontUtil.isIntegerString = isIntegerString;
    function makeUUID() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
    InfrontUtil.makeUUID = makeUUID;
    //Array utility methods
    function shiftArray(array, left) {
        return left ? shiftArrayLeft(array) : shiftArrayRight(array);
    }
    InfrontUtil.shiftArray = shiftArray;
    function shiftArrayRight(array) {
        var overflow = array[array.length - 1];
        for (var i = array.length - 2; i >= 0; i--) {
            array[i + 1] = array[i];
        }
        array[0] = overflow;
        return array;
    }
    function shiftArrayLeft(array) {
        var overflow = array[0];
        for (var i = 1; i < array.length; i++) {
            array[i - 1] = array[i];
        }
        array[array.length - 1] = overflow;
        return array;
    }
    function sumArray(array) {
        var retVal = 0;
        for (var i = 0; i < array.length; i++) {
            if (InfrontUtil.isNumber(array[i])) {
                retVal += array[i];
            }
        }
        return retVal;
    }
    InfrontUtil.sumArray = sumArray;
    function noElementsUndefined(array) {
        var retVal = true;
        for (var i = 0; i < array.length; i++) {
            if (typeof (array[i]) == "undefined") {
                retVal = false;
                break;
            }
        }
        return retVal;
    }
    InfrontUtil.noElementsUndefined = noElementsUndefined;
    function arrayContains(array, equalsFunc) {
        for (var i = 0; i < array.length; i++) {
            if (equalsFunc(array[i])) {
                return true;
            }
        }
        return false;
    }
    InfrontUtil.arrayContains = arrayContains;
    function checkArrayItems(array, check) {
        for (var i = 0; i < array.length; i++) {
            if (!check(array[i])) {
                return false;
            }
        }
        return true;
    }
    InfrontUtil.checkArrayItems = checkArrayItems;
    //Helpers to facilitate working with older Browsers or automating tedious tasks.
    function addClassName(element, className) {
        if (InfrontUtil.isString(className) && !hasClass(element, className)) {
            var classes = element.className.split(" ");
            classes.push(className);
            element.className = classes.join(" ");
        }
    }
    InfrontUtil.addClassName = addClassName;
    function removeClassName(element, className) {
        var classes = element.className.split(" ");
        var idx = classes.indexOf(className);
        if (idx > -1) {
            classes.splice(idx, 1);
            element.className = classes.join(" ");
        }
    }
    InfrontUtil.removeClassName = removeClassName;
    function hasClass(element, className) {
        return (" " + element.className + " ").indexOf(" " + className + " ") > -1;
    }
    InfrontUtil.hasClass = hasClass;
    var IEEventMap = {
        "click": "onclick"
    };
    function addEventListener(element, event, callback) {
        if (element.addEventListener) {
            element.addEventListener(event, callback);
        }
        else if (element["attachEvent"]) {
            var useEvent = event;
            if (IEEventMap.hasOwnProperty(event)) {
                useEvent = IEEventMap[event];
            }
            element["attachEvent"](useEvent, callback);
        }
    }
    InfrontUtil.addEventListener = addEventListener;
    function removeEventListener(element, event, callback) {
        if (element.removeEventListener) {
            element.removeEventListener(event, callback);
        }
        else if (element["detachEvent"]) {
            var useEvent = event;
            if (IEEventMap.hasOwnProperty(event)) {
                useEvent = IEEventMap[event];
            }
            element["detachEvent"](useEvent, callback);
        }
    }
    InfrontUtil.removeEventListener = removeEventListener;
    function removeAlLChildNodes(element) {
        while (element.hasChildNodes()) {
            element.removeChild(element.lastChild);
        }
    }
    InfrontUtil.removeAlLChildNodes = removeAlLChildNodes;
    function validatePropertyExists(obj, constant) {
        if (typeof (constant) == "string") {
            for (var p in obj) {
                if (obj.hasOwnProperty(p) && obj[p] == constant) {
                    return true;
                }
            }
        }
        return false;
    }
    InfrontUtil.validatePropertyExists = validatePropertyExists;
    //Date utility functions
    function getApiDateString(date) {
        if (date) {
            return "" + date.getFullYear() + "-" + zeroPad(date.getMonth() + 1) + "-" + zeroPad(date.getDate());
        }
        return null;
    }
    InfrontUtil.getApiDateString = getApiDateString;
    function getTradingAlgoParamDateString(date) {
        if (date) {
            return "" + date.getFullYear() + zeroPad(date.getMonth() + 1) + zeroPad(date.getDate());
        }
        return null;
    }
    InfrontUtil.getTradingAlgoParamDateString = getTradingAlgoParamDateString;
    function parseIso8601DateString(date) {
        var retVal = null;
        var year = null;
        var month = null;
        var day = null;
        if (date.length == 10) {
            year = parseInt(date.substr(0, 4));
            month = parseInt(date.substr(5, 2));
            day = parseInt(date.substr(8, 2));
        }
        if (year && month && day) {
            retVal = new Date();
            retVal.setFullYear(year);
            retVal.setMonth(month - 1);
            retVal.setDate(day);
        }
        return retVal;
    }
    InfrontUtil.parseIso8601DateString = parseIso8601DateString;
    function parseTradingAlgoParamDateString(date) {
        var retVal = null;
        var year = null;
        var month = null;
        var day = null;
        if (date.length == 8) {
            year = parseInt(date.substr(0, 4));
            month = parseInt(date.substr(4, 2));
            day = parseInt(date.substr(6, 2));
        }
        if (year && month && day) {
            retVal = new Date();
            retVal.setFullYear(year);
            retVal.setMonth(month - 1);
            retVal.setDate(day);
        }
        return retVal;
    }
    InfrontUtil.parseTradingAlgoParamDateString = parseTradingAlgoParamDateString;
    function formatTradingAlgoPriceString(src) {
        var value = src.replace(",", ".");
        return value;
    }
    InfrontUtil.formatTradingAlgoPriceString = formatTradingAlgoPriceString;
    /**
     * Replaces '_' with ' ' and capitalizes words for easier printing of server-provided enums.
     */
    function formatEnumString(value) {
        var parts = value.split("_");
        for (var i = 0; i < parts.length; i++) {
            parts[i] = parts[i].toLowerCase().replace(/^./, function (a) { return a.toUpperCase(); });
        }
        return parts.join(" ");
    }
    InfrontUtil.formatEnumString = formatEnumString;
    /**
     * Formats a date the way we show a standard timestamp.
     * If the date is today, we show just time (hh:mm:ss),
     * if the date is not today, we show just the date (DD.MM.YYYY).
     */
    function formatStandardTimestamp(date, seconds) {
        if (seconds === void 0) { seconds = true; }
        if (InfrontUtil.isNumber(date.getTime())) {
            if (isToday(date)) {
                return getPlainTimestring(date, seconds);
            }
            else {
                return getPlainDatestring(date);
            }
        }
        else {
            return "";
        }
    }
    InfrontUtil.formatStandardTimestamp = formatStandardTimestamp;
    function formatDate(date) {
        return getPlainDatestring(date);
    }
    InfrontUtil.formatDate = formatDate;
    function parsePlainTimeString(time) {
        var retVal = null;
        var parts = time.split(":");
        if (parts.length == 3 || parts.length == 2) {
            retVal = new Date();
            retVal.setHours(parseInt(parts[0]));
            retVal.setMinutes(parseInt(parts[1]));
            retVal.setSeconds(parts.length == 3 ? parseInt(parts[2]) : 0);
        }
        return retVal;
    }
    InfrontUtil.parsePlainTimeString = parsePlainTimeString;
    function getPlainTimestring(date, seconds) {
        if (seconds === void 0) { seconds = true; }
        if (date) {
            return zeroPad(date.getHours()) + ":" + zeroPad(date.getMinutes()) + (seconds ? ":" + zeroPad(date.getSeconds()) : "");
        }
        return null;
    }
    InfrontUtil.getPlainTimestring = getPlainTimestring;
    function getPlainDatestring(date) {
        if (date) {
            var year = date.getFullYear().toString().substr(2, 2);
            return zeroPad(date.getDate()) + "." + zeroPad(date.getMonth() + 1) + "." + year;
        }
        return null;
    }
    InfrontUtil.getPlainDatestring = getPlainDatestring;
    function zeroPad(val) {
        var retVal = val.toString();
        if (val < 10) {
            retVal = "0" + retVal;
        }
        return retVal;
    }
    function today() {
        var retVal = new Date();
        retVal.setHours(0, 0, 0, 0);
        return retVal;
    }
    InfrontUtil.today = today;
    function addDays(src, days) {
        var retVal = new Date(src.getTime());
        retVal.setDate(src.getDate() + days);
        return retVal;
    }
    InfrontUtil.addDays = addDays;
    function addMonths(src, months) {
        var retVal = new Date(src.getTime());
        retVal.setMonth(src.getMonth() + months);
        return retVal;
    }
    InfrontUtil.addMonths = addMonths;
    function isTimeStampSameDate(d1, d2) {
        // millisecondsInOneDay = 86400000;
        return (Math.floor(d1 / 86400000) === Math.floor(d2 / 86400000));
    }
    InfrontUtil.isTimeStampSameDate = isTimeStampSameDate;
    function isSameDate(d1, d2) {
        if (d1 && d2) {
            return isTimeStampSameDate(d1.getTime(), d2.getTime());
        }
        else {
            return false;
        }
    }
    InfrontUtil.isSameDate = isSameDate;
    function isLeapYear(year) {
        var leapYear;
        if (year % 4 > 0) {
            leapYear = false;
        }
        else if (year % 100 > 0) {
            leapYear = true;
        }
        else if (year % 400 > 0) {
            leapYear = false;
        }
        else {
            leapYear = true;
        }
        return leapYear;
    }
    InfrontUtil.isLeapYear = isLeapYear;
    /**
     * Returns the number of days in a given month.
     * @param month Zero-based month.
     * @param year four digit year.
     */
    function daysInMonth(month, year) {
        return [31, (InfrontUtil.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
    }
    InfrontUtil.daysInMonth = daysInMonth;
    function isToday(src) {
        return isSameDate(src, new Date());
    }
    InfrontUtil.isToday = isToday;
    //Number-formatting
    function formatPercent(val, decimals) {
        if (decimals === void 0) { decimals = 2; }
        return typeof (val) == "number" ? formatNumber(val, decimals) + "%" : "";
    }
    InfrontUtil.formatPercent = formatPercent;
    function formatNumber(val, decimals) {
        if (isNumber(val)) {
            if (InfrontUtil.formatSettings.useBrowserFormatting) {
                return customFormatNumber(val, decimals, InfrontUtil.formatSettings.browserThousandsSeparator, InfrontUtil.formatSettings.browserDecimalSeparator);
            }
            else {
                return customFormatNumber(val, decimals, InfrontUtil.formatSettings.thousandsSeparator, InfrontUtil.formatSettings.decimalSeparator);
            }
        }
        else {
            return "";
        }
    }
    InfrontUtil.formatNumber = formatNumber;
    function customFormatNumber(val, decimals, thousandsSeparator, decimalSeparator) {
        var prefix = "";
        if (roundTo(val, decimals) < 0) {
            prefix = "-";
        }
        val = Math.abs(val);
        var parts = val.toFixed(decimals).split('.');
        if (thousandsSeparator) {
            var i = parts[0].length;
            var str = '';
            var j;
            while (i > 3) {
                j = i - 3;
                str = thousandsSeparator + parts[0].slice(j, i) + str;
                i = j;
            }
            str = parts[0].slice(0, i) + str;
            parts[0] = str;
        }
        return prefix + parts.join(decimalSeparator);
    }
    function sign(value) {
        if (InfrontUtil.isNumber(value)) {
            if (value > 0) {
                return 1;
            }
            else if (value < 0) {
                return -1;
            }
            else {
                return 0;
            }
        }
        else {
            return NaN;
        }
    }
    InfrontUtil.sign = sign;
    function roundTo(value, decimals) {
        var pow = Math.pow(10, decimals);
        return Math.round(value * pow) / pow;
    }
    /**
     * Formats an integer suitable for use as an id.
     * @param id
     */
    function formatID(id) {
        var str = id.toString();
        var idx;
        if ((idx = str.indexOf(".")) > -1) {
            str = str.substring(0, idx);
        }
        else if ((idx = str.indexOf(",")) > -1) {
            str = str.substring(0, idx);
        }
        return str;
    }
    InfrontUtil.formatID = formatID;
    /**
     * Formats the value as an integer, but shortens it with k or m (or other letters specified in the language-dictionary):
     * Otherwise just format as integer.
     */
    function formatAndShorten(val, nb) {
        var retVal = null;
        try {
            if (isNumber(val)) {
                retVal = formatAndShortenInteger(val, nb);
            }
            else if (isNumeric(val)) {
                retVal = formatAndShortenInteger(parseFloat(val), nb);
            }
            else {
                var sData = val;
                retVal = shorterName(sData, nb);
            }
        }
        catch (e) {
            0;
        }
        return retVal;
    }
    InfrontUtil.formatAndShorten = formatAndShorten;
    /**
     * Formats the value as an integer, but shortens it with k or m (or other letters specified in the language-dictionary):
     * If val is bigger than 100 000, shorten with k (250 000 => 250k)
     * If val is bigger than 10 000 000, shorten with m (35 000 000 =>35m)
     * Otherwise just format as integer.
     */
    function formatAndShortenInteger(val, nb) {
        var retVal = null;
        var decimal = (nb) ? nb : 0;
        var absVal = Math.abs(val);
        if (InfrontUtil.formatSettings.useKiloMegaFormat) {
            if (absVal > 10000000) {
                retVal = formatNumber(val / 1000000, decimal) + InfrontUtil.formatSettings.mega;
            }
            else if (absVal > 100000) {
                retVal = formatNumber(val / 1000, decimal) + InfrontUtil.formatSettings.kilo;
            }
            else {
                retVal = formatNumber(val, decimal);
            }
        }
        else {
            retVal = formatNumber(val, decimal);
        }
        return retVal;
    }
    InfrontUtil.formatAndShortenInteger = formatAndShortenInteger;
    function parseFloatValue(src) {
        return src != null && InfrontUtil.isNumeric(src) ? parseFloat(src.replace(",", ".")) : null;
    }
    InfrontUtil.parseFloatValue = parseFloatValue;
    //Math utility methods
    function roundFloat(value, decimals) {
        var factor = Math.pow(10, decimals);
        return Math.round(value * factor) / factor;
    }
    InfrontUtil.roundFloat = roundFloat;
    /**Implements an actual zero-delay timeout.
     * This is used to ensure asynchronous behaviour, even when it is not strictly necessary.
     * The use of postMessage instead of setTimeout is for performance-reasons. postMessage has a significantly
     * shorter delay compared to setTimeout for a zero delay callback.
     * See FeedHandler or Observable for example.
     *
     * Edit: Changed implementation (at least temporarily) to setTimeout since performance for postMessage seems abysmal in IE.
     */
    //var zeroTimeoutMessageName = "zero-timeout-message";
    var timeouts = [];
    var zeroTimeoutId = null;
    var isLastTimeout = false;
    InfrontUtil.zeroTimeoutCount = 0;
    InfrontUtil.zeroTimeoutExecutionCount = 0;
    function setZeroTimeout(callback) {
        //zeroTimeoutCount++;
        setTimeout(callback, 0);
        //timeouts.push(callback);
        //if (zeroTimeoutId == null) {
        //    isLastTimeout = true;
        //    zeroTimeoutId = setTimeout(() => {
        //        executeZeroTimeouts();
        //    }, 0);
        //}
        //else {
        //    isLastTimeout = false;
        //}
        //window.postMessage(zeroTimeoutMessageName, "*");
    }
    InfrontUtil.setZeroTimeout = setZeroTimeout;
    function executeZeroTimeouts() {
        if (isLastTimeout) {
            //isLastTimeout = false;
            var currentTimeouts = timeouts;
            timeouts = [];
            zeroTimeoutId = null;
            //let limit = timeouts.length;
            InfrontUtil.zeroTimeoutExecutionCount++;
            //for (let i = 0; i < limit; i++) {
            //    timeouts[i]();
            //}
            //timeouts = timeouts.slice(limit);
            while (currentTimeouts.length > 0) {
                currentTimeouts.pop()();
            }
        }
        else {
            isLastTimeout = true;
            zeroTimeoutId = setTimeout(function () {
                executeZeroTimeouts();
            }, 0);
        }
    }
    //function handlePostMessage(event:MessageEvent) {
    //    if (event.source == window && event.data == zeroTimeoutMessageName) {
    //        event.stopPropagation();
    //        if (timeouts.length > 0) {
    //            var callback = timeouts.shift();
    //            callback();
    //        }
    //    }
    //}
    //window.addEventListener("message", handlePostMessage, true);
    //Parse JSON, if necessary remove control-characters from the string.
    function parseJson(str) {
        var retVal = null;
        try {
            retVal = JSON.parse(str);
        }
        catch (e) {
            var charArray = str.split("");
            for (var i = 0; i < charArray.length; i++) {
                var code = charArray[i].charCodeAt(0);
                if (code < 32) {
                    charArray.splice(i, 1);
                    i--;
                }
            }
            var newStr = charArray.join("");
            try {
                retVal = JSON.parse(newStr);
            }
            catch (e2) {
                //Still no joy, actual JSON syntax error
                0;
            }
        }
        return retVal;
    }
    InfrontUtil.parseJson = parseJson;
    //Standard error-callback that just logs the error-message.
    function errorLogCallback(error_code, error_message) {
        0;
    }
    InfrontUtil.errorLogCallback = errorLogCallback;
    function isPositive(double) {
        return (double >= 0);
    }
    InfrontUtil.isPositive = isPositive;
    //Infinancials function for Not Meaningful: NM
    function isNM(value) {
        try {
            return (value.trim() == "N/M" || value.trim() == "N/M+" || value.trim() == "N/M-");
        }
        catch (e) {
            return false;
        }
    }
    InfrontUtil.isNM = isNM;
    //Infinancials function for NM+ -  Non meaningful (Financial ratios, growth rates) :
    // - ratios > 1000%
    // - growth > 100%
    function isNMPlus(value) {
        try {
            return (value.trim() == "N/M+");
        }
        catch (e) {
            return false;
        }
    }
    InfrontUtil.isNMPlus = isNMPlus;
    //Infinancials function for NM- -  Non meaningful (Financial ratios, growth rates).
    // - ratios < 100%
    // - growth < 100%
    function isNMMinus(value) {
        try {
            return (value.trim() == "N/M-");
        }
        catch (e) {
            return false;
        }
    }
    InfrontUtil.isNMMinus = isNMMinus;
    //Infinancials function for NA - Not avalaible value.
    function isNA(value) {
        try {
            return (value.trim() == "N/A");
        }
        catch (e) {
            return false;
        }
    }
    InfrontUtil.isNA = isNA;
    function removeSpaceAndPercent(value) {
        var myValue = value;
        try {
            myValue = myValue.replace("%", "");
            myValue = myValue.replace(" ", "");
            return myValue;
        }
        catch (e) {
            return value;
        }
    }
    InfrontUtil.removeSpaceAndPercent = removeSpaceAndPercent;
    function shorterName(str, nb) {
        if (nb === void 0) { nb = 700; }
        if (str != null && str != "N/A" && str != "N/M" && str != "N/M+" && str != "N/M-")
            return (str.length >= (nb - 2)) ? str.substring(0, nb) + ".." : str;
        else if (str == null)
            return "";
        else
            return str;
    }
    InfrontUtil.shorterName = shorterName;
    function replaceAll(search, replace, subject) {
        return subject.split(search).join(replace);
    }
    InfrontUtil.replaceAll = replaceAll;
    //Standard error-callback that just logs the error-message.
    function debugLog(error_txt, error_obj) {
        var obj = error_obj || null;
        if (DEBUG) {
            var logs = "[" + getTime() + "] -- " + error_txt;
            obj != null ? 0 : 0;
        }
    }
    InfrontUtil.debugLog = debugLog;
    //Standard error-callback that just logs the error-message.
    function errorLog(error_txt, error_obj) {
        var obj = error_obj || null;
        if (DEBUG) {
            var logs = "[ERROR][" + getTime() + "] -- " + error_txt;
            obj != null ? 0 : 0;
        }
    }
    InfrontUtil.errorLog = errorLog;
    function getTime() {
        var dt = new Date();
        var hours = (dt.getHours() < 10) ? "0" + dt.getHours() : "" + dt.getHours();
        var minutes = (dt.getMinutes() < 10) ? "0" + dt.getMinutes() : "" + dt.getMinutes();
        var seconds = (dt.getSeconds() < 10) ? "0" + dt.getSeconds() : "" + dt.getSeconds();
        var time = hours + ":" + minutes + ":" + seconds;
        return time;
    }
    InfrontUtil.getTime = getTime;
})(InfrontUtil || (InfrontUtil = {}));
///<reference path='Util.ts' />
var Infront;
///<reference path='Util.ts' />
(function (Infront) {
    var GenericBinding = (function () {
        function GenericBinding(boundObject, transmitter, formatter) {
            var _this = this;
            this.object = boundObject;
            this.transmitter = transmitter;
            this.formatter = formatter;
            //The listen-method enables support for changeable formats.
            if (this.formatter && this.formatter.listen) {
                this.formatter.listen(function () {
                    if (typeof _this.cachedValue != "undefined") {
                        _this.valueUpdated(_this.cachedValue);
                    }
                });
            }
        }
        GenericBinding.prototype.cleanup = function () {
            if (this.formatter && this.formatter.cleanupFormatter) {
                this.formatter.cleanupFormatter();
            }
        };
        /**
         * This method is called by the observable this binding is watching when the value
         * of the observable has been updated.
         * @param val
         */
        GenericBinding.prototype.valueUpdated = function (val) {
            this.cachedValue = val;
            var formattedValue = this.formatter ? this.formatter.format(val) : val;
            this.transmitter.transmit(this.object, formattedValue);
        };
        return GenericBinding;
    }());
    Infront.GenericBinding = GenericBinding;
    var InlineTransmitter = (function () {
        function InlineTransmitter(callback) {
            this.callback = callback;
        }
        InlineTransmitter.prototype.transmit = function (target, value) {
            this.callback(value);
        };
        return InlineTransmitter;
    }());
    var FieldTransmitter = (function () {
        function FieldTransmitter(field) {
            this.field = field;
        }
        FieldTransmitter.prototype.transmit = function (target, value) {
            target[this.field] = value;
        };
        return FieldTransmitter;
    }());
    var HTMLAttributeTransmitter = (function () {
        function HTMLAttributeTransmitter(attribute) {
            this.attribute = attribute;
        }
        HTMLAttributeTransmitter.prototype.transmit = function (target, value) {
            target.setAttribute(this.attribute, value);
        };
        return HTMLAttributeTransmitter;
    }());
    var DOMElementTransmitter = (function () {
        function DOMElementTransmitter() {
        }
        DOMElementTransmitter.prototype.transmit = function (target, value) {
            InfrontUtil.removeAlLChildNodes(target);
            target.appendChild(value);
        };
        return DOMElementTransmitter;
    }());
    //    export class StyleBinding implements Binding {
    //        private boundEl:HTMLElement;
    //        private translator:(value:any)=>string;
    //
    //        constructor(boundEl:HTMLElement, translator:(value:any)=>string) {
    //            this.boundEl = boundEl;
    //            this.translator = translator;
    //        }
    //
    //        public valueUpdated(value:any):void {
    //            this.boundEl.setAttribute("style", this.translator(value));
    //        }
    //    }
    var ObjectFieldFormatter = (function () {
        function ObjectFieldFormatter(sourceField) {
            this.sourceField = sourceField;
        }
        ObjectFieldFormatter.prototype.format = function (value) {
            var retVal = value;
            if (value != null) {
                for (var i = 0; i < this.sourceField.length; i++) {
                    retVal = retVal[this.sourceField[i]];
                }
                return retVal;
            }
            else {
                return "";
            }
        };
        return ObjectFieldFormatter;
    }());
    var NumberFormatter = (function () {
        function NumberFormatter(decimals) {
            this.decimals = typeof (decimals) == "number" ? decimals : 0;
        }
        NumberFormatter.prototype.format = function (value) {
            if (value == undefined || value == null) {
                return "";
            }
            else if (InfrontUtil.isNumber(value)) {
                return InfrontUtil.formatNumber(value, this.decimals);
            }
            else {
                return value;
            }
        };
        return NumberFormatter;
    }());
    var VolumeFormatter = (function () {
        function VolumeFormatter() {
        }
        VolumeFormatter.prototype.format = function (value) {
            if (InfrontUtil.isNumber(value)) {
                this.decimals = (value != Math.floor(value)) ? (value.toString()).split('.')[1].length : 0;
                if (this.decimals > 4) {
                    this.decimals = 4;
                }
                return InfrontUtil.formatNumber(value, this.decimals);
            }
            else if (value == null) {
                return "";
            }
            else {
                return value;
            }
        };
        return VolumeFormatter;
    }());
    var DynamicNumberFormatter = (function (_super) {
        __extends(DynamicNumberFormatter, _super);
        function DynamicNumberFormatter(decimals) {
            var _this = _super.call(this, decimals.get()) || this;
            _this.decimalsObs = decimals;
            _this.decimalsObs.observe(_this);
            return _this;
        }
        DynamicNumberFormatter.prototype.valueUpdated = function (val) {
            this.decimals = val;
            if (this.decimalsUpdated) {
                this.decimalsUpdated();
            }
        };
        DynamicNumberFormatter.prototype.listen = function (changeCallback) {
            this.decimalsUpdated = changeCallback;
        };
        DynamicNumberFormatter.prototype.cleanupFormatter = function () {
            this.decimalsObs.unbind(this);
            this.decimalsUpdated = null;
        };
        return DynamicNumberFormatter;
    }(NumberFormatter));
    var ShortenNumberFormatter = (function () {
        function ShortenNumberFormatter() {
        }
        ShortenNumberFormatter.prototype.format = function (value) {
            if (InfrontUtil.isNumber(value)) {
                return InfrontUtil.formatAndShortenInteger(value);
            }
            else {
                return value;
            }
        };
        return ShortenNumberFormatter;
    }());
    var IDFormatter = (function () {
        function IDFormatter() {
        }
        IDFormatter.prototype.format = function (value) {
            if (InfrontUtil.isNumber(value)) {
                return InfrontUtil.formatID(value);
            }
            else {
                return value;
            }
        };
        return IDFormatter;
    }());
    var PercentFormatter = (function () {
        function PercentFormatter(decimals) {
            this.decimals = decimals;
        }
        PercentFormatter.prototype.format = function (value) {
            if (typeof (value) == "number") {
                return InfrontUtil.formatPercent(value, this.decimals);
            }
            else {
                return value;
            }
        };
        return PercentFormatter;
    }());
    var TimestampFormatter = (function () {
        function TimestampFormatter(showSeconds) {
            this.showSeconds = showSeconds;
        }
        TimestampFormatter.prototype.format = function (value) {
            var date = null;
            if (InfrontUtil.isString(value) && value.length > 0) {
                date = new Date(value);
            }
            else if (InfrontUtil.isDate(value)) {
                date = (value);
            }
            var str = "";
            if (date) {
                str = InfrontUtil.formatStandardTimestamp(date, this.showSeconds);
            }
            return str;
        };
        return TimestampFormatter;
    }());
    var DateFormatter = (function () {
        function DateFormatter() {
        }
        DateFormatter.prototype.format = function (value) {
            var date = null;
            if (InfrontUtil.isString(value)) {
                date = new Date(value);
            }
            else if (InfrontUtil.isDate(value)) {
                date = value;
            }
            var str = "";
            if (date) {
                str = InfrontUtil.formatDate(date);
            }
            return str;
        };
        return DateFormatter;
    }());
    var InlineFormatter = (function () {
        function InlineFormatter(translateFunction) {
            this.translateFunction = translateFunction;
        }
        InlineFormatter.prototype.format = function (value) {
            return this.translateFunction(value);
        };
        return InlineFormatter;
    }());
    var BindingFactory = (function () {
        function BindingFactory() {
        }
        BindingFactory.createInlineBinding = function (callback) {
            return new GenericBinding(null, new InlineTransmitter(callback), null);
        };
        BindingFactory.createValueBinding = function (target, field) {
            if (field === void 0) { field = "innerHTML"; }
            return new GenericBinding(target, new FieldTransmitter(field), null);
        };
        BindingFactory.createDOMElementBinding = function (target) {
            return new GenericBinding(target, new DOMElementTransmitter(), null);
        };
        BindingFactory.createNumberFormatValueBinding = function (target, field, decimals) {
            return new GenericBinding(target, new FieldTransmitter(field), decimals instanceof Infront.Observable ? new DynamicNumberFormatter(decimals) : new NumberFormatter(decimals));
        };
        BindingFactory.createVolumeNumberBinding = function (target, field) {
            return new GenericBinding(target, new FieldTransmitter(field), new VolumeFormatter());
        };
        BindingFactory.createShortenNumberBinding = function (target, field) {
            return new GenericBinding(target, new FieldTransmitter(field), new ShortenNumberFormatter());
        };
        BindingFactory.createIDBinding = function (target, field) {
            return new GenericBinding(target, new FieldTransmitter(field), new IDFormatter());
        };
        BindingFactory.createPercentBinding = function (target, decimals, field) {
            if (field === void 0) { field = "innerHTML"; }
            return new GenericBinding(target, new FieldTransmitter(field), new PercentFormatter(decimals));
        };
        BindingFactory.createTimestampBinding = function (target, showSeconds, field) {
            if (showSeconds === void 0) { showSeconds = true; }
            if (field === void 0) { field = "innerHTML"; }
            return new GenericBinding(target, new FieldTransmitter(field), new TimestampFormatter(showSeconds));
        };
        BindingFactory.createDateBinding = function (target, field) {
            if (field === void 0) { field = "innerHTML"; }
            return new GenericBinding(target, new FieldTransmitter(field), new DateFormatter());
        };
        BindingFactory.createTranslateBinding = function (target, translateFunction, field) {
            if (field === void 0) { field = "innerHTML"; }
            return new GenericBinding(target, new FieldTransmitter(field), new InlineFormatter(translateFunction));
        };
        BindingFactory.createBinding = function (target, formatter, transmitter) {
            return new GenericBinding(target, transmitter, formatter);
        };
        BindingFactory.createObjectFieldBinding = function (target, sourceField, targetField) {
            if (targetField === void 0) { targetField = "innerHTML"; }
            return new GenericBinding(target, new FieldTransmitter(targetField), new ObjectFieldFormatter(sourceField));
        };
        BindingFactory.createHTMLAttributeBinding = function (target, attribute, formatter) {
            return new GenericBinding(target, new HTMLAttributeTransmitter(attribute), new InlineFormatter(formatter));
        };
        return BindingFactory;
    }());
    Infront.BindingFactory = BindingFactory;
    var ClassBinding = (function () {
        function ClassBinding(boundEl, className, condition) {
            this.boundEl = boundEl;
            this.condition = condition;
            this.className = className;
        }
        ClassBinding.prototype.valueUpdated = function (val) {
            if (this.condition(val)) {
                InfrontUtil.addClassName(this.boundEl, this.className);
            }
            else {
                InfrontUtil.removeClassName(this.boundEl, this.className);
            }
        };
        return ClassBinding;
    }());
    Infront.ClassBinding = ClassBinding;
    var SelectInputBinding = (function () {
        function SelectInputBinding(notifyListenersOnBind) {
            if (notifyListenersOnBind === void 0) { notifyListenersOnBind = true; }
            var _this = this;
            this.observers = [];
            this.changeListener = function (event) { return _this.selectedUpdated(); };
            this.value = null;
        }
        SelectInputBinding.prototype.getSelectedIndex = function () {
            if (this.element) {
                return this.element.selectedIndex;
            }
            else {
                return this.value;
            }
        };
        SelectInputBinding.prototype.bind = function (element) {
            var _this = this;
            this.element = element;
            if (element != null && this.value != null) {
                element.selectedIndex = this.value;
            }
            InfrontUtil.addEventListener(element, "change", this.changeListener);
            if (this.notifyListenersOnBind) {
                InfrontUtil.setZeroTimeout(function () {
                    _this.selectedUpdated();
                });
            }
        };
        SelectInputBinding.prototype.unbind = function () {
            if (this.element) {
                InfrontUtil.removeEventListener(this.element, "change", this.changeListener);
                this.element = null;
            }
        };
        SelectInputBinding.prototype.selectedUpdated = function () {
            for (var i = 0; i < this.observers.length; i++) {
                this.observers[i](this.element.selectedIndex);
            }
        };
        SelectInputBinding.prototype.setSelected = function (newSelected) {
            this.value = newSelected;
            if (this.element) {
                this.element.selectedIndex = newSelected;
                this.selectedUpdated();
            }
        };
        SelectInputBinding.prototype.observe = function (observer) {
            this.observers.push(observer);
        };
        SelectInputBinding.prototype.setDisabled = function (disabled) {
            if (this.element) {
                this.element.disabled = disabled;
            }
        };
        return SelectInputBinding;
    }());
    Infront.SelectInputBinding = SelectInputBinding;
    var SelectStringValueInputBinding = (function () {
        function SelectStringValueInputBinding() {
            var _this = this;
            this.observers = [];
            this.changeListener = function (event) { return _this.selectedUpdated(); };
        }
        SelectStringValueInputBinding.prototype.bind = function (element) {
            this.element = element;
            InfrontUtil.addEventListener(element, "change", this.changeListener);
            this.selectedUpdated();
        };
        SelectStringValueInputBinding.prototype.unbind = function () {
            if (this.element) {
                InfrontUtil.removeEventListener(this.element, "change", this.changeListener);
                this.element = null;
            }
        };
        SelectStringValueInputBinding.prototype.observe = function (observer) {
            this.observers.push(observer);
        };
        SelectStringValueInputBinding.prototype.selectedUpdated = function () {
            for (var i = 0; i < this.observers.length; i++) {
                this.observers[i](this.element.value);
            }
        };
        SelectStringValueInputBinding.prototype.setDisabled = function (disabled) {
            if (this.element) {
                this.element.disabled = disabled;
            }
        };
        SelectStringValueInputBinding.prototype.setSelected = function (newSelected) {
            if (this.element) {
                this.element.selectedIndex = newSelected;
            }
        };
        return SelectStringValueInputBinding;
    }());
    Infront.SelectStringValueInputBinding = SelectStringValueInputBinding;
    var RadioInputBinding = (function () {
        function RadioInputBinding(onChange) {
            var _this = this;
            this.boundElements = [];
            this.onChange = onChange;
            this.value = null;
            this.onClickEvent = function (event) {
                _this.elementClicked(event.srcElement);
            };
        }
        RadioInputBinding.prototype.getValue = function () {
            return this.value;
        };
        RadioInputBinding.prototype.setSelectedValue = function (value) {
            for (var i = 0; i < this.boundElements.length; i++) {
            }
        };
        RadioInputBinding.prototype.elementClicked = function (element) {
            var newVal = element.value;
            if (this.value != newVal) {
                this.value = newVal;
                if (this.onChange) {
                    this.onChange(this.value);
                }
            }
        };
        RadioInputBinding.prototype.bind = function (element) {
            if (this.boundElements.indexOf(element) == -1) {
                this.boundElements.push(element);
                InfrontUtil.addEventListener(element, "click", this.onClickEvent);
                if (element.checked) {
                    this.value = element.value;
                }
            }
        };
        RadioInputBinding.prototype.unbind = function () {
            for (var i = 0; i < this.boundElements.length; i++) {
                InfrontUtil.removeEventListener(this.boundElements[i], "click", this.onClickEvent);
            }
            this.boundElements = [];
        };
        return RadioInputBinding;
    }());
    Infront.RadioInputBinding = RadioInputBinding;
    var CheckBoxInputBinding = (function () {
        function CheckBoxInputBinding(onChange) {
            var _this = this;
            this.onChange = onChange;
            this.value = null;
            this.onClickEvent = function (event) {
                _this.elementClicked(event.srcElement);
            };
        }
        CheckBoxInputBinding.prototype.getValue = function () {
            return this.value;
        };
        CheckBoxInputBinding.prototype.setValue = function (value) {
            this.value = value;
            if (this.element) {
                this.element.checked = value;
            }
        };
        CheckBoxInputBinding.prototype.elementClicked = function (element) {
            if (this.value != element.checked) {
                this.value = element.checked;
                if (this.onChange) {
                    this.onChange(this.value);
                }
            }
        };
        CheckBoxInputBinding.prototype.bind = function (element) {
            this.element = element;
            if (this.value) {
                element.checked = this.value;
            }
            InfrontUtil.addEventListener(element, "click", this.onClickEvent);
            if (element.checked) {
                this.value = element.checked;
            }
        };
        CheckBoxInputBinding.prototype.unbind = function () {
            InfrontUtil.removeEventListener(this.element, "click", this.onClickEvent);
            this.element = null;
        };
        return CheckBoxInputBinding;
    }());
    Infront.CheckBoxInputBinding = CheckBoxInputBinding;
    var TextInputBinding = (function () {
        function TextInputBinding() {
            var _this = this;
            this.lastvalue = null;
            this.observers = [];
            this.keyUpListener = function (event) { return _this.keyUp(event); };
            this.keyDownListener = function (event) { return _this.keyDown(event); };
            this.focusListener = function (event) { return _this.focus(); };
            this.blurListener = function (event) { return _this.blur(); };
            this.keyCodeObservers = {};
            this.placeholder = null;
        }
        TextInputBinding.prototype.bind = function (element) {
            this.element = element;
            if (this.lastvalue) {
                this.element.value = this.lastvalue;
            }
            InfrontUtil.addEventListener(element, "keyup", this.keyUpListener);
            InfrontUtil.addEventListener(element, "keydown", this.keyDownListener);
            if (this.scrolling) {
                //element.onscroll = (event:Event)=>this.scroll(event);
                InfrontUtil.addEventListener(element, "mousewheel", this.scrollCallback);
            }
            if (this.onClickCallback) {
                InfrontUtil.addEventListener(element, "click", this.onClickCallback);
            }
            InfrontUtil.addEventListener(element, "focus", this.focusListener);
            InfrontUtil.addEventListener(element, "blur", this.blurListener);
            if (this.placeholder != null && this.element.value === "") {
                this.element.value = this.placeholder;
                this.element.setAttribute('placeholder', this.placeholder);
                InfrontUtil.addClassName(this.element, TextInputBinding.kPlaceholderClass);
            }
        };
        TextInputBinding.prototype.unbind = function () {
            if (this.element) {
                InfrontUtil.removeEventListener(this.element, "keyup", this.keyUpListener);
                InfrontUtil.removeEventListener(this.element, "keydown", this.keyDownListener);
                InfrontUtil.removeEventListener(this.element, "focus", this.focusListener);
                InfrontUtil.removeEventListener(this.element, "blur", this.blurListener);
                if (this.scrolling) {
                    InfrontUtil.removeEventListener(this.element, "mousewheel", this.scrollCallback);
                }
                if (this.onClickCallback) {
                    InfrontUtil.removeEventListener(this.element, "click", this.onClickCallback);
                }
                this.element = null;
            }
        };
        TextInputBinding.prototype.setNumeric = function (value) {
            this.numeric = value;
        };
        TextInputBinding.prototype.setScrolling = function (value, callback) {
            this.scrolling = true;
            this.scrollCallback = callback;
        };
        TextInputBinding.prototype.setOnClick = function (callback) {
            this.onClickCallback = callback;
        };
        TextInputBinding.prototype.isNumeric = function (event) {
            if (event.altKey == false && event.shiftKey == false && event.ctrlKey == false) {
                switch (event.keyCode) {
                    case TextInputBinding.kKeycodeLeftArrow:
                    case TextInputBinding.kKeycodeRightArrow:
                    case TextInputBinding.kKeycodeUpArrow:
                    case TextInputBinding.kKeycodeDownArrow:
                    case TextInputBinding.kKeycodeEnter:
                    case TextInputBinding.kKeycodeEscape:
                    case TextInputBinding.kKeycodeTab:
                    case TextInputBinding.kKeycodeBackspace:
                    case TextInputBinding.kKeycodeDelete:
                    case TextInputBinding.kKeycodeNumpadComma:
                        return true;
                }
                if ((event.keyCode > 95 && event.keyCode < 106) || (event.keyCode > 47 && event.keyCode < 58) || event.keyCode == 188 || event.keyCode == 190) {
                    return true;
                }
                else {
                    return false;
                }
            }
        };
        TextInputBinding.prototype.setValidator = function (validator) {
            this.validator = validator;
            this.validate();
        };
        TextInputBinding.prototype.isValid = function () {
            if (this.validator) {
                return this.validator(this.lastvalue);
            }
            else {
                return true;
            }
        };
        TextInputBinding.prototype.validate = function () {
            if (this.element) {
                if (this.validator && !this.validator(this.lastvalue)) {
                    InfrontUtil.addClassName(this.element, TextInputBinding.kInvalidClass);
                }
                else {
                    InfrontUtil.removeClassName(this.element, TextInputBinding.kInvalidClass);
                }
            }
        };
        TextInputBinding.prototype.setPlaceholder = function (placeholder) {
            this.placeholder = placeholder;
        };
        TextInputBinding.prototype.setEnabled = function (enabled) {
            if (this.element) {
                if (enabled) {
                    this.element.removeAttribute("disabled");
                }
                else {
                    this.element.setAttribute("disabled", "disabled");
                }
            }
        };
        TextInputBinding.prototype.isEnabled = function () {
            var retVal = true;
            if (this.element) {
                if (this.element.getAttribute("disabled")) {
                    retVal = false;
                }
            }
            return retVal;
        };
        TextInputBinding.prototype.focus = function () {
            if (this.placeholder != null && this.element.value === this.placeholder) {
                this.element.value = "";
                this.element.setAttribute('placeholder', this.placeholder);
                InfrontUtil.removeClassName(this.element, TextInputBinding.kPlaceholderClass);
            }
            if (this.focusCallback) {
                this.focusCallback();
            }
        };
        TextInputBinding.prototype.blur = function () {
            if (this.blurCallback) {
                this.blurCallback();
            }
            /**
             * The reason we do this on blur is because of the very special case where you press tab while the last character-
             * key is still pressed. This will cause the value of the text-field to change, but we don't get a keyUp event, so we don't detect the last change.
             * By doing this on the blur-event, we can cover this eventuality as well.
             */
            this.valueUpdated();
            if (this.placeholder != null && this.element.value === "") {
                this.element.value = this.placeholder;
                this.element.setAttribute('placeholder', this.placeholder);
                InfrontUtil.addClassName(this.element, TextInputBinding.kPlaceholderClass);
            }
        };
        TextInputBinding.prototype.keyDown = function (event) {
            if (this.numeric) {
                if (!this.isNumeric(event)) {
                    event.preventDefault();
                }
            }
            if (event.defaultPrevented) {
                return;
            }
            var key = event.keyCode.toString();
            if (this.keyCodeObservers.hasOwnProperty(key)) {
                var obs = this.keyCodeObservers[key];
                for (var i = 0; i < obs.length; i++) {
                    obs[i](event.keyCode);
                }
                //We probably want to send the tab- and backspace-key on.
                if (event.keyCode != TextInputBinding.kKeycodeTab && event.keyCode != TextInputBinding.kKeycodeBackspace) {
                    event.preventDefault();
                }
            }
        };
        TextInputBinding.prototype.keyUp = function (event) {
            if (this.numeric) {
                if (!this.isNumeric(event)) {
                    event.preventDefault();
                }
            }
            if (event.defaultPrevented) {
                return;
            }
            switch (event.keyCode) {
                default:
                    this.valueUpdated();
                    event.preventDefault();
                    break;
            }
        };
        TextInputBinding.prototype.valueUpdated = function () {
            if (this.element && this.lastvalue != this.element.value) {
                this.lastvalue = this.element.value;
                this.updateObservers();
                this.validate();
            }
        };
        TextInputBinding.prototype.updateObservers = function () {
            for (var i = 0; i < this.observers.length; i++) {
                this.observers[i](this.lastvalue);
            }
        };
        TextInputBinding.prototype.onFocus = function (callback) {
            this.focusCallback = callback;
        };
        TextInputBinding.prototype.onBlur = function (callback) {
            this.blurCallback = callback;
        };
        TextInputBinding.prototype.observe = function (callback) {
            this.observers.push(callback);
        };
        TextInputBinding.prototype.observeSpecialKeys = function (keyCodes, callback) {
            var actualKeyCodes;
            if (typeof (keyCodes) == "number") {
                actualKeyCodes = [];
                actualKeyCodes.push(keyCodes);
            }
            else {
                actualKeyCodes = keyCodes;
            }
            for (var i = 0; i < actualKeyCodes.length; i++) {
                this.addKeyCodeObserver(actualKeyCodes[i], callback);
            }
        };
        TextInputBinding.prototype.addKeyCodeObserver = function (keyCode, callback) {
            var key = keyCode.toString();
            if (!this.keyCodeObservers.hasOwnProperty(key)) {
                this.keyCodeObservers[key] = [];
            }
            this.keyCodeObservers[key].push(callback);
        };
        TextInputBinding.prototype.get = function () {
            return this.lastvalue;
        };
        /**
         * Sets the value of this observer and the input-element it links to. If updateObservers is false this
         * will not trigger an event for the observers.
         * @param value
         * @param updateObservers
         */
        TextInputBinding.prototype.set = function (value, updateObservers) {
            if (updateObservers === void 0) { updateObservers = true; }
            if (InfrontUtil.isNumber(value)) {
                value = value.toString();
            }
            if (this.element) {
                this.element.value = value;
            }
            this.lastvalue = value;
            if (updateObservers) {
                this.updateObservers();
            }
            this.validate();
        };
        TextInputBinding.prototype.requestFocus = function () {
            if (this.element) {
                this.element.focus();
            }
        };
        TextInputBinding.prototype.requestBlur = function () {
            if (this.element) {
                this.element.blur();
                0;
            }
        };
        TextInputBinding.prototype.getElement = function () {
            return this.element;
        };
        TextInputBinding.kPlaceholderClass = "cell-input--placeholder";
        TextInputBinding.kInvalidClass = "cell-input--invalid";
        TextInputBinding.kKeycodeLeftArrow = 37;
        TextInputBinding.kKeycodeRightArrow = 39;
        TextInputBinding.kKeycodeUpArrow = 38;
        TextInputBinding.kKeycodeDownArrow = 40;
        TextInputBinding.kKeycodeEnter = 13;
        TextInputBinding.kKeycodeEscape = 27;
        TextInputBinding.kKeycodeTab = 9;
        TextInputBinding.kKeycodeBackspace = 8;
        TextInputBinding.kKeycodeDelete = 46;
        TextInputBinding.kKeycodeNumpadComma = 110;
        return TextInputBinding;
    }());
    Infront.TextInputBinding = TextInputBinding;
    var ArrayBinding = (function () {
        function ArrayBinding(boundEl, rowFactory, observer) {
            this.el = boundEl;
            this.factory = rowFactory;
            this.observer = observer;
        }
        ArrayBinding.prototype.reInit = function (items) {
            while (this.el.firstChild) {
                this.el.removeChild(this.el.firstChild);
            }
            for (var i = 0; i < items.length; i++) {
                this.itemAdded(items[i], i);
            }
        };
        ArrayBinding.prototype.itemAdded = function (item, index) {
            var inserted = this.factory.createRow(item, index);
            if (index < this.el.childNodes.length) {
                var collection = this.el.childNodes;
                var current = collection[index];
                this.el.insertBefore(inserted, current);
            }
            else {
                this.el.appendChild(inserted);
            }
            if (this.observer != null) {
                this.observer.htmlElementTreeAdded(inserted, item, index);
            }
        };
        ArrayBinding.prototype.itemRemoved = function (item, index) {
            var current = this.el.childNodes[index];
            this.el.removeChild(current);
            if (this.observer != null) {
                this.observer.htmlElementTreeRemoved(current, item, index);
            }
        };
        ArrayBinding.prototype.itemMoved = function (item, oldIndex, newIndex) {
            var current = this.el.childNodes[oldIndex];
            this.el.removeChild(current);
            var insertIndex = newIndex > oldIndex ? newIndex - 1 : newIndex;
            if (insertIndex >= this.el.childNodes.length) {
                this.el.appendChild(current);
            }
            else {
                var insertBefore = this.el.childNodes[insertIndex];
                this.el.insertBefore(current, insertBefore);
            }
            if (this.observer != null) {
                this.observer.htmlElementTreeMoved(current, item, oldIndex, newIndex);
            }
        };
        return ArrayBinding;
    }());
    Infront.ArrayBinding = ArrayBinding;
    var InlineRowFactory = (function () {
        function InlineRowFactory(createRowFunction) {
            this.createRowFunction = createRowFunction;
        }
        InlineRowFactory.prototype.createRow = function (item, index) {
            return this.createRowFunction(item, index);
        };
        return InlineRowFactory;
    }());
    Infront.InlineRowFactory = InlineRowFactory;
})(Infront || (Infront = {}));
///<reference path="LibraryConstants.ts" />
var Infront;
(function (Infront) {
    var TransportFactory = (function () {
        function TransportFactory(httpPoolSize, usingNodeJS) {
            this.httpPoolSize = httpPoolSize;
            this.usingNodeJS = usingNodeJS;
        }
        TransportFactory.prototype.transportForUrl = function (url, observer) {
            var lcUrl = url.toLowerCase();
            var retVal = null;
            if (lcUrl.slice(0, 5) === "https") {
                retVal = new HTTPTransportPool(url, observer, this.httpPoolSize, this.usingNodeJS);
            }
            else if (lcUrl.slice(0, 4) === "http") {
                retVal = new HTTPTransportPool(url, observer, this.httpPoolSize, this.usingNodeJS);
            }
            else if (lcUrl.slice(0, 3) === "wss") {
                retVal = new ReconnectWebsocketTransport(url, observer, 10, false, this.usingNodeJS);
            }
            else if (lcUrl.slice(0, 2) === "ws") {
                retVal = new ReconnectWebsocketTransport(url, observer, 10, false, this.usingNodeJS);
            }
            return retVal;
        };
        return TransportFactory;
    }());
    Infront.TransportFactory = TransportFactory;
    var HTTPTransportPool = (function () {
        function HTTPTransportPool(url, observer, poolSize, usingNodeJS) {
            if (usingNodeJS === void 0) { usingNodeJS = false; }
            var _this = this;
            this.observer = observer;
            this.nextTransport = 0;
            this.transports = [];
            for (var i = 0; i < poolSize; i++) {
                var transport = new HTTPTransport(url, this, usingNodeJS);
                this.transports.push(transport);
            }
            setTimeout(function () {
                _this.observer.transportConnected(_this);
            }, 1);
        }
        HTTPTransportPool.prototype.sendMessage = function (message) {
            this.transports[this.nextTransport].sendMessage(message);
            this.nextTransport = this.nextTransport < this.transports.length - 1 ? this.nextTransport + 1 : 0;
        };
        HTTPTransportPool.prototype.supportsStreaming = function () {
            return false;
        };
        HTTPTransportPool.prototype.close = function () {
            var _this = this;
            this.transports = [];
            setTimeout(function () {
                _this.observer.transportDisconnected(_this);
            }, 1);
        };
        HTTPTransportPool.prototype.transportConnected = function (source) { };
        HTTPTransportPool.prototype.transportDisconnected = function (source) { };
        HTTPTransportPool.prototype.transportReconnected = function (source) { };
        HTTPTransportPool.prototype.transportReconnecting = function (source) { };
        HTTPTransportPool.prototype.transportError = function (source, sourceMessage, code, message) {
            this.observer.transportError(this, sourceMessage, code, message);
        };
        HTTPTransportPool.prototype.messageReceived = function (source, message) {
            this.observer.messageReceived(this, message);
        };
        HTTPTransportPool.prototype.reconnectToUrl = function (url) {
            for (var i = 0; i < this.transports.length; i++) {
                this.transports[i].reconnectToUrl(url);
            }
        };
        HTTPTransportPool.prototype.prepareLogout = function () {
            for (var i = 0; i < this.transports.length; i++) {
                this.transports[i].prepareLogout();
            }
        };
        return HTTPTransportPool;
    }());
    Infront.HTTPTransportPool = HTTPTransportPool;
    var HTTPTransport = (function () {
        function HTTPTransport(url, observer, usingNodeJS) {
            var _this = this;
            this.pipeline = new Array();
            this.url = url;
            this.observer = observer;
            var _XMLHttpRequest;
            if (usingNodeJS) {
                _XMLHttpRequest = require('xhr2');
            }
            else {
                _XMLHttpRequest = XMLHttpRequest;
            }
            this.xhr = new _XMLHttpRequest();
            if ("withCredentials" in this.xhr) {
                this.corsSupported = true;
                this.usingXDomainRequest = false;
            }
            else if (typeof XDomainRequest != "undefined") {
                this.xhr = new XDomainRequest();
                this.corsSupported = true;
                this.usingXDomainRequest = true;
            }
            else {
                this.corsSupported = false;
                this.usingXDomainRequest = false;
            }
            this.xhr.onload = function (event) { return _this.onLoad(event); };
            this.xhr.onerror = function (event) { return _this.onError(event); };
            this.transportAvailable = true;
        }
        HTTPTransport.prototype.sendMessage = function (message) {
            this.pipeline.push(message);
            if (this.transportAvailable) {
                this.processNext();
            }
        };
        HTTPTransport.prototype.processNext = function () {
            var _this = this;
            if (this.pipeline.length > 0) {
                this.transportAvailable = false;
                setTimeout(function () {
                    _this.sendNextMessage();
                }, 1);
            }
        };
        HTTPTransport.prototype.sendNextMessage = function () {
            this.xhr.open("POST", this.url);
            if (this.corsSupported) {
                this.xhr.withCredentials = true;
            }
            this.xhr.send(this.pipeline[0]);
        };
        HTTPTransport.prototype.onError = function (event) {
            this.observer.transportError(this, this.pipeline[0], -1, "Error while processing HTTP POST");
            this.currentMessageFinished();
        };
        HTTPTransport.prototype.onLoad = function (event) {
            this.observer.messageReceived(this, this.xhr.responseText);
            this.currentMessageFinished();
        };
        HTTPTransport.prototype.currentMessageFinished = function () {
            this.pipeline.splice(0, 1);
            this.transportAvailable = true;
            this.processNext();
        };
        HTTPTransport.prototype.close = function () {
            var _this = this;
            this.xhr.onload = null;
            this.xhr.onerror = null;
            this.xhr = null;
            setTimeout(function () {
                _this.observer.transportDisconnected(_this);
            }, 1);
        };
        HTTPTransport.prototype.supportsStreaming = function () {
            return false;
        };
        HTTPTransport.prototype.prepareLogout = function () {
        };
        HTTPTransport.prototype.reconnectToUrl = function (url) {
            var _this = this;
            this.url = url;
            if (this.xhr.onload == null)
                this.xhr.onload = function (event) { return _this.onLoad(event); };
            if (this.xhr.onerror == null)
                this.xhr.onerror = function (event) { return _this.onError(event); };
        };
        return HTTPTransport;
    }());
    Infront.HTTPTransport = HTTPTransport;
    var WebsocketTransport = (function () {
        function WebsocketTransport(url, observer, useMock, usingNodeJS) {
            if (useMock === void 0) { useMock = false; }
            if (usingNodeJS === void 0) { usingNodeJS = false; }
            var _this = this;
            this.observer = observer;
            this.url = url;
            this.useMock = Infront.LibraryConstants.kBuildType == 'dev' && useMock;
            this.usingNodeJS = usingNodeJS;
            this.messageQueue = [];
            this.createSocket();
            this.beforeUnloadListener = function (event) { return _this.close(); };
            if (!this.usingNodeJS) {
                window.addEventListener("beforeunload", this.beforeUnloadListener, false);
            }
        }
        WebsocketTransport.prototype.createSocket = function () {
            var _this = this;
            try {
                if (this.usingNodeJS) {
                    var WebsocketLib = require("ws");
                    this.websocket = new WebsocketLib(this.url);
                }
                else if (this.useMock) {
                    this.websocket = new MockWebsocket(this.url);
                }
                else {
                    this.websocket = new WebSocket(this.url);
                }
                this.websocket.onopen = function () { return _this.onWebsocketOpen(); };
                this.websocket.onerror = function (error) { return _this.onWebsocketError(error); };
                this.websocket.onmessage = function (message) { return _this.onWebsocketMessage(message); };
                this.websocket.onclose = function () { return _this.onWebsocketClose(); };
            }
            catch (e) {
                0;
                if (this.observer) {
                    this.observer.transportError(this, null, -1, e.toString());
                }
            }
        };
        WebsocketTransport.prototype.onWebsocketOpen = function () {
            this.observer.transportConnected(this);
            for (var i = 0; i < this.messageQueue.length; i++) {
                this.sendMessage(this.messageQueue[i]);
            }
            this.messageQueue = [];
        };
        WebsocketTransport.prototype.onWebsocketError = function (error) {
            this.observer.transportError(this, null, -1, error.toString());
        };
        WebsocketTransport.prototype.onWebsocketMessage = function (message) {
            this.observer.messageReceived(this, message.data);
        };
        WebsocketTransport.prototype.onWebsocketClose = function () {
            this.observer.transportDisconnected(this);
        };
        WebsocketTransport.prototype.sendMessage = function (message) {
            if (this.websocket.readyState == WebsocketTransport.CONNECTING) {
                this.messageQueue.push(message);
            }
            else if (this.websocket.readyState == WebsocketTransport.OPEN) {
                this.websocket.send(message);
            }
            else {
                this.observer.transportError(this, null, -1, "Connection closed");
            }
        };
        WebsocketTransport.prototype.supportsStreaming = function () {
            return true;
        };
        WebsocketTransport.prototype.close = function () {
            if (!this.usingNodeJS) {
                window.removeEventListener("beforeunload", this.beforeUnloadListener, false);
            }
            //            this.websocket.onopen = ():void => {};
            //            this.websocket.onerror = (error:ErrorEvent):void => {};
            //            this.websocket.onmessage = (message:MessageEvent):void => {};
            //            this.websocket.onclose = ():void => {};
            this.websocket.close();
            // this.websocket = null; Don't null websocket, we already check if it's closed
        };
        WebsocketTransport.isWebsocketsSupported = function (usingNodeJS) {
            if (usingNodeJS === void 0) { usingNodeJS = false; }
            try {
                if (usingNodeJS) {
                    var assertNodeJs = require("ws");
                }
                return true;
            }
            catch (Error) {
                0;
            }
            if (InfrontUtil.browserInfo.safari && InfrontUtil.browserInfo.versionMajor < 7) {
                return false;
            }
            else {
                return typeof (window["WebSocket"]) != "undefined";
            }
        };
        WebsocketTransport.prototype.prepareLogout = function () {
        };
        WebsocketTransport.prototype.reconnectToUrl = function (url) {
            if (this.websocket && this.websocket.readyState != WebsocketTransport.CLOSED) {
                //TODO: Close if not closed?
            }
            else {
                this.url = url;
                this.createSocket();
            }
        };
        WebsocketTransport.CONNECTING = 0;
        WebsocketTransport.OPEN = 1;
        WebsocketTransport.CLOSING = 2;
        WebsocketTransport.CLOSED = 3;
        return WebsocketTransport;
    }());
    Infront.WebsocketTransport = WebsocketTransport;
    var ReconnectWebsocketTransport = (function (_super) {
        __extends(ReconnectWebsocketTransport, _super);
        function ReconnectWebsocketTransport(url, observer, retryCount, useMock, usingNodeJS) {
            if (useMock === void 0) { useMock = false; }
            if (usingNodeJS === void 0) { usingNodeJS = false; }
            var _this = _super.call(this, url, observer, useMock, usingNodeJS) || this;
            _this.directlyClosed = false;
            _this.reconnecting = false;
            _this.maxRetryCount = retryCount >= 0 ? retryCount : 0;
            _this.retryCount = _this.maxRetryCount;
            _this.reconnectTime = (new Date()).getTime();
            _this.successfullyConnected = false;
            return _this;
        }
        ReconnectWebsocketTransport.prototype.sendMessage = function (message) {
            if (this.websocket.readyState != WebsocketTransport.OPEN) {
                this.messageQueue.push(message);
            }
            else {
                _super.prototype.sendMessage.call(this, message);
            }
        };
        ReconnectWebsocketTransport.prototype.onWebsocketOpen = function () {
            this.directlyClosed = false;
            this.successfullyConnected = true;
            if (this.reconnecting) {
                this.observer.transportReconnected(this);
            }
            else {
                this.observer.transportConnected(this);
            }
            this.reconnecting = false;
            for (var i = 0; i < this.messageQueue.length; i++) {
                this.sendMessage(this.messageQueue[i]);
            }
            this.messageQueue = [];
        };
        ReconnectWebsocketTransport.prototype.onWebsocketError = function (error) {
            0;
            0;
        };
        ReconnectWebsocketTransport.prototype.onWebsocketClose = function () {
            var _this = this;
            //If a suitable amount of time has passed we reset the retry-count
            //This way this "artifical socket" can live "forever", while still dieing if the server really goes down.
            var now = (new Date()).getTime();
            if ((now - this.reconnectTime) > ReconnectWebsocketTransport.kRetryResetThreshold) {
                this.retryCount = this.maxRetryCount;
            }
            //If we retry the maximum ammount (or we are closed by way of the close() method) we disconnect with the
            //proper event.
            if (this.retryCount == 0 || this.directlyClosed) {
                this.reconnecting = false;
                this.observer.transportDisconnected(this);
            }
            else {
                this.observer.transportReconnecting(this);
                this.reconnectTime = now;
                //We don't consider this a reconnect for the purpose of our observer if we never successfully connected
                //in the first place.
                this.reconnecting = this.successfullyConnected;
                //Retry with exponential backoff.
                //Combines quick reconnect with avoiding Denial of Service-like situation.
                this.retryCount--;
                setTimeout(function () {
                    _this.createSocket();
                }, Math.pow(2, this.maxRetryCount - this.retryCount));
            }
        };
        ReconnectWebsocketTransport.prototype.close = function () {
            this.directlyClosed = true;
            _super.prototype.close.call(this);
        };
        ReconnectWebsocketTransport.prototype.prepareLogout = function () {
            this.directlyClosed = true;
        };
        ReconnectWebsocketTransport.prototype.reconnectToUrl = function (url) {
            this.reconnecting = true;
            _super.prototype.reconnectToUrl.call(this, url);
        };
        ReconnectWebsocketTransport.kRetryResetThreshold = 60 * 1000;
        return ReconnectWebsocketTransport;
    }(WebsocketTransport));
    Infront.ReconnectWebsocketTransport = ReconnectWebsocketTransport;
    var MockWebsocket = (function () {
        function MockWebsocket(url) {
            var _this = this;
            this.websocket = new WebSocket(url);
            this.websocket.onclose = function (evt) { return _this.pOnclose(evt); };
            this.websocket.onerror = function (evt) { return _this.pOnerror(evt); };
            this.websocket.onmessage = function (evt) { return _this.pOnmessage(evt); };
            this.websocket.onopen = function () { return _this.pOnopen(); };
            this.readyState = 0; //Connecting
            window["infrontMockWebsocket"] = this;
        }
        MockWebsocket.prototype.send = function (msg) {
            this.websocket.send(msg);
        };
        MockWebsocket.prototype.pOnclose = function (evt) {
            this.readyState = this.websocket.readyState;
            this["onclose"](evt);
        };
        MockWebsocket.prototype.pOnerror = function (evt) {
            this.readyState = this.websocket.readyState;
            this["onerror"](evt);
        };
        MockWebsocket.prototype.pOnmessage = function (evt) {
            this["onmessage"](evt);
        };
        MockWebsocket.prototype.pOnopen = function () {
            this.readyState = this.websocket.readyState;
            this["onopen"]();
        };
        MockWebsocket.prototype.close = function () {
            this.websocket.close();
        };
        MockWebsocket.prototype.forceError = function () {
            0;
            var evt = {};
            evt.message = "Simulated websocket-error";
            this.pOnerror(evt);
            this.close();
        };
        return MockWebsocket;
    }());
})(Infront || (Infront = {}));
/**
 * Created by hage on 14.01.14.
 */
var Infront;
/**
 * Created by hage on 14.01.14.
 */
(function (Infront) {
    var InfrontObject = (function () {
        function InfrontObject() {
        }
        InfrontObject.prototype.hasOwnProperty = function (property) {
            return Object.prototype.hasOwnProperty.call(this, property);
        };
        return InfrontObject;
    }());
    Infront.InfrontObject = InfrontObject;
    var Instrument = (function (_super) {
        __extends(Instrument, _super);
        function Instrument(feed, ticker) {
            var _this = _super.call(this) || this;
            _this.feed = feed;
            _this.ticker = ticker;
            return _this;
        }
        Instrument.prototype.isValid = function () {
            return InfrontUtil.isNumber(this.feed) && InfrontUtil.isString(this.ticker);
        };
        Instrument.prototype.equals = function (object) {
            return typeof (object) != "undefined" && object != null && object.hasOwnProperty("feed") && object.hasOwnProperty("ticker") && object["feed"] == this.feed && object["ticker"] == this.ticker;
        };
        Instrument.isSameInstrument = function (instrument1, instrument2) {
            return Instrument.isInstrument(instrument1) && Instrument.isInstrument(instrument2) &&
                instrument1.feed == instrument2.feed && instrument1.ticker == instrument2.ticker;
        };
        Instrument.prototype.inf_clone = function () {
            return Instrument.inf_clone(this);
        };
        Instrument.inf_clone = function (instr) {
            return new Instrument(instr.feed, instr.ticker);
        };
        Instrument.isInstrument = function (obj) {
            return InfrontUtil.isObject(obj) && obj.hasOwnProperty("feed") && obj.hasOwnProperty("ticker");
        };
        Instrument.prototype.toJson = function () {
            var instrumentFeedTicker = {};
            instrumentFeedTicker.feed = this.feed;
            instrumentFeedTicker.ticker = this.ticker;
            return JSON.stringify(instrumentFeedTicker);
        };
        Instrument.jsonToInstrument = function (json) {
            if (json && json != "") {
                var instrumentFeedTicker = JSON.parse(json);
                return new Instrument(instrumentFeedTicker.feed, instrumentFeedTicker.ticker);
            }
            else
                return null;
        };
        return Instrument;
    }(InfrontObject));
    Infront.Instrument = Instrument;
    var Chain = (function (_super) {
        __extends(Chain, _super);
        function Chain() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return Chain;
    }(InfrontObject));
    Infront.Chain = Chain;
    var ChainNode = (function (_super) {
        __extends(ChainNode, _super);
        function ChainNode() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ChainNode;
    }(InfrontObject));
    Infront.ChainNode = ChainNode;
    var PidSidsObject = (function (_super) {
        __extends(PidSidsObject, _super);
        function PidSidsObject() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return PidSidsObject;
    }(InfrontObject));
    Infront.PidSidsObject = PidSidsObject;
    var FeedMetaData = (function (_super) {
        __extends(FeedMetaData, _super);
        function FeedMetaData() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return FeedMetaData;
    }(InfrontObject));
    Infront.FeedMetaData = FeedMetaData;
    var Strategy = (function () {
        function Strategy() {
        }
        Strategy.prototype.findParam = function (paramId) {
            var retVal = null;
            for (var i = 0; i < this.params.length; i++) {
                if (this.params[i].id.toLowerCase() == paramId.toLowerCase()) {
                    retVal = this.params[i];
                    break;
                }
            }
            return retVal;
        };
        Strategy.parseFromResultObject = function (res) {
            var s = new Strategy();
            s.id = res[Infront.MWSConstants.id];
            s.label = res[Infront.MWSConstants.label];
            s.description = res[Infront.MWSConstants.description];
            s.params = res[Infront.MWSConstants.params];
            return s;
        };
        return Strategy;
    }());
    Infront.Strategy = Strategy;
    var AlgoParam = (function () {
        function AlgoParam() {
        }
        AlgoParam.FREE_TEXT = "FREE_TEXT";
        AlgoParam.DOUBLE = "DOUBLE";
        AlgoParam.STOP_PRICE = "STOP_PRICE";
        AlgoParam.INIT_CODE = "INIT_CODE";
        AlgoParam.PARENT = "PARENT";
        AlgoParam.DONE_CODE = "DONE_CODE";
        AlgoParam.VALID_SESSION = "VALID_SESSION";
        AlgoParam.TIME = "TIME";
        AlgoParam.DATE = "DATE";
        AlgoParam.DATETIME = "DATETIME";
        AlgoParam.DROPDOWN = "DROPDOWN";
        AlgoParam.BOOL = "BOOL";
        AlgoParam.INT = "INT";
        AlgoParam.INSTRUMENT = "INSTRUMENT";
        AlgoParam.PRICE = "PRICE";
        AlgoParam.VOLUME_LOTS = "VOLUME_LOTS";
        AlgoParam.MULTI_LINE = "MULTI_LINE";
        return AlgoParam;
    }());
    Infront.AlgoParam = AlgoParam;
    var DropdownParam = (function () {
        function DropdownParam() {
        }
        DropdownParam.prototype.setLabel = function (label) {
            this.label = label;
            return this;
        };
        DropdownParam.prototype.setValue = function (value) {
            this.value = value;
            return this;
        };
        return DropdownParam;
    }());
    Infront.DropdownParam = DropdownParam;
    var NewsType;
    (function (NewsType) {
        NewsType[NewsType["FlashLevel1"] = 0] = "FlashLevel1";
        NewsType[NewsType["FlashLevel2"] = 1] = "FlashLevel2";
        NewsType[NewsType["FlashLevel3"] = 2] = "FlashLevel3";
        NewsType[NewsType["Url"] = 3] = "Url";
        NewsType[NewsType["Regular"] = 4] = "Regular";
    })(NewsType = Infront.NewsType || (Infront.NewsType = {}));
    var BuyOrSell;
    (function (BuyOrSell) {
        BuyOrSell[BuyOrSell["Buy"] = 0] = "Buy";
        BuyOrSell[BuyOrSell["Sell"] = 1] = "Sell";
    })(BuyOrSell = Infront.BuyOrSell || (Infront.BuyOrSell = {}));
    var InfinInstrument = (function (_super) {
        __extends(InfinInstrument, _super);
        function InfinInstrument(efcode, name, isin, ticker) {
            var _this = _super.call(this) || this;
            _this.efcode = efcode;
            _this.name = name;
            _this.isin = isin;
            _this.ticker = ticker;
            return _this;
        }
        InfinInstrument.prototype.equals = function (object) {
            return typeof (object) != "undefined" && object != null && object.hasOwnProperty("efcode") && object.hasOwnProperty("isin") && object["efcode"] == this.efcode && object["isin"] == this.isin;
        };
        InfinInstrument.prototype.inf_clone = function () {
            return new InfinInstrument(this.efcode, this.name, this.isin, this.ticker);
        };
        InfinInstrument.prototype.getCompany = function () {
            return (typeof this.isin != "undefined" && this.isin != null) ? this.isin : this.efcode;
        };
        return InfinInstrument;
    }(InfrontObject));
    Infront.InfinInstrument = InfinInstrument;
    var UniverseObject = (function (_super) {
        __extends(UniverseObject, _super);
        function UniverseObject(code, type, mkp, sector, country) {
            if (code === void 0) { code = "WORLD"; }
            if (type === void 0) { type = Infront.UniverseType.REGION; }
            var _this = _super.call(this) || this;
            _this.code = code;
            _this.type = type;
            _this.marketPlaces = mkp;
            _this.sector = sector;
            _this.country = country;
            return _this;
        }
        UniverseObject.prototype.equals = function (object) {
            return typeof (object) != "undefined" && object != null
                && object.hasOwnProperty("code") && object["code"] == this.code
                && object.hasOwnProperty("type") && object["type"] == this.type;
        };
        UniverseObject.prototype.inf_clone = function () {
            return new UniverseObject(this.code, this.type, this.marketPlaces, this.sector, this.country);
        };
        UniverseObject.prototype.setCode = function (code) { this.code = code; };
        UniverseObject.prototype.setType = function (type) { this.type = type; };
        UniverseObject.prototype.setSector = function (sector) { this.sector = sector; };
        UniverseObject.prototype.setCountry = function (country) { this.country = country; };
        UniverseObject.prototype.setMarketPlaces = function (mkp) { this.marketPlaces = mkp; };
        UniverseObject.prototype.getCode = function () { return this.code; };
        UniverseObject.prototype.getType = function () { return this.type; };
        UniverseObject.prototype.getAdditionalSector = function () { return this.sector; };
        UniverseObject.prototype.getAdditionalCountry = function () { return this.country; };
        UniverseObject.prototype.getAdditionalMkpList = function () { return this.marketPlaces; };
        return UniverseObject;
    }(InfrontObject));
    Infront.UniverseObject = UniverseObject;
    var EtaObject = (function (_super) {
        __extends(EtaObject, _super);
        function EtaObject(isin) {
            var _this = _super.call(this) || this;
            _this.isin = isin;
            return _this;
        }
        EtaObject.prototype.equals = function (object) {
            return typeof (object) != "undefined" && object != null && object.hasOwnProperty("isin") && object["isin"] == this.isin;
        };
        EtaObject.prototype.inf_clone = function () {
            return new EtaObject(this.isin);
        };
        return EtaObject;
    }(InfrontObject));
    Infront.EtaObject = EtaObject;
    var FieldObject = (function (_super) {
        __extends(FieldObject, _super);
        function FieldObject() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return FieldObject;
    }(InfrontObject));
    Infront.FieldObject = FieldObject;
    var DropdownObj = (function (_super) {
        __extends(DropdownObj, _super);
        function DropdownObj(value, description) {
            var _this = _super.call(this) || this;
            _this.value = value;
            _this.description = description;
            return _this;
        }
        return DropdownObj;
    }(InfrontObject));
    Infront.DropdownObj = DropdownObj;
})(Infront || (Infront = {}));
///<reference path='Datatypes.ts' />
/**
 * Created by hage on 07.01.14.
 */
/**
 * Contains string-constants for use in messaging, mainly field-names in request- and response-messages.
 */
var Infront;
///<reference path='Datatypes.ts' />
/**
 * Created by hage on 07.01.14.
 */
/**
 * Contains string-constants for use in messaging, mainly field-names in request- and response-messages.
 */
(function (Infront) {
    var MWSConstants = (function () {
        function MWSConstants() {
        }
        //Common fields
        MWSConstants.feed = "feed";
        MWSConstants.ticker = "ticker";
        MWSConstants.type = "type";
        MWSConstants.date = "date";
        MWSConstants.description = "description";
        //public static name = "name";
        MWSConstants.orders = "orders";
        MWSConstants.trades = "trades";
        MWSConstants.price = "price";
        MWSConstants.country = "country";
        MWSConstants.iso_country = "iso_country";
        MWSConstants.iso_8601_format = true;
        MWSConstants.error_code = "error_code";
        MWSConstants.error_message = "error_message";
        MWSConstants.instrument = "instrument";
        MWSConstants.volume = "volume";
        MWSConstants.amount = "amount";
        MWSConstants.id = "id";
        MWSConstants.label = "label";
        MWSConstants.currency = "currency";
        //Instrument
        MWSConstants.instrument_type = "instrument_type";
        MWSConstants.full_name = "full_name";
        MWSConstants.isin = "isin";
        MWSConstants.contract_size = "contract_size";
        MWSConstants.strike_price = "strike_price";
        //Snapshot
        MWSConstants.last = "last";
        MWSConstants.last_valid = "last_valid";
        MWSConstants.bid = "bid";
        MWSConstants.ask = "ask";
        MWSConstants.asks = "asks";
        MWSConstants.bids = "bids";
        MWSConstants.hist_performance = "hist_performance";
        MWSConstants.pct_change = "pct_change";
        MWSConstants.change = "change";
        MWSConstants.previous_close = "previous_close";
        MWSConstants.tick_size_id = "tick_size_id";
        //OrderLevel
        MWSConstants.level = "level";
        //News Sources
        MWSConstants.source_name = "name";
        MWSConstants.source_short = "short_name";
        MWSConstants.region = "region";
        //News regions
        MWSConstants.code = "code";
        //News Items
        MWSConstants.news_id = "news_id";
        MWSConstants.news_feed = "news_feed";
        MWSConstants.headline = "headline";
        MWSConstants.category = "category";
        MWSConstants.time = "time";
        MWSConstants.language = "language";
        MWSConstants.url = "url";
        //News body
        MWSConstants.mime_type = "mime_type";
        MWSConstants.body = "body";
        //md_get_market_activity_response
        MWSConstants.up = "up";
        MWSConstants.down = "down";
        MWSConstants.unchanged = "unchanged";
        MWSConstants.up_pct = "up_pct";
        MWSConstants.down_pct = "down_pct";
        MWSConstants.unchanged_pct = "unchanged_pct";
        MWSConstants.turnover = "turnover";
        //Search results
        MWSConstants.type_instrument = "INSTRUMENT";
        MWSConstants.chain = "chain";
        //Company information
        MWSConstants.urls = "urls";
        //Company history
        MWSConstants.splits = "splits";
        MWSConstants.dividends = "dividends";
        MWSConstants.factor = "factor";
        //Field names
        MWSConstants.error_displayable = "error_displayable";
        MWSConstants.request_data = "request_data";
        MWSConstants.update_data = "update_data";
        MWSConstants.session_token = "session_token";
        MWSConstants.session_timeout = "session_timeout";
        MWSConstants.features = "features";
        // Trading features
        MWSConstants.show_deleted_orders = "SHOW_DELETED_ORDERS";
        MWSConstants.allow_comments = "ALLOW_COMMENTS";
        MWSConstants.static_portfolios = "STATIC_PORTFOLIOS";
        MWSConstants.order_activation = "ORDER_ACTIVATION";
        MWSConstants.internal_access = "INTERNAL_ACCESS";
        MWSConstants.modify_remaining_volume = "MODIFY_REMAINING_VOLUME";
        MWSConstants.modify_validity_not_supported = "MODIFY_VALIDITY_NOT_SUPPORTED";
        MWSConstants.modify_open_volume_not_supported = "MODIFY_OPEN_VOLUME_NOT_SUPPORTED";
        MWSConstants.modify_order_activation = "MODIFY_ORDER_ACTIVATION";
        MWSConstants.decimals = "decimals";
        //Routings response
        MWSConstants.tr_routings = "tr_routings";
        MWSConstants.provider = "provider";
        MWSConstants.service = "service";
        MWSConstants.tradable_feeds = "tradable_feeds";
        MWSConstants.input_fields = "input_fields";
        MWSConstants.nodes = "nodes";
        MWSConstants.key = "key";
        MWSConstants.may_persist = "may_persist";
        MWSConstants.mask = "mask";
        MWSConstants.keyboard = "keyboard";
        MWSConstants.connections = "connections";
        //Historical Fields
        MWSConstants.HF_OPEN = "OPEN";
        MWSConstants.HF_HIGH = "HIGH";
        MWSConstants.HF_LOW = "LOW";
        MWSConstants.HF_LAST = "LAST";
        MWSConstants.HF_VOLUME = "VOLUME";
        //Intraday Trades
        MWSConstants.buyer = "buyer";
        MWSConstants.seller = "seller";
        MWSConstants.buyer_full = "buyer_full";
        MWSConstants.seller_full = "seller_full";
        MWSConstants.seq_id = "seq_id";
        //Trading
        //Common
        MWSConstants.value = "value";
        MWSConstants.base_currency = "base_currency";
        MWSConstants.order_id = "order_id";
        MWSConstants.avg_price = "avg_price";
        //TradingPower
        MWSConstants.trading_power = "trading_power";
        MWSConstants.base_trading_power = "base_trading_power";
        MWSConstants.is_shortable = "is_shortable";
        MWSConstants.margin_rate = "margin_rate";
        //TradingLogin
        MWSConstants.pid = "pid";
        MWSConstants.sid = "sid";
        //Portfolio info
        MWSConstants.portfolios = "portfolios";
        //Portfolio subscriptions
        MWSConstants.positions = "positions";
        MWSConstants.total_invested = "total_invested";
        MWSConstants.values = "values";
        MWSConstants.alerts = "alerts";
        MWSConstants.position = "position";
        MWSConstants.alert = "alert";
        //Position
        MWSConstants.portfolio = "portfolio";
        MWSConstants.cash = "cash";
        MWSConstants.invested = "invested";
        MWSConstants.multiplier = "multiplier";
        MWSConstants.collateral = "collateral";
        MWSConstants.profit = "profit";
        MWSConstants.base_invested = "base_invested";
        MWSConstants.ytd_base_invested = "ytd_base_invested";
        MWSConstants.base_value = "base_value";
        MWSConstants.base_result = "base_result";
        MWSConstants.market_price = "market_price";
        //Order subscriptions
        MWSConstants.order = "order";
        //Order
        MWSConstants.customer_id = "customer_id";
        MWSConstants.buy_or_sell = "buy_or_sell";
        MWSConstants.open_volume = "open_volume";
        MWSConstants.order_type = "order_type";
        MWSConstants.changed = "changed";
        MWSConstants.valid_until = "valid_until";
        MWSConstants.order_status = "order_status";
        MWSConstants.volume_filled = "volume_filled";
        MWSConstants.accumulated_traded = "accumulated_traded";
        MWSConstants.transaction_id = "transaction_id";
        MWSConstants.exchange_order_id = "exchange_order_id";
        MWSConstants.read_only = "read_only";
        MWSConstants.custom_tags = "custom_tags";
        MWSConstants.algo_id = "algo_id";
        MWSConstants.algo_params = "algo_params";
        MWSConstants.created = "created";
        MWSConstants.comment = "comment";
        MWSConstants.fail_code = "fail_code";
        //Algo
        MWSConstants.params = "params";
        MWSConstants.default = "default";
        MWSConstants.mandatory = "mandatory";
        MWSConstants.min = "min";
        MWSConstants.max = "max";
        MWSConstants.dropdown_items = "dropdown_items";
        MWSConstants.modify = "modify";
        //Trade
        MWSConstants.trade_id = "trade_id";
        MWSConstants.trade_time = "trade_time";
        //Trade-subscriptions
        MWSConstants.trade = "trade";
        //Net trades
        MWSConstants.net_trade = "net_trade";
        MWSConstants.net_trades = "net_trades";
        MWSConstants.buy_vwap = "buy_vwap";
        MWSConstants.sell_vwap = "sell_vwap";
        MWSConstants.buy_volume = "buy_volume";
        MWSConstants.sell_volume = "sell_volume";
        MWSConstants.net_volume = "net_volume";
        MWSConstants.buy_value = "buy_value";
        MWSConstants.sell_value = "sell_value";
        MWSConstants.net_value = "net_value";
        MWSConstants.net_price = "net_price";
        //Get Algos
        MWSConstants.algos = "algos";
        //Value
        MWSConstants.string_value = "string_value";
        //Order Entry
        MWSConstants.upper_bound = "upper_bound";
        MWSConstants.lower_bound = "lower_bound";
        MWSConstants.size = "size";
        //Alert
        MWSConstants.state = "state";
        MWSConstants.data = "data";
        MWSConstants.message = "message";
        MWSConstants.payload = "payload";
        //Reference-data
        MWSConstants.instruments = "instruments";
        MWSConstants.under_feed = "under_feed";
        MWSConstants.under_ticker = "under_ticker";
        MWSConstants.expiry_date = "expiry_date";
        MWSConstants.issuer = "issuer";
        MWSConstants.issuer_full_name = "issuer_full_name";
        //Message-keys
        MWSConstants.md_instrument_update = "md_instrument_update";
        //System Events
        MWSConstants.termination_code = "termination_code";
        MWSConstants.termination_reason = "termination_reason";
        //API-version we currently use
        MWSConstants.api_version = "1.5.1";
        return MWSConstants;
    }());
    Infront.MWSConstants = MWSConstants;
    var MWSBuyOrSell = (function () {
        function MWSBuyOrSell() {
        }
        MWSBuyOrSell.isBuy = function (val) {
            return val.toUpperCase() == MWSBuyOrSell.BUY;
        };
        MWSBuyOrSell.isSell = function (val) {
            return val.toUpperCase() == MWSBuyOrSell.SELL;
        };
        MWSBuyOrSell.BUY = "BUY";
        MWSBuyOrSell.SELL = "SELL";
        return MWSBuyOrSell;
    }());
    Infront.MWSBuyOrSell = MWSBuyOrSell;
    var OrderType = (function () {
        function OrderType() {
        }
        OrderType.NORMAL = "NORMAL";
        OrderType.FILL_OR_KILL = "FILL_OR_KILL";
        OrderType.FILL_AND_KILL = "FILL_AND_KILL";
        OrderType.FILL_OR_NOTHING = "FILL_OR_NOTHING";
        OrderType.CROSS = "CROSS";
        OrderType.BEST_POSSIBLE = "BEST_POSSIBLE";
        OrderType.AT_MARKET = "AT_MARKET";
        OrderType.MARKET = "MARKET";
        OrderType.MARKET_TO_LIMIT = "MARKET_TO_LIMIT";
        OrderType.AT_MARKET_AND_KILL = "AT_MARKET_AND_KILL";
        OrderType.QUOTE_ORDER = "QUOTE_ORDER";
        OrderType.LIMIT_OR_MARKET_ON_CLOSE = "LIMIT_OR_MARKET_ON_CLOSE";
        OrderType.STOP_LIMIT = "STOP_LIMIT";
        OrderType.STOP_LOSS = "STOP_LOSS";
        OrderType.CONTINGENT = "CONTINGENT";
        OrderType.FLEX = "FLEX";
        OrderType.INTEREST = "INTEREST";
        OrderType.ACCEPT = "ACCEPT";
        OrderType.PARENT = "PARENT";
        OrderType.STRATEGY = "STRATEGY";
        OrderType.FIX = "FIX";
        OrderType.LIMIT_TO_MARKET = "LIMIT_TO_MARKET";
        OrderType.BEST_TO_LIMIT = "BEST_TO_LIMIT";
        OrderType.MULTI_LEG = "MULTI_LEG";
        OrderType.MARKET_FOK = "MARKET_FOK";
        OrderType.MARKET_FAK = "MARKET_FAK";
        OrderType.PEGGED = "PEGGED";
        OrderType.AT_OPEN = "AT_OPEN";
        OrderType.AT_CLOSE = "AT_CLOSE";
        OrderType.OCO = "OCO";
        return OrderType;
    }());
    Infront.OrderType = OrderType;
    var MWSNewsType = (function () {
        function MWSNewsType() {
        }
        MWSNewsType.FLASH_LEVEL_1 = "FLASH_LEVEL_1";
        MWSNewsType.FLASH_LEVEL_2 = "FLASH_LEVEL_2";
        MWSNewsType.FLASH_LEVEL_3 = "FLASH_LEVEL_3";
        MWSNewsType.URL = "URL";
        MWSNewsType.REGULAR = "REGULAR";
        MWSNewsType.typeMap = function () {
            var map = {};
            map[MWSNewsType.FLASH_LEVEL_1] = Infront.NewsType.FlashLevel1;
            map[MWSNewsType.FLASH_LEVEL_2] = Infront.NewsType.FlashLevel2;
            map[MWSNewsType.FLASH_LEVEL_3] = Infront.NewsType.FlashLevel3;
            map[MWSNewsType.URL] = Infront.NewsType.Url;
            map[MWSNewsType.REGULAR] = Infront.NewsType.Regular;
            return map;
        }();
        return MWSNewsType;
    }());
    Infront.MWSNewsType = MWSNewsType;
    var Resolution = (function () {
        function Resolution() {
        }
        Resolution.TICKS = "TICKS";
        Resolution.MINUTES = "MINUTES";
        Resolution.HOURS = "HOURS";
        Resolution.SECONDS = "SECONDS";
        return Resolution;
    }());
    Infront.Resolution = Resolution;
    var QuoteFields = (function () {
        function QuoteFields() {
        }
        QuoteFields.ACC_VOLUME = "ACC_VOLUME";
        QuoteFields.TURNOVER = "TURNOVER";
        QuoteFields.ONEXCH_VOLUME = "ONEXCH_VOLUME";
        QuoteFields.ONEXCH_TURNOVER = "ONEXCH_TURNOVER";
        QuoteFields.BID = "BID";
        QuoteFields.ASK = "ASK";
        QuoteFields.BID_SIZE = "BID_SIZE";
        QuoteFields.ASK_SIZE = "ASK_SIZE";
        QuoteFields.NUM_BIDS = "NUM_BIDS";
        QuoteFields.NUM_ASKS = "NUM_ASKS";
        QuoteFields.ORDERBOOK = "ORDERBOOK";
        QuoteFields.VWAP = "VWAP";
        QuoteFields.AVG_VOLUME = "AVG_VOLUME";
        QuoteFields.AVG_VALUE = "AVG_VALUE";
        QuoteFields.OPEN = "OPEN";
        QuoteFields.HIGH = "HIGH";
        QuoteFields.LOW = "LOW";
        QuoteFields.LAST = "LAST";
        QuoteFields.PREVIOUS_CLOSE = "PREVIOUS_CLOSE";
        QuoteFields.TIME = "TIME";
        QuoteFields.CHANGE = "CHANGE";
        QuoteFields.PCT_CHANGE = "PCT_CHANGE";
        QuoteFields.NUM_TRADES = "NUM_TRADES";
        QuoteFields.GICS = "GICS";
        QuoteFields.NUM_SHARES = "NUM_SHARES";
        QuoteFields.RSI14 = "RSI14";
        QuoteFields.MACD = "MACD";
        QuoteFields.MA50 = "MA50";
        QuoteFields.MA100 = "MA100";
        QuoteFields.MA200 = "MA200";
        QuoteFields.CONTRACT_SIZE = "CONTRACT_SIZE";
        QuoteFields.TRADING_STATUS = "TRADING_STATUS";
        QuoteFields.TICK_SIZE_ID = "TICK_SIZE_ID";
        QuoteFields.SEGMENT = "SEGMENT";
        QuoteFields.INDIC_PRICE = "INDIC_PRICE";
        QuoteFields.INDIC_VOLUME = "INDIC_VOLUME";
        QuoteFields.TRADES = "TRADES";
        return QuoteFields;
    }());
    Infront.QuoteFields = QuoteFields;
    var IntradayFields = (function () {
        function IntradayFields() {
        }
        IntradayFields.BID = "BID";
        IntradayFields.ASK = "ASK";
        IntradayFields.OPEN = "OPEN";
        IntradayFields.HIGH = "HIGH";
        IntradayFields.LOW = "LOW";
        IntradayFields.LAST = "LAST";
        IntradayFields.VOLUME = "VOLUME";
        IntradayFields.TURNOVER = "TURNOVER";
        return IntradayFields;
    }());
    Infront.IntradayFields = IntradayFields;
    var KeyrboardType = (function () {
        function KeyrboardType() {
        }
        KeyrboardType.NUMERIC = "NUMERIC";
        KeyrboardType.ALPHANUMERIC = "ALPHANUMERIC";
        return KeyrboardType;
    }());
    Infront.KeyrboardType = KeyrboardType;
    var ChainTypes = (function () {
        function ChainTypes() {
        }
        ChainTypes.FEED = "FEED";
        ChainTypes.INDEX = "INDEX";
        ChainTypes.GLOBAL = "GLOBAL";
        return ChainTypes;
    }());
    Infront.ChainTypes = ChainTypes;
    var IntradaySorting = (function () {
        function IntradaySorting() {
        }
        IntradaySorting.TIME_DESC = "TIME_DESC";
        IntradaySorting.TIME_ASC = "TIME_ASC";
        IntradaySorting.VOLUME_DESC = "VOLUME_DESC";
        IntradaySorting.VOLUME_ASC = "VOLUME_ASC";
        IntradaySorting.PRICE_DESC = "PRICE_DESC";
        IntradaySorting.PRICE_ASC = "PRICE_ASC";
        return IntradaySorting;
    }());
    Infront.IntradaySorting = IntradaySorting;
    var VisualConstants = (function () {
        function VisualConstants() {
        }
        VisualConstants.instrument = "instrument";
        VisualConstants.read_only = "read_only";
        VisualConstants.custom_tags = "custom_tags";
        VisualConstants.algo_id = "algo_id";
        VisualConstants.algo_params = "algo_params";
        //Equity Trade Advisor
        VisualConstants.eta = "eta";
        VisualConstants.field = "field";
        //Common fields
        VisualConstants.company = "company";
        VisualConstants.efcode = "efcode";
        VisualConstants.isin = "isin";
        VisualConstants.ticker = "ticker";
        VisualConstants.market_cap = "market_cap";
        //Company title
        VisualConstants.companyInfos = "companyInfos";
        return VisualConstants;
    }());
    Infront.VisualConstants = VisualConstants;
    var TerminationCodes = (function () {
        function TerminationCodes() {
        }
        TerminationCodes.kickout = 2;
        return TerminationCodes;
    }());
    Infront.TerminationCodes = TerminationCodes;
    var InfrontAlertOperators = (function () {
        function InfrontAlertOperators() {
        }
        InfrontAlertOperators.GT = "GT";
        InfrontAlertOperators.LT = "LT";
        return InfrontAlertOperators;
    }());
    Infront.InfrontAlertOperators = InfrontAlertOperators;
    var InfrontAlertTypes = (function () {
        function InfrontAlertTypes() {
        }
        InfrontAlertTypes.INSTRUMENT_ALERT = "INSTRUMENT_ALERT";
        InfrontAlertTypes.NEWS_ALERT = "NEWS_ALERT";
        return InfrontAlertTypes;
    }());
    Infront.InfrontAlertTypes = InfrontAlertTypes;
    var InfrontAlertValue = (function () {
        function InfrontAlertValue() {
            this.md_field = "";
        }
        return InfrontAlertValue;
    }());
    Infront.InfrontAlertValue = InfrontAlertValue;
    var InfrontAlertNode = (function (_super) {
        __extends(InfrontAlertNode, _super);
        function InfrontAlertNode() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return InfrontAlertNode;
    }(Infront.InfrontObject));
    Infront.InfrontAlertNode = InfrontAlertNode;
    var InfrontAlert = (function (_super) {
        __extends(InfrontAlert, _super);
        function InfrontAlert() {
            var _this = _super.call(this) || this;
            _this.comment = "";
            _this.enabled = true;
            _this.trigger_type = "";
            _this.type = "";
            _this.rule = new InfrontAlertNode();
            _this.rule.left = new InfrontAlertNode();
            _this.rule.right = new InfrontAlertNode();
            return _this;
        }
        return InfrontAlert;
    }(Infront.InfrontObject));
    Infront.InfrontAlert = InfrontAlert;
    var FeedDataTypes = (function () {
        function FeedDataTypes() {
        }
        FeedDataTypes.STOCKS = "STOCKS";
        FeedDataTypes.NEWS = "NEWS";
        FeedDataTypes.BONDS = "BONDS";
        FeedDataTypes.EUROOPTIONS = "EUROOPTIONS";
        FeedDataTypes.FUTURES = "FUTURES";
        FeedDataTypes.COMMODITIES = "COMMODITIES";
        FeedDataTypes.INDICIES = "INDICIES";
        FeedDataTypes.FOREX = "FOREX";
        FeedDataTypes.USOPTIONS = "USOPTIONS";
        FeedDataTypes.FUNDS = "FUNDS";
        FeedDataTypes.CHAT = "CHAT";
        FeedDataTypes.URLNEWS = "URLNEWS";
        FeedDataTypes.ORDERS = "ORDERS";
        FeedDataTypes.WARRANTS = "WARRANTS";
        FeedDataTypes.FEATURES = "FEATURES";
        FeedDataTypes.SYMBOL_EXT = "SYMBOL_EXT";
        FeedDataTypes.CALENDAR_UPDATES = "CALENDAR_UPDATES";
        FeedDataTypes.FX_DEPOSIT_RATES = "FX_DEPOSIT_RATES";
        FeedDataTypes.MTF = "MTF";
        FeedDataTypes.WEB_FEATURE = "WEB_FEATURE";
        return FeedDataTypes;
    }());
    Infront.FeedDataTypes = FeedDataTypes;
})(Infront || (Infront = {}));
///<reference path='Transport.ts' />
///<reference path='Controller.ts' />
///<reference path='Util.ts' />
///<reference path='MWSConstants.ts' />
///<reference path='Datatypes.ts' />
var Infront;
///<reference path='Transport.ts' />
///<reference path='Controller.ts' />
///<reference path='Util.ts' />
///<reference path='MWSConstants.ts' />
///<reference path='Datatypes.ts' />
(function (Infront) {
    var RequestOptions = (function () {
        function RequestOptions() {
        }
        return RequestOptions;
    }());
    Infront.RequestOptions = RequestOptions;
    var SubscriptionOptions = (function (_super) {
        __extends(SubscriptionOptions, _super);
        function SubscriptionOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return SubscriptionOptions;
    }(RequestOptions));
    Infront.SubscriptionOptions = SubscriptionOptions;
    var MessageType;
    (function (MessageType) {
        MessageType[MessageType["InfinData"] = 0] = "InfinData";
        MessageType[MessageType["Connect"] = 1] = "Connect";
        MessageType[MessageType["MarketData"] = 2] = "MarketData";
        MessageType[MessageType["Trading"] = 3] = "Trading";
        MessageType[MessageType["Routing"] = 4] = "Routing";
    })(MessageType = Infront.MessageType || (Infront.MessageType = {}));
    var MessageContainer = (function () {
        function MessageContainer(message, messageType) {
            this.message = message;
            this.messageType = messageType;
        }
        return MessageContainer;
    }());
    Infront.MessageContainer = MessageContainer;
    var ResponseObserver = (function () {
        function ResponseObserver() {
        }
        return ResponseObserver;
    }());
    var ResponseObserverMap = (function () {
        function ResponseObserverMap() {
            this.map = {};
        }
        ResponseObserverMap.prototype.add = function (requestData, observer) {
            this.map[requestData] = observer;
        };
        ResponseObserverMap.prototype.get = function (requestData) {
            var retVal = null;
            if (this.map.hasOwnProperty(requestData)) {
                retVal = this.map[requestData];
            }
            return retVal;
        };
        ResponseObserverMap.prototype.remove = function (requestData) {
            var retVal = this.get(requestData);
            if (retVal != null) {
                delete this.map[requestData];
            }
            return retVal;
        };
        ResponseObserverMap.prototype.getAll = function () {
            var retVal = [];
            for (var p in this.map) {
                if (this.map.hasOwnProperty(p)) {
                    retVal.push(this.map[p]);
                }
            }
            return retVal;
        };
        return ResponseObserverMap;
    }());
    var MessageService = (function () {
        function MessageService(observer, httpPoolSize, usingNodeJS) {
            this.observer = observer;
            this.responseObservers = new ResponseObserverMap();
            this.subscriptionObservers = new Object();
            this.specialEventObservers = {};
            this.transportFactory = new Infront.TransportFactory(httpPoolSize, usingNodeJS);
            this.marketDataTransport = null;
            this.tradingTransport = null;
            this.visualTransport = null;
            this.routingTransport = null;
            this.visualConnectTransport = null;
            this.routingUrl = null;
            this.visualConnectUrl = null;
            this.marketDataUrl = null;
            this.visualUrl = null;
            this.tradingUrl = null;
        }
        MessageService.prototype.registerSpecialEventObserver = function (updateKey, callback) {
            if (!this.specialEventObservers.hasOwnProperty(updateKey)) {
                this.specialEventObservers[updateKey] = [];
            }
            this.specialEventObservers[updateKey].push(callback);
        };
        MessageService.prototype.reconnectMarketData = function (url) {
            this.marketDataUrl = url;
            this.getMarketDataTransport().reconnectToUrl(url);
        };
        MessageService.prototype.prepareTradingLogout = function () {
            if (this.tradingTransport)
                this.tradingTransport.prepareLogout();
        };
        MessageService.prototype.prepareMDLogout = function () {
            if (this.marketDataTransport)
                this.marketDataTransport.prepareLogout();
        };
        MessageService.prototype.supportsStreaming = function (messageType) {
            switch (messageType) {
                case MessageType.MarketData:
                    return this.marketDataTransport.supportsStreaming();
                case MessageType.Trading:
                    return this.tradingTransport.supportsStreaming();
                case MessageType.Routing:
                    return this.routingTransport.supportsStreaming();
                case MessageType.Connect:
                    return this.visualConnectTransport.supportsStreaming();
                case MessageType.InfinData:
                    return this.visualTransport.supportsStreaming();
            }
            return false;
        };
        MessageService.prototype.disconnectMarketData = function () {
            this.marketDataTransport.close();
        };
        MessageService.prototype.disconnectTrading = function () {
            this.tradingTransport.close();
        };
        MessageService.prototype.disconnectVisual = function () {
            this.visualTransport.close();
        };
        MessageService.prototype.setMarketDataSessionToken = function (token) {
            this.marketDataSessionToken = token;
        };
        MessageService.prototype.setTradingSessionToken = function (token) {
            this.tradingSessionToken = token;
        };
        MessageService.prototype.setVisualSessionToken = function (token) {
            this.visualSessionToken = token;
        };
        MessageService.prototype.setRoutingUrl = function (url) {
            this.routingTransport = null;
            this.routingUrl = url;
        };
        MessageService.prototype.getRoutingUrl = function () {
            return this.routingUrl;
        };
        MessageService.prototype.setVisualConnectUrl = function (url) {
            this.visualConnectTransport = null;
            this.visualConnectUrl = url;
        };
        MessageService.prototype.getVisualConnectUrl = function () {
            return this.visualConnectUrl;
        };
        MessageService.prototype.setMarketDataUrl = function (url) {
            this.marketDataTransport = null;
            this.marketDataUrl = url;
        };
        MessageService.prototype.getMarketDataUrl = function () {
            return this.marketDataUrl;
        };
        MessageService.prototype.setVisualUrl = function (url) {
            this.visualTransport = null;
            this.visualUrl = url;
        };
        MessageService.prototype.getVisualUrl = function () {
            return this.visualUrl;
        };
        MessageService.prototype.setTradingUrl = function (url) {
            this.tradingTransport = null;
            this.tradingUrl = url;
        };
        MessageService.prototype.getTradingUrl = function () {
            return this.tradingUrl;
        };
        MessageService.prototype.sendSubscriptionRequest = function (request, options) {
            var _this = this;
            //Create update_data. Updatedata is different from request-data, in that all subscriptions to the same item must have the same update-data.
            //This is therefore created in the request-object itself.
            var update_data = request.getUpdateData();
            //Set the update-data on the message itself.
            request[Infront.MWSConstants.update_data] = update_data;
            //Create a callback-function for the event-update from the server
            var updateCallback = function (data) {
                if (data.hasOwnProperty(request.getEventKey())) {
                    options.onData(data[request.getEventKey()]);
                }
            };
            //Get or create the observer-container for this specific update-data and add our callback to it.
            var obsContainer = null;
            if (this.subscriptionObservers.hasOwnProperty(update_data)) {
                obsContainer = this.subscriptionObservers[update_data];
            }
            else {
                obsContainer = new SubscriptionObserverContainer();
                this.subscriptionObservers[update_data] = obsContainer;
            }
            var obs = new SubscriptionObserver();
            obs.updateCallback = updateCallback;
            obs.sourceRequest = request;
            obsContainer.add(obs);
            //Now send the message as normal.
            this.sendMessage(request, false, function (result) {
                if (_this.validateResult(result, request)) {
                    if (result[Infront.MWSConstants.error_code] === 0) {
                        options.onSuccess(result[request.getResponseKey()]);
                    }
                }
                else {
                    options.onError(-1, "Malformed response from server");
                }
            });
            //Create an unsubscribe-request, pack it in a callback and return it.
            var unsubscribeRequest = request.createUnsubscribeRequest();
            return function (unsubOptions) {
                _this.sendRequest(unsubscribeRequest, unsubOptions);
                obsContainer.remove(obs);
            };
        };
        MessageService.prototype.instrumentSubscribe = function (request, options) {
            this.sendRequest(request, options, false);
        };
        MessageService.prototype.sendRequest = function (request, options, resend) {
            var _this = this;
            if (resend === void 0) { resend = true; }
            this.sendMessage(request, resend, function (result) {
                if (_this.validateResult(result, request)) {
                    if (result[Infront.MWSConstants.error_code] === 0) {
                        var innerResult = result[request.getResponseKey()];
                        options.onSuccess(innerResult);
                    }
                    else {
                        if ("error_displayable" in result) {
                            options.onError(result[Infront.MWSConstants.error_code], result["error_displayable"]);
                        }
                        else {
                            options.onError(result[Infront.MWSConstants.error_code], result["error_message"]);
                        }
                    }
                }
                else {
                    options.onError(-1, "Malformed response from server");
                }
            });
        };
        MessageService.prototype.sendMessage = function (message, resendOnReconnect, callback) {
            var requestData = MessageService.getUniqueRequestData();
            var messageRoot = {};
            messageRoot[message.getMessageKey()] = message;
            messageRoot[Infront.MWSConstants.request_data] = requestData;
            if (message.getMessageType() != MessageType.Routing && message.getMessageType() != MessageType.Connect && message.getMessageType() != MessageType.InfinData) {
                var token = this.getSessionToken(message.getMessageType());
                if (message.getMessageKey() != "md_login_request" && (token == null || typeof (token) != "string")) {
                    InfrontUtil.errorLog("Trying to send a normal message without session-key: " + message.getMessageKey());
                }
                if (token != null) {
                    messageRoot[Infront.MWSConstants.session_token] = this.getSessionToken(message.getMessageType());
                }
            }
            var json = JSON.stringify(messageRoot);
            var obs = new ResponseObserver();
            obs.messageRoot = messageRoot;
            obs.callback = callback;
            obs.encodedMessage = json;
            obs.sourceMessage = message;
            obs.resendOnReconnect = resendOnReconnect;
            this.responseObservers.add(requestData, obs);
            var container = new MessageContainer(json, message.getMessageType());
            //container.message = json;
            //container.messageType = message.getMessageType();
            this.routeMessage(container);
        };
        MessageService.prototype.setSessionTerminatedObserver = function (callback) {
            this.sessionTerminatedObserver = callback;
        };
        MessageService.prototype.transportConnected = function (source) {
        };
        MessageService.prototype.transportDisconnected = function (source) {
            if (source === this.marketDataTransport) {
                this.observer.marketDataDisconnected();
                //this.marketDataTransport = null;
                this.marketDataSessionToken = null;
            }
            else if (source === this.tradingTransport) {
                this.observer.tradingDisconnected();
                this.tradingTransport = null;
                this.tradingSessionToken = null;
            }
        };
        MessageService.prototype.transportReconnecting = function (source) {
            if (source === this.tradingTransport) {
                this.observer.tradingReconnecting();
            }
        };
        MessageService.prototype.transportReconnected = function (source) {
            if (source.supportsStreaming()) {
                var resendType = null;
                if (source == this.marketDataTransport) {
                    resendType = MessageType.MarketData;
                    if (this.onMarketdataReconnect) {
                        this.onMarketdataReconnect();
                    }
                }
                else if (source == this.tradingTransport) {
                    resendType = MessageType.Trading;
                    this.observer.tradingReconnected();
                }
                if (resendType != null) {
                    var observers = this.responseObservers.getAll();
                    for (var i = 0; i < observers.length; i++) {
                        if (observers[i].resendOnReconnect && observers[i].sourceMessage.getMessageType() == resendType) {
                            var messageRoot = observers[i].messageRoot;
                            var messageType = observers[i].sourceMessage.getMessageType();
                            if (messageRoot[Infront.MWSConstants.session_token])
                                messageRoot[Infront.MWSConstants.session_token] = this.getSessionToken(messageType); //When resending we have to make sure we still have the right session_token
                            var container = new MessageContainer(JSON.stringify(messageRoot), messageType);
                            this.routeMessage(container);
                        }
                    }
                    for (var p in this.subscriptionObservers) {
                        if (this.subscriptionObservers.hasOwnProperty(p)) {
                            var obsContainer = this.subscriptionObservers[p];
                            var subObservers = obsContainer.getAll();
                            for (var i = 0; i < subObservers.length; i++) {
                                this.sendMessage(subObservers[i].sourceRequest, false, function (result) {
                                    //TODO: At the moment we haven't anything good to do here. If this fails there is no suitable callback.
                                    //TODO: We should establish a Subscription-interrupted callback for this situation.
                                });
                            }
                        }
                    }
                }
            }
        };
        MessageService.prototype.transportError = function (source, sourceMessage, code, message) {
            //console.error("Transport Error: " + message + " (" + code + ")");
        };
        MessageService.prototype.messageReceived = function (source, message) {
            //            var messageRoot = JSON.parse(message);
            var messageRoot = InfrontUtil.parseJson(message);
            //Test if the message is not null (visual issue)
            if (messageRoot == null)
                return;
            //First handle session terminated as a special case
            if (source == this.marketDataTransport && messageRoot.hasOwnProperty(MessageService.kSessionTerminatedKey)) {
                this.marketDataTransport.close();
                if (this.sessionTerminatedObserver) {
                    this.sessionTerminatedObserver(messageRoot[MessageService.kSessionTerminatedKey]);
                }
                return;
            }
            //Handle special observers
            for (var key in messageRoot) {
                if (this.specialEventObservers.hasOwnProperty(key)) {
                    var obsArr = this.specialEventObservers[key];
                    for (var i = 0; i < obsArr.length; i++) {
                        obsArr[i](messageRoot);
                    }
                }
            }
            if (messageRoot.hasOwnProperty(Infront.MWSConstants.request_data)) {
                var obs = this.responseObservers.remove(messageRoot[Infront.MWSConstants.request_data]);
                if (obs != null) {
                    obs.callback(messageRoot);
                }
            }
            else if (messageRoot.hasOwnProperty(Infront.MWSConstants.update_data)) {
                var updateData = messageRoot[Infront.MWSConstants.update_data];
                if (this.subscriptionObservers.hasOwnProperty(updateData)) {
                    var observerContainer = this.subscriptionObservers[updateData];
                    observerContainer.call(messageRoot);
                }
                else {
                    //console.warn("Received EventUpdate with no registered listener");
                    //console.log(messageRoot);
                }
            }
            else {
                //console.warn("Received message with no observer");
                //console.log(messageRoot);
            }
        };
        MessageService.prototype.routeMessage = function (container) {
            var transport = null;
            switch (container.messageType) {
                case MessageType.MarketData:
                    transport = this.getMarketDataTransport();
                    break;
                case MessageType.Trading:
                    transport = this.getTradingTransport();
                    break;
                case MessageType.Routing:
                    transport = this.getRoutingTransport();
                    break;
                case MessageType.Connect:
                    transport = this.getVisualConnectTransport();
                    break;
                case MessageType.InfinData:
                    transport = this.getVisualTransport();
                    break;
            }
            if (transport != null) {
                transport.sendMessage(container.message);
            }
        };
        MessageService.prototype.getRoutingTransport = function () {
            if (this.routingTransport == null && this.routingUrl != null) {
                this.routingTransport = this.transportFactory.transportForUrl(this.routingUrl, this);
            }
            return this.routingTransport;
        };
        MessageService.prototype.getMarketDataTransport = function () {
            if (this.marketDataTransport == null && this.marketDataUrl != null) {
                this.marketDataTransport = this.transportFactory.transportForUrl(this.marketDataUrl, this);
            }
            return this.marketDataTransport;
        };
        MessageService.prototype.getVisualConnectTransport = function () {
            if (this.visualConnectTransport == null && this.visualConnectUrl != null) {
                this.visualConnectTransport = this.transportFactory.transportForUrl(this.visualConnectUrl, this);
            }
            return this.visualConnectTransport;
        };
        MessageService.prototype.getVisualTransport = function () {
            if (this.visualTransport == null && this.visualUrl != null) {
                this.visualTransport = this.transportFactory.transportForUrl(this.visualUrl, this);
            }
            return this.visualTransport;
        };
        MessageService.prototype.getTradingTransport = function () {
            if (this.tradingTransport == null && this.tradingUrl != null) {
                this.tradingTransport = this.transportFactory.transportForUrl(this.tradingUrl, this);
            }
            return this.tradingTransport;
        };
        MessageService.prototype.getSessionToken = function (messageType) {
            switch (messageType) {
                case MessageType.MarketData:
                    return this.marketDataSessionToken;
                case MessageType.Trading:
                    return this.tradingSessionToken;
                case MessageType.InfinData:
                    return this.visualSessionToken;
                default:
                    return null;
            }
        };
        MessageService.prototype.validateResult = function (result, request) {
            return result.hasOwnProperty(Infront.MWSConstants.error_code) && (result[Infront.MWSConstants.error_code] != 0 || result.hasOwnProperty(request.getResponseKey()));
        };
        MessageService.getUniqueRequestData = function () {
            return (MessageService.messageCounter++).toString();
        };
        MessageService.kSessionTerminatedKey = "md_session_terminated";
        MessageService.messageCounter = 0;
        return MessageService;
    }());
    Infront.MessageService = MessageService;
    var SubscriptionObserver = (function () {
        function SubscriptionObserver() {
        }
        return SubscriptionObserver;
    }());
    var SubscriptionObserverContainer = (function () {
        function SubscriptionObserverContainer() {
            this.observers = [];
        }
        SubscriptionObserverContainer.prototype.call = function (data) {
            var calledObservers = [];
            for (var i = 0; i < this.observers.length; i++) {
                var obs = this.observers[i];
                if (calledObservers.indexOf(obs.updateCallback) === -1) {
                    obs.updateCallback(data);
                    calledObservers.push(obs.updateCallback);
                }
            }
        };
        SubscriptionObserverContainer.prototype.add = function (obs) {
            this.observers.push(obs);
        };
        SubscriptionObserverContainer.prototype.remove = function (obs) {
            var idx = this.observers.indexOf(obs);
            if (idx > -1) {
                this.observers.splice(idx, 1);
            }
        };
        SubscriptionObserverContainer.prototype.getAll = function () {
            return this.observers;
        };
        return SubscriptionObserverContainer;
    }());
    /**
     * When you reach this point you may ask yourself: "Pray, why did he implement it this way instead of traditional getters?"
     * The reason is simple: Subclasses of this class is serialized directly into JSON. If we stored messageKey, responseKey
     * and type as member variables they would be added to the JSON message, which the server api doesn't support.
     * Function-members are ignored when serializing JSON,  so that is why we do it this way.
     */
    var GenericRequest = (function () {
        function GenericRequest(messageKey, responseKey, type) {
            this._getMessageKey = function () { return messageKey; };
            this._getResponseKey = function () { return responseKey; };
            this._getMessageType = function () { return type; };
        }
        GenericRequest.prototype.getMessageType = function () {
            return this._getMessageType();
        };
        GenericRequest.prototype.getMessageKey = function () {
            return this._getMessageKey();
        };
        GenericRequest.prototype.getResponseKey = function () {
            return this._getResponseKey();
        };
        return GenericRequest;
    }());
    Infront.GenericRequest = GenericRequest;
    var SubscribeAlertsRequest = (function (_super) {
        __extends(SubscribeAlertsRequest, _super);
        function SubscribeAlertsRequest(updateData) {
            var _this = _super.call(this, "md_subscribe_alerts_request", "md_subscribe_alerts_response", MessageType.MarketData) || this;
            _this._getUpdateData = function () { return updateData; };
            return _this;
        }
        SubscribeAlertsRequest.prototype.getEventKey = function () {
            return "md_alert_update";
        };
        SubscribeAlertsRequest.prototype.createUnsubscribeRequest = function () {
            var request = new UnsubscribeAlertsRequest();
            request.update_data = this.getUpdateData();
            return request;
        };
        SubscribeAlertsRequest.prototype.getUpdateData = function () {
            return this._getUpdateData();
        };
        return SubscribeAlertsRequest;
    }(GenericRequest));
    Infront.SubscribeAlertsRequest = SubscribeAlertsRequest;
    var UnsubscribeAlertsRequest = (function (_super) {
        __extends(UnsubscribeAlertsRequest, _super);
        function UnsubscribeAlertsRequest() {
            return _super.call(this, "md_unsubscribe_alerts_request", "md_unsubscribe_alerts_response", MessageType.MarketData) || this;
        }
        return UnsubscribeAlertsRequest;
    }(GenericRequest));
    Infront.UnsubscribeAlertsRequest = UnsubscribeAlertsRequest;
    var SubscribeNewsRequest = (function (_super) {
        __extends(SubscribeNewsRequest, _super);
        function SubscribeNewsRequest(updateData) {
            var _this = _super.call(this, "md_subscribe_news_request", "md_subscribe_news_response", MessageType.MarketData) || this;
            _this._getUpdateData = function () { return updateData; };
            return _this;
        }
        SubscribeNewsRequest.prototype.getEventKey = function () {
            return "md_news_update";
        };
        SubscribeNewsRequest.prototype.createUnsubscribeRequest = function () {
            var request = new UnsubscribeNewsRequest();
            request.update_data = this.getUpdateData();
            return request;
        };
        SubscribeNewsRequest.prototype.getUpdateData = function () {
            return this._getUpdateData();
        };
        return SubscribeNewsRequest;
    }(GenericRequest));
    Infront.SubscribeNewsRequest = SubscribeNewsRequest;
    var UnsubscribeNewsRequest = (function (_super) {
        __extends(UnsubscribeNewsRequest, _super);
        function UnsubscribeNewsRequest() {
            return _super.call(this, "md_unsubscribe_news_request", "md_unsubscribe_news_response", MessageType.MarketData) || this;
        }
        return UnsubscribeNewsRequest;
    }(GenericRequest));
    Infront.UnsubscribeNewsRequest = UnsubscribeNewsRequest;
    var SubscribeMarketActivityRequest = (function (_super) {
        __extends(SubscribeMarketActivityRequest, _super);
        function SubscribeMarketActivityRequest(updateData) {
            var _this = _super.call(this, "md_subscribe_market_activity_request", "md_get_market_activity_response", MessageType.MarketData) || this;
            _this._getUpdateData = function () {
                return updateData;
            };
            return _this;
        }
        SubscribeMarketActivityRequest.prototype.getEventKey = function () {
            return "md_activity_update";
        };
        SubscribeMarketActivityRequest.prototype.createUnsubscribeRequest = function () {
            return new UnsubscribeMarketActivityRequest(this._getUpdateData());
        };
        SubscribeMarketActivityRequest.prototype.getUpdateData = function () {
            return this._getUpdateData();
        };
        return SubscribeMarketActivityRequest;
    }(GenericRequest));
    Infront.SubscribeMarketActivityRequest = SubscribeMarketActivityRequest;
    var UnsubscribeMarketActivityRequest = (function (_super) {
        __extends(UnsubscribeMarketActivityRequest, _super);
        function UnsubscribeMarketActivityRequest(updateData) {
            var _this = _super.call(this, "md_unsubscribe_market_activity_request", "md_unsubscribe_market_activity_response", MessageType.MarketData) || this;
            _this.update_data = updateData;
            return _this;
        }
        return UnsubscribeMarketActivityRequest;
    }(GenericRequest));
    Infront.UnsubscribeMarketActivityRequest = UnsubscribeMarketActivityRequest;
    var GetServiceRoutingsRequest = (function (_super) {
        __extends(GetServiceRoutingsRequest, _super);
        function GetServiceRoutingsRequest() {
            return _super.call(this, "get_service_routings_request", "get_service_routings_response", MessageType.Routing) || this;
        }
        return GetServiceRoutingsRequest;
    }(GenericRequest));
    Infront.GetServiceRoutingsRequest = GetServiceRoutingsRequest;
    var MarketDataLoginRequest = (function (_super) {
        __extends(MarketDataLoginRequest, _super);
        function MarketDataLoginRequest() {
            return _super.call(this, "md_login_request", "md_login_response", MessageType.MarketData) || this;
        }
        return MarketDataLoginRequest;
    }(GenericRequest));
    Infront.MarketDataLoginRequest = MarketDataLoginRequest;
    var MarketDataLogoutRequest = (function (_super) {
        __extends(MarketDataLogoutRequest, _super);
        function MarketDataLogoutRequest() {
            return _super.call(this, "md_logout_request", "md_logout_response", MessageType.MarketData) || this;
        }
        return MarketDataLogoutRequest;
    }(GenericRequest));
    Infront.MarketDataLogoutRequest = MarketDataLogoutRequest;
    var MarketDataKeepAliveRequest = (function (_super) {
        __extends(MarketDataKeepAliveRequest, _super);
        function MarketDataKeepAliveRequest() {
            return _super.call(this, "md_keep_alive_request", "md_keep_alive_response", MessageType.MarketData) || this;
        }
        return MarketDataKeepAliveRequest;
    }(GenericRequest));
    Infront.MarketDataKeepAliveRequest = MarketDataKeepAliveRequest;
    var GetFeedMetadataRequest = (function (_super) {
        __extends(GetFeedMetadataRequest, _super);
        function GetFeedMetadataRequest() {
            return _super.call(this, "md_get_feed_metadata_request", "md_get_feed_metadata_response", MessageType.MarketData) || this;
        }
        return GetFeedMetadataRequest;
    }(GenericRequest));
    Infront.GetFeedMetadataRequest = GetFeedMetadataRequest;
    var GetReferenceDataRequest = (function (_super) {
        __extends(GetReferenceDataRequest, _super);
        function GetReferenceDataRequest() {
            return _super.call(this, "md_get_reference_data_request", "md_get_reference_data_response", MessageType.MarketData) || this;
        }
        return GetReferenceDataRequest;
    }(GenericRequest));
    Infront.GetReferenceDataRequest = GetReferenceDataRequest;
    var GetSnapshotRequest = (function (_super) {
        __extends(GetSnapshotRequest, _super);
        function GetSnapshotRequest() {
            var _this = _super.call(this, "md_get_snapshot_request", "md_get_snapshot_response", MessageType.MarketData) || this;
            _this.instruments = [];
            return _this;
        }
        GetSnapshotRequest.prototype.setInstrument = function (instrument) {
            this.instruments[0] = instrument;
        };
        GetSnapshotRequest.prototype.setInstruments = function (instruments) {
            this.instruments = instruments;
        };
        GetSnapshotRequest.prototype.setFields = function (fields) {
            this.fields = fields;
        };
        GetSnapshotRequest.prototype.getInstrument = function () {
            return this.instruments.length > 0 ? this.instruments[0] : null;
        };
        GetSnapshotRequest.prototype.getFields = function () {
            return this.fields;
        };
        return GetSnapshotRequest;
    }(GenericRequest));
    Infront.GetSnapshotRequest = GetSnapshotRequest;
    var InstrumentSubscriptionRequest = (function (_super) {
        __extends(InstrumentSubscriptionRequest, _super);
        function InstrumentSubscriptionRequest() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        InstrumentSubscriptionRequest.prototype.getMessageKey = function () {
            return "md_subscribe_instrument_request";
        };
        InstrumentSubscriptionRequest.prototype.getResponseKey = function () {
            return "md_subscribe_instrument_response";
        };
        InstrumentSubscriptionRequest.prototype.getEventKey = function () {
            return "md_instrument_update";
        };
        InstrumentSubscriptionRequest.prototype.getUpdateData = function () {
            return "md_instrument_update"; //this.getInstrument().feed + "/" + this.getInstrument().ticker;
        };
        InstrumentSubscriptionRequest.prototype.createUnsubscribeRequest = function () {
            var retVal = new InstrumentUnsubscribeRequest();
            retVal.fields = this.fields;
            retVal.instruments = this.instruments;
            return retVal;
        };
        return InstrumentSubscriptionRequest;
    }(GetSnapshotRequest));
    Infront.InstrumentSubscriptionRequest = InstrumentSubscriptionRequest;
    var InstrumentUnsubscribeRequest = (function (_super) {
        __extends(InstrumentUnsubscribeRequest, _super);
        function InstrumentUnsubscribeRequest() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        InstrumentUnsubscribeRequest.prototype.getMessageKey = function () {
            return "md_unsubscribe_instrument_request";
        };
        InstrumentUnsubscribeRequest.prototype.getResponseKey = function () {
            return "md_unsubscribe_instrument_response";
        };
        return InstrumentUnsubscribeRequest;
    }(GetSnapshotRequest));
    Infront.InstrumentUnsubscribeRequest = InstrumentUnsubscribeRequest;
    var RankedListSubscriptionRequest = (function (_super) {
        __extends(RankedListSubscriptionRequest, _super);
        function RankedListSubscriptionRequest() {
            return _super.call(this, "md_subscribe_ranked_list_request", "md_subscribe_ranked_list_response", MessageType.MarketData) || this;
        }
        RankedListSubscriptionRequest.prototype.getEventKey = function () {
            return "md_rank_update";
        };
        RankedListSubscriptionRequest.prototype.createUnsubscribeRequest = function () {
            var req = new RankedListUnsubscribeRequest();
            req.feed = this.feed;
            req.sort_order = this.sort_order;
            return req;
        };
        RankedListSubscriptionRequest.prototype.getUpdateData = function () {
            return MessageService.getUniqueRequestData();
        };
        return RankedListSubscriptionRequest;
    }(GenericRequest));
    Infront.RankedListSubscriptionRequest = RankedListSubscriptionRequest;
    var RankedListUnsubscribeRequest = (function (_super) {
        __extends(RankedListUnsubscribeRequest, _super);
        function RankedListUnsubscribeRequest() {
            return _super.call(this, "md_unsubscribe_ranked_list_request", "md_unsubscribe_ranked_list_response", MessageType.MarketData) || this;
        }
        return RankedListUnsubscribeRequest;
    }(GenericRequest));
    Infront.RankedListUnsubscribeRequest = RankedListUnsubscribeRequest;
    var GetRankedListRequest = (function (_super) {
        __extends(GetRankedListRequest, _super);
        function GetRankedListRequest() {
            return _super.call(this, "md_get_ranked_list_request", "md_get_ranked_list_response", MessageType.MarketData) || this;
        }
        return GetRankedListRequest;
    }(GenericRequest));
    Infront.GetRankedListRequest = GetRankedListRequest;
    var GetChainRequest = (function (_super) {
        __extends(GetChainRequest, _super);
        function GetChainRequest() {
            return _super.call(this, "md_get_chain_request", "md_get_chain_response", MessageType.MarketData) || this;
        }
        return GetChainRequest;
    }(GenericRequest));
    Infront.GetChainRequest = GetChainRequest;
    var GetChainsRequest = (function (_super) {
        __extends(GetChainsRequest, _super);
        function GetChainsRequest() {
            return _super.call(this, "md_get_chains_request", "md_get_chains_response", MessageType.MarketData) || this;
        }
        return GetChainsRequest;
    }(GenericRequest));
    Infront.GetChainsRequest = GetChainsRequest;
    var GetNewsSourcesRequest = (function (_super) {
        __extends(GetNewsSourcesRequest, _super);
        function GetNewsSourcesRequest() {
            return _super.call(this, "md_get_news_sources_request", "md_get_news_sources_response", MessageType.MarketData) || this;
        }
        return GetNewsSourcesRequest;
    }(GenericRequest));
    Infront.GetNewsSourcesRequest = GetNewsSourcesRequest;
    var GetNewsItemsRequest = (function (_super) {
        __extends(GetNewsItemsRequest, _super);
        function GetNewsItemsRequest() {
            return _super.call(this, "md_get_news_items_request", "md_get_news_items_response", MessageType.MarketData) || this;
        }
        return GetNewsItemsRequest;
    }(GenericRequest));
    Infront.GetNewsItemsRequest = GetNewsItemsRequest;
    var GetNewsBodyRequest = (function (_super) {
        __extends(GetNewsBodyRequest, _super);
        function GetNewsBodyRequest() {
            return _super.call(this, "md_get_news_body_request", "md_get_news_body_response", MessageType.MarketData) || this;
        }
        return GetNewsBodyRequest;
    }(GenericRequest));
    Infront.GetNewsBodyRequest = GetNewsBodyRequest;
    var GetAlertsRequest = (function (_super) {
        __extends(GetAlertsRequest, _super);
        function GetAlertsRequest() {
            return _super.call(this, "md_get_alerts_request", "md_get_alerts_response", MessageType.MarketData) || this;
        }
        return GetAlertsRequest;
    }(GenericRequest));
    Infront.GetAlertsRequest = GetAlertsRequest;
    var AddAlertRequest = (function (_super) {
        __extends(AddAlertRequest, _super);
        function AddAlertRequest() {
            return _super.call(this, "md_add_alert_request", "md_add_alert_response", MessageType.MarketData) || this;
        }
        return AddAlertRequest;
    }(GenericRequest));
    Infront.AddAlertRequest = AddAlertRequest;
    var ModifyAlertRequest = (function (_super) {
        __extends(ModifyAlertRequest, _super);
        function ModifyAlertRequest() {
            return _super.call(this, "md_modify_alert_request", "md_modify_alert_response", MessageType.MarketData) || this;
        }
        return ModifyAlertRequest;
    }(GenericRequest));
    Infront.ModifyAlertRequest = ModifyAlertRequest;
    var DeleteAlertRequest = (function (_super) {
        __extends(DeleteAlertRequest, _super);
        function DeleteAlertRequest() {
            return _super.call(this, "md_delete_alert_request", "md_delete_alert_response", MessageType.MarketData) || this;
        }
        return DeleteAlertRequest;
    }(GenericRequest));
    Infront.DeleteAlertRequest = DeleteAlertRequest;
    var InstrumentSearchRequest = (function (_super) {
        __extends(InstrumentSearchRequest, _super);
        function InstrumentSearchRequest() {
            return _super.call(this, "md_instrument_search_request", "md_instrument_search_response", MessageType.MarketData) || this;
        }
        return InstrumentSearchRequest;
    }(GenericRequest));
    Infront.InstrumentSearchRequest = InstrumentSearchRequest;
    var CompanyInfoRequest = (function (_super) {
        __extends(CompanyInfoRequest, _super);
        function CompanyInfoRequest() {
            return _super.call(this, "md_get_company_request", "md_get_company_response", MessageType.MarketData) || this;
        }
        return CompanyInfoRequest;
    }(GenericRequest));
    Infront.CompanyInfoRequest = CompanyInfoRequest;
    var BrokerStatsRequest = (function (_super) {
        __extends(BrokerStatsRequest, _super);
        function BrokerStatsRequest() {
            return _super.call(this, "md_get_broker_stats_request", "md_get_broker_stats_response", MessageType.MarketData) || this;
        }
        return BrokerStatsRequest;
    }(GenericRequest));
    Infront.BrokerStatsRequest = BrokerStatsRequest;
    var TickSizesRequest = (function (_super) {
        __extends(TickSizesRequest, _super);
        function TickSizesRequest() {
            return _super.call(this, "md_get_tick_sizes_request", "md_get_tick_sizes_response", MessageType.MarketData) || this;
        }
        return TickSizesRequest;
    }(GenericRequest));
    Infront.TickSizesRequest = TickSizesRequest;
    var CalendarRequest = (function (_super) {
        __extends(CalendarRequest, _super);
        function CalendarRequest() {
            return _super.call(this, "md_get_calendar_request", "md_get_calendar_response", MessageType.MarketData) || this;
        }
        return CalendarRequest;
    }(GenericRequest));
    Infront.CalendarRequest = CalendarRequest;
    var TradesByDateRequest = (function (_super) {
        __extends(TradesByDateRequest, _super);
        function TradesByDateRequest() {
            return _super.call(this, "md_get_trades_by_date_request", "md_get_trades_by_date_response", MessageType.MarketData) || this;
        }
        return TradesByDateRequest;
    }(GenericRequest));
    Infront.TradesByDateRequest = TradesByDateRequest;
    var TradesByDaysRequest = (function (_super) {
        __extends(TradesByDaysRequest, _super);
        function TradesByDaysRequest() {
            return _super.call(this, "md_get_trades_by_days_request", "md_get_trades_by_days_response", MessageType.MarketData) || this;
        }
        return TradesByDaysRequest;
    }(GenericRequest));
    Infront.TradesByDaysRequest = TradesByDaysRequest;
    var HistoricalTradesRequest = (function (_super) {
        __extends(HistoricalTradesRequest, _super);
        function HistoricalTradesRequest() {
            return _super.call(this, "md_get_historical_trades_request", "md_get_historical_trades_response", MessageType.MarketData) || this;
        }
        return HistoricalTradesRequest;
    }(GenericRequest));
    Infront.HistoricalTradesRequest = HistoricalTradesRequest;
    var InstrumentIntradayTradesRequest = (function (_super) {
        __extends(InstrumentIntradayTradesRequest, _super);
        function InstrumentIntradayTradesRequest() {
            return _super.call(this, "md_get_intraday_trades_request", "md_get_intraday_trades_response", MessageType.MarketData) || this;
        }
        return InstrumentIntradayTradesRequest;
    }(GenericRequest));
    Infront.InstrumentIntradayTradesRequest = InstrumentIntradayTradesRequest;
    var CompanyHistoryRequest = (function (_super) {
        __extends(CompanyHistoryRequest, _super);
        function CompanyHistoryRequest() {
            return _super.call(this, "md_get_company_history_request", "md_get_company_history_response", MessageType.MarketData) || this;
        }
        return CompanyHistoryRequest;
    }(GenericRequest));
    Infront.CompanyHistoryRequest = CompanyHistoryRequest;
    var MarketListRequest = (function (_super) {
        __extends(MarketListRequest, _super);
        function MarketListRequest() {
            return _super.call(this, "md_get_markets_request", "md_get_markets_response", MessageType.MarketData) || this;
        }
        return MarketListRequest;
    }(GenericRequest));
    Infront.MarketListRequest = MarketListRequest;
    var FinancialCalendarRequest = (function (_super) {
        __extends(FinancialCalendarRequest, _super);
        function FinancialCalendarRequest() {
            return _super.call(this, "md_get_calendar_request", "md_get_calendar_response", MessageType.MarketData) || this;
        }
        return FinancialCalendarRequest;
    }(GenericRequest));
    Infront.FinancialCalendarRequest = FinancialCalendarRequest;
    var MarketActivityRequest = (function (_super) {
        __extends(MarketActivityRequest, _super);
        function MarketActivityRequest() {
            return _super.call(this, "md_get_market_activity_request", "md_get_market_activity_response", MessageType.MarketData) || this;
        }
        return MarketActivityRequest;
    }(GenericRequest));
    Infront.MarketActivityRequest = MarketActivityRequest;
    var GetListNamesRequest = (function (_super) {
        __extends(GetListNamesRequest, _super);
        function GetListNamesRequest() {
            return _super.call(this, "md_get_list_names_request", "md_get_list_names_response", MessageType.MarketData) || this;
        }
        return GetListNamesRequest;
    }(GenericRequest));
    Infront.GetListNamesRequest = GetListNamesRequest;
    var GetUserListRequest = (function (_super) {
        __extends(GetUserListRequest, _super);
        function GetUserListRequest() {
            return _super.call(this, "md_get_user_list_request", "md_get_user_list_response", MessageType.MarketData) || this;
        }
        return GetUserListRequest;
    }(GenericRequest));
    Infront.GetUserListRequest = GetUserListRequest;
    var SaveUserListRequest = (function (_super) {
        __extends(SaveUserListRequest, _super);
        function SaveUserListRequest() {
            return _super.call(this, "md_save_user_list_request", "md_save_user_list_response", MessageType.MarketData) || this;
        }
        return SaveUserListRequest;
    }(GenericRequest));
    Infront.SaveUserListRequest = SaveUserListRequest;
    var DeleteUserListRequest = (function (_super) {
        __extends(DeleteUserListRequest, _super);
        function DeleteUserListRequest() {
            return _super.call(this, "md_delete_user_list_request", "md_delete_user_list_response", MessageType.MarketData) || this;
        }
        return DeleteUserListRequest;
    }(GenericRequest));
    Infront.DeleteUserListRequest = DeleteUserListRequest;
    var GetInfinancialsUrlRequest = (function (_super) {
        __extends(GetInfinancialsUrlRequest, _super);
        function GetInfinancialsUrlRequest() {
            return _super.call(this, "md_get_infinancials_request", "md_get_infinancials_response", MessageType.MarketData) || this;
        }
        return GetInfinancialsUrlRequest;
    }(GenericRequest));
    Infront.GetInfinancialsUrlRequest = GetInfinancialsUrlRequest;
    //Trading Messages
    var TradingRequest = (function (_super) {
        __extends(TradingRequest, _super);
        function TradingRequest(messageKey, responseKey) {
            return _super.call(this, messageKey, responseKey, MessageType.Trading) || this;
        }
        return TradingRequest;
    }(GenericRequest));
    Infront.TradingRequest = TradingRequest;
    var TradingSubscriptionRequest = (function (_super) {
        __extends(TradingSubscriptionRequest, _super);
        function TradingSubscriptionRequest(messageKey, responseKey, eventKey, updateData) {
            var _this = _super.call(this, messageKey, responseKey) || this;
            _this._getEventKey = function () { return eventKey; };
            _this._getUpdateData = function () { return updateData; };
            return _this;
        }
        TradingSubscriptionRequest.prototype.getEventKey = function () {
            return this._getEventKey();
        };
        TradingSubscriptionRequest.prototype.getUpdateData = function () {
            return this._getUpdateData();
        };
        TradingSubscriptionRequest.prototype.createUnsubscribeRequest = function () {
            return null;
        };
        return TradingSubscriptionRequest;
    }(TradingRequest));
    Infront.TradingSubscriptionRequest = TradingSubscriptionRequest;
    var TRLoginRequest = (function (_super) {
        __extends(TRLoginRequest, _super);
        function TRLoginRequest() {
            return _super.call(this, "tr_login_request", "tr_login_response") || this;
        }
        return TRLoginRequest;
    }(TradingRequest));
    Infront.TRLoginRequest = TRLoginRequest;
    var TRLogoutRequest = (function (_super) {
        __extends(TRLogoutRequest, _super);
        function TRLogoutRequest() {
            return _super.call(this, "tr_logout_request", "tr_logout_response") || this;
        }
        return TRLogoutRequest;
    }(TradingRequest));
    Infront.TRLogoutRequest = TRLogoutRequest;
    var TRKeepAliveRequest = (function (_super) {
        __extends(TRKeepAliveRequest, _super);
        function TRKeepAliveRequest() {
            return _super.call(this, "tr_keep_alive_request", "tr_keep_alive_response") || this;
        }
        return TRKeepAliveRequest;
    }(TradingRequest));
    Infront.TRKeepAliveRequest = TRKeepAliveRequest;
    var TRGetPortfolioNamesRequest = (function (_super) {
        __extends(TRGetPortfolioNamesRequest, _super);
        function TRGetPortfolioNamesRequest() {
            return _super.call(this, "tr_get_portfolio_names_request", "tr_get_portfolio_names_response") || this;
        }
        return TRGetPortfolioNamesRequest;
    }(TradingRequest));
    Infront.TRGetPortfolioNamesRequest = TRGetPortfolioNamesRequest;
    var TRGetTradingPowerRequest = (function (_super) {
        __extends(TRGetTradingPowerRequest, _super);
        function TRGetTradingPowerRequest() {
            return _super.call(this, "tr_get_trading_power_request", "tr_get_trading_power_response") || this;
        }
        return TRGetTradingPowerRequest;
    }(TradingRequest));
    Infront.TRGetTradingPowerRequest = TRGetTradingPowerRequest;
    var TRSubscribePortfolioRequest = (function (_super) {
        __extends(TRSubscribePortfolioRequest, _super);
        function TRSubscribePortfolioRequest() {
            return _super.call(this, "tr_subscribe_portfolio_request", "tr_subscribe_portfolio_response", "tr_portfolio_update", MessageService.getUniqueRequestData()) || this;
        }
        TRSubscribePortfolioRequest.prototype.createUnsubscribeRequest = function () {
            var req = new TRUnsubscribePortfolioRequest();
            req.portfolios = this.portfolios;
            return req;
        };
        return TRSubscribePortfolioRequest;
    }(TradingSubscriptionRequest));
    Infront.TRSubscribePortfolioRequest = TRSubscribePortfolioRequest;
    var TRUnsubscribePortfolioRequest = (function (_super) {
        __extends(TRUnsubscribePortfolioRequest, _super);
        function TRUnsubscribePortfolioRequest() {
            return _super.call(this, "tr_unsubscribe_portfolio_request", "tr_unsubscribe_portfolio_response") || this;
        }
        return TRUnsubscribePortfolioRequest;
    }(TradingRequest));
    Infront.TRUnsubscribePortfolioRequest = TRUnsubscribePortfolioRequest;
    var TRGetPortfolioRequest = (function (_super) {
        __extends(TRGetPortfolioRequest, _super);
        function TRGetPortfolioRequest() {
            return _super.call(this, "tr_get_portfolio_request", "tr_get_portfolio_response") || this;
        }
        return TRGetPortfolioRequest;
    }(TradingRequest));
    Infront.TRGetPortfolioRequest = TRGetPortfolioRequest;
    var TRSubsribeOrdersRequest = (function (_super) {
        __extends(TRSubsribeOrdersRequest, _super);
        function TRSubsribeOrdersRequest() {
            return _super.call(this, "tr_subscribe_orders_request", "tr_subscribe_orders_response", "tr_order_update", MessageService.getUniqueRequestData()) || this;
        }
        TRSubsribeOrdersRequest.prototype.createUnsubscribeRequest = function () {
            var req = new TRUnsubscribeOrdersRequest();
            req.portfolios = this.portfolios;
            return req;
        };
        return TRSubsribeOrdersRequest;
    }(TradingSubscriptionRequest));
    Infront.TRSubsribeOrdersRequest = TRSubsribeOrdersRequest;
    var TRUnsubscribeOrdersRequest = (function (_super) {
        __extends(TRUnsubscribeOrdersRequest, _super);
        function TRUnsubscribeOrdersRequest() {
            return _super.call(this, "tr_unsubscribe_orders_request", "tr_unsubscribe_orders_response") || this;
        }
        return TRUnsubscribeOrdersRequest;
    }(TradingRequest));
    Infront.TRUnsubscribeOrdersRequest = TRUnsubscribeOrdersRequest;
    var TRGetOrdersRequest = (function (_super) {
        __extends(TRGetOrdersRequest, _super);
        function TRGetOrdersRequest() {
            return _super.call(this, "tr_get_orders_request", "tr_get_orders_response") || this;
        }
        return TRGetOrdersRequest;
    }(TradingRequest));
    Infront.TRGetOrdersRequest = TRGetOrdersRequest;
    var TRSubscribeTradesRequest = (function (_super) {
        __extends(TRSubscribeTradesRequest, _super);
        function TRSubscribeTradesRequest() {
            return _super.call(this, "tr_subscribe_trades_request", "tr_subscribe_trades_response", "tr_trade_update", MessageService.getUniqueRequestData()) || this;
        }
        TRSubscribeTradesRequest.prototype.createUnsubscribeRequest = function () {
            return new TRUnsubscribeTradesRequest(this.portfolios);
        };
        return TRSubscribeTradesRequest;
    }(TradingSubscriptionRequest));
    Infront.TRSubscribeTradesRequest = TRSubscribeTradesRequest;
    var TRUnsubscribeTradesRequest = (function (_super) {
        __extends(TRUnsubscribeTradesRequest, _super);
        function TRUnsubscribeTradesRequest(portfolios) {
            var _this = _super.call(this, "tr_unsubscribe_trades_request", "tr_unsubscribe_trades_response") || this;
            _this.portfolios = portfolios;
            return _this;
        }
        return TRUnsubscribeTradesRequest;
    }(TradingRequest));
    Infront.TRUnsubscribeTradesRequest = TRUnsubscribeTradesRequest;
    var TRGetTradesRequest = (function (_super) {
        __extends(TRGetTradesRequest, _super);
        function TRGetTradesRequest() {
            return _super.call(this, "tr_get_trades_request", "tr_get_trades_response") || this;
        }
        return TRGetTradesRequest;
    }(TradingRequest));
    Infront.TRGetTradesRequest = TRGetTradesRequest;
    var TRSubscribeNetTradesRequest = (function (_super) {
        __extends(TRSubscribeNetTradesRequest, _super);
        function TRSubscribeNetTradesRequest() {
            return _super.call(this, "tr_subscribe_net_trades_request", "tr_subscribe_net_trades_response", "tr_net_trade_update", MessageService.getUniqueRequestData()) || this;
        }
        TRSubscribeNetTradesRequest.prototype.createUnsubscribeRequest = function () {
            return new TRUnsubscribeNetTradesRequest(this.portfolios);
        };
        return TRSubscribeNetTradesRequest;
    }(TradingSubscriptionRequest));
    Infront.TRSubscribeNetTradesRequest = TRSubscribeNetTradesRequest;
    var TRUnsubscribeNetTradesRequest = (function (_super) {
        __extends(TRUnsubscribeNetTradesRequest, _super);
        function TRUnsubscribeNetTradesRequest(portfolios) {
            var _this = _super.call(this, "tr_unsubscribe_net_trades_request", "tr_unsubscribe_net_trades_response") || this;
            _this.portfolios = portfolios;
            return _this;
        }
        return TRUnsubscribeNetTradesRequest;
    }(TradingRequest));
    Infront.TRUnsubscribeNetTradesRequest = TRUnsubscribeNetTradesRequest;
    var TRGetNetTradesRequest = (function (_super) {
        __extends(TRGetNetTradesRequest, _super);
        function TRGetNetTradesRequest() {
            return _super.call(this, "tr_get_net_trades_request", "tr_get_net_trades_response") || this;
        }
        return TRGetNetTradesRequest;
    }(TradingRequest));
    Infront.TRGetNetTradesRequest = TRGetNetTradesRequest;
    var TRInsertOrderRequest = (function (_super) {
        __extends(TRInsertOrderRequest, _super);
        function TRInsertOrderRequest() {
            return _super.call(this, "tr_insert_order_request", "tr_insert_order_response") || this;
        }
        return TRInsertOrderRequest;
    }(TradingRequest));
    Infront.TRInsertOrderRequest = TRInsertOrderRequest;
    var TRDeleteOrderRequest = (function (_super) {
        __extends(TRDeleteOrderRequest, _super);
        function TRDeleteOrderRequest() {
            return _super.call(this, "tr_delete_order_request", "tr_delete_order_response") || this;
        }
        return TRDeleteOrderRequest;
    }(TradingRequest));
    Infront.TRDeleteOrderRequest = TRDeleteOrderRequest;
    var TRModifyOrderRequest = (function (_super) {
        __extends(TRModifyOrderRequest, _super);
        function TRModifyOrderRequest() {
            return _super.call(this, "tr_modify_order_request", "tr_modify_order_response") || this;
        }
        return TRModifyOrderRequest;
    }(TradingRequest));
    Infront.TRModifyOrderRequest = TRModifyOrderRequest;
    var TRGetAlgosRequest = (function (_super) {
        __extends(TRGetAlgosRequest, _super);
        function TRGetAlgosRequest() {
            return _super.call(this, "tr_get_algos_request", "tr_get_algos_response") || this;
        }
        return TRGetAlgosRequest;
    }(TradingRequest));
    Infront.TRGetAlgosRequest = TRGetAlgosRequest;
    var TRActivateOrderRequest = (function (_super) {
        __extends(TRActivateOrderRequest, _super);
        function TRActivateOrderRequest() {
            return _super.call(this, "tr_activate_order_request", "tr_activate_order_response") || this;
        }
        return TRActivateOrderRequest;
    }(TradingRequest));
    Infront.TRActivateOrderRequest = TRActivateOrderRequest;
    var TRDeactivateOrderRequest = (function (_super) {
        __extends(TRDeactivateOrderRequest, _super);
        function TRDeactivateOrderRequest() {
            return _super.call(this, "tr_deactivate_order_request", "tr_deactivate_order_response") || this;
        }
        return TRDeactivateOrderRequest;
    }(TradingRequest));
    Infront.TRDeactivateOrderRequest = TRDeactivateOrderRequest;
    var VisualLoginRequest = (function (_super) {
        __extends(VisualLoginRequest, _super);
        function VisualLoginRequest() {
            return _super.call(this, "vs_login_request", "vs_login_response", MessageType.Connect) || this;
        }
        return VisualLoginRequest;
    }(GenericRequest));
    Infront.VisualLoginRequest = VisualLoginRequest;
    var ComponentRequest = (function (_super) {
        __extends(ComponentRequest, _super);
        function ComponentRequest() {
            return _super.call(this, "component", "component_response", MessageType.InfinData) || this;
        }
        return ComponentRequest;
    }(GenericRequest));
    Infront.ComponentRequest = ComponentRequest;
    var ScreenerRequest = (function (_super) {
        __extends(ScreenerRequest, _super);
        function ScreenerRequest() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ScreenerRequest;
    }(ComponentRequest));
    Infront.ScreenerRequest = ScreenerRequest;
    var EurofinRequest = (function (_super) {
        __extends(EurofinRequest, _super);
        function EurofinRequest() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return EurofinRequest;
    }(ComponentRequest));
    Infront.EurofinRequest = EurofinRequest;
    var UniverseRequest = (function (_super) {
        __extends(UniverseRequest, _super);
        function UniverseRequest() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return UniverseRequest;
    }(ComponentRequest));
    Infront.UniverseRequest = UniverseRequest;
    var ChartRequest = (function (_super) {
        __extends(ChartRequest, _super);
        function ChartRequest() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ChartRequest;
    }(ComponentRequest));
    Infront.ChartRequest = ChartRequest;
    var EvolutionChartRequest = (function (_super) {
        __extends(EvolutionChartRequest, _super);
        function EvolutionChartRequest() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return EvolutionChartRequest;
    }(ComponentRequest));
    Infront.EvolutionChartRequest = EvolutionChartRequest;
    var EtaRequest = (function (_super) {
        __extends(EtaRequest, _super);
        function EtaRequest() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return EtaRequest;
    }(ComponentRequest));
    Infront.EtaRequest = EtaRequest;
    var EstimatesRequest = (function (_super) {
        __extends(EstimatesRequest, _super);
        function EstimatesRequest() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return EstimatesRequest;
    }(ComponentRequest));
    Infront.EstimatesRequest = EstimatesRequest;
    var SearchRequest = (function (_super) {
        __extends(SearchRequest, _super);
        function SearchRequest() {
            return _super.call(this, "search_request", "search_response", MessageType.InfinData) || this;
        }
        return SearchRequest;
    }(GenericRequest));
    Infront.SearchRequest = SearchRequest;
    var FundamentalsRequest = (function (_super) {
        __extends(FundamentalsRequest, _super);
        function FundamentalsRequest() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return FundamentalsRequest;
    }(ComponentRequest));
    Infront.FundamentalsRequest = FundamentalsRequest;
    var FundDetailsRequest = (function (_super) {
        __extends(FundDetailsRequest, _super);
        function FundDetailsRequest() {
            return _super.call(this, "md_get_fund_details_request", "md_get_fund_details_response", MessageType.MarketData) || this;
        }
        return FundDetailsRequest;
    }(GenericRequest));
    Infront.FundDetailsRequest = FundDetailsRequest;
})(Infront || (Infront = {}));
///<reference path='Transport.ts' />
///<reference path="Util.ts" />
///<reference path="Messaging.ts" />
///<reference path="LibraryConstants.ts" />
var Infront;
///<reference path='Transport.ts' />
///<reference path="Util.ts" />
///<reference path="Messaging.ts" />
///<reference path="LibraryConstants.ts" />
(function (Infront) {
    var EventCallbackMap = (function () {
        function EventCallbackMap() {
            this.map = {};
        }
        EventCallbackMap.prototype.registerEventCallback = function (eventName, callback) {
            if (!(eventName in this.map)) {
                this.map[eventName] = [];
            }
            this.map[eventName].push(callback);
        };
        EventCallbackMap.prototype.unregisterEventCallback = function (eventName, callback) {
            if (eventName in this.map) {
                for (var i = 0; i < this.map[eventName].length; i++) {
                    if (this.map[eventName][i] === callback) {
                        this.map[eventName].splice(i, 1);
                        break;
                    }
                }
            }
        };
        EventCallbackMap.prototype.publish = function (event) {
            if (this.map.hasOwnProperty(event.name)) {
                var callbacks = this.map[event.name];
                for (var i = 0; i < callbacks.length; i++) {
                    callbacks[i](event);
                }
            }
        };
        return EventCallbackMap;
    }());
    Infront.EventCallbackMap = EventCallbackMap;
})(Infront || (Infront = {}));
if (typeof (Infront.EventCallbackMap.eventMap) == "undefined") {
    Infront.EventCallbackMap.eventMap = new Infront.EventCallbackMap();
}
(function (Infront) {
    var MWSConstants = Infront.MWSConstants;
    var InfrontStatus;
    (function (InfrontStatus) {
        InfrontStatus[InfrontStatus["Uninitialized"] = 0] = "Uninitialized";
        InfrontStatus[InfrontStatus["Connecting"] = 1] = "Connecting";
        InfrontStatus[InfrontStatus["Connected"] = 2] = "Connected";
        InfrontStatus[InfrontStatus["Disconnected"] = 3] = "Disconnected";
    })(InfrontStatus = Infront.InfrontStatus || (Infront.InfrontStatus = {}));
    var AuthenticationType;
    (function (AuthenticationType) {
        AuthenticationType[AuthenticationType["Username"] = 0] = "Username";
        AuthenticationType[AuthenticationType["OAUTH"] = 1] = "OAUTH";
        AuthenticationType[AuthenticationType["ExistingSession"] = 2] = "ExistingSession";
        AuthenticationType[AuthenticationType["JWT"] = 3] = "JWT";
        AuthenticationType[AuthenticationType["NONE"] = 4] = "NONE";
    })(AuthenticationType = Infront.AuthenticationType || (Infront.AuthenticationType = {}));
    var TradingLoginModes;
    (function (TradingLoginModes) {
        TradingLoginModes[TradingLoginModes["USER_PASS"] = 0] = "USER_PASS";
        TradingLoginModes[TradingLoginModes["JWT_AUTO"] = 1] = "JWT_AUTO";
        TradingLoginModes[TradingLoginModes["IDP_SUPER_USER"] = 2] = "IDP_SUPER_USER";
        TradingLoginModes[TradingLoginModes["IDP_AUTO"] = 3] = "IDP_AUTO";
    })(TradingLoginModes = Infront.TradingLoginModes || (Infront.TradingLoginModes = {}));
    var ConnectionSecurity;
    (function (ConnectionSecurity) {
        ConnectionSecurity[ConnectionSecurity["Require"] = 0] = "Require";
        ConnectionSecurity[ConnectionSecurity["Prefer"] = 1] = "Prefer";
        ConnectionSecurity[ConnectionSecurity["Unimportant"] = 2] = "Unimportant";
    })(ConnectionSecurity = Infront.ConnectionSecurity || (Infront.ConnectionSecurity = {}));
    var TradingLoginOptions = (function () {
        function TradingLoginOptions() {
        }
        return TradingLoginOptions;
    }());
    Infront.TradingLoginOptions = TradingLoginOptions;
    var LoginOptions = (function () {
        function LoginOptions() {
        }
        LoginOptions.prototype.getAuthenticationType = function () {
            if (typeof (this.user_id) != "undefined") {
                return AuthenticationType.Username;
            }
            else if (typeof (this.auth_token) != "undefined") {
                return AuthenticationType.OAUTH;
            }
            else if (typeof (this.session_token) != "undefined" && typeof (this.session_urls) != "undefined") {
                return AuthenticationType.ExistingSession;
            }
            else if (typeof (this.signed_token) != "undefined") {
                return AuthenticationType.JWT;
            }
            return AuthenticationType.NONE;
        };
        return LoginOptions;
    }());
    Infront.LoginOptions = LoginOptions;
    var ControllerOptions = (function (_super) {
        __extends(ControllerOptions, _super);
        function ControllerOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ControllerOptions;
    }(LoginOptions));
    Infront.ControllerOptions = ControllerOptions;
    var InfrontEvent = (function () {
        function InfrontEvent() {
        }
        return InfrontEvent;
    }());
    Infront.InfrontEvent = InfrontEvent;
    /**
     * We send this when the library is logged in and has a valid marketdata-session.
     */
    var ReadyEvent = (function (_super) {
        __extends(ReadyEvent, _super);
        function ReadyEvent() {
            var _this = _super.call(this) || this;
            _this.name = ReadyEvent.kEventName;
            return _this;
        }
        ReadyEvent.kEventName = "onReady";
        return ReadyEvent;
    }(InfrontEvent));
    Infront.ReadyEvent = ReadyEvent;
    /**
     * We send this when the library is logged in into visual application and has a valid visual-session.
     */
    var VisualReadyEvent = (function (_super) {
        __extends(VisualReadyEvent, _super);
        function VisualReadyEvent() {
            var _this = _super.call(this) || this;
            _this.name = VisualReadyEvent.kEventName;
            return _this;
        }
        VisualReadyEvent.kEventName = "vsOnReady";
        return VisualReadyEvent;
    }(InfrontEvent));
    Infront.VisualReadyEvent = VisualReadyEvent;
    /**
     * We send this when we disconnect or fail to log in or getServiceRoutings.
     */
    var DisconnectEventReason;
    (function (DisconnectEventReason) {
        DisconnectEventReason[DisconnectEventReason["KICKOUT"] = 0] = "KICKOUT";
        DisconnectEventReason[DisconnectEventReason["DISCONNECT"] = 1] = "DISCONNECT";
        DisconnectEventReason[DisconnectEventReason["UNKNOWN"] = 2] = "UNKNOWN";
    })(DisconnectEventReason = Infront.DisconnectEventReason || (Infront.DisconnectEventReason = {}));
    var DisconnectEvent = (function (_super) {
        __extends(DisconnectEvent, _super);
        function DisconnectEvent() {
            var _this = _super.call(this) || this;
            _this.reason = DisconnectEventReason.UNKNOWN;
            _this.name = DisconnectEvent.kEventName;
            return _this;
        }
        DisconnectEvent.kEventName = "onDisconnect";
        return DisconnectEvent;
    }(InfrontEvent));
    Infront.DisconnectEvent = DisconnectEvent;
    /**
     * We send this when connection succeeds but username or password is wrong.
     */
    var LoginFailedEvent = (function (_super) {
        __extends(LoginFailedEvent, _super);
        function LoginFailedEvent() {
            var _this = _super.call(this) || this;
            _this.name = LoginFailedEvent.kEventName;
            return _this;
        }
        LoginFailedEvent.kEventName = "onLoginFailed";
        return LoginFailedEvent;
    }(InfrontEvent));
    Infront.LoginFailedEvent = LoginFailedEvent;
    /**
     * We send this when connection succeeds but username or password is wrong in Visual !
     */
    var VisualLoginFailedEvent = (function (_super) {
        __extends(VisualLoginFailedEvent, _super);
        function VisualLoginFailedEvent() {
            var _this = _super.call(this) || this;
            _this.name = VisualLoginFailedEvent.kEventName;
            return _this;
        }
        VisualLoginFailedEvent.kEventName = "vsOnLoginFailed";
        return VisualLoginFailedEvent;
    }(InfrontEvent));
    Infront.VisualLoginFailedEvent = VisualLoginFailedEvent;
    var TradingConnectedEvent = (function (_super) {
        __extends(TradingConnectedEvent, _super);
        function TradingConnectedEvent(pid, sid) {
            var _this = _super.call(this) || this;
            _this.name = TradingConnectedEvent.kEventName;
            _this.pid = pid;
            _this.sid = sid;
            return _this;
        }
        TradingConnectedEvent.kEventName = "onTradingLogin";
        return TradingConnectedEvent;
    }(InfrontEvent));
    Infront.TradingConnectedEvent = TradingConnectedEvent;
    var TradingDisconnectedEvent = (function (_super) {
        __extends(TradingDisconnectedEvent, _super);
        function TradingDisconnectedEvent() {
            var _this = _super.call(this) || this;
            _this.name = TradingDisconnectedEvent.kEventName;
            return _this;
        }
        TradingDisconnectedEvent.kEventName = "onTradingDisconnect";
        return TradingDisconnectedEvent;
    }(InfrontEvent));
    Infront.TradingDisconnectedEvent = TradingDisconnectedEvent;
    var TradingReconnectingEvent = (function (_super) {
        __extends(TradingReconnectingEvent, _super);
        function TradingReconnectingEvent() {
            var _this = _super.call(this) || this;
            _this.name = TradingReconnectingEvent.kEventName;
            return _this;
        }
        TradingReconnectingEvent.kEventName = "onTradingReconnecting";
        return TradingReconnectingEvent;
    }(InfrontEvent));
    Infront.TradingReconnectingEvent = TradingReconnectingEvent;
    var TradingReconnectedEvent = (function (_super) {
        __extends(TradingReconnectedEvent, _super);
        function TradingReconnectedEvent() {
            var _this = _super.call(this) || this;
            _this.name = TradingReconnectedEvent.kEventName;
            return _this;
        }
        TradingReconnectedEvent.kEventName = "onTradingReconnected";
        return TradingReconnectedEvent;
    }(InfrontEvent));
    Infront.TradingReconnectedEvent = TradingReconnectedEvent;
    var TradingLoginFailedEvent = (function (_super) {
        __extends(TradingLoginFailedEvent, _super);
        function TradingLoginFailedEvent() {
            var _this = _super.call(this) || this;
            _this.name = TradingLoginFailedEvent.kEventName;
            return _this;
        }
        TradingLoginFailedEvent.kEventName = "onTradingLoginFailed";
        return TradingLoginFailedEvent;
    }(InfrontEvent));
    Infront.TradingLoginFailedEvent = TradingLoginFailedEvent;
    var TradingLoginCanceledEvent = (function (_super) {
        __extends(TradingLoginCanceledEvent, _super);
        function TradingLoginCanceledEvent() {
            var _this = _super.call(this) || this;
            _this.name = TradingLoginCanceledEvent.kEventName;
            return _this;
        }
        TradingLoginCanceledEvent.kEventName = "onTradingLoginCanceled";
        return TradingLoginCanceledEvent;
    }(InfrontEvent));
    Infront.TradingLoginCanceledEvent = TradingLoginCanceledEvent;
    /**
     * Stores data needed to recreate a session (session-token, url) in HTML5 sessionStorage.
     */
    var StoredMwsSession = (function () {
        function StoredMwsSession() {
            this.sessionToken = null;
            this.url = null;
            this.sessionTimeout = null;
            this.routingResponse = null;
            this.pid = null;
            this.sid = null;
            this.userId = null;
            this.password = null;
            this.features = [];
        }
        return StoredMwsSession;
    }());
    Infront.StoredMwsSession = StoredMwsSession;
    var LocallyStoredSession = (function () {
        //public sessionToken:string;
        //public url:string;
        //public sessionTimeout:number;
        //public routingResponse: Object;
        //public pid: number;
        //public sid: number;
        //public userId:string;
        //public password:string;
        //public features:string[];
        function LocallyStoredSession(name) {
            this.name = name;
            this.loaded = false;
            this.load();
            if (!this.loaded)
                this.values = new StoredMwsSession();
        }
        LocallyStoredSession.prototype.set = function (userId, password, sessionToken, url, sessionTimeout, routingResponse, features, pid, sid) {
            if (pid === void 0) { pid = -1; }
            if (sid === void 0) { sid = -1; }
            this.values.sessionToken = sessionToken;
            this.values.url = url;
            this.values.sessionTimeout = sessionTimeout;
            this.values.routingResponse = routingResponse;
            this.values.userId = userId;
            this.values.password = password;
            this.values.features = features;
            if (pid != -1)
                this.values.pid = pid;
            if (sid != -1)
                this.values.sid = sid;
            this.store();
            this.loaded = true;
        };
        LocallyStoredSession.prototype.clear = function () {
            this.values = new StoredMwsSession();
            //this.sessionToken = null;
            //this.sessionTimeout = null;
            //this.url = null;
            //this.routingResponse = null;
            this.loaded = false;
            //this.features = [];
            if (InfrontUtil.isLocalStorageSupported()) {
                window.sessionStorage.removeItem(this.key());
            }
        };
        LocallyStoredSession.prototype.store = function () {
            if (InfrontUtil.isLocalStorageSupported()) {
                //var obj = new Object();
                //obj[LocallyStoredSession.kSessionTokenKey] = this.sessionToken;
                //obj[LocallyStoredSession.kSessionTimeoutKey] = this.sessionTimeout;
                //obj[LocallyStoredSession.kUrlKey] = this.url;
                //obj[LocallyStoredSession.kRoutingResponseKey] = this.routingResponse;
                //obj[LocallyStoredSession.kUserIdKey] = this.userId;
                //obj[LocallyStoredSession.kPasswordKey] = this.password;
                //obj[LocallyStoredSession.kFeatures] = this.features;
                //var str = JSON.stringify(obj);
                var str = JSON.stringify(this.values);
                window.sessionStorage.setItem(this.key(), str);
            }
        };
        LocallyStoredSession.prototype.load = function () {
            if (InfrontUtil.isLocalStorageSupported()) {
                if (window.sessionStorage[this.key()]) {
                    var str = window.sessionStorage.getItem(this.key());
                    try {
                        this.values = JSON.parse(str);
                        this.loaded = true;
                    }
                    catch (error) {
                        this.loaded = false;
                    }
                    //var obj = JSON.parse(str);
                    //this.sessionToken = obj[LocallyStoredSession.kSessionTokenKey];
                    //this.sessionTimeout = obj[LocallyStoredSession.kSessionTimeoutKey];
                    //this.url = obj[LocallyStoredSession.kUrlKey];
                    //this.routingResponse = obj[LocallyStoredSession.kRoutingResponseKey];
                    //this.userId = obj[LocallyStoredSession.kUserIdKey];
                    //this.password = obj[LocallyStoredSession.kPasswordKey];
                    //this.features = obj[LocallyStoredSession.kFeatures];
                }
            }
        };
        LocallyStoredSession.prototype.key = function () {
            return LocallyStoredSession.kInfrontStoragePrefix + this.name;
        };
        LocallyStoredSession.kInfrontStoragePrefix = "com.goinfront.wtk.";
        return LocallyStoredSession;
    }());
    Infront.LocallyStoredSession = LocallyStoredSession;
    /**
     * The controller stores the state of the system, establishes and administrates sessions.
     */
    var Controller = (function () {
        function Controller() {
            this.status = InfrontStatus.Uninitialized;
            this.tradingStatus = InfrontStatus.Uninitialized;
            this.eventMap = Infront.EventCallbackMap.eventMap;
            this.storedMDSession = new LocallyStoredSession(Controller.kStoredMarketDataSessionName);
            this.storedVSSession = new LocallyStoredSession(Controller.kStoredVisualSessionName);
            this.storedTRSession = new LocallyStoredSession(Controller.kStoredTradingSessionName);
        }
        Controller.prototype.init = function (controllerOptions) {
            var _this = this;
            //General setup of the controller
            var defaults = new ControllerOptions();
            defaults.routingUrl = Infront.LibraryConstants.kRoutingUrl;
            defaults.streaming = false;
            defaults.store_session = false;
            defaults.client_application = Infront.LibraryConstants.kDefaultClientApplication;
            defaults.disableWebsocket = false;
            defaults.httpPoolSize = 4;
            defaults.keepSessionAlive = true;
            defaults.usingNodeJS = false;
            defaults.tradingOptions = null;
            defaults.infrontWidgetAccess = true;
            defaults.visualWidgetAccess = false;
            this.options = InfrontUtil.mergeRecursive(defaults, controllerOptions);
            this.userId = this.options.user_id;
            this.password = this.options.password;
            this.sidPidMatches = [];
            if (this.options.routing_response) {
                this.routingResponse = this.options.routing_response;
            }
            if (!this.options.disableWebsocket && Infront.WebsocketTransport.isWebsocketsSupported(this.options.usingNodeJS)) {
                if (this.options.secureConnection == ConnectionSecurity.Require) {
                    this.protocolPriorityList = ["wss://", "https://"];
                }
                else if (this.options.secureConnection == ConnectionSecurity.Prefer) {
                    this.protocolPriorityList = ["wss://", "ws://", "https://", "http://"];
                }
                else {
                    this.protocolPriorityList = ["ws://", "wss://", "http://", "https://"];
                }
            }
            else {
                if (this.options.secureConnection == ConnectionSecurity.Require) {
                    this.protocolPriorityList = ["https://"];
                }
                else if (this.options.secureConnection == ConnectionSecurity.Prefer) {
                    this.protocolPriorityList = ["https://", "http://"];
                }
                else {
                    this.protocolPriorityList = ["http://", "https://"];
                }
            }
            this.messageService = new Infront.MessageService(this, this.options.httpPoolSize, this.options.usingNodeJS);
            this.messageService.setRoutingUrl(this.options.routingUrl);
            this.messageService.setVisualConnectUrl(Infront.LibraryConstants.kVisualConnectUrl);
            this.messageService.setVisualUrl(Infront.LibraryConstants.kComponentUrl);
            this.messageService.setSessionTerminatedObserver(function (event) { return _this.sessionTerminated(event); });
            this.keepAliveSender = new KeepAliveSender(this.messageService, 60000, this, mdKeepAliveFactory);
            this.tradingKeepAliveSender = new KeepAliveSender(this.messageService, 60000, this, trKeepAliveFactory);
            if (!this.options.store_session) {
                if (this.options.infrontWidgetAccess) {
                    this.storedMDSession.clear();
                }
                if (this.options.visualWidgetAccess) {
                    this.storedVSSession.clear();
                }
            }
            //Check if we have an already established session in session-storage
            if (this.options.infrontWidgetAccess) {
                if (this.storedMDSession.loaded) {
                    InfrontUtil.debugLog("storedMDSession : ", this.storedMDSession);
                    this.setValuesAndDoMarketDataLogin(this.storedMDSession.values.userId, this.storedMDSession.values.password, this.storedMDSession.values.sessionToken, this.storedMDSession.values.url, this.storedMDSession.values.routingResponse);
                }
                else if (this.options.md_mws_url) {
                    this.setValuesAndDoMarketDataLogin(this.options.user_id, this.options.password, null, this.options.md_mws_url);
                }
                else {
                    //Get serviceroutings to start initialization
                    this.startNewSessionCreation();
                }
            }
            //Infinancials Access change routing url
            if (this.options.visualWidgetAccess) {
                InfrontUtil.debugLog("Visual Access change routing url");
                if (this.storedVSSession.loaded) {
                    this.setValuesAndDoVisualLogin(this.storedVSSession.values.userId, this.storedVSSession.values.password, this.storedVSSession.values.sessionToken, this.storedVSSession.values.routingResponse);
                }
                else {
                    this.startVisualNewSessionCreation();
                }
            }
        };
        Controller.prototype.logOut = function () {
            var _this = this;
            var req = new Infront.MarketDataLogoutRequest();
            var reqOpts = new Infront.RequestOptions();
            reqOpts.onError = function (error_code, error_message) {
                _this.messageService.disconnectMarketData();
                _this.storedMDSession.clear();
                _this.storedVSSession.clear();
            };
            reqOpts.onSuccess = function (result) {
                _this.messageService.disconnectMarketData();
                _this.storedMDSession.clear();
                _this.storedVSSession.clear();
            };
            this.messageService.sendRequest(req, reqOpts);
            this.keepAliveSender.stop();
            this.tradingLogout();
        };
        Controller.prototype.startNewSessionCreation = function () {
            var _this = this;
            this.messageService.setMarketDataSessionToken(null);
            this.messageService.setMarketDataUrl(null);
            if (this.options.getAuthenticationType() === AuthenticationType.NONE) {
                this.status = InfrontStatus.Disconnected;
                this.eventMap.publish(new LoginFailedEvent());
            }
            else if (this.options.getAuthenticationType() === AuthenticationType.Username
                || this.options.getAuthenticationType() === AuthenticationType.OAUTH
                || this.options.getAuthenticationType() == AuthenticationType.JWT) {
                var req = new Infront.GetServiceRoutingsRequest();
                req.client_application = this.options.client_application;
                req.client_application_version = Infront.LibraryConstants.kApplicationVersion;
                req.country_code = "no";
                req.language_code = "en";
                req.api_version = MWSConstants.api_version;
                if (this.options.getAuthenticationType() == AuthenticationType.Username) {
                    req.login_id = this.userId;
                    req.password = this.password;
                }
                else if (this.options.getAuthenticationType() == AuthenticationType.JWT) {
                    req.token_type = this.options.token_type;
                    req.signed_token = this.options.signed_token;
                }
                var reqOpts = new Infront.RequestOptions();
                reqOpts.onError = function (error_code, error_message) {
                    _this.status = InfrontStatus.Disconnected;
                    var event = null;
                    if (error_code === 403) {
                        event = new LoginFailedEvent();
                    }
                    else {
                        event = new DisconnectEvent();
                    }
                    event.message = error_message;
                    _this.eventMap.publish(event);
                };
                reqOpts.onSuccess = function (result) {
                    _this.routingResponse = result;
                    if (("md_routings" in result) && result["md_routings"].length > 0) {
                        var md_routings = result["md_routings"][0];
                        if ("nodes" in md_routings && md_routings["nodes"].length > 0) {
                            _this.nodes = md_routings["nodes"];
                            _this.marketDataLogin();
                            return;
                        }
                    }
                };
                this.messageService.sendRequest(req, reqOpts);
                this.status = InfrontStatus.Connecting;
            }
            else if (this.options.getAuthenticationType() == AuthenticationType.ExistingSession) {
                this.setValuesAndDoMarketDataLogin(this.options.session_user, this.options.session_pw, this.options.session_token, this.options.session_urls, this.options.routing_response);
            }
        };
        Controller.prototype.reconnectSession = function (sessionToken, sessionUrls, routingResponse) {
            //this.setValuesAndDoMarketDataLogin(null, null, sessionToken, sessionUrls, null, true);
            if (sessionToken)
                this.messageService.setMarketDataSessionToken(sessionToken);
            if (sessionUrls)
                this.setNodesToUrl(sessionUrls);
            if (routingResponse)
                this.routingResponse = routingResponse;
            var url = this.getNextMarketDataUrl();
            if (url != null) {
                this.messageService.reconnectMarketData(url);
            }
        };
        Controller.prototype.setValuesAndDoMarketDataLogin = function (userId, password, sessionToken, sessionUrl, routingResponse) {
            if (userId)
                this.userId = userId;
            if (password)
                this.password = password;
            if (sessionToken)
                this.messageService.setMarketDataSessionToken(sessionToken);
            if (sessionUrl)
                this.setNodesToUrl(sessionUrl);
            if (routingResponse)
                this.routingResponse = routingResponse;
            this.marketDataLogin();
        };
        Controller.prototype.setValuesAndDoVisualLogin = function (userId, password, sessionToken, routingResponse) {
            if (userId)
                this.userId = userId;
            if (password)
                this.password = password;
            if (sessionToken)
                this.messageService.setVisualSessionToken(sessionToken);
            if (routingResponse)
                this.routingResponse = routingResponse;
            this.visualLogin();
        };
        Controller.prototype.setNodesToUrl = function (url) {
            var urlArray = InfrontUtil.isString(url) ? [url] : url;
            this.nodes = [
                {
                    "connections": urlArray
                }
            ];
        };
        Controller.prototype.startVisualNewSessionCreation = function () {
            InfrontUtil.debugLog("[Controller]VisualAccess - startVisualNewSessionCreation");
            this.messageService.setVisualSessionToken(null);
            this.visualLogin();
        };
        Controller.prototype.visualLogin = function () {
            var _this = this;
            InfrontUtil.debugLog("[Controller]visualLogin function");
            if (this.options.getAuthenticationType() === AuthenticationType.NONE) {
                this.status = InfrontStatus.Disconnected;
                this.eventMap.publish(new LoginFailedEvent());
            }
            else if (this.options.getAuthenticationType() === AuthenticationType.Username
                || this.options.getAuthenticationType() === AuthenticationType.OAUTH
                || this.options.getAuthenticationType() == AuthenticationType.JWT) {
                var req = new Infront.VisualLoginRequest();
                if (this.options.getAuthenticationType() == AuthenticationType.Username) {
                    req.login_id = this.userId;
                    req.password = this.password;
                }
                else if (this.options.getAuthenticationType() == AuthenticationType.JWT) {
                    req.token_type = this.options.token_type;
                    req.signed_token = this.options.signed_token;
                }
                req.client_application = this.options.client_application;
                req.country_code = "fr";
                req.api_version = MWSConstants.api_version;
                var reqOpts = new Infront.RequestOptions();
                reqOpts.onError = function (error_code, error_message) {
                    _this.status = InfrontStatus.Disconnected;
                    var event = new VisualLoginFailedEvent();
                    event.message = error_message;
                    _this.eventMap.publish(event);
                };
                reqOpts.onSuccess = function (result) {
                    var sessionToken = result[MWSConstants.session_token];
                    var sessionTimeout = result.hasOwnProperty(MWSConstants.session_timeout) ? result[MWSConstants.session_timeout] : null;
                    InfrontUtil.debugLog("[Controller]visualLoginSuccess request.");
                    _this.visualLoginSuccess(sessionToken, sessionTimeout);
                };
                this.messageService.sendRequest(req, reqOpts);
            }
        };
        Controller.prototype.getNextMarketDataUrl = function () {
            var retVal = null;
            if (this.nodes.length > 0) {
                this.currentNode = this.nodes[0];
                this.nodes.splice(0, 1);
                retVal = this.pickPrioritizedUrl(this.currentNode["connections"]);
            }
            return retVal;
        };
        Controller.prototype.getNextTradingUrl = function () {
            var retVal = null;
            if (this.tradingNodes.length > 0) {
                this.currentTradingNode = this.tradingNodes[0];
                this.tradingNodes.splice(0, 1);
                retVal = this.pickPrioritizedUrl(this.currentTradingNode["connections"]);
            }
            return retVal;
        };
        Controller.prototype.marketDataLogin = function (onSuccess) {
            var _this = this;
            var url = this.getNextMarketDataUrl();
            if (url != null) {
                this.messageService.setMarketDataUrl(url);
                var reqOpts = new Infront.RequestOptions();
                reqOpts.onError = function (error_code, error_message) {
                    _this.storedMDSession.clear();
                    _this.marketDataLogin();
                };
                reqOpts.onSuccess = function (result) {
                    var sessionToken = result[MWSConstants.session_token];
                    var sessionTimeout = result.hasOwnProperty(MWSConstants.session_timeout) ? result[MWSConstants.session_timeout] : null;
                    _this.marketDataLoginSuccess(sessionToken, sessionTimeout);
                };
                this.sendMarketDataLogin(reqOpts);
            }
            else {
                this.status = InfrontStatus.Disconnected;
                var event = new LoginFailedEvent(); // new DisconnectEvent();
                this.eventMap.publish(event);
            }
        };
        Controller.prototype.sendMarketDataLogin = function (reqOpts) {
            var req = new Infront.MarketDataLoginRequest();
            if (this.options.getAuthenticationType() == AuthenticationType.Username) {
                req.login_id = this.userId;
                req.password = this.password;
            }
            else if (this.options.getAuthenticationType() == AuthenticationType.JWT) {
                req.token_type = this.options.token_type;
                req.signed_token = this.options.signed_token;
            }
            req.client_application = this.options.client_application;
            req.client_application_version = "3.0.1";
            req.country_code = "no";
            req.language_code = "en";
            req.api_version = MWSConstants.api_version;
            this.messageService.sendRequest(req, reqOpts);
        };
        Controller.prototype.marketDataLoginSuccess = function (sessionToken, sessionTimeout) {
            this.status = InfrontStatus.Connected;
            this.messageService.setMarketDataSessionToken(sessionToken);
            if (this.options.keepSessionAlive) {
                if (sessionTimeout) {
                    this.keepAliveSender.setInterval(sessionTimeout / 2);
                }
                this.keepAliveSender.start();
            }
            this.storedMDSession.set(this.userId, this.password, sessionToken, this.messageService.getMarketDataUrl(), sessionTimeout, this.routingResponse, []);
            this.establishTradingSession();
            var event = new ReadyEvent();
            event.sessionToken = sessionToken;
            event.urls = this.currentNode["connections"];
            this.eventMap.publish(event);
        };
        /**
         * Infinancials Login Success */
        Controller.prototype.visualLoginSuccess = function (sessionToken, sessionTimeout) {
            this.status = InfrontStatus.Connected;
            this.messageService.setVisualSessionToken(sessionToken);
            this.storedVSSession.set(this.userId, this.password, sessionToken, this.messageService.getVisualUrl(), sessionTimeout, this.routingResponse, []);
            var event = new VisualReadyEvent();
            event.sessionToken = sessionToken;
            this.eventMap.publish(event);
        };
        Controller.prototype.establishTradingSession = function () {
            var _this = this;
            var tradingLoginMode = this.getTradingLoginMode();
            if (tradingLoginMode == TradingLoginModes.JWT_AUTO) {
                this.storedTRSession.clear();
                this.loginWithTradingOptionsJWT();
            }
            else if (this.storedTRSession.loaded) {
                this.tradingStatus = InfrontStatus.Connecting;
                this.messageService.setTradingSessionToken(this.storedTRSession.values.sessionToken);
                this.messageService.setTradingUrl(this.storedTRSession.values.url);
                this.tradingKeepAliveSender.setInterval(this.storedTRSession.values.sessionTimeout / 2);
                var req = new Infront.TRKeepAliveRequest();
                var reqOpts = new Infront.RequestOptions();
                reqOpts.onError = function (error_code, error_message) {
                    _this.tradingStatus = InfrontStatus.Disconnected;
                    _this.eventMap.publish(new TradingLoginFailedEvent());
                    _this.tradingLoginFailed(0, null);
                    _this.storedTRSession.clear();
                };
                reqOpts.onSuccess = function (result) {
                    _this.tradingStatus = InfrontStatus.Connected;
                    _this.eventMap.publish(_this.tradingConnectedEventFromStorage());
                    _this.tradingKeepAliveSender.start();
                };
                this.messageService.sendRequest(req, reqOpts);
            }
            else if (tradingLoginMode == TradingLoginModes.IDP_AUTO) {
                this.loginWithTradingOptionsIDP();
            }
        };
        /**
         * Returns a TradingConnectedEvent with pid/sid from storedTRSession
         */
        Controller.prototype.tradingConnectedEventFromStorage = function () {
            return new TradingConnectedEvent(this.storedTRSession.values.pid, this.storedTRSession.values.sid);
        };
        /**
         * Log in to trading using only the pre-supplied options given on library construction.
         */
        Controller.prototype.loginWithTradingOptionsIDP = function () {
            var match = this.getSidPidMatches()[0];
            this.tradingNodes = match[MWSConstants.nodes];
            this.tradingEndpoint = {
                provider: match[MWSConstants.provider],
                service: match[MWSConstants.service]
            };
            this.tradingToken = this.options.tradingOptions.signed_token;
            this.tradingTokenType = this.options.tradingOptions.token_type;
            this.tradingStatus = InfrontStatus.Connecting;
            this.tradingLogin();
        };
        Controller.prototype.loginWithTradingOptionsJWT = function () {
            if (this.options.tradingOptions) {
                if (this.options.tradingOptions.md_user_id) {
                    this.storedMDSession.values.userId = this.options.tradingOptions.md_user_id;
                    this.storedMDSession.values.password = this.options.tradingOptions.md_password;
                }
                if (InfrontUtil.isArray(this.options.tradingOptions.url)) {
                    this.tradingNodes = [{ connections: this.options.tradingOptions.url }];
                }
                else {
                    this.tradingNodes = [{ connections: [this.options.tradingOptions.url] }];
                }
                if (this.options.tradingOptions.provider && this.options.tradingOptions.service) {
                    this.tradingEndpoint = {
                        provider: this.options.tradingOptions.provider,
                        service: this.options.tradingOptions.service
                    };
                }
                if (this.options.tradingOptions.user_id) {
                    this.tradingUserId = this.options.tradingOptions.user_id;
                    this.tradingPassword = this.options.tradingOptions.password;
                    this.tradingPin = this.options.tradingOptions.pin;
                }
                else {
                    this.tradingToken = this.options.tradingOptions.signed_token;
                    this.tradingTokenType = this.options.tradingOptions.token_type;
                }
                this.tradingStatus = InfrontStatus.Connecting;
                this.tradingLogin();
            }
        };
        Controller.prototype.startTradingLogin = function (provider, service, username, password, pin) {
            var _this = this;
            this.tradingToken = null;
            this.tradingTokenType = null;
            this.tradingEndpoint = this.findTradingService(provider, service);
            if (this.tradingEndpoint) {
                if (this.getTradingLoginMode() == TradingLoginModes.IDP_SUPER_USER) {
                    var sidPidMatches = this.getSidPidMatches();
                    var matchIndex = sidPidMatches.indexOf(this.tradingEndpoint);
                    if (matchIndex >= 0) {
                        this.tradingToken = this.options.tradingOptions.signed_token;
                        this.tradingTokenType = this.options.tradingOptions.token_type;
                    }
                    else {
                        this.tradingUserId = username;
                        this.tradingPassword = password;
                        this.tradingPin = pin;
                    }
                }
                else {
                    this.tradingUserId = username;
                    this.tradingPassword = password;
                    this.tradingPin = pin;
                }
                this.tradingNodes = [].concat(this.tradingEndpoint[MWSConstants.nodes]);
                this.tradingStatus = InfrontStatus.Connecting;
                this.tradingLogin();
            }
            else {
                InfrontUtil.setZeroTimeout(function () {
                    _this.tradingLoginFailed(0, null);
                });
            }
        };
        Controller.prototype.getTradableFeeds = function () {
            return this.tradingEndpoint && this.tradingEndpoint.hasOwnProperty(MWSConstants.tradable_feeds) ?
                this.tradingEndpoint[MWSConstants.tradable_feeds] : null;
        };
        Controller.prototype.getTradingRoutings = function () {
            return this.routingResponse && this.routingResponse.hasOwnProperty(MWSConstants.tr_routings) ? this.routingResponse[MWSConstants.tr_routings] : null;
        };
        Controller.prototype.getSidPidMatches = function () {
            if (this.sidPidMatches.length == 0) {
                this.generate_pid_sid_matches();
            }
            return this.sidPidMatches;
        };
        Controller.prototype.getTradingLoginMode = function () {
            if (!this.tradingLoginMode) {
                this.determineTradingLoginMode();
            }
            return this.tradingLoginMode;
        };
        Controller.prototype.determineTradingLoginMode = function () {
            var tradingLoginMode = TradingLoginModes.USER_PASS;
            var tradingOptions = this.options.tradingOptions;
            if (tradingOptions && tradingOptions.token_type == "JWT") {
                tradingLoginMode = TradingLoginModes.JWT_AUTO;
            }
            else if (tradingOptions && tradingOptions.token_type == "IDP") {
                if (this.getSidPidMatches().length == 1) {
                    tradingLoginMode = TradingLoginModes.IDP_AUTO;
                }
                else {
                    tradingLoginMode = TradingLoginModes.IDP_SUPER_USER;
                }
            }
            this.tradingLoginMode = tradingLoginMode;
        };
        Controller.prototype.generate_pid_sid_matches = function () {
            var tradingRouting = this.getTradingRoutings();
            if (this.options.tradingOptions) {
                var validSidsPids = this.options.tradingOptions.valid_sids_pids || [];
                for (var i = 0; i < validSidsPids.length; i++) {
                    for (var j = 0; j < tradingRouting.length; j++) {
                        if (tradingRouting[j][MWSConstants.provider] == validSidsPids[i].pid && tradingRouting[j][MWSConstants.service] == validSidsPids[i].sid) {
                            this.sidPidMatches.push(tradingRouting[j]);
                        }
                    }
                }
            }
        };
        Controller.prototype.findTradingService = function (provider, service) {
            var retVal = null;
            if (this.routingResponse && this.routingResponse.hasOwnProperty(MWSConstants.tr_routings)) {
                var routings = this.routingResponse[MWSConstants.tr_routings];
                for (var i = 0; i < routings.length; i++) {
                    if (routings[i][MWSConstants.provider] == provider && routings[i][MWSConstants.service] == service) {
                        retVal = routings[i];
                        break;
                    }
                }
            }
            return retVal;
        };
        Controller.prototype.tradingLogin = function () {
            var _this = this;
            var url = this.getNextTradingUrl();
            if (url != null) {
                this.messageService.setTradingUrl(url);
                var request = new Infront.TRLoginRequest();
                request.api_version = MWSConstants.api_version;
                request.client_application = this.options.client_application;
                request.client_application_version = "3.0.1";
                request.currency = this.options.baseCurrency;
                request.provider = this.tradingEndpoint["provider"];
                request.service = this.tradingEndpoint["service"];
                if (this.tradingToken) {
                    request.signed_token = this.tradingToken;
                    request.token_type = this.tradingTokenType;
                    if (this.tradingLoginMode == TradingLoginModes.IDP_AUTO || this.tradingLoginMode == TradingLoginModes.IDP_SUPER_USER) {
                        request.user_id = this.options.tradingOptions.user_id;
                    }
                }
                else {
                    if (this.storedMDSession.values.userId) {
                        request.ias_user_id = this.storedMDSession.values.userId;
                        request.ias_password = this.storedMDSession.values.password;
                    }
                    else if (this.options.signed_token) {
                        request.signed_token = this.options.signed_token;
                        request.token_type = "IDP";
                    }
                    request.user_id = this.tradingUserId;
                    request.password = this.tradingPassword;
                    request.pin = this.tradingPin;
                }
                var reqOpts = new Infront.RequestOptions();
                reqOpts.onSuccess = function (result) {
                    _this.tradingLoginSuccess(result, request);
                };
                reqOpts.onError = function (error_code, error_message) {
                    _this.tradingLoginMode = TradingLoginModes.USER_PASS;
                    _this.tradingStatus = InfrontStatus.Disconnected;
                    _this.getMessageService().setTradingSessionToken(null);
                    // TODO: check if username/password is incorrect
                    _this.tradingLoginFailed(error_code, error_message);
                    // attempt to login again using the same credentials (maybe on a different server)
                    //                    this.tradingLogin();
                };
                this.messageService.sendRequest(request, reqOpts);
            }
            else {
                this.tradingLoginFailed(0, null);
            }
        };
        Controller.prototype.tradingLoginFailed = function (error_code, error_message) {
            //Reset trading-nodes.
            if (this.tradingEndpoint && this.tradingEndpoint.hasOwnProperty(MWSConstants.nodes)) {
                this.tradingNodes = [].concat(this.tradingEndpoint[MWSConstants.nodes]);
            }
            var event = new TradingLoginFailedEvent();
            if (error_message) {
                event.message = error_message;
            }
            this.eventMap.publish(event);
        };
        Controller.prototype.tradingLoginSuccess = function (trLoginResponse, trLoginRequest) {
            var sessionToken = trLoginResponse[MWSConstants.session_token];
            var sessionTimeout = trLoginResponse.hasOwnProperty(MWSConstants.session_timeout) ? trLoginResponse[MWSConstants.session_timeout] : null;
            var tradingFeatures = trLoginResponse.hasOwnProperty(MWSConstants.features) ? trLoginResponse[MWSConstants.features] : [];
            var url = this.pickPrioritizedUrl(this.currentTradingNode["connections"]);
            this.storedTRSession.set(undefined, undefined, sessionToken, url, sessionTimeout, undefined, tradingFeatures, trLoginRequest.provider, trLoginRequest.service);
            this.tradingStatus = InfrontStatus.Connected;
            this.getMessageService().setTradingSessionToken(sessionToken);
            this.eventMap.publish(new TradingConnectedEvent(trLoginRequest.provider, trLoginRequest.service));
            if (sessionTimeout) {
                this.tradingKeepAliveSender.setInterval(sessionTimeout / 2);
            }
            this.tradingKeepAliveSender.start();
        };
        Controller.prototype.tradingLogout = function () {
            var _this = this;
            var msg = new Infront.TRLogoutRequest();
            var reqOpts = new Infront.RequestOptions();
            reqOpts.onError = function (error_code, error_Message) {
                _this.messageService.disconnectTrading();
                _this.storedTRSession.clear();
            };
            reqOpts.onSuccess = function (data) {
                _this.messageService.disconnectTrading();
                _this.storedTRSession.clear();
            };
            this.messageService.prepareTradingLogout();
            this.messageService.sendRequest(msg, reqOpts);
            this.tradingKeepAliveSender.stop();
        };
        Controller.prototype.getTradingFeatures = function () {
            return this.storedTRSession.values.features;
        };
        Controller.prototype.doDisconnect = function (reason, message) {
            if (this.status != InfrontStatus.Disconnected) {
                this.status = InfrontStatus.Disconnected;
                this.keepAliveSender.stop();
                var evt = new DisconnectEvent();
                evt.message = message;
                evt.reason = reason;
                this.eventMap.publish(evt);
            }
        };
        Controller.prototype.sessionTerminated = function (event) {
            this.doDisconnect(event[MWSConstants.termination_code] == Infront.TerminationCodes.kickout ? DisconnectEventReason.KICKOUT : DisconnectEventReason.UNKNOWN, event[MWSConstants.termination_reason]);
        };
        Controller.prototype.marketDataDisconnected = function () {
            this.doDisconnect(DisconnectEventReason.DISCONNECT, null);
        };
        Controller.prototype.tradingDisconnected = function () {
            this.tradingStatus = InfrontStatus.Disconnected;
            this.tradingKeepAliveSender.stop();
            this.storedTRSession.clear();
            this.eventMap.publish(new TradingDisconnectedEvent());
        };
        Controller.prototype.tradingReconnecting = function () {
            this.tradingStatus = InfrontStatus.Disconnected;
            this.eventMap.publish(new TradingReconnectingEvent());
        };
        Controller.prototype.tradingReconnected = function () {
            this.tradingStatus = InfrontStatus.Connected;
            this.eventMap.publish(new TradingReconnectedEvent());
        };
        Controller.prototype.registerEventObserver = function (eventName, callback) {
            this.eventMap.registerEventCallback(eventName, callback);
        };
        Controller.prototype.unregisterEventObserver = function (eventName, callback) {
            this.eventMap.unregisterEventCallback(eventName, callback);
        };
        Controller.prototype.getMessageService = function () {
            return this.messageService;
        };
        Controller.prototype.getStatus = function () {
            return this.status;
        };
        Controller.prototype.getTradingStatus = function () {
            return this.tradingStatus;
        };
        Controller.prototype.getSession = function () {
            return this.storedMDSession;
        };
        Controller.prototype.isStreaming = function () {
            return this.options.streaming && this.messageService != null ? this.messageService.supportsStreaming(Infront.MessageType.MarketData) : false;
        };
        Controller.prototype.isTradingStreaming = function () {
            return this.messageService.supportsStreaming(Infront.MessageType.Trading);
        };
        Controller.prototype.keepAliveFailed = function (error_code, error_message) {
            this.status = InfrontStatus.Disconnected;
            this.keepAliveSender.stop();
            this.tradingKeepAliveSender.stop();
            //console.log("KeepAlive failed: " + error_code + ", " + error_message);
        };
        Controller.prototype.pickPrioritizedUrl = function (urls) {
            for (var i = 0; i < this.protocolPriorityList.length; i++) {
                for (var j = 0; j < urls.length; j++) {
                    if (urls[j].indexOf(this.protocolPriorityList[i]) === 0) {
                        return urls[j];
                    }
                }
            }
            return null;
        };
        Controller.prototype.publishEvent = function (evt) {
            this.eventMap.publish(evt);
        };
        Controller.kStoredMarketDataSessionName = "marketDataSession";
        Controller.kStoredVisualSessionName = "visualDataSession";
        Controller.kStoredTradingSessionName = "tradingSession";
        return Controller;
    }());
    Infront.Controller = Controller;
    var KeepAliveSender = (function () {
        function KeepAliveSender(messageService, interval, observer, messageFactory) {
            this.messageService = messageService;
            this.interval = interval;
            this.running = false;
            this.observer = observer;
            this.messageFactory = messageFactory;
        }
        KeepAliveSender.prototype.start = function () {
            var _this = this;
            if (!this.running) {
                this.handle = setInterval(function () { _this.sendKeepAliveMessage(); }, this.interval);
                this.running = true;
            }
        };
        KeepAliveSender.prototype.stop = function () {
            if (this.running) {
                clearInterval(this.handle);
                this.running = false;
            }
        };
        KeepAliveSender.prototype.setInterval = function (interval) {
            this.interval = interval;
            if (this.running) {
                this.stop();
                this.start();
            }
        };
        KeepAliveSender.prototype.sendKeepAliveMessage = function () {
            var _this = this;
            var req = this.messageFactory();
            var reqOpts = new Infront.RequestOptions();
            reqOpts.onError = function (error_code, error_message) {
                _this.observer.keepAliveFailed(error_code, error_message);
            };
            reqOpts.onSuccess = function (result) { };
            this.messageService.sendRequest(req, reqOpts);
        };
        return KeepAliveSender;
    }());
    function mdKeepAliveFactory() {
        return new Infront.MarketDataKeepAliveRequest();
    }
    function trKeepAliveFactory() {
        return new Infront.TRKeepAliveRequest();
    }
})(Infront || (Infront = {}));
/**
 * Created by hage on 30.01.2015.
 */
var Infront;
/**
 * Created by hage on 30.01.2015.
 */
(function (Infront) {
    var CurrencyConverter = (function () {
        function CurrencyConverter(infront, streaming) {
            this.infront = infront;
            this.streaming = streaming;
            this.cacheKey = InfrontUtil.makeUUID();
            this.loadedCurrencies = [];
        }
        CurrencyConverter.prototype.createConvertedFieldFromCache = function (field, fromCurrencyRef, toCurrency, targetFieldRef) {
            var _this = this;
            if (targetFieldRef === void 0) { targetFieldRef = null; }
            var fieldReference = targetFieldRef == null ? new Infront.FieldReference(this.cacheKey, InfrontUtil.makeUUID()) : targetFieldRef;
            this.infront.getCache().asyncGet(function (val) {
                _this.doCreateConvertedField(fieldReference, field, val, toCurrency);
            }, fromCurrencyRef);
            return fieldReference;
        };
        CurrencyConverter.prototype.doCreateConvertedField = function (targetReference, field, fromCurrency, toCurrency) {
            if (fromCurrency.toUpperCase() != toCurrency.toUpperCase()) {
                //GBX GBP in Pence, that is 1 GBP = 100 GBX
                //This is not handled by the server.
                var factor = 1;
                if (fromCurrency == "GBX") {
                    fromCurrency = "GBP";
                    factor = 0.01;
                }
                else if (toCurrency == "GBX") {
                    toCurrency = "GBP";
                    factor = 100;
                }
                var fromInstr = fromCurrency == CurrencyConverter.kUSD ? null : new Infront.Instrument(CurrencyConverter.kWFXFeed, CurrencyConverter.kUSD + fromCurrency);
                var toInstr = toCurrency == CurrencyConverter.kUSD ? null : new Infront.Instrument(CurrencyConverter.kWFXFeed, CurrencyConverter.kUSD + toCurrency);
                var fromRef = null;
                var toRef = null;
                if (fromInstr) {
                    this.loadCurrency(fromInstr);
                    fromRef = new Infront.FieldReference(Infront.CacheKeyFactory.createInstrumentKey(fromInstr), "last_valid");
                }
                if (toInstr) {
                    this.loadCurrency(toInstr);
                    toRef = new Infront.FieldReference(Infront.CacheKeyFactory.createInstrumentKey(toInstr), "last_valid");
                }
                if (fromInstr && toInstr) {
                    /**
                     * Generic conversion-function.
                     * args:
                     * 0 - value to be converted.
                     * 1 - USD[FROM] rate
                     * 2 - USD[TO] rate
                     */
                    this.infront.getCache().createComputedField(targetReference.cacheKey, targetReference.fieldSpec, [field, fromRef, toRef], function (args) {
                        if (InfrontUtil.checkArrayItems(args, InfrontUtil.isNumber)) {
                            return (args[0] / args[1]) * args[2] * factor;
                        }
                        else {
                            return "-";
                        }
                    });
                }
                else if (fromInstr) {
                    /**
                     * Converts directly from one currency to another.
                     * Divides by rate.
                     * args:
                     * 0 - value to be converted
                     * 1 - rate.
                     */
                    this.infront.getCache().createComputedField(targetReference.cacheKey, targetReference.fieldSpec, [field, fromRef], function (args) {
                        if (InfrontUtil.noElementsUndefined(args)) {
                            return (args[0] / args[1]) * factor;
                        }
                        else {
                            return "-";
                        }
                    });
                }
                else {
                    /**
                     * Converts directly from one currency to another.
                     * Multiplies by rate.
                     * args:
                     * 0 - value to be converted
                     * 1 - rate.
                     */
                    this.infront.getCache().createComputedField(targetReference.cacheKey, targetReference.fieldSpec, [field, toRef], function (args) {
                        if (InfrontUtil.noElementsUndefined(args)) {
                            return args[0] * args[1] * factor;
                        }
                        else {
                            return "-";
                        }
                    });
                }
            }
            else {
                this.infront.getCache().createComputedField(targetReference.cacheKey, targetReference.fieldSpec, [field], function (args) {
                    return args[0];
                });
            }
        };
        CurrencyConverter.prototype.loadCurrency = function (instrument) {
            if (this.loadedCurrencies.indexOf(instrument.ticker) == -1) {
                if (this.streaming) {
                    this.infront.subscribe(instrument, ["LAST_VALID"]);
                }
                else {
                    this.infront.snapshot(instrument, ["LAST_VALID"]);
                }
                this.loadedCurrencies.push(instrument.ticker);
            }
        };
        CurrencyConverter.kUSD = "USD";
        CurrencyConverter.kWFXFeed = 12;
        return CurrencyConverter;
    }());
    Infront.CurrencyConverter = CurrencyConverter;
})(Infront || (Infront = {}));
/**
 * Cache for storing data that can be observed by the UI.
 *
 * This cache stores (potentially partial) COPIES of the object that is passed in, and allows the UI to bind to it's
 * contents. This is an efficient way of updating the UI to updates of streaming data.
 *
 * Terms:
 * KEY: Key identifying the root object in the cache map.
 * FIELD: Key identifying a field in an object contained in the cache map. Possibly in a nested Object, specified by
 * supplying an array to the field-parameter.
 */
var Infront;
/**
 * Cache for storing data that can be observed by the UI.
 *
 * This cache stores (potentially partial) COPIES of the object that is passed in, and allows the UI to bind to it's
 * contents. This is an efficient way of updating the UI to updates of streaming data.
 *
 * Terms:
 * KEY: Key identifying the root object in the cache map.
 * FIELD: Key identifying a field in an object contained in the cache map. Possibly in a nested Object, specified by
 * supplying an array to the field-parameter.
 */
(function (Infront) {
    var ComputationStrategy;
    (function (ComputationStrategy) {
        ComputationStrategy[ComputationStrategy["PASSIVE"] = 0] = "PASSIVE";
        ComputationStrategy[ComputationStrategy["AGGRESSIVE"] = 1] = "AGGRESSIVE";
        ComputationStrategy[ComputationStrategy["FIRST_RESULT"] = 2] = "FIRST_RESULT";
    })(ComputationStrategy = Infront.ComputationStrategy || (Infront.ComputationStrategy = {}));
    /**
     * A reference to a value in the cache consists of two things:
     * - A cache-key (string)
     * - A field-spec (a string or array of strings)
     */
    var FieldReference = (function () {
        function FieldReference(key, spec) {
            this.cacheKey = key;
            this.fieldSpec = spec;
        }
        FieldReference.prototype.getSpecAsArray = function () {
            return InfrontUtil.isArray(this.fieldSpec) ? this.fieldSpec : [this.fieldSpec];
        };
        FieldReference.prototype.toString = function () {
            return this.cacheKey + "/" + this.getSpecAsArray().reduce(function (prev, val, idx, array) { return prev + "/" + val; }, "");
        };
        return FieldReference;
    }());
    Infront.FieldReference = FieldReference;
    var CachedArrayContainer = (function () {
        function CachedArrayContainer() {
        }
        return CachedArrayContainer;
    }());
    var BindingCache = (function () {
        function BindingCache() {
            //this.cachedObjects = new Object();
            this.cachedObservables = {};
            this.cachedArrays = {};
            this.unboundObservers = {};
            this.unboundArrayObservers = {};
            this.snapshotCallback = null;
            this.feedCallback = null;
        }
        /**
         * Creates an array in the cache. If you supply an itemKeyCreator, such an array only stores cache-keys, so it's really just a list of references
         * to other objects in the cache. If not itemKeyCreator is provided, objects are stored directly in the array. If you supply a compareFunction
         * the array will be sorted (without streaming-support). We treat arrays differently and
         * store them in their own store, just to make things simple for ourselves. Cache-arrays are standard ObservableArray.
         * @param key
         * @param itemKeyCreator
         */
        BindingCache.prototype.createArray = function (key, itemKeyCreator, compareFunction, allowDuplicates) {
            if (allowDuplicates === void 0) { allowDuplicates = true; }
            if (this.cachedArrays.hasOwnProperty(key)) {
                var container = this.cachedArrays[key];
                if (!container.itemKeyCreator) {
                    container.itemKeyCreator = itemKeyCreator;
                }
            }
            else {
                var container = new CachedArrayContainer();
                if (compareFunction) {
                    container.array = new Infront.SortedObservableArray(compareFunction, allowDuplicates);
                }
                else {
                    container.array = new Infront.ObservableArray();
                }
                if (itemKeyCreator) {
                    container.itemKeyCreator = itemKeyCreator;
                }
                this.cachedArrays[key] = container;
                if (this.unboundArrayObservers.hasOwnProperty(key)) {
                    var arr = this.unboundArrayObservers[key];
                    var binding = arr.pop();
                    while (binding) {
                        container.array.observe(binding);
                        binding = arr.pop();
                    }
                    delete this.unboundArrayObservers[key];
                }
            }
        };
        /**
         * Updates an item in an array, or adds it if it is new. If the array doesn't contain keys, this will do nothing.
         * @param key
         * @param item
         */
        BindingCache.prototype.updateArrayItem = function (key, item) {
            if (this.cachedArrays.hasOwnProperty(key)) {
                var container = this.cachedArrays[key];
                if (container.itemKeyCreator) {
                    var itemKey = container.itemKeyCreator(item);
                    this.update(itemKey, item);
                    if (container.array.indexOf(itemKey) == -1) {
                        container.array.push(itemKey);
                    }
                }
            }
        };
        BindingCache.prototype.updateArrayItems = function (key, items) {
            if (this.cachedArrays.hasOwnProperty(key)) {
                var container = this.cachedArrays[key];
                if (container.itemKeyCreator) {
                    for (var i = 0; i < items.length; i++) {
                        var itemKey = container.itemKeyCreator(items[i]);
                        this.update(itemKey, items[i]);
                        if (container.array.indexOf(itemKey) == -1) {
                            container.array.push(itemKey);
                        }
                    }
                }
            }
        };
        BindingCache.prototype.insertKeyIntoArray = function (arrayKey, itemKey) {
            if (this.cachedArrays.hasOwnProperty(arrayKey)) {
                var container = this.cachedArrays[arrayKey];
                container.array.push(itemKey);
            }
        };
        BindingCache.prototype.deleteArrayItem = function (arrayKey, item) {
            var arrC = this.lookupArrayContainer(arrayKey);
            if (arrC && arrC.itemKeyCreator) {
                var itemKey = arrC.itemKeyCreator(item);
                var idx = arrC.array.indexOf(itemKey);
                if (idx > -1) {
                    arrC.array.removeItemAt(idx);
                }
            }
        };
        BindingCache.prototype.clearArray = function (arrayKey) {
            var arrC = this.lookupArrayContainer(arrayKey);
            if (arrC) {
                for (var i = 0; i < arrC.array.length(); i++) {
                    this.delete(arrC.array.item(i));
                }
                arrC.array.clear();
            }
        };
        BindingCache.prototype.lookupArrayContainer = function (arrayKey) {
            var retVal = null;
            if (this.cachedArrays.hasOwnProperty(arrayKey)) {
                retVal = this.cachedArrays[arrayKey];
            }
            return retVal;
        };
        /**
         * Binds to a cached Array.
         * @param key
         * @param binding
         */
        BindingCache.prototype.bindToArray = function (key, binding) {
            var _this = this;
            var container;
            if (!this.cachedArrays.hasOwnProperty(key)) {
                if (!this.unboundArrayObservers.hasOwnProperty(key)) {
                    this.unboundArrayObservers[key] = [];
                }
                this.unboundArrayObservers[key].push(binding);
            }
            else {
                container = this.cachedArrays[key];
                container.array.observe(binding);
            }
            return function () {
                // container.array.unbind(binding);
                _this.unbindFromArray(key, binding);
            };
        };
        BindingCache.prototype.unbindFromArray = function (key, binding) {
            if (this.cachedArrays.hasOwnProperty(key)) {
                var container = this.cachedArrays[key];
                container.array.unbind(binding);
            }
            else if (this.unboundArrayObservers.hasOwnProperty(key)) {
                var arr = this.unboundArrayObservers[key];
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i] === binding) {
                        arr.splice(i, 1);
                        break;
                    }
                }
            }
        };
        BindingCache.prototype.getArray = function (arrayKey) {
            var container = this.lookupArrayContainer(arrayKey);
            if (container) {
                return container.array;
            }
            return null;
        };
        /**
         * Update an object in the cache with data.
         *
         * This method will create a copy of all objects and primitives with corresponding observable values. If the
         * object (in part or in whole) exists it will be updated with the new data. Any bound elements will be updated.
         *
         * @param key key Identifier of the Object (for example a snapshot of instrument-data).
         * @param data The object containing the new data.
         */
        BindingCache.prototype.update = function (key, data) {
            if (key != null && data != null) {
                this.updateObservableFields(key, data);
                this.updateCacheTimestamp(key);
            }
        };
        BindingCache.prototype.deleteValue = function (valueKey) {
            if (this.cachedObservables[valueKey]) {
                this.cachedObservables[valueKey].set(null);
                delete this.cachedObservables[valueKey];
            }
        };
        BindingCache.prototype.delete = function (key) {
            if (key.length > 4) {
                var toBeDeleted = [];
                for (var p in this.cachedObservables) {
                    if (this.cachedObservables.hasOwnProperty(p)) {
                        if (p.substr(0, key.length) == key && p.substr(key.length, 1) == "/") {
                            toBeDeleted.push(p);
                        }
                    }
                }
                for (var i = 0; i < toBeDeleted.length; i++) {
                    this.deleteValue(toBeDeleted[i]);
                }
            }
        };
        /**
         * Identical to customBind, only it uses a FieldReference object to identify the field to bind to.
         * @param fieldRef
         * @param binding
         * @returns {function(): void}
         */
        BindingCache.prototype.customBindFR = function (fieldRef, binding) {
            return this.customBind(fieldRef.cacheKey, fieldRef.fieldSpec, binding);
        };
        BindingCache.prototype.inlineBind = function (key, field, callback) {
            return this.customBind(key, field, Infront.BindingFactory.createInlineBinding(callback));
        };
        BindingCache.prototype.customBind = function (key, field, binding) {
            var _this = this;
            var unbind = null;
            if (binding != null && field != null) {
                var fieldArray = InfrontUtil.isArray(field) ? field : [field];
                var mapKey = this.createObserverKey(key, fieldArray);
                if (this.cachedObservables.hasOwnProperty(mapKey)) {
                    this.cachedObservables[mapKey].observe(binding);
                }
                else {
                    if (!this.unboundObservers.hasOwnProperty(mapKey)) {
                        this.unboundObservers[mapKey] = [];
                    }
                    this.unboundObservers[mapKey].push(binding);
                }
                unbind = function () {
                    _this.unbind(key, fieldArray, binding);
                };
            }
            return unbind;
        };
        /**
         * A modifyable version of customBind, where the cache-key is an observable that may be changed.
         *
         * @param key Identifier of the Object (for example a snapshot of instrument-data), packed in an Observable
         * @param field The name of the field to observe. This may be nested in contained objects by providing an array of fields.
         * @param binding The binding you want to receive updates through.
         * @return A function you can call to unbind.
         */
        BindingCache.prototype.modifyableBind = function (key, field, binding) {
            var _this = this;
            var currentKey;
            var currentUnbind;
            var keyBinding = Infront.BindingFactory.createInlineBinding(function (val) {
                if (currentUnbind) {
                    currentUnbind();
                }
                binding.valueUpdated(null);
                currentUnbind = _this.customBind(val, field, binding);
            });
            key.observe(keyBinding);
            return function () {
                if (currentUnbind) {
                    currentUnbind();
                }
                key.unbind(keyBinding);
            };
        };
        BindingCache.prototype.unbind = function (key, field, binding) {
            var mapKey = this.createObserverKey(key, field);
            if (this.cachedObservables.hasOwnProperty(mapKey)) {
                this.cachedObservables[mapKey].unbind(binding);
            }
            else if (this.unboundObservers.hasOwnProperty(mapKey)) {
                var bindings = this.unboundObservers[mapKey];
                var idx = bindings.indexOf(binding);
                if (idx > -1) {
                    bindings.splice(idx, 1);
                }
                if (bindings.length == 0) {
                    delete this.unboundObservers[mapKey];
                }
            }
        };
        /**
         * Adds any currently unbound observers to the given observable.
         * @param key
         * @param observable
         */
        BindingCache.prototype.addUnboundObservers = function (key, observable) {
            if (this.unboundObservers.hasOwnProperty(key)) {
                var bindings = this.unboundObservers[key];
                for (var i = 0; i < bindings.length; i++) {
                    observable.observe(bindings[i]);
                }
                delete this.unboundObservers[key];
            }
        };
        BindingCache.prototype.createObserverKey = function (key, field) {
            var retVal = key != null ? key : BindingCache.kDefaultCacheKey;
            for (var i = 0; i < field.length; i++) {
                retVal = retVal + "/" + field[i];
            }
            return retVal;
        };
        BindingCache.prototype.bind = function (key, field, htmlElement) {
            var unbind = null;
            if (htmlElement != null) {
                var binding = Infront.BindingFactory.createValueBinding(htmlElement, "innerHTML");
                unbind = this.customBind(key, field, binding);
            }
            return unbind;
        };
        BindingCache.prototype.has = function (key, field) {
            var mapKey = this.createObserverKey(key, InfrontUtil.isArray(field) ? field : [field]);
            return this.cachedObservables.hasOwnProperty(mapKey);
        };
        BindingCache.prototype.get = function (key, field) {
            var retVal = null;
            if (key != null && field != null /*&& this.cachedObjects.hasOwnProperty(key)*/) {
                var fieldArray = InfrontUtil.isArray(field) ? field : [field];
                var mapKey = this.createObserverKey(key, fieldArray);
                var val = this.cachedObservables[mapKey];
                //var val:Observable = BindingCache.getField(fieldArray, this.cachedObjects[key]);
                if (val != null) {
                    retVal = val.get();
                }
            }
            return retVal;
        };
        BindingCache.prototype.asyncGet = function (callback, key, field) {
            if (field === void 0) { field = null; }
            var useKey;
            var useField;
            if (typeof (key) == "string") {
                useKey = key;
                useField = field;
            }
            else {
                useKey = key.cacheKey;
                useField = key.fieldSpec;
            }
            var unbind;
            unbind = this.customBind(useKey, useField, Infront.BindingFactory.createInlineBinding(function (val) {
                callback(val);
                InfrontUtil.setZeroTimeout(function () {
                    unbind();
                });
            }));
        };
        BindingCache.prototype.guaranteedAsyncGet = function (callback, key, field) {
            var _this = this;
            if (field === void 0) { field = null; }
            var useKey;
            var useField;
            if (typeof (key) == "string") {
                useKey = key;
                useField = field;
            }
            else {
                useKey = key.cacheKey;
                useField = key.fieldSpec;
            }
            var timestampUnbind;
            timestampUnbind = this.customBind(useKey, BindingCache.kCacheTimestampField, Infront.BindingFactory.createInlineBinding(function (val) {
                callback(_this.get(useKey, useField));
                //InfrontUtil.setZeroTimeout(()=>{
                timestampUnbind();
                //});
            }));
        };
        /**
         * Many objects in the cache contains an instrument. This is a utility-function for retrieving
         * feed and ticker and creating an Instrument-object from them.
         * Returns null if no instrument is found.
         *
         * @param key
         */
        BindingCache.prototype.getInstrument = function (key) {
            var retVal = null;
            var feed = this.get(key, [Infront.MWSConstants.instrument, Infront.MWSConstants.feed]);
            var ticker = this.get(key, [Infront.MWSConstants.instrument, Infront.MWSConstants.ticker]);
            if (feed && ticker) {
                retVal = new Infront.Instrument(feed, ticker);
                var isin = this.get(key, [Infront.MWSConstants.instrument, Infront.MWSConstants.isin]);
                if (isin) {
                    retVal.isin = isin;
                }
                var currency = this.get(key, [Infront.MWSConstants.instrument, Infront.MWSConstants.currency]);
                if (currency) {
                    retVal.currency = currency;
                }
                var fullName = this.get(key, [Infront.MWSConstants.instrument, Infront.MWSConstants.full_name]);
                if (fullName) {
                    retVal.full_name = fullName;
                }
            }
            return retVal;
        };
        BindingCache.prototype.search = function (needle) {
            var regex;
            if (needle instanceof RegExp) {
                regex = needle;
            }
            else {
                regex = new RegExp(needle);
            }
            var objects = [];
            var tmpStorage = {};
            var currentCacheKey = null;
            var rootObject = null;
            for (var p in this.cachedObservables) {
                if (this.cachedObservables.hasOwnProperty(p)) {
                    if (regex.test(p)) {
                        var parts = p.split("/");
                        if (currentCacheKey != parts[0]) {
                            currentCacheKey = parts[0];
                            if (!tmpStorage.hasOwnProperty(currentCacheKey)) {
                                tmpStorage[currentCacheKey] = {};
                                objects.push(tmpStorage[currentCacheKey]);
                            }
                            rootObject = tmpStorage[currentCacheKey];
                        }
                        var value = this.cachedObservables[p].get();
                        var currentObject = rootObject;
                        for (var i = 1; i < parts.length; i++) {
                            if (i == parts.length - 1) {
                                if (parts[i].substr(0, 1) != "_") {
                                    currentObject[parts[i]] = value;
                                }
                            }
                            else if (currentObject.hasOwnProperty(parts[i])) {
                                currentObject = currentObject[parts[i]];
                            }
                            else {
                                var tmp = {};
                                currentObject[parts[i]] = tmp;
                                currentObject = tmp;
                            }
                        }
                    }
                }
            }
            return objects;
        };
        /**
         * Debug-method that searches through the cache for keywords in keys and dumps the contents to the console.
         * @param needle
         */
        BindingCache.prototype.debugSearch = function (needle) {
            var lcNeedle = needle.toLowerCase();
            var obj = {};
            for (var p in this.cachedObservables) {
                if (this.cachedObservables.hasOwnProperty(p)) {
                    if (p.indexOf(lcNeedle) > -1) {
                        obj[p] = this.cachedObservables[p];
                    }
                }
            }
            0;
        };
        /**
         * Creates a computed field in the cache. A computed field is a field that has no data of itself, but merely updates
         * its value when ever one of the parameter fields updates.
         *
         * The computation is done through a supplied function, the arguments to the function is passed in the same order they are
         * specified in the fields array.
         *
         * If a field with this name exists in the object referenced by the key, nothing happens. For this reason it is important to
         * avoid the existing key-names when creating computed fields. Use the function fieldExists(key, field) to check.
         *
         * @param key The cache-key where the field is placed
         * @param name The name of the field. Must not exist from before
         * @param fields The fields used as arguments for the computation. Each member of the array can be a string or an array of strings. (as in bind() or customBind())
         */
        BindingCache.prototype.createComputedField = function (key, name, fields, computation) {
            if (!this.fieldExists(key, name)) {
                var obj = new Infront.CacheComputedObservable(this, fields, computation);
                var fieldSpec = [name];
                var mapKey = this.createObserverKey(key, fieldSpec);
                this.cachedObservables[mapKey] = obj;
                this.addUnboundObservers(mapKey, obj);
            }
        };
        /**
         * Creates a special kind of computed field that computes a value over all fields of objects contained in an Array.
         *
         * @param targetRef Field-reference to where we should store the computed value.
         * @param field Field-name (string or array of strings) you want to observe and calculate over.
         * @param array Array you want to calculate over
         * @param cacheKeyCreator Function to turn array-item into key suitable for looking up in the cache. This is widget-dependent.
         * @param computation Function to perform the actual computation.
         */
        BindingCache.prototype.createFieldComputation = function (targetRef, field, array, strategy, cacheKeyCreator, computation) {
            var obs = new Infront.CacheColumnComputedObservable(this, field, cacheKeyCreator, computation, strategy);
            array.observe(obs);
            var unbind = function () {
                array.unbind(obs);
            };
            var mapKey = this.createObserverKey(targetRef.cacheKey, targetRef.getSpecAsArray());
            this.cachedObservables[mapKey] = obs;
            this.addUnboundObservers(mapKey, obs);
            return unbind;
        };
        BindingCache.prototype.createWeightedAverage = function (targetRef, valueField, weightField, array, valueCacheKeyCreator, weightCacheKeyCreator) {
            var obs = new Infront.CacheWeightedAverageObservable(this, valueField, weightField, valueCacheKeyCreator, weightCacheKeyCreator);
            array.observe(obs);
            var unbind = function () {
                array.unbind(obs);
            };
            var mapKey = this.createObserverKey(targetRef.cacheKey, targetRef.getSpecAsArray());
            this.cachedObservables[mapKey] = obs;
            this.addUnboundObservers(mapKey, obs);
            return unbind;
        };
        BindingCache.prototype.createInCacheFieldComputation = function (targetKey, targetField, arrayKey, sourceField, strategy, cacheKeyCreator, computation) {
            if (this.cachedArrays.hasOwnProperty(arrayKey)) {
                var arrayContainer = this.cachedArrays[arrayKey];
                var targetObserverKey = this.createObserverKey(targetKey, InfrontUtil.isArray(targetField) ? targetField : [targetField]);
                var obs = new Infront.CacheColumnComputedObservable(this, sourceField, (cacheKeyCreator ? cacheKeyCreator : arrayContainer.cacheKeyCreator), computation, strategy);
                arrayContainer.array.observe(obs);
                var unbind = function () {
                    arrayContainer.unbind(obs);
                };
                this.cachedObservables[targetObserverKey] = obs;
                this.addUnboundObservers(targetObserverKey, obs);
                return unbind;
            }
        };
        BindingCache.prototype.updateCacheTimestamp = function (keyBase) {
            var tsKey = this.createObserverKey(keyBase, [BindingCache.kCacheTimestampField]);
            //var tsKey = keyBase + "/" + BindingCache.kCacheTimestampField;
            if (!this.cachedObservables.hasOwnProperty(tsKey)) {
                this.cachedObservables[tsKey] = new Infront.Observable();
                this.addUnboundObservers(tsKey, this.cachedObservables[tsKey]);
            }
            var obs = this.cachedObservables[tsKey];
            obs.set(Date.now());
        };
        BindingCache.prototype.updateObservableFields = function (keyBase, source) {
            for (var p in source) {
                if (source.hasOwnProperty(p)) {
                    var key = keyBase + "/" + p;
                    if (source[p] == null || InfrontUtil.isPrimitive(source[p]) || InfrontUtil.isArray(source[p])) {
                        if (!this.cachedObservables.hasOwnProperty(key)) {
                            this.cachedObservables[key] = new Infront.Observable();
                            this.addUnboundObservers(key, this.cachedObservables[key]);
                        }
                        this.cachedObservables[key].set(source[p]);
                        //} else if( InfrontUtil.isArray(source[p]) ) {
                    }
                    else if (InfrontUtil.isObject(source[p])) {
                        this.updateObservableFields(key, source[p]);
                    }
                }
            }
        };
        BindingCache.prototype.fieldExists = function (key, field) {
            var retVal = false;
            var fieldArray = InfrontUtil.isArray(field) ? field : [field];
            var mapKey = this.createObserverKey(key, fieldArray);
            retVal = this.cachedObservables.hasOwnProperty(mapKey);
            return retVal;
        };
        /**
         * Returns a callback that is guaranteed to update the cache (if only with a timestamp), even if the
         * returning callback is empty.
         *
         * @param reqInstrument
         * @returns {function(Object): void}
         */
        BindingCache.prototype.getGuaranteedSnapshotOnDataCallback = function (reqInstrument) {
            var _this = this;
            return function (result) {
                if (result.hasOwnProperty("instrument")) {
                    _this.update(CacheKeyFactory.createInstrumentKey(result["instrument"]), result);
                }
                else if (reqInstrument) {
                    _this.updateCacheTimestamp(CacheKeyFactory.createInstrumentKey(reqInstrument));
                }
            };
        };
        BindingCache.prototype.getGuaranteedSnapshotOnErrorCallback = function (reqInstrument, log) {
            var _this = this;
            if (log === void 0) { log = false; }
            return function (error_code, error_message) {
                if (reqInstrument) {
                    _this.updateCacheTimestamp(CacheKeyFactory.createInstrumentKey(reqInstrument));
                }
                if (log) {
                    //InfrontUtil.errorLogCallback(error_code, error_message);
                    //console.log("Error for " + reqInstrument.feed + "/" + reqInstrument.ticker + ": " + error_message);
                }
            };
        };
        BindingCache.prototype.getSnapshotOnDataCallback = function () {
            var _this = this;
            if (this.snapshotCallback == null) {
                this.snapshotCallback = function (result) {
                    if (result.hasOwnProperty("instrument")) {
                        _this.update(CacheKeyFactory.createInstrumentKey(result["instrument"]), result);
                    }
                };
            }
            return this.snapshotCallback;
        };
        BindingCache.prototype.getFeedOnDataCallback = function () {
            var _this = this;
            if (this.feedCallback == null) {
                this.feedCallback = function (result) {
                    if (result.hasOwnProperty("feed")) {
                        _this.update(CacheKeyFactory.createFeedKey(result["feed"]), result);
                    }
                };
            }
            return this.feedCallback;
        };
        BindingCache.objectHasField = function (obj, field) {
            return obj.hasOwnProperty(field[0]) && (field.length == 1 || BindingCache.objectHasField(obj[field[0]], field.slice(1)));
        };
        BindingCache.kCacheTimestampField = "_cache_time_stamp";
        /**
         * Creates a key to look up in this.unboundObservers for a given cache-key/field combo.
         * @param key
         * @param field
         * @returns {string}
         */
        BindingCache.kDefaultCacheKey = "DEFAULT";
        return BindingCache;
    }());
    Infront.BindingCache = BindingCache;
    /**
     * Helper class that searches for data inside the cache.
     */
    var DataAccess = (function () {
        function DataAccess(cache, tradingManager) {
            this.cache = cache;
            this.tradingManager = tradingManager;
        }
        DataAccess.prototype.getOrder = function (orderId, portfolioName) {
            var retVal = null;
            var portfolio;
            if (!portfolioName) {
                portfolio = this.tradingManager.getCurrentPortfolio();
            }
            else {
                portfolio = this.tradingManager.getPortfolio(portfolioName);
            }
            if (portfolio) {
                var needle = "order_" + portfolio.name + "\\|" + orderId;
                var result = this.cache.search(needle);
                if (result.length > 0) {
                    retVal = result[0];
                }
            }
            return retVal;
        };
        return DataAccess;
    }());
    Infront.DataAccess = DataAccess;
    var CacheKeyFactory = (function () {
        function CacheKeyFactory() {
        }
        CacheKeyFactory.createInstrumentKey = function (instrument) {
            return instrument[Infront.MWSConstants.feed] + "|" + instrument[Infront.MWSConstants.ticker];
        };
        CacheKeyFactory.createInstrumentFromKey = function (cacheKey) {
            var feedTicker = cacheKey.split("|");
            return new Infront.Instrument(Number(feedTicker[0]), feedTicker[1]);
        };
        CacheKeyFactory.createFeedKey = function (feedNumber) {
            return "feed_" + feedNumber;
        };
        /**
         * Key for broker/feed pairing.
         * @param broker
         * @returns {string}
         */
        CacheKeyFactory.createBrokerKey = function (period, feed, broker) {
            return period + "|" + "broker_" + broker + "|" + feed;
        };
        /**
         * Key for Broker/Instrument pairing
         * @param instrument
         * @param broker
         * @returns {string}
         */
        CacheKeyFactory.createInstrumentBrokerstatsKey = function (period, instrument, broker) {
            return period + "|" + "broker_" + broker + "|" + instrument[Infront.MWSConstants.feed] + "|" + instrument[Infront.MWSConstants.ticker];
        };
        CacheKeyFactory.createPortfolioKey = function (portfolio) {
            var portfolioName = portfolio == null ? "" : portfolio.name;
            return "portfolio_" + portfolioName;
        };
        CacheKeyFactory.createPortfolioDataKey = function (portfolio) {
            return CacheKeyFactory.createPortfolioKey(portfolio) + "|data";
        };
        CacheKeyFactory.createPortfolioPositionsKey = function (portfolio) {
            return CacheKeyFactory.createPortfolioKey(portfolio) + "|positions";
        };
        CacheKeyFactory.createPortfolioCashPositionsKey = function (portfolio) {
            return CacheKeyFactory.createPortfolioKey(portfolio) + "|cashPositions";
        };
        CacheKeyFactory.createPortfolioOrdersKey = function (portfolio) {
            return CacheKeyFactory.createPortfolioKey(portfolio) + "|orders";
        };
        CacheKeyFactory.createPortfolioTradesKey = function (portfolio) {
            return CacheKeyFactory.createPortfolioKey(portfolio) + "|trades";
        };
        CacheKeyFactory.createPortfolioValuesKey = function (portfolio) {
            return CacheKeyFactory.createPortfolioKey(portfolio) + "|values";
        };
        CacheKeyFactory.createPortfolioAlertsKey = function (portfolio) {
            return CacheKeyFactory.createPortfolioKey(portfolio) + "|alerts";
        };
        CacheKeyFactory.createOrderKey = function (order) {
            return CacheKeyFactory.createOrderIdKey(order[Infront.MWSConstants.portfolio], order[Infront.MWSConstants.order_id]);
        };
        CacheKeyFactory.createOrderIdKey = function (portfolio, orderId) {
            return "order_" + portfolio + "|" + orderId;
        };
        CacheKeyFactory.createTradeKey = function (trade) {
            return "trade_" + trade[Infront.MWSConstants.portfolio] + "|" + trade[Infront.MWSConstants.trade_id];
        };
        CacheKeyFactory.createCashPositionKey = function (position) {
            return "cash_" + position[Infront.MWSConstants.portfolio] + "|" + position[Infront.MWSConstants.cash][Infront.MWSConstants.currency];
        };
        CacheKeyFactory.createValueKey = function (value) {
            return CacheKeyFactory.createPotentialValueKey(value[Infront.MWSConstants.portfolio], value["name"]);
        };
        CacheKeyFactory.createPotentialValueKey = function (portfolioName, valueName) {
            return "value_" + portfolioName + "|" + Infront.TradingManager.createPortfolioValueId(valueName);
        };
        CacheKeyFactory.createPositionKey = function (position) {
            if (position.hasOwnProperty(Infront.MWSConstants.cash)) {
                return CacheKeyFactory.createCashPositionKey(position);
            }
            else {
                return CacheKeyFactory.createPotentialPositionKey(position[Infront.MWSConstants.portfolio], position[Infront.MWSConstants.instrument]);
            }
        };
        /**
         * As you can see, this is the same as createPositionKey. The name is just to reflect that the key is not created from an actual position-object,
         * but from parts that could (potentially :) belong to one.
         */
        CacheKeyFactory.createPotentialPositionKey = function (portfolioName, instrument) {
            return portfolioName + "|" + CacheKeyFactory.createInstrumentKey(instrument);
        };
        CacheKeyFactory.createNetTradesKey = function (portfolio) {
            return CacheKeyFactory.createPortfolioKey(portfolio) + "|net_trades";
        };
        CacheKeyFactory.createNetTradeKey = function (portfolio, instrument) {
            return "net_trade_" + CacheKeyFactory.createPortfolioKey(portfolio) + "|" + CacheKeyFactory.createInstrumentKey(instrument);
        };
        CacheKeyFactory.createVisualKey = function (name) {
            return "visual|" + name;
        };
        CacheKeyFactory.createTradesArrayKey = function (instrument) {
            return "trades_" + CacheKeyFactory.createInstrumentKey(instrument);
        };
        CacheKeyFactory.createBidsArrayKey = function (instrument) {
            return "bids_" + CacheKeyFactory.createInstrumentKey(instrument);
        };
        CacheKeyFactory.createAsksArrayKey = function (instrument) {
            return "asks_" + CacheKeyFactory.createInstrumentKey(instrument);
        };
        return CacheKeyFactory;
    }());
    Infront.CacheKeyFactory = CacheKeyFactory;
    var CacheBasedFilter = (function () {
        function CacheBasedFilter(cache, observer, fields, cacheKeyCreator, filterFunc) {
            this.cache = cache;
            this.filterKey = CacheBasedFilter.kFilterUnbindKeyPrefix + (CacheBasedFilter.filterCounter++);
            this.observer = observer;
            this.fields = fields;
            this.filterFunc = filterFunc;
            this.cacheKeyCreator = cacheKeyCreator;
            this.unbinds = {};
            this.values = {};
        }
        CacheBasedFilter.prototype.updateItem = function (item, val) {
            var localKey = this.getLocalKey(item, true);
            if (!this.values.hasOwnProperty(localKey) || this.values[localKey] != val) {
                this.values[localKey] = val;
                if (val) {
                    this.observer.itemAdded(item, 0);
                }
                else {
                    this.observer.itemRemoved(item, 0);
                }
            }
        };
        CacheBasedFilter.prototype.reInit = function (items) {
            for (var i = 0; i < items.length; i++) {
                this.itemAdded(items[i], i);
            }
        };
        CacheBasedFilter.prototype.itemAdded = function (item, index) {
            var _this = this;
            var key = this.cacheKeyCreator(item, null);
            var refs = [];
            for (var i = 0; i < this.fields.length; i++) {
                refs.push(new FieldReference(this.cacheKeyCreator(item, this.fields[i]), this.fields[i]));
            }
            this.cache.createComputedField(key, this.filterKey, refs, this.filterFunc);
            //Do an initial update.
            this.updateItem(item, this.cache.get(key, this.filterKey));
            var localKey = this.getLocalKey(item, true);
            this.unbinds[localKey] = this.cache.inlineBind(key, this.filterKey, function (val) {
                _this.updateItem(item, val);
            });
        };
        CacheBasedFilter.prototype.itemRemoved = function (item, index) {
            var localKey;
            if ((localKey = this.getLocalKey(item)) != null && this.unbinds.hasOwnProperty(localKey)) {
                this.unbinds[localKey]();
                delete item[this.filterKey];
                if (this.values.hasOwnProperty(localKey))
                    delete this.values[localKey];
            }
            this.observer.itemRemoved(item, 0);
        };
        CacheBasedFilter.prototype.itemMoved = function (item, oldIndex, newIndex) { }; //Doesn't support ordering, just contents.
        CacheBasedFilter.prototype.reset = function () {
            for (var p in this.unbinds) {
                if (this.unbinds.hasOwnProperty(p)) {
                    this.unbinds[p]();
                    delete this.unbinds[p];
                }
            }
            this.values = {};
        };
        CacheBasedFilter.prototype.destroy = function () {
            this.observer = null;
            this.reset();
        };
        CacheBasedFilter.prototype.getLocalKey = function (item, create) {
            if (create === void 0) { create = false; }
            if (typeof (item) == "string") {
                return item;
            }
            else {
                var retVal = null;
                if (item.hasOwnProperty(this.filterKey)) {
                    retVal = item[this.filterKey];
                }
                else if (create) {
                    item[this.filterKey] = InfrontUtil.makeUUID();
                    retVal = item[this.filterKey];
                }
                return retVal;
            }
        };
        CacheBasedFilter.kFilterUnbindKeyPrefix = "_CacheBasedFilter";
        CacheBasedFilter.filterCounter = 0;
        return CacheBasedFilter;
    }());
    Infront.CacheBasedFilter = CacheBasedFilter;
})(Infront || (Infront = {}));
var InfrontConstants;
(function (InfrontConstants) {
    var OrderType = (function () {
        function OrderType() {
        }
        OrderType.ASK = "ASK";
        OrderType.BID = "BID";
        return OrderType;
    }());
    InfrontConstants.OrderType = OrderType;
    var Fields = (function () {
        function Fields() {
        }
        Fields.ACC_VOLUME = "ACC_VOLUME";
        Fields.TURNOVER = "TURNOVER";
        Fields.ONEXCH_VOLUME = "ONEXCH_VOLUME";
        Fields.ONEXCH_TURNOVER = "ONEXCH_TURNOVER";
        Fields.BID = "BID";
        Fields.BID_SIZE = "BID_SIZE";
        Fields.ASK_SIZE = "ASK_SIZE";
        Fields.NUM_BIDS = "NUM_BIDS";
        Fields.NUM_ASKS = "NUM_ASKS";
        Fields.ORDERBOOK = "ORDERBOOK";
        Fields.VWAP = "VWAP";
        Fields.AVG_VOLUME = "AVG_VOLUME";
        Fields.AVG_VALUE = "AVG_VALUE";
        Fields.OPEN = "OPEN";
        Fields.ASK = "ASK";
        Fields.HIGH = "HIGH";
        Fields.LOW = "LOW";
        Fields.LAST = "LAST";
        Fields.LAST_VALID = "LAST_VALID";
        Fields.PREVIOUS_CLOSE = "PREVIOUS_CLOSE";
        Fields.TIME = "TIME";
        Fields.CHANGE = "CHANGE";
        Fields.PCT_CHANGE = "PCT_CHANGE";
        Fields.NUM_TRADES = "NUM_TRADES";
        Fields.GICS = "GICS";
        Fields.NUM_SHARES = "NUM_SHARES";
        Fields.RSI14 = "RSI14";
        Fields.MACD = "MACD";
        Fields.MA50 = "MA50";
        Fields.MA100 = "MA100";
        Fields.MA200 = "MA200";
        Fields.CONTRACT_SIZE = "CONTRACT_SIZE";
        Fields.TRADING_STATUS = "TRADING_STATUS";
        Fields.TICK_SIZE_ID = "TICK_SIZE_ID";
        Fields.SEGMENT = "SEGMENT";
        Fields.INDIC_PRICE = "INDIC_PRICE";
        Fields.INDIC_VOLUME = "INDIC_VOLUME";
        Fields.TRADING_POWER = "TRADING_POWER";
        return Fields;
    }());
    InfrontConstants.Fields = Fields;
    var FieldGroups = (function () {
        function FieldGroups() {
        }
        FieldGroups.FUND_DETAILS = "FUND_DETAILS";
        FieldGroups.HIST_PERFORMANCE = "HIST_PERFORMANCE";
        FieldGroups.REF_DATA_DETAILS = "REF_DATA_DETAILS";
        return FieldGroups;
    }());
    InfrontConstants.FieldGroups = FieldGroups;
    var FundDetailsMap = (function () {
        function FundDetailsMap() {
        }
        FundDetailsMap.ROOT_OBJECT = "fund_details";
        FundDetailsMap.FUND_STYLEBOX = [FundDetailsMap.ROOT_OBJECT, "portfolio", "equity_style_box"];
        FundDetailsMap.START_DATE = [FundDetailsMap.ROOT_OBJECT, "operation", "start_date"];
        FundDetailsMap.INVESTMENT_MANDATE = [FundDetailsMap.ROOT_OBJECT, "investment_mandate"];
        FundDetailsMap.PROSPECTIVE_BOOK_VALUE_YIELD = [FundDetailsMap.ROOT_OBJECT, "portfolio", "prospective_book_value_yield"];
        FundDetailsMap.PROSPECTIVE_EARNINGS_YIELD = [FundDetailsMap.ROOT_OBJECT, "portfolio", "prospective_earnings_yield"];
        FundDetailsMap.TOP_COUNTRIES = [FundDetailsMap.ROOT_OBJECT, "portfolio", "exposure", "top_countries"];
        FundDetailsMap.TOP_SECTORS = [FundDetailsMap.ROOT_OBJECT, "portfolio", "exposure", "top_sectors"];
        FundDetailsMap.TOP_HOLDINGS = [FundDetailsMap.ROOT_OBJECT, "portfolio", "top_holdings"];
        FundDetailsMap.ASSET_ALLOCATION = [FundDetailsMap.ROOT_OBJECT, "portfolio", "asset_allocation"];
        FundDetailsMap.MIN_INIT_INVESTMENT = [FundDetailsMap.ROOT_OBJECT, "operation", "min_init_investment"];
        FundDetailsMap.FOR_SALE_IN = [FundDetailsMap.ROOT_OBJECT, "operation", "for_sale_in"];
        FundDetailsMap.FUND_MANAGER = [FundDetailsMap.ROOT_OBJECT, "operation", "fund_manager"];
        FundDetailsMap.HOMEPAGE = [FundDetailsMap.ROOT_OBJECT, "operation", "homepage"];
        FundDetailsMap.TRACKING_ERROR = [FundDetailsMap.ROOT_OBJECT, "performance", "tracking_error"];
        FundDetailsMap.ACTIVE_SHARE = [FundDetailsMap.ROOT_OBJECT, "portfolio", "active_share"];
        return FundDetailsMap;
    }());
    InfrontConstants.FundDetailsMap = FundDetailsMap;
    var IntradayTradesSortValues = (function () {
        function IntradayTradesSortValues() {
        }
        IntradayTradesSortValues.TIME_DESC = "TIME_DESC";
        IntradayTradesSortValues.TIME_ASC = "TIME_ASC";
        IntradayTradesSortValues.VOLUME_DESC = "VOLUME_DESC";
        IntradayTradesSortValues.VOLUME_ASC = "VOLUME_ASC";
        IntradayTradesSortValues.PRICE_DESC = "PRICE_DESC";
        IntradayTradesSortValues.PRICE_ASC = "PRICE_ASC";
        return IntradayTradesSortValues;
    }());
    InfrontConstants.IntradayTradesSortValues = IntradayTradesSortValues;
    var SortOrder = (function () {
        function SortOrder() {
        }
        SortOrder.PCT_CHANGE_DESC = "PCT_CHANGE_DESC";
        SortOrder.PCT_CHANGE_ASC = "PCT_CHANGE_ASC";
        SortOrder.LV_PCT_CHANGE_DESC = "LV_PCT_CHANGE_DESC";
        SortOrder.LV_PCT_CHANGE_ASC = "LV_PCT_CHANGE_ASC";
        SortOrder.TURNOVER_DESC = "TURNOVER_DESC";
        SortOrder.TURNOVER_ASC = "TURNOVER_ASC";
        SortOrder.TICKER_DESC = "TICKER_DESC";
        SortOrder.TICKER_ASC = "TICKER_ASC";
        SortOrder.VOLUME_DESC = "VOLUME_DESC";
        SortOrder.VOLUME_ASC = "VOLUME_ASC";
        SortOrder.ONE_W_PCT_CHANGE_DESC = "ONE_W_PCT_CHANGE_DESC";
        SortOrder.ONE_W_PCT_CHANGE_ASC = "ONE_W_PCT_CHANGE_ASC";
        SortOrder.ONE_M_PCT_CHANGE_DESC = "ONE_M_PCT_CHANGE_DESC";
        SortOrder.ONE_M_PCT_CHANGE_ASC = "ONE_M_PCT_CHANGE_ASC";
        SortOrder.THREE_M_PCT_CHANGE_DESC = "THREE_M_PCT_CHANGE_DESC";
        SortOrder.THREE_M_PCT_CHANGE_ASC = "THREE_M_PCT_CHANGE_ASC";
        SortOrder.SIX_M_PCT_CHANGE_DESC = "SIX_M_PCT_CHANGE_DESC";
        SortOrder.SIX_M_PCT_CHANGE_ASC = "SIX_M_PCT_CHANGE_ASC";
        SortOrder.ONE_Y_PCT_CHANGE_DESC = "ONE_Y_PCT_CHANGE_DESC";
        SortOrder.ONE_Y_PCT_CHANGE_ASC = "ONE_Y_PCT_CHANGE_ASC";
        SortOrder.TWO_Y_PCT_CHANGE_DESC = "TWO_Y_PCT_CHANGE_DESC";
        SortOrder.TWO_Y_PCT_CHANGE_ASC = "TWO_Y_PCT_CHANGE_ASC";
        SortOrder.THREE_Y_PCT_CHANGE_DESC = "THREE_Y_PCT_CHANGE_DESC";
        SortOrder.THREE_Y_PCT_CHANGE_ASC = "THREE_Y_PCT_CHANGE_ASC";
        SortOrder.FIVE_Y_PCT_CHANGE_DESC = "FIVE_Y_PCT_CHANGE_DESC";
        SortOrder.FIVE_Y_PCT_CHANGE_ASC = "FIVE_Y_PCT_CHANGE_ASC";
        SortOrder.YTD_PCT_CHANGE_DESC = "YTD_PCT_CHANGE_DESC";
        SortOrder.YTD_PCT_CHANGE_ASC = "YTD_PCT_CHANGE_ASC";
        return SortOrder;
    }());
    InfrontConstants.SortOrder = SortOrder;
    var InstrumentType = (function () {
        function InstrumentType() {
        }
        InstrumentType.NONE = "NONE";
        InstrumentType.STOCK = "STOCK";
        InstrumentType.NEWS = "NEWS";
        InstrumentType.BOND = "BOND";
        InstrumentType.EURO_OPTION = "EURO_OPTION";
        InstrumentType.FUTURES = "FUTURES";
        InstrumentType.COMMODITY = "COMMODITY";
        InstrumentType.INDEX = "INDEX";
        InstrumentType.FOREX = "FOREX";
        InstrumentType.US_OPTION = "US_OPTION";
        InstrumentType.FUND = "FUND";
        InstrumentType.OPTION = "OPTION";
        InstrumentType.COMBO = "COMBO";
        InstrumentType.CFD = "CFD";
        InstrumentType.CERTIFICATE = "CERTIFICATE";
        InstrumentType.UNKNOWN = "UNKNOWN";
        return InstrumentType;
    }());
    InfrontConstants.InstrumentType = InstrumentType;
    var SearchItemType = (function () {
        function SearchItemType() {
        }
        SearchItemType.INSTRUMENT = "INSTRUMENT";
        SearchItemType.CHAIN = "CHAIN";
        SearchItemType.FEED = "FEED";
        return SearchItemType;
    }());
    InfrontConstants.SearchItemType = SearchItemType;
    var EventNames = (function () {
        function EventNames() {
        }
        EventNames.kLoginFailed = "onLoginFailed";
        EventNames.kOnReady = "onReady";
        EventNames.kOnDisconnect = "onDisconnect";
        return EventNames;
    }());
    InfrontConstants.EventNames = EventNames;
    var BrokerStatsPeriodes = (function () {
        function BrokerStatsPeriodes() {
        }
        BrokerStatsPeriodes.INTRADAY = "INTRADAY";
        BrokerStatsPeriodes.TWO_DAYS = "TWO_DAYS";
        BrokerStatsPeriodes.ONE_WEEK = "ONE_WEEK";
        BrokerStatsPeriodes.ONE_MONTH = "ONE_MONTH";
        BrokerStatsPeriodes.THREE_MONTHS = "THREE_MONTHS";
        BrokerStatsPeriodes.SIX_MONTHS = "SIX_MONTHS";
        BrokerStatsPeriodes.ONE_YEAR = "ONE_YEAR";
        BrokerStatsPeriodes.YTD = "YTD";
        return BrokerStatsPeriodes;
    }());
    InfrontConstants.BrokerStatsPeriodes = BrokerStatsPeriodes;
    var HistoricalPeriodes = (function () {
        function HistoricalPeriodes() {
        }
        HistoricalPeriodes.ONE_WEEK = "1W";
        HistoricalPeriodes.ONE_MONTH = "1M";
        HistoricalPeriodes.THREE_MONTH = "3M";
        HistoricalPeriodes.SIX_MONTH = "6M";
        HistoricalPeriodes.ONE_YEAR = "1Y";
        HistoricalPeriodes.TWO_YEAR = "2Y";
        HistoricalPeriodes.THREE_YEAR = "3Y";
        HistoricalPeriodes.FIVE_YEAR = "5Y";
        HistoricalPeriodes.YTD = "YTD";
        return HistoricalPeriodes;
    }());
    InfrontConstants.HistoricalPeriodes = HistoricalPeriodes;
    var FundAllocationType = (function () {
        function FundAllocationType() {
        }
        FundAllocationType.COUNTRY = "Country";
        FundAllocationType.ASSET = "Asset";
        FundAllocationType.SECTOR = "Sector";
        return FundAllocationType;
    }());
    InfrontConstants.FundAllocationType = FundAllocationType;
})(InfrontConstants || (InfrontConstants = {}));
/**
 * Created by Andreas on 20.01.14.
 */
var Infront;
/**
 * Created by Andreas on 20.01.14.
 */
(function (Infront) {
    var FeedHandler = (function () {
        function FeedHandler(controller) {
            this.feeds = {};
            this.feedsInRequest = {};
            this.controller = controller;
        }
        FeedHandler.prototype.getFeed = function (feedNumber, callback) {
            var _this = this;
            var key = Number(feedNumber).toString();
            if (this.feeds.hasOwnProperty(key)) {
                setTimeout(function () {
                    callback(_this.feeds[key]);
                }, 0);
            }
            else if (this.feedsInRequest.hasOwnProperty(key)) {
                this.feedsInRequest[key].push(callback);
            }
            else {
                var request = new Infront.GetFeedMetadataRequest();
                request.feed = feedNumber;
                var reqOpts = new Infront.RequestOptions();
                reqOpts.onError = function (error_code, error_Message) {
                    setTimeout(function () {
                        callback(null);
                    });
                };
                reqOpts.onSuccess = function (response) {
                    _this.storeFeedMetadata(response);
                    _this.processCallbacks(key);
                };
                if (!this.feedsInRequest.hasOwnProperty(key)) {
                    this.feedsInRequest[key] = [];
                }
                this.feedsInRequest[key].push(callback);
                this.controller.getMessageService().sendRequest(request, reqOpts);
            }
        };
        FeedHandler.prototype.getFeeds = function (feedNumbers, callback) {
            var retVal = [];
            var handler = new InfrontUtil.MultiCallbackHandler(function () {
                callback(retVal);
            });
            for (var i = 0; i < feedNumbers.length; i++) {
                this.getFeed(feedNumbers[i], handler.addCallback(function (feed) {
                    if (feed) {
                        retVal.push(feed);
                    }
                }));
            }
        };
        FeedHandler.prototype.dumpFeed = function (feedNumber) {
            this.getFeed(feedNumber, function (feedMetaData) {
                0;
            });
        };
        FeedHandler.prototype.processCallbacks = function (key) {
            var callbacks = this.feedsInRequest[key];
            var feedMetaData = this.feeds[key];
            for (var i = 0; i < callbacks.length; i++) {
                callbacks[i](feedMetaData);
            }
            delete this.feedsInRequest[key];
        };
        FeedHandler.prototype.storeFeedMetadata = function (data) {
            var key = Number(data["feed"]).toString();
            var metadata = InfrontUtil.mergeRecursive(new Infront.FeedMetaData(), data);
            metadata.iso_country = Countries.getAlpha2(metadata.country);
            this.feeds[key] = metadata;
        };
        return FeedHandler;
    }());
    Infront.FeedHandler = FeedHandler;
    var Countries = (function () {
        function Countries() {
        }
        Countries.getAlpha2 = function (infrontCountry) {
            if (InfrontUtil.isNumber(infrontCountry)) {
                var key = infrontCountry.toString();
                if (Countries.WT_COUNTRY_CODES.hasOwnProperty(key)) {
                    return Countries.WT_COUNTRY_CODES[key].a2;
                }
            }
            return null;
        };
        Countries.getCountryName = function (infrontCountry) {
            if (InfrontUtil.isNumber(infrontCountry)) {
                var key = infrontCountry.toString();
                if (Countries.WT_COUNTRY_CODES.hasOwnProperty(key)) {
                    return Countries.WT_COUNTRY_CODES[key].country_name;
                }
            }
            return null;
        };
        Countries.WT_COUNTRY_CODES = {
            "0": {
                "a2": "",
                "country_name": "Global"
            },
            "1": {
                "a2": "US",
                "country_name": "United States"
            },
            "2": {
                "a2": "CA",
                "country_name": "Canada"
            },
            "7": {
                "a2": "RU",
                "country_name": "Russian Federation"
            },
            "20": {
                "a2": "EG",
                "country_name": "Egypt"
            },
            "27": {
                "a2": "ZA",
                "country_name": "South Africa"
            },
            "30": {
                "a2": "GR",
                "country_name": "Greece"
            },
            "31": {
                "a2": "NL",
                "country_name": "Netherlands"
            },
            "32": {
                "a2": "BE",
                "country_name": "Belgium"
            },
            "33": {
                "a2": "FR",
                "country_name": "France"
            },
            "34": {
                "a2": "ES",
                "country_name": "Spain"
            },
            "36": {
                "a2": "HU",
                "country_name": "Hungary"
            },
            "39": {
                "a2": "IT",
                "country_name": "Italy"
            },
            "40": {
                "a2": "RO",
                "country_name": "Romania"
            },
            "41": {
                "a2": "CH",
                "country_name": "Switzerland"
            },
            "43": {
                "a2": "AT",
                "country_name": "Austria"
            },
            "44": {
                "a2": "GB",
                "country_name": "United Kingdom"
            },
            "45": {
                "a2": "DK",
                "country_name": "Denmark"
            },
            "46": {
                "a2": "SE",
                "country_name": "Sweden"
            },
            "47": {
                "a2": "NO",
                "country_name": "Norway"
            },
            "48": {
                "a2": "PL",
                "country_name": "Poland"
            },
            "49": {
                "a2": "DE",
                "country_name": "Germany"
            },
            "52": {
                "a2": "MX",
                "country_name": "Mexico"
            },
            "54": {
                "a2": "AR",
                "country_name": "Argentina"
            },
            "55": {
                "a2": "BR",
                "country_name": "Brazil"
            },
            "56": {
                "a2": "CL",
                "country_name": "Chile"
            },
            "58": {
                "a2": "VE",
                "country_name": "Venezuela, Bolivarian Republic of"
            },
            "60": {
                "a2": "MY",
                "country_name": "Malaysia"
            },
            "61": {
                "a2": "AU",
                "country_name": "Australia"
            },
            "62": {
                "a2": "ID",
                "country_name": "Indonesia"
            },
            "63": {
                "a2": "PH",
                "country_name": "Philippines"
            },
            "64": {
                "a2": "NZ",
                "country_name": "New Zealand"
            },
            "65": {
                "a2": "SG",
                "country_name": "Singapore"
            },
            "66": {
                "a2": "TH",
                "country_name": "Thailand"
            },
            "81": {
                "a2": "JP",
                "country_name": "Japan"
            },
            "82": {
                "a2": "KR",
                "country_name": "Korea, Republic of"
            },
            "84": {
                "a2": "VN",
                "country_name": "Vietnam"
            },
            "86": {
                "a2": "CN",
                "country_name": "China"
            },
            "90": {
                "a2": "TR",
                "country_name": "Turkey"
            },
            "91": {
                "a2": "IN",
                "country_name": "India"
            },
            "92": {
                "a2": "PA",
                "country_name": "Pakistan"
            },
            "212": {
                "a2": "MA",
                "country_name": "Morocco"
            },
            "216": {
                "a2": "TN",
                "country_name": "Tunisia"
            },
            "230": {
                "a2": "MU",
                "country_name": "Mauritius"
            },
            "233": {
                "a2": "GH",
                "country_name": "Ghana"
            },
            "234": {
                "a2": "NG",
                "country_name": "Nigeria"
            },
            "250": {
                "a2": "RW",
                "country_name": "Rwanda"
            },
            "254": {
                "a2": "KE",
                "country_name": "Kenya"
            },
            "255": {
                "a2": "TZ",
                "country_name": "Tanzania, United Republic of"
            },
            "256": {
                "a2": "UG",
                "country_name": "Uganda"
            },
            "258": {
                "a2": "MZ",
                "country_name": "Mozambique"
            },
            "260": {
                "a2": "ZM",
                "country_name": "Zambia"
            },
            "263": {
                "a2": "ZW",
                "country_name": "Zimbabwe"
            },
            "264": {
                "a2": "NA",
                "country_name": "Namibia"
            },
            "265": {
                "a2": "MW",
                "country_name": "Malawi"
            },
            "267": {
                "a2": "BW",
                "country_name": "Botswana"
            },
            "268": {
                "a2": "SZ",
                "country_name": "Swaziland"
            },
            "351": {
                "a2": "PT",
                "country_name": "Portugal"
            },
            "352": {
                "a2": "LU",
                "country_name": "Luxembourg"
            },
            "353": {
                "a2": "IE",
                "country_name": "Ireland"
            },
            "354": {
                "a2": "IS",
                "country_name": "Iceland"
            },
            "356": {
                "a2": "MT",
                "country_name": "Malta"
            },
            "358": {
                "a2": "FI",
                "country_name": "Finland"
            },
            "359": {
                "a2": "BG",
                "country_name": "Bulgaria"
            },
            "370": {
                "a2": "LT",
                "country_name": "Lithuania"
            },
            "371": {
                "a2": "LV",
                "country_name": "Latvia"
            },
            "372": {
                "a2": "EE",
                "country_name": "Estonia"
            },
            "380": {
                "a2": "UA",
                "country_name": "Ukraine"
            },
            "385": {
                "a2": "HR",
                "country_name": "Croatia"
            },
            "386": {
                "a2": "SI",
                "country_name": "Slovenia"
            },
            "420": {
                "a2": "CZ",
                "country_name": "Czech Republic"
            },
            "852": {
                "a2": "HK",
                "country_name": "Hong Kong"
            },
            "886": {
                "a2": "TW",
                "country_name": "Taiwan, Province of China"
            },
            "972": {
                "a2": "IL",
                "country_name": "Israel"
            },
            "996": {
                "a2": "KG",
                "country_name": "Kyrgyzstan"
            },
            "997": {
                "a2": "",
                "country_name": "Infront (997)"
            }
        };
        return Countries;
    }());
    Infront.Countries = Countries;
})(Infront || (Infront = {}));
///<reference path='InfrontConstants.ts' />
///<reference path='Datatypes.ts' />
///<reference path='MWSConstants.ts' />
///<reference path='Controller.ts' />
///<reference path='FeedHandler.ts' />
///<reference path='B64.ts' />
var Infront;
///<reference path='InfrontConstants.ts' />
///<reference path='Datatypes.ts' />
///<reference path='MWSConstants.ts' />
///<reference path='Controller.ts' />
///<reference path='FeedHandler.ts' />
///<reference path='B64.ts' />
(function (Infront) {
    var RequestOptions = Infront.RequestOptions;
    var InstrumentSubscriptionRequest = Infront.InstrumentSubscriptionRequest;
    var UniverseType;
    (function (UniverseType) {
        UniverseType[UniverseType["COUNTRY"] = 0] = "COUNTRY";
        UniverseType[UniverseType["MARKETPLACE"] = 1] = "MARKETPLACE";
        UniverseType[UniverseType["REGION"] = 2] = "REGION";
        UniverseType[UniverseType["REGION_SECTOR"] = 3] = "REGION_SECTOR";
        UniverseType[UniverseType["COUNTRY_SECTOR"] = 4] = "COUNTRY_SECTOR";
        UniverseType[UniverseType["MARKETPLACE_SECTOR"] = 5] = "MARKETPLACE_SECTOR";
    })(UniverseType = Infront.UniverseType || (Infront.UniverseType = {}));
    var BaseDataOptions = (function () {
        function BaseDataOptions() {
        }
        return BaseDataOptions;
    }());
    Infront.BaseDataOptions = BaseDataOptions;
    var DataOptions = (function (_super) {
        __extends(DataOptions, _super);
        function DataOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return DataOptions;
    }(BaseDataOptions));
    Infront.DataOptions = DataOptions;
    var QuoteOptions = (function (_super) {
        __extends(QuoteOptions, _super);
        function QuoteOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return QuoteOptions;
    }(DataOptions));
    Infront.QuoteOptions = QuoteOptions;
    var QuoteListOptions = (function (_super) {
        __extends(QuoteListOptions, _super);
        function QuoteListOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return QuoteListOptions;
    }(QuoteOptions));
    Infront.QuoteListOptions = QuoteListOptions;
    var RankedListOptions = (function (_super) {
        __extends(RankedListOptions, _super);
        function RankedListOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return RankedListOptions;
    }(DataOptions));
    Infront.RankedListOptions = RankedListOptions;
    var ReferenceDataOptions = (function (_super) {
        __extends(ReferenceDataOptions, _super);
        function ReferenceDataOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ReferenceDataOptions;
    }(DataOptions));
    Infront.ReferenceDataOptions = ReferenceDataOptions;
    var ChainOptions = (function (_super) {
        __extends(ChainOptions, _super);
        function ChainOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ChainOptions;
    }(DataOptions));
    Infront.ChainOptions = ChainOptions;
    var ChainsOptions = (function (_super) {
        __extends(ChainsOptions, _super);
        function ChainsOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ChainsOptions;
    }(DataOptions));
    Infront.ChainsOptions = ChainsOptions;
    var NewsItemsOptions = (function (_super) {
        __extends(NewsItemsOptions, _super);
        function NewsItemsOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return NewsItemsOptions;
    }(DataOptions));
    Infront.NewsItemsOptions = NewsItemsOptions;
    var AlertsOptions = (function (_super) {
        __extends(AlertsOptions, _super);
        function AlertsOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return AlertsOptions;
    }(DataOptions));
    Infront.AlertsOptions = AlertsOptions;
    var ModifyAlertOptions = (function (_super) {
        __extends(ModifyAlertOptions, _super);
        function ModifyAlertOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ModifyAlertOptions;
    }(DataOptions));
    Infront.ModifyAlertOptions = ModifyAlertOptions;
    var SearchOptions = (function (_super) {
        __extends(SearchOptions, _super);
        function SearchOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return SearchOptions;
    }(DataOptions));
    Infront.SearchOptions = SearchOptions;
    var BrokerStatsOptions = (function (_super) {
        __extends(BrokerStatsOptions, _super);
        function BrokerStatsOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return BrokerStatsOptions;
    }(DataOptions));
    Infront.BrokerStatsOptions = BrokerStatsOptions;
    var TickSizesOptions = (function (_super) {
        __extends(TickSizesOptions, _super);
        function TickSizesOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return TickSizesOptions;
    }(DataOptions));
    Infront.TickSizesOptions = TickSizesOptions;
    var CalendarOptions = (function (_super) {
        __extends(CalendarOptions, _super);
        function CalendarOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return CalendarOptions;
    }(DataOptions));
    Infront.CalendarOptions = CalendarOptions;
    var IntradayTradesOptions = (function (_super) {
        __extends(IntradayTradesOptions, _super);
        function IntradayTradesOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return IntradayTradesOptions;
    }(DataOptions));
    Infront.IntradayTradesOptions = IntradayTradesOptions;
    var InstrumentIntradayTradesOptions = (function (_super) {
        __extends(InstrumentIntradayTradesOptions, _super);
        function InstrumentIntradayTradesOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return InstrumentIntradayTradesOptions;
    }(DataOptions));
    Infront.InstrumentIntradayTradesOptions = InstrumentIntradayTradesOptions;
    var InsertOrderOptions = (function (_super) {
        __extends(InsertOrderOptions, _super);
        function InsertOrderOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return InsertOrderOptions;
    }(DataOptions));
    Infront.InsertOrderOptions = InsertOrderOptions;
    var ActivateOrderOptions = (function (_super) {
        __extends(ActivateOrderOptions, _super);
        function ActivateOrderOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ActivateOrderOptions;
    }(DataOptions));
    Infront.ActivateOrderOptions = ActivateOrderOptions;
    var ModifyOrderOptions = (function (_super) {
        __extends(ModifyOrderOptions, _super);
        function ModifyOrderOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ModifyOrderOptions;
    }(DataOptions));
    Infront.ModifyOrderOptions = ModifyOrderOptions;
    var DeleteOrderOptions = (function (_super) {
        __extends(DeleteOrderOptions, _super);
        function DeleteOrderOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return DeleteOrderOptions;
    }(DataOptions));
    Infront.DeleteOrderOptions = DeleteOrderOptions;
    var GetAlgosOptions = (function (_super) {
        __extends(GetAlgosOptions, _super);
        function GetAlgosOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return GetAlgosOptions;
    }(DataOptions));
    Infront.GetAlgosOptions = GetAlgosOptions;
    var TradesByDaysOptions = (function (_super) {
        __extends(TradesByDaysOptions, _super);
        function TradesByDaysOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return TradesByDaysOptions;
    }(IntradayTradesOptions));
    Infront.TradesByDaysOptions = TradesByDaysOptions;
    var TradesByDateOptions = (function (_super) {
        __extends(TradesByDateOptions, _super);
        function TradesByDateOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return TradesByDateOptions;
    }(IntradayTradesOptions));
    Infront.TradesByDateOptions = TradesByDateOptions;
    var HistoricalTradesOptions = (function (_super) {
        __extends(HistoricalTradesOptions, _super);
        function HistoricalTradesOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return HistoricalTradesOptions;
    }(DataOptions));
    Infront.HistoricalTradesOptions = HistoricalTradesOptions;
    var MarketActivityOptions = (function (_super) {
        __extends(MarketActivityOptions, _super);
        function MarketActivityOptions() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.streaming = false;
            return _this;
        }
        return MarketActivityOptions;
    }(DataOptions));
    Infront.MarketActivityOptions = MarketActivityOptions;
    var TradingPowerOptions = (function (_super) {
        __extends(TradingPowerOptions, _super);
        function TradingPowerOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return TradingPowerOptions;
    }(DataOptions));
    Infront.TradingPowerOptions = TradingPowerOptions;
    var InfinancialsUrlOptions = (function (_super) {
        __extends(InfinancialsUrlOptions, _super);
        function InfinancialsUrlOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return InfinancialsUrlOptions;
    }(DataOptions));
    Infront.InfinancialsUrlOptions = InfinancialsUrlOptions;
    var ComponentOptions = (function (_super) {
        __extends(ComponentOptions, _super);
        function ComponentOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ComponentOptions;
    }(DataOptions));
    Infront.ComponentOptions = ComponentOptions;
    var ChartOptions = (function (_super) {
        __extends(ChartOptions, _super);
        function ChartOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ChartOptions;
    }(ComponentOptions));
    Infront.ChartOptions = ChartOptions;
    var EvolutionChartOptions = (function (_super) {
        __extends(EvolutionChartOptions, _super);
        function EvolutionChartOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return EvolutionChartOptions;
    }(ComponentOptions));
    Infront.EvolutionChartOptions = EvolutionChartOptions;
    var EtaOptions = (function (_super) {
        __extends(EtaOptions, _super);
        function EtaOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return EtaOptions;
    }(DataOptions));
    Infront.EtaOptions = EtaOptions;
    var UniverseOptions = (function (_super) {
        __extends(UniverseOptions, _super);
        function UniverseOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return UniverseOptions;
    }(ComponentOptions));
    Infront.UniverseOptions = UniverseOptions;
    var FundamentalsOptions = (function (_super) {
        __extends(FundamentalsOptions, _super);
        function FundamentalsOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return FundamentalsOptions;
    }(DataOptions));
    Infront.FundamentalsOptions = FundamentalsOptions;
    var EurofinOptions = (function (_super) {
        __extends(EurofinOptions, _super);
        function EurofinOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return EurofinOptions;
    }(DataOptions));
    Infront.EurofinOptions = EurofinOptions;
    var InfrontOptions = (function (_super) {
        __extends(InfrontOptions, _super);
        function InfrontOptions() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return InfrontOptions;
    }(Infront.ControllerOptions));
    Infront.InfrontOptions = InfrontOptions;
    /**
     * Entry-point of the core library. Use this if you want to request data directly
     */
    var Model = (function () {
        function Model(options) {
            this.options = options;
            if (!this.options.baseCurrency) {
                this.options.baseCurrency = Model.kDefaultBaseCurrency;
            }
            this.controller = new Infront.Controller();
            this.feedHandler = new Infront.FeedHandler(this.controller);
            this.tradingManager = new Infront.TradingManager(this);
            this.cache = new Infront.BindingCache();
            //this.sentFundDetailsReq = {};
        }
        Model.prototype.getBaseCurrency = function () {
            return this.options.baseCurrency;
        };
        Model.prototype.getCurrencyConverter = function () {
            if (!this.currencyConverter) {
                this.currencyConverter = new Infront.CurrencyConverter(this, false);
            }
            return this.currencyConverter;
        };
        Model.prototype.publishEvent = function (event) {
            this.controller.publishEvent(event);
        };
        Model.prototype.getCache = function () {
            return this.cache;
        };
        Model.prototype.getFeedHandler = function () {
            return this.feedHandler;
        };
        Model.prototype.setLoginInformation = function (options) {
            InfrontUtil.mergeRecursive(this.options, options);
        };
        Model.prototype.getTradingRoutings = function () {
            return this.controller.getTradingRoutings();
        };
        Model.prototype.getSidPidMatches = function () {
            return this.controller.getSidPidMatches();
        };
        Model.prototype.getTradableFeeds = function () {
            return this.controller.getTradableFeeds();
        };
        Model.prototype.getSession = function () {
            return this.controller.getSession();
        };
        Model.prototype.tradingLogin = function (provider, service, username, password, pin) {
            this.controller.startTradingLogin(provider, service, username, password, pin);
        };
        Model.prototype.tradingLogout = function () {
            this.controller.tradingLogout();
        };
        Model.prototype.getTradingManager = function () {
            return this.tradingManager;
        };
        Model.prototype.getTradingFeatures = function () {
            return this.controller.getTradingFeatures();
        };
        Model.prototype.init = function () {
            this.controller.init(this.options);
            this.instrumentManager = new Infront.InstrumentManager(this.controller.getMessageService(), this);
        };
        Model.prototype.logout = function () {
            this.controller.logOut();
        };
        Model.prototype.reconnectSession = function (sessionToken, sessionUrls, routingResponse) {
            this.controller.reconnectSession(sessionToken, sessionUrls, routingResponse);
        };
        Model.prototype.registerEventObserver = function (eventName, callback) {
            this.controller.registerEventObserver(eventName, callback);
        };
        Model.prototype.unregisterEventObserver = function (eventName, callback) {
            this.controller.unregisterEventObserver(eventName, callback);
        };
        Model.prototype.registerTradingConnectedEventObserverWithCallbackIfConnected = function (callback) {
            var _this = this;
            if (this.getTradingStatus() == Infront.InfrontStatus.Connected) {
                InfrontUtil.setZeroTimeout(function () {
                    callback(_this.controller.tradingConnectedEventFromStorage());
                });
            }
            this.registerEventObserver(Infront.TradingConnectedEvent.kEventName, callback);
        };
        Model.prototype.getStatus = function () {
            return this.controller.getStatus();
        };
        Model.prototype.getTradingStatus = function () {
            return this.controller.getTradingStatus();
        };
        Model.prototype.isStreaming = function () {
            return this.controller.isStreaming();
        };
        Model.prototype.instrumentInfo = function (arg1, arg2, arg3) {
            var instrument, options;
            if (InfrontUtil.isObject(arg1)) {
                instrument = arg1;
                options = arg2;
            }
            else {
                var instrument = new Infront.Instrument(arg1, arg2);
                options = arg3;
            }
            var completeResult = {};
            var handler = new InfrontUtil.MultiCallbackHandler(function () {
                options.onData(completeResult);
            });
            var request = new Infront.GetSnapshotRequest();
            request.setFields(["LAST"]);
            request.setInstrument(instrument);
            var reqOpts = new RequestOptions();
            var callbacks = handler.addCallbackWithError(function (result) {
                if (result.hasOwnProperty("instruments") && result["instruments"].length > 0) {
                    completeResult["instrument"] = result["instruments"][0]["instrument"];
                }
            }, function (error_code, error_message) { });
            reqOpts.onError = callbacks.error;
            reqOpts.onSuccess = callbacks.callback;
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.orderbook = function (instrument, options) {
            var _this = this;
            var retVal;
            if (this.controller.isStreaming() && options.streaming) {
                var cb = function (data) {
                    options.onData(data);
                };
                this.subscribe(instrument, ["ORDERBOOK"], false, cb);
                retVal = function () {
                    _this.unsubscribe(instrument, ["ORDERBOOK"], cb);
                };
            }
            else {
                var request = new Infront.GetSnapshotRequest();
                request.setFields([]);
                request.setInstrument(instrument);
                var reqOpts = new RequestOptions();
                reqOpts.onError = options.onError;
                reqOpts.onSuccess = function (result) {
                    if (!(result.hasOwnProperty("instruments")) || result["instruments"].length == 0) {
                        options.onError(404, "Unknown instrument");
                    }
                    else {
                        options.onData(result["instruments"][0]);
                    }
                };
                this.controller.getMessageService().sendRequest(request, reqOpts);
                retVal = function () { };
            }
            return retVal;
        };
        Model.prototype.subscribe = function (instruments, fields, guarantee, callback, numTrades) {
            var _this = this;
            if (guarantee === void 0) { guarantee = false; }
            if (numTrades === void 0) { numTrades = 0; }
            this.instrumentManager.subscribe(InfrontUtil.isArray(instruments) ? instruments : [instruments], fields, guarantee, callback, numTrades);
            return function () {
                _this.unsubscribe(instruments, fields, callback);
            };
        };
        Model.prototype.unsubscribe = function (instruments, fields, callback) {
            this.instrumentManager.unsubscribe(InfrontUtil.isArray(instruments) ? instruments : [instruments], fields, callback);
        };
        Model.prototype.snapshot = function (instruments, fields, guarantee, callback, numTrades) {
            if (guarantee === void 0) { guarantee = false; }
            if (numTrades === void 0) { numTrades = 0; }
            this.instrumentManager.snapshot(InfrontUtil.isArray(instruments) ? instruments : [instruments], fields, guarantee, callback, numTrades);
        };
        Model.prototype.rankedList = function (feed, options) {
            var defaults = new RankedListOptions();
            defaults.fields = ["LAST", "CHANGE", "PCT_CHANGE"];
            defaults.sortOrder = InfrontConstants.SortOrder.PCT_CHANGE_DESC;
            var opts = InfrontUtil.mergeRecursive(defaults, options);
            var req = new Infront.GetRankedListRequest();
            req.feed = feed;
            req.fields = opts.fields;
            req.items = opts.items;
            req.sort_order = opts.sortOrder;
            req.instrument_types = opts.instrumentTypes;
            req.min_turnover = opts.minTurnover;
            req.page = opts.page;
            if (options.chain) {
                req.chain = options.chain;
            }
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                if (!(result.hasOwnProperty("instruments")) || result["instruments"].length == 0) {
                    options.onError(404, "Unknown feed");
                }
                else {
                    options.onData(result["instruments"]);
                }
            };
            this.controller.getMessageService().sendRequest(req, reqOpts);
        };
        /**
         * We use a separate function for subscription here because the result is VERY different. The results from this function
         * is not actually sorted, it is just the contents of the ranked list so we have to sort it ourselves.
         */
        Model.prototype.rankedListSubscription = function (feed, options) {
            var req = new Infront.RankedListSubscriptionRequest();
            req.feed = feed;
            req.sort_order = options.sortOrder;
            var subOpts = new Infront.SubscriptionOptions();
            subOpts.onData = function (data) {
                if (data.hasOwnProperty("updates")) {
                    options.onData(data["updates"]);
                }
                else {
                    InfrontUtil.errorLogCallback(404, "Empty update");
                }
            };
            subOpts.onError = options.onError;
            subOpts.onSuccess = function (data) { };
            var unsub = this.controller.getMessageService().sendSubscriptionRequest(req, subOpts);
            return function () {
                var unsubOpts = new RequestOptions();
                unsubOpts.onError = InfrontUtil.errorLogCallback;
                unsubOpts.onSuccess = function (data) { };
                unsub(unsubOpts);
            };
        };
        Model.prototype.chain = function (chainName, feedNumber, options) {
            if (typeof (chainName) != "undefined" && typeof (feedNumber) != "undefined" && InfrontUtil.isObject(options)) {
                var request = new Infront.GetChainRequest();
                request.chain = chainName;
                request.feed = feedNumber;
                request.access = "HISTORICAL";
                if (typeof (options.provider) != "undefined") {
                    request.provider = options.provider;
                }
                if (typeof (options.homeProvider) != "undefined") {
                    request.home_provider = options.homeProvider;
                }
                var reqOpts = new RequestOptions();
                reqOpts.onError = options.onError;
                reqOpts.onSuccess = function (result) {
                    if (!result.hasOwnProperty("items") || result["items"].length == 0) {
                        options.onError(404, "Empty Chain");
                    }
                    else {
                        options.onData(result);
                    }
                };
                this.controller.getMessageService().sendRequest(request, reqOpts);
            }
        };
        Model.prototype.chains = function (feed, options) {
            var request = new Infront.GetChainsRequest();
            request.feed = feed;
            if (options.hasOwnProperty("homeProvider")) {
                request.home_provider = options.homeProvider;
            }
            if (options.hasOwnProperty("provider")) {
                request.provider = options.provider;
            }
            if (options.types) {
                request.types = options.types;
            }
            if (options.hasOwnProperty("tree")) {
                request.tree = options.tree;
            }
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                if (result.hasOwnProperty("chains") && result["chains"].length > 0) {
                    options.onData(result["chains"]);
                }
                else if (result.hasOwnProperty("chain_tree") && result["chain_tree"]["nodes"].length > 0) {
                    options.onData(result["chain_tree"]["nodes"]);
                }
                else {
                    options.onError(404, "No chains found");
                }
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.referenceData = function (options) {
            if (options.feedNumber || options.instrument) {
                var request = new Infront.GetReferenceDataRequest();
                if (options.feedNumber) {
                    request.feed = options.feedNumber;
                }
                if (options.instrument) {
                    request.instrument = options.instrument;
                }
                var reqOpts = new RequestOptions();
                reqOpts.onError = options.onError;
                reqOpts.onSuccess = function (result) {
                    options.onData(result);
                };
                this.controller.getMessageService().sendRequest(request, reqOpts);
            }
        };
        Model.prototype.newsSources = function (options) {
            var request = new Infront.GetNewsSourcesRequest();
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                if (result.hasOwnProperty("sources")) {
                    options.onData(result);
                }
                else {
                    options.onError(404, "No news sources found");
                }
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.alertsData = function (options) {
            var request = new Infront.GetAlertsRequest();
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                options.onData(result["alerts"]);
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.addAlert = function (options) {
            var request = new Infront.AddAlertRequest();
            request.id = InfrontUtil.makeUUID();
            request.type = options.type;
            request.comment = options.comment;
            request.rule = options.rule;
            request.trigger_type = options.trigger_type;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.deleteAlert = function (options) {
            var request = new Infront.DeleteAlertRequest();
            request.alert_id = options.alert_id;
            request.index = options.index;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.modifyAlert = function (options) {
            var request = new Infront.ModifyAlertRequest();
            request.alert = options.alert;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.alerts = function (options) {
            var retval = function () { };
            var subReqeust = new Infront.SubscribeAlertsRequest(InfrontUtil.makeUUID());
            var subOptions = new Infront.SubscriptionOptions();
            subOptions.onError = options.onError;
            subOptions.onSuccess = function (result) {
            };
            subOptions.onData = function (data) {
                options.onData(data);
            };
            var unsubFunc = this.controller.getMessageService().sendSubscriptionRequest(subReqeust, subOptions);
            var unsubOpts = new RequestOptions();
            unsubOpts.onError = options.onError;
            unsubOpts.onSuccess = function () { };
            retval = function () {
                unsubFunc(unsubOpts);
            };
            return retval;
        };
        Model.prototype.newsItems = function (options) {
            var request = new Infront.GetNewsItemsRequest();
            if (options.feedNumbers) {
                request.news_feeds = options.feedNumbers;
            }
            if (options.startTime) {
                request.start_time = options.startTime.toISOString();
            }
            if (options.endTime) {
                request.end_time = options.endTime.toISOString();
            }
            request.max_items = options.maxItems;
            request.instrument = options.instrument;
            request.offset = options.offset;
            request.regions = options.regions;
            if (options.types) {
                request.types = options.types;
            }
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                if (result.hasOwnProperty("items")) {
                    var items = result["items"];
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].hasOwnProperty(Infront.MWSConstants.type) && Infront.MWSNewsType.typeMap.hasOwnProperty(items[i][Infront.MWSConstants.type])) {
                            items[i][Infront.MWSConstants.type] = Infront.MWSNewsType.typeMap[items[i][Infront.MWSConstants.type]];
                        }
                        else {
                            items[i][Infront.MWSConstants.type] = Infront.NewsType.Regular;
                        }
                    }
                    options.onData(result["items"]);
                }
                else {
                    options.onData([]);
                }
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.subscribeNewsItems = function (options) {
            var retVal = function () { };
            var request = new Infront.SubscribeNewsRequest(InfrontUtil.makeUUID());
            if (options.instrument) {
                request.instruments = [options.instrument];
            }
            if (options.feedNumbers) {
                request.news_feeds = options.feedNumbers;
            }
            if (options.types) {
                request.types = options.types;
            }
            if (options.regions) {
                request.regions = options.regions;
            }
            var subOptions = new Infront.SubscriptionOptions();
            subOptions.onError = options.onError;
            subOptions.onSuccess = function (result) {
            };
            subOptions.onData = function (data) {
                if (data.hasOwnProperty(Infront.MWSConstants.type) && Infront.MWSNewsType.typeMap.hasOwnProperty(data[Infront.MWSConstants.type])) {
                    data[Infront.MWSConstants.type] = Infront.MWSNewsType.typeMap[data[Infront.MWSConstants.type]];
                }
                else {
                    data[Infront.MWSConstants.type] = Infront.NewsType.Regular;
                }
                options.onData(data);
            };
            var unsubFunc = this.controller.getMessageService().sendSubscriptionRequest(request, subOptions);
            var unsubOpts = new RequestOptions();
            unsubOpts.onError = options.onError;
            unsubOpts.onSuccess = function () { };
            retVal = function () {
                unsubFunc(unsubOpts);
            };
            return retVal;
        };
        Model.prototype.newsItem = function (feed, id, options) {
            var request = new Infront.GetNewsBodyRequest();
            request.news_feed = feed;
            request.news_id = id;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                var b64 = result["body"];
                var decoder = new Infront.Base64();
                var data = {};
                data["mime_type"] = result["mime_type"];
                data["body"] = decoder.decode(b64);
                options.onData(data);
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.lookupInstrument = function (instrument, options) {
            var opts = new SearchOptions();
            opts.useAccesses = true;
            opts.onData = function (data) {
                var results = data;
                if (results.length > 0 && results[0].hasOwnProperty("type") && results[0]["type"] == "INSTRUMENT") {
                    options.onData(results[0]["instrument"]);
                }
                else {
                    options.onError(404, "Not found");
                }
            };
            opts.onError = options.onError;
            this.search(instrument, opts);
        };
        Model.prototype.search = function (query, options) {
            var request = (options.infinancialsSearch) ? new Infront.SearchRequest() : new Infront.InstrumentSearchRequest();
            if (InfrontUtil.isString(query)) {
                request.query = query;
            }
            else {
                request.instruments = [query];
            }
            if (options.instrumentTypes) {
                request.instrument_types = options.instrumentTypes;
            }
            if (options.maxResults) {
                request.max_results = options.maxResults;
            }
            if (options.itemTypes) {
                request.item_types = options.itemTypes;
            }
            if (options.feeds) {
                request.feeds = options.feeds;
            }
            if (options.ticker) {
                request.ticker = options.ticker;
            }
            if (options.useAccesses) {
                request.use_accesses = options.useAccesses;
            }
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                if (result.hasOwnProperty("items")) {
                    options.onData(result["items"]);
                }
                else if (result.hasOwnProperty("results")) {
                    options.onData(result["results"]);
                }
                else {
                    options.onData([]);
                }
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.brokerStats = function (periode, options) {
            var request = new Infront.BrokerStatsRequest();
            if (options.broker) {
                request.broker = options.broker;
            }
            if (options.feed) {
                request.feed = options.feed;
            }
            if (options.instrument) {
                request.instrument = options.instrument;
            }
            request.period = periode;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                if (result.hasOwnProperty("brokers")) {
                    options.onData(result["brokers"]);
                }
                else {
                    options.onData([]);
                }
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.tickSizes = function (feed, options) {
            var request = new Infront.TickSizesRequest();
            request.feed = feed;
            if (options.id) {
                request.id = options.id;
            }
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                if (result.hasOwnProperty("tick_sizes")) {
                    options.onData(result["tick_sizes"]);
                }
                else {
                    options.onData([]);
                }
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.marketLists = function (options) {
            var request = new Infront.MarketListRequest();
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (data) {
                if (data) {
                    options.onData(data["markets"]);
                }
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.calendar = function (options) {
            var request = new Infront.FinancialCalendarRequest();
            request.sources = options.feeds;
            request.countries = options.countries;
            request.instrument = options.instrument;
            if (options.startDate) {
                request.start_date = InfrontUtil.getApiDateString(options.startDate);
            }
            if (options.endDate) {
                request.end_date = InfrontUtil.getApiDateString(options.endDate);
            }
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                if (result.hasOwnProperty("events")) {
                    options.onData(result["events"]);
                }
                else {
                    options.onError(404, "Events not found");
                }
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.tradesByDate = function (instrument, options) {
            var defaults = new TradesByDateOptions();
            defaults.resolution = Infront.Resolution.MINUTES;
            defaults.stepSize = 1;
            var useOptions = InfrontUtil.mergeRecursive(defaults, options);
            var request = new Infront.TradesByDateRequest();
            request.instrument = instrument;
            if (useOptions.startDate) {
                request.start_date = InfrontUtil.getApiDateString(useOptions.startDate);
            }
            if (useOptions.endDate) {
                request.end_date = InfrontUtil.getApiDateString(useOptions.endDate);
            }
            if (InfrontUtil.validatePropertyExists(Infront.Resolution, useOptions.resolution)) {
                request.resolution = useOptions.resolution;
            }
            request.step_size = useOptions.stepSize;
            request.fields = useOptions.fields;
            var reqOpts = new RequestOptions();
            reqOpts.onError = useOptions.onError;
            reqOpts.onSuccess = function (result) {
                if (result.hasOwnProperty("trades")) {
                    options.onData(result["trades"]);
                }
                else {
                    options.onError(404, "Data not found");
                }
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.tradesByDays = function (instrument, options) {
            var defaults = new TradesByDaysOptions();
            defaults.resolution = Infront.Resolution.MINUTES;
            defaults.stepSize = 1;
            defaults.days_back = 1;
            defaults.fields = ["LAST"];
            var useOptions = InfrontUtil.mergeRecursive(defaults, options);
            var request = new Infront.TradesByDaysRequest();
            request.instrument = instrument;
            request.days_back = useOptions.days_back;
            request.num_days = useOptions.num_days;
            request.fields = useOptions.fields;
            request.resolution = useOptions.resolution;
            request.step_size = useOptions.stepSize;
            var reqOpts = new RequestOptions();
            reqOpts.onError = useOptions.onError;
            reqOpts.onSuccess = function (result) {
                if (result.hasOwnProperty("trades")) {
                    options.onData(result);
                }
                else if (options.onError) {
                    options.onError(404, "Data not found");
                }
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.historicalTrades = function (instrument, options) {
            var request = new Infront.HistoricalTradesRequest();
            if (options.start_date) {
                request.start_date = InfrontUtil.getApiDateString(options.start_date);
            }
            if (options.end_date) {
                request.end_date = InfrontUtil.getApiDateString(options.end_date);
            }
            request.fields = options.fields;
            request.instrument = instrument;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                if (result.hasOwnProperty("trades")) {
                    options.onData(result["trades"]);
                }
                else {
                    options.onError(404, "Data not found");
                }
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.companyHistory = function (instrument, options) {
            var request = new Infront.CompanyHistoryRequest();
            request.instrument = instrument;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.instrumentIntradayTrades = function (instrument, options) {
            var request = new Infront.InstrumentIntradayTradesRequest();
            if (options.items) {
                request.num_trades = options.items;
            }
            if (options.offset) {
                request.offset = options.offset;
            }
            if (options.hasOwnProperty("highestSeqenceId")) {
                request.highest_seq_id = options.highestSeqenceId;
            }
            if (options.sortBy) {
                request.sort_by = options.sortBy;
            }
            request.instrument = instrument;
            var reqOpts = new RequestOptions();
            reqOpts.onError = function (error_code, error_message) {
                options.onError;
            };
            reqOpts.onSuccess = function (result) {
                if (result.hasOwnProperty("trades") && options.onData) {
                    options.onData(result["trades"]);
                }
                else if (!options.onData) {
                    options.onError(0, "IntradayTrades missing opts.onData");
                }
                else {
                    options.onError(404, "Data not found");
                }
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.instrumentIntradayTradesOld = function (instrument, options) {
            var defaults = new InstrumentIntradayTradesOptions();
            defaults.streaming = true;
            var mergedOptions = InfrontUtil.mergeRecursive(defaults, options);
            //Functions that potentially subscribes to something returns an unsubscribe-function.
            var retVal = function () { };
            var keyframeReady = false;
            var request = new Infront.InstrumentIntradayTradesRequest();
            if (mergedOptions.items) {
                request.num_trades = mergedOptions.items;
            }
            if (mergedOptions.offset) {
                request.offset = mergedOptions.offset;
            }
            if (mergedOptions.hasOwnProperty("highestSeqenceId")) {
                request.highest_seq_id = mergedOptions.highestSeqenceId;
            }
            if (mergedOptions.sortBy) {
                request.sort_by = mergedOptions.sortBy;
            }
            request.instrument = instrument;
            var reqOpts = new RequestOptions();
            reqOpts.onError = function (error_code, error_message) {
                mergedOptions.onError;
                keyframeReady = true;
            };
            reqOpts.onSuccess = function (result) {
                keyframeReady = true;
                if (result.hasOwnProperty("trades")) {
                    mergedOptions.onData(result["trades"]);
                }
                else {
                    mergedOptions.onError(404, "Data not found");
                }
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
            //Streaming is only available if we sort by TIME_DESC. We check for the offset-property, since if this is
            //a paging-request we don't stream.
            if (this.controller.isStreaming() && mergedOptions.streaming && mergedOptions.sortBy == "TIME_DESC" && !mergedOptions.hasOwnProperty("offset")) {
                var subRequest = new InstrumentSubscriptionRequest();
                subRequest.setInstrument(instrument);
                subRequest.setFields(["TRADES"]);
                if (mergedOptions.items) {
                    subRequest.num_trades = mergedOptions.items;
                }
                var subOpts = new Infront.SubscriptionOptions();
                subOpts.onError = mergedOptions.onError;
                subOpts.onSuccess = function () { };
                subOpts.onData = function (data) {
                    if (keyframeReady && data.hasOwnProperty("trades")) {
                        mergedOptions.onData(data["trades"]);
                    }
                };
                var unsubFunc = this.controller.getMessageService().sendSubscriptionRequest(subRequest, subOpts);
                var unsubOpts = new RequestOptions();
                unsubOpts.onError = options.onError;
                unsubOpts.onSuccess = function () { };
                retVal = function () {
                    unsubFunc(unsubOpts);
                };
            }
            return retVal;
        };
        Model.prototype.marketActivity = function (feed, options) {
            var retVal = null;
            if (options.streaming && this.isStreaming()) {
                var req = new Infront.SubscribeMarketActivityRequest(Infront.MessageService.getUniqueRequestData());
                req.feed = feed;
                if (options.chain) {
                    req.chain = options.chain;
                }
                var subOpts = new Infront.SubscriptionOptions();
                subOpts.onData = options.onData;
                subOpts.onError = options.onError;
                subOpts.onSuccess = function (data) { };
                var unsub = this.controller.getMessageService().sendSubscriptionRequest(req, subOpts);
                retVal = function () {
                    var unsubOpts = new RequestOptions();
                    unsubOpts.onSuccess = function (data) { };
                    unsubOpts.onError = InfrontUtil.errorLogCallback;
                    unsub(unsubOpts);
                };
            }
            else {
                var request = new Infront.MarketActivityRequest();
                request.feed = feed;
                if (options.chain) {
                    request.chain = options.chain;
                }
                var reqOpts = new RequestOptions();
                reqOpts.onError = options.onError;
                reqOpts.onSuccess = options.onData;
                this.controller.getMessageService().sendRequest(request, reqOpts);
                retVal = function () { };
            }
            return retVal;
        };
        Model.prototype.userLists = function (options) {
            var request = new Infront.GetListNamesRequest();
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (data) {
                if (data.hasOwnProperty("lists")) {
                    options.onData(data["lists"]);
                }
                else {
                    options.onError(404, "Data not found");
                }
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.userListContents = function (listName, options) {
            var request = new Infront.GetUserListRequest();
            request.list = listName;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (data) {
                if (data.hasOwnProperty("items")) {
                    options.onData(data["items"]);
                }
                else {
                    options.onError(404, "No data found");
                }
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.saveUserList = function (listName, contents, options) {
            var request = new Infront.SaveUserListRequest();
            request.list = listName;
            request.items = contents;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.deleteUserList = function (listName, options) {
            var request = new Infront.DeleteUserListRequest();
            request.list = listName;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.infinancialsUrl = function (options) {
            var request = new Infront.GetInfinancialsUrlRequest();
            request.instrument = options.instrument;
            request.widgets = options.widgets;
            request.width = options.width;
            request.height = options.height;
            request.dark_theme = options.darkTheme;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (data) {
                if (data.hasOwnProperty(Infront.MWSConstants.url)) {
                    options.onData(data[Infront.MWSConstants.url]);
                }
                else {
                    options.onError(404, "Object not found");
                }
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.trGetPortfolioNames = function (options) {
            var request = new Infront.TRGetPortfolioNamesRequest();
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.prototype.trGetTradingPower = function (portfolioName, options) {
            var request = new Infront.TRGetTradingPowerRequest();
            request.portfolio = portfolioName;
            if (options.instrument) {
                request.instrument = options.instrument;
            }
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        /**
         * Requests data about a portfolio (positions, values, etc.) and, if trading-streaming is on, subscribes to the same data.
         *
         * @param portfolioName
         * @param options
         * @returns {function(): void}
         */
        Model.prototype.trPortfolio = function (portfolioName, options) {
            var unsubFunc = function () { };
            var req = new Infront.TRGetPortfolioRequest();
            req.portfolio = portfolioName;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(req, reqOpts);
            if (this.controller.isTradingStreaming()) {
                var subReq = new Infront.TRSubscribePortfolioRequest();
                subReq.portfolios = [portfolioName];
                var subReqOpts = new Infront.SubscriptionOptions();
                subReqOpts.onData = options.onData;
                subReqOpts.onError = options.onError;
                subReqOpts.onSuccess = function () { };
                var ctrlUnsubFunc = this.controller.getMessageService().sendSubscriptionRequest(subReq, subReqOpts);
                var unsubReqOpts = new RequestOptions();
                unsubReqOpts.onSuccess = function (result) { };
                unsubReqOpts.onError = function (error_code, error_message) { return 0; };
                unsubFunc = function () {
                    ctrlUnsubFunc(unsubReqOpts);
                };
            }
            return unsubFunc;
        };
        Model.prototype.trOrders = function (portfolioName, inpOptions) {
            var defaults = new DataOptions();
            defaults.streaming = true;
            var options = InfrontUtil.mergeRecursive(defaults, inpOptions);
            var unsubFunc = function () { };
            var req = new Infront.TRGetOrdersRequest();
            req.portfolio = portfolioName;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(req, reqOpts);
            if (options.streaming && this.controller.isTradingStreaming()) {
                var subReq = new Infront.TRSubsribeOrdersRequest();
                subReq.portfolios = [portfolioName];
                var subReqOpts = new Infront.SubscriptionOptions();
                subReqOpts.onData = options.onData;
                subReqOpts.onError = options.onError;
                subReqOpts.onSuccess = function () { };
                var ctrlUnsubFunc = this.controller.getMessageService().sendSubscriptionRequest(subReq, subReqOpts);
                var unsubReqOpts = new RequestOptions();
                unsubReqOpts.onSuccess = function (result) { };
                unsubReqOpts.onError = function (error_code, error_message) { return 0; };
                unsubFunc = function () {
                    ctrlUnsubFunc(unsubReqOpts);
                };
            }
            return unsubFunc;
        };
        Model.prototype.trTrades = function (portfolioName, options) {
            var unsubFunc = function () { };
            var req = new Infront.TRGetTradesRequest();
            req.portfolio = portfolioName;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(req, reqOpts);
            if (this.controller.isTradingStreaming()) {
                var subReq = new Infront.TRSubscribeTradesRequest();
                subReq.portfolios = [portfolioName];
                var subReqOpts = new Infront.SubscriptionOptions();
                subReqOpts.onData = options.onData;
                subReqOpts.onError = options.onError;
                subReqOpts.onSuccess = function () { };
                var ctrlUnsubFunc = this.controller.getMessageService().sendSubscriptionRequest(subReq, subReqOpts);
                var unsubReqOpts = new RequestOptions();
                unsubReqOpts.onSuccess = function (result) { };
                unsubReqOpts.onError = function (error_code, error_message) { return 0; };
                unsubFunc = function () {
                    ctrlUnsubFunc(unsubReqOpts);
                };
            }
            return unsubFunc;
        };
        Model.prototype.trNetTrades = function (portfolioName, options) {
            var unsubFunc = function () { };
            var req = new Infront.TRGetNetTradesRequest();
            req.portfolio = portfolioName;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(req, reqOpts);
            if (this.controller.isTradingStreaming()) {
                var subReq = new Infront.TRSubscribeNetTradesRequest();
                subReq.portfolios = [portfolioName];
                var subReqOpts = new Infront.SubscriptionOptions();
                subReqOpts.onData = options.onData;
                subReqOpts.onError = options.onError;
                subReqOpts.onSuccess = function () { };
                var ctrlUnsubFunc = this.controller.getMessageService().sendSubscriptionRequest(subReq, subReqOpts);
                var unsubReqOpts = new RequestOptions();
                unsubReqOpts.onSuccess = function (result) { };
                unsubReqOpts.onError = function (error_code, error_message) { return 0; };
                unsubFunc = function () {
                    ctrlUnsubFunc(unsubReqOpts);
                };
            }
            return unsubFunc;
        };
        Model.prototype.trInsertOrder = function (options) {
            var req = new Infront.TRInsertOrderRequest();
            req.instrument = options.instrument;
            req.portfolio = options.portfolio;
            req.buy_or_sell = options.buyOrSell == Infront.BuyOrSell.Buy ? Infront.MWSBuyOrSell.BUY : Infront.MWSBuyOrSell.SELL;
            req.price = options.price;
            req.volume = options.volume;
            req.active_order = options.activeOrder;
            if (options.orderType) {
                req.order_type = options.orderType;
            }
            if (options.validUntil) {
                req.valid_until = InfrontUtil.getApiDateString(options.validUntil);
            }
            if (typeof (options.openVolume) == "number") {
                req.open_volume = options.openVolume;
            }
            if (options.algoId && options.algoParams) {
                req.algo_id = options.algoId;
                req.algo_params = options.algoParams;
            }
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(req, reqOpts);
            //console.log(JSON.stringify(req, null, "   "));
        };
        Model.prototype.trDeleteOrder = function (portfolio, orderId, options) {
            var req = new Infront.TRDeleteOrderRequest();
            req.order_id = orderId;
            req.portfolio = portfolio;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(req, reqOpts);
        };
        Model.prototype.trModifyOrder = function (portfolio, orderId, options) {
            var req = new Infront.TRModifyOrderRequest();
            req.portfolio = portfolio;
            req.order_id = orderId;
            if (options.hasOwnProperty("price")) {
                req.price = options.price;
            }
            if (options.hasOwnProperty("volume")) {
                req.volume = options.volume;
            }
            if (typeof (options.openVolume) == "number") {
                req.open_volume = options.openVolume;
            }
            if (typeof (options.activeOrder) == "boolean") {
                req.active_order = options.activeOrder;
            }
            if (options.hasOwnProperty("validUntil")) {
                req.valid_until = InfrontUtil.getApiDateString(options.validUntil);
            }
            if (options.hasOwnProperty("customerReference")) {
                req.customer_reference = options.customerReference;
            }
            if (options.hasOwnProperty("comment")) {
                req.comment = options.comment;
            }
            if (options.hasOwnProperty("customTags")) {
                req.custom_tags = options.customTags;
            }
            if (options.hasOwnProperty("algoParams")) {
                req.algo_params = options.algoParams;
            }
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(req, reqOpts);
        };
        Model.prototype.trGetAlgos = function (options) {
            var req = new Infront.TRGetAlgosRequest();
            req.feed = options.feed;
            if (options.algoId) {
                req.algo_id = options.algoId;
            }
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(req, reqOpts);
        };
        Model.prototype.activateOrder = function (options) {
            var req = new Infront.TRActivateOrderRequest();
            req.portfolio = options.portfolio;
            req.order_id = options.order_id;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(req, reqOpts);
        };
        Model.prototype.deactivateOrder = function (options) {
            var req = new Infront.TRDeactivateOrderRequest();
            req.portfolio = options.portfolio;
            req.order_id = options.order_id;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = options.onData;
            this.controller.getMessageService().sendRequest(req, reqOpts);
        };
        //        public trSubscribeOrders(portfolio: string, options: DataOptions): () => void {
        //            var retVal = (): void => { }; //Empty unsubscribe by default;
        //
        //            if (this.controller.isTradingStreaming()) {
        //                var subRequest: TROrdersSubscriptionRequest = new TROrdersSubscriptionRequest();
        //                subRequest.portfolios[0] = portfolio;
        //
        //                var subReqOpts: SubscriptionOptions = new SubscriptionOptions();
        //                subReqOpts.onError = options.onError;
        //                subReqOpts.onSuccess = (result: Object): void => { };
        //                subReqOpts.onData = (data: Object): void => {
        //                    options.onData(data);
        //                }
        //
        //                var unsubOpts: RequestOptions = new RequestOptions();
        //                unsubOpts.onError = options.onError; //If this fails we might retry, but most likely it is because the original connection is down.
        //                unsubOpts.onSuccess = (result: Object): void => { }
        //
        //                var unsub = this.controller.getMessageService().sendSubscriptionRequest(subRequest, subReqOpts);
        //
        //                retVal = (): void => {
        //                    unsub(unsubOpts);
        //                };
        //            }
        //
        //            return retVal;
        //        }
        //        public trInsertOrderRaw(data: Object, options: DataOptions) {
        //            var request = <TRInsertOrderRequest>InfrontUtil.mergeRecursive(new TRInsertOrderRequest(), data, false);
        //
        //            var reqOpts = new RequestOptions();
        //            reqOpts.onError = options.onError;
        //            reqOpts.onSuccess = options.onData;
        //
        //            //console.log("INSERTING: " + JSON.stringify(request));
        //
        //            this.controller.getMessageService().sendRequest(request, reqOpts);
        //        }
        //        public trDeleteOrder(portfolio: string, orderId: number, options: DataOptions) {
        //            var request = new TRDeleteOrderRequest();
        //            request.portfolio = portfolio;
        //            request.order_id = orderId;
        //
        //            var reqOpts = new RequestOptions();
        //            reqOpts.onError = options.onError;
        //            reqOpts.onSuccess = options.onData;
        //
        //            this.controller.getMessageService().sendRequest(request, reqOpts);
        //        }
        //        public trGetOrders(portfolio: string, options: DataOptions) {
        //            var request = new TRGetOrdersRequest();
        //            request.portfolio = portfolio;
        //
        //            var reqOpts = new RequestOptions();
        //            reqOpts.onError = options.onError;
        //            reqOpts.onSuccess = options.onData;
        //
        //            //console.log(JSON.stringify(request));
        //
        //            this.controller.getMessageService().sendRequest(request, reqOpts);
        //        }
        //        public trGetTrades(portfolio: string, options: DataOptions) {
        //            var request = new TRGetTradesRequest();
        //            request.portfolio = portfolio;
        //
        //            var reqOpts = new RequestOptions();
        //            reqOpts.onError = options.onError;
        //            reqOpts.onSuccess = options.onData;
        //
        //            this.controller.getMessageService().sendRequest(request, reqOpts);
        //        }
        //        public trGetAlgos(feed: number, options: DataOptions) {
        //            var request = new TRGetAlgosRequest();
        //            request.feed = feed;
        //
        //            var reqOpts = new RequestOptions();
        //            reqOpts.onError = options.onError;
        //            reqOpts.onSuccess = options.onData;
        //
        //            this.controller.getMessageService().sendRequest(request, reqOpts);
        //        }
        Model.prototype.logOut = function () {
            this.controller.logOut();
        };
        /**
         * Generic Component data with all standards information request. */
        Model.prototype.componentData = function (options) {
            var request = new Infront.ComponentRequest();
            request.response = "json";
            request.id = options.id;
            request.company = options.company;
            if (options.peergroup != null)
                request.peergroup = options.peergroup;
            if (options.custom != null)
                request.custom = options.custom;
            if (options.currency != null)
                request.currency = options.currency;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                options.onData(result);
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        /**
         * Generic Component data with all standards information request. */
        Model.prototype.eurofinData = function (options) {
            var request = new Infront.EurofinRequest();
            request.response = "json";
            request.id = options.id;
            request.pgaction = options.action;
            if (options.company)
                request.company = options.company;
            if (options.name)
                request.pgname = options.name;
            if (options.entries)
                request.pgentries = options.entries;
            if (options.arr_names)
                request.pgnames = options.arr_names;
            if (options.arr_entries)
                request.pglistentries = options.arr_entries;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                options.onData(result);
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        /**
         * Generic ChartComponent data with all standards information request. */
        Model.prototype.chartComponentData = function (options) {
            var request = new Infront.ChartRequest();
            request.response = "json";
            request.id = options.id;
            request.company = options.company;
            if (options.peergroup != null)
                request.peergroup = options.peergroup;
            if (options.chart != null)
                request.chart = options.chart;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                options.onData(result);
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        /**
         * Generic ChartComponent data with all standards information request. */
        Model.prototype.evolutionComponentData = function (options) {
            var request = new Infront.EvolutionChartRequest();
            request.response = "json";
            request.id = options.id;
            request.company = options.company;
            if (options.peergroup != null)
                request.peergroup = options.peergroup;
            if (options.predefinedSeries != null)
                request.predefinedSeries = options.predefinedSeries;
            if (options.fields != null)
                request.fields = options.fields;
            if (options.periods != null)
                request.periods = options.periods;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                options.onData(result);
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        /**
         * Generic ChartComponent data with all standards information request. */
        Model.prototype.etaComponentData = function (options) {
            var request = new Infront.EtaRequest();
            request.response = "json";
            request.id = options.id;
            request.company = options.company;
            if (options.mode != null)
                request.mode = options.mode;
            if (options.view != null)
                request.view = options.view;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                options.onData(result);
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        /** SectorMomentum Data request: specific type of request, so we are using a dedicated method. */
        Model.prototype.sectorIndiceComponentData = function (options) {
            var request = new Infront.UniverseRequest();
            request.response = "json";
            request.id = options.id;
            if (options.index != null)
                request.index = options.index;
            if (options.region != null)
                request.region = options.region;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                options.onData(result);
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        /** Screener Data request: specific type of request, so we are using a dedicated method. */
        Model.prototype.leagueTablesData = function (options) {
            var request = new Infront.UniverseRequest();
            request.response = "json";
            request.id = options.id;
            request.type = options.type;
            if (options.index != null)
                request.index = options.index;
            if (options.sector != null)
                request.sector = options.sector;
            if (options.country != null)
                request.country = options.country;
            if (options.region != null)
                request.region = options.region;
            if (options.field != null)
                request.field = options.field;
            if (options.nbtop != null)
                request.nbtop = options.nbtop;
            if (options.nbbot != null)
                request.nbbot = options.nbbot;
            if (options.marketplace != null)
                request.marketplace = options.marketplace;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                options.onData(result);
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        /** Screener Data request: specific type of request, so we are using a dedicated method. */
        Model.prototype.screenerData = function (request, options) {
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                options.onData(result);
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        /**
         * Fundamentals data*/
        Model.prototype.fundamentalsData = function (options) {
            var request = new Infront.FundamentalsRequest();
            request.response = "json";
            request.id = options.id;
            request.company = options.company;
            if (options.mode != null)
                request.mode = options.mode;
            if (options.currency != null)
                request.currency = options.currency;
            var reqOpts = new RequestOptions();
            reqOpts.onError = options.onError;
            reqOpts.onSuccess = function (result) {
                options.onData(result);
            };
            this.controller.getMessageService().sendRequest(request, reqOpts);
        };
        Model.kDefaultBaseCurrency = "USD";
        return Model;
    }());
    Infront.Model = Model;
})(Infront || (Infront = {}));
/**
 * Created by hage on 15.08.2016.
 */
var Infront;
/**
 * Created by hage on 15.08.2016.
 */
(function (Infront) {
    var SubRequest = (function () {
        function SubRequest(fields) {
            this.instruments = [];
            this.numTrades = 0;
            this.fields = fields;
        }
        return SubRequest;
    }());
    var InstrumentFieldCounter = (function () {
        function InstrumentFieldCounter(instrument, instrumentKey) {
            this.fieldCounts = {};
            this.pendingSubFields = [];
            this.pendingUnsubFields = [];
            this.instrument = instrument;
            this.instrumentKey = instrumentKey;
        }
        InstrumentFieldCounter.prototype.checkIfFieldShoulSubOrUnsub = function (field, oldValue) {
            if (this.fieldCounts[field] >= 1 && oldValue == 0) {
                var unsubIndex = this.pendingUnsubFields.indexOf(field);
                if (unsubIndex == -1) {
                    this.pendingSubFields.push(field); //Not pending for unsub so we add to subQueue
                }
                else {
                    this.pendingUnsubFields.splice(unsubIndex, 1); //If pending for unsub just remove from pending unsubs
                }
            }
            else if (this.fieldCounts[field] == 0 && oldValue > 0) {
                var subIndex = this.pendingSubFields.indexOf(field);
                if (subIndex == -1) {
                    this.pendingUnsubFields.push(field); //Not pending for sub so we add to unsubQueue
                }
                else {
                    this.pendingSubFields.splice(subIndex, 1); //If pending for subscription just remove so we never subscribe
                }
                delete this.fieldCounts[field];
            }
        };
        InstrumentFieldCounter.prototype.addFieldCount = function (field, count) {
            if (!this.fieldCounts.hasOwnProperty(field)) {
                this.fieldCounts[field] = 0;
            }
            var oldValue = this.fieldCounts[field];
            this.fieldCounts[field] += count;
            this.checkIfFieldShoulSubOrUnsub(field, oldValue);
            if (this.fieldCounts[field] > 10)
                InfrontUtil.errorLog("Warning: More than 10 subs for " + this.instrument.ticker + " / " + field, this);
        };
        /**
         * Gets a list of subscribed fields for this instrument
         * Remember to call clearPendingSubscribeFields if sending subscribes
         * @param onlyPending Whether to only get fields not previously subscribed or all fields needed.
         */
        InstrumentFieldCounter.prototype.getSubscribeFields = function (onlyPending) {
            if (onlyPending === void 0) { onlyPending = true; }
            var returnFields = [];
            if (onlyPending) {
                returnFields = this.pendingSubFields.slice();
            }
            else {
                for (var fieldCount in this.fieldCounts) {
                    returnFields.push(fieldCount);
                }
            }
            return returnFields;
        };
        InstrumentFieldCounter.prototype.getPendingUnsubscribeFields = function () {
            var pendingFieldsCopy = [];
            pendingFieldsCopy = this.pendingUnsubFields.slice();
            return pendingFieldsCopy;
        };
        InstrumentFieldCounter.prototype.clearPendingSubscribeFields = function () {
            this.pendingSubFields = [];
        };
        InstrumentFieldCounter.prototype.clearPendingUnsubscribeFields = function () {
            this.pendingUnsubFields = [];
        };
        return InstrumentFieldCounter;
    }());
    var SubscriptionCounter = (function () {
        function SubscriptionCounter() {
            this.subscriptionCounts = {};
        }
        SubscriptionCounter.prototype.getInstrumentCountObj = function (instrument) {
            var instrumentKey = Infront.CacheKeyFactory.createInstrumentKey(instrument);
            if (instrumentKey) {
                if (!this.subscriptionCounts.hasOwnProperty(instrumentKey)) {
                    this.subscriptionCounts[instrumentKey] = new InstrumentFieldCounter(instrument, instrumentKey);
                }
                return this.subscriptionCounts[instrumentKey];
            }
            else
                return null;
        };
        SubscriptionCounter.prototype.addCountFor = function (instruments, fields, count) {
            if (count === void 0) { count = 1; }
            for (var _i = 0, instruments_1 = instruments; _i < instruments_1.length; _i++) {
                var instrument = instruments_1[_i];
                var instrumentCounter = this.getInstrumentCountObj(instrument);
                if (instrumentCounter) {
                    for (var _a = 0, fields_1 = fields; _a < fields_1.length; _a++) {
                        var field = fields_1[_a];
                        instrumentCounter.addFieldCount(field, count);
                    }
                }
            }
        };
        SubscriptionCounter.prototype.getSubscriptionRequests = function (onlyPending) {
            if (onlyPending === void 0) { onlyPending = true; }
            var subRequests = {};
            var fields;
            for (var subCountKey in this.subscriptionCounts) {
                var instrumentCounter = this.subscriptionCounts[subCountKey];
                fields = instrumentCounter.getSubscribeFields(onlyPending);
                instrumentCounter.clearPendingSubscribeFields();
                this.addFieldInstrumentToRequestDict(subRequests, fields, instrumentCounter.instrument);
            }
            return subRequests;
        };
        SubscriptionCounter.prototype.addFieldInstrumentToRequestDict = function (reqDictionary, fields, instrument) {
            if (fields.length > 0) {
                var fieldsKey = fields.join("");
                if (!reqDictionary[fieldsKey]) {
                    reqDictionary[fieldsKey] = new SubRequest(fields);
                }
                reqDictionary[fieldsKey].instruments.push(instrument);
            }
        };
        SubscriptionCounter.prototype.getUnsubscribeRequests = function () {
            var unsubRequests = {};
            var fields;
            for (var subCountKey in this.subscriptionCounts) {
                var instrumentCounter = this.subscriptionCounts[subCountKey];
                fields = instrumentCounter.getPendingUnsubscribeFields();
                instrumentCounter.clearPendingUnsubscribeFields();
                this.addFieldInstrumentToRequestDict(unsubRequests, fields, instrumentCounter.instrument);
            }
            return unsubRequests;
        };
        return SubscriptionCounter;
    }());
    var InstrumentManager = (function () {
        function InstrumentManager(messageService, infront) {
            var _this = this;
            this.nextTradeNumReq = 0;
            this.delayedSubscribeTimerID = null;
            this.delayedUnsubscribeTimerID = null;
            this.subscriptionCounter = new SubscriptionCounter();
            this.infront = infront;
            this.cache = infront.getCache();
            this.messageService = messageService;
            messageService.registerSpecialEventObserver(Infront.MWSConstants.md_instrument_update, function (data) { _this.instrumentUpdate(data); });
            messageService.onMarketdataReconnect = function () { _this.resubscribeAllInstruments(); };
            this.observers = {};
            this.subscriptionQueues = {};
            this.orderbookMaxvolumes = {};
        }
        InstrumentManager.prototype.registerObserver = function (instruments, callback) {
            for (var i = 0; i < instruments.length; i++) {
                var id = Infront.CacheKeyFactory.createInstrumentKey(instruments[i]);
                if (!this.observers.hasOwnProperty(id)) {
                    this.observers[id] = [];
                }
                this.observers[id].push(callback);
            }
        };
        InstrumentManager.prototype.unregisterObserver = function (instruments, callback) {
            for (var i = 0; i < instruments.length; i++) {
                var id = Infront.CacheKeyFactory.createInstrumentKey(instruments[i]);
                if (this.observers.hasOwnProperty(id)) {
                    var cbs = this.observers[id];
                    for (var j = 0; j < cbs.length; j++) {
                        if (cbs[j] === callback) {
                            cbs.splice(j, 1);
                            break;
                        }
                    }
                }
            }
        };
        InstrumentManager.prototype.delayedSubscribeExecute = function () {
            var subrequests = this.subscriptionCounter.getSubscriptionRequests(true);
            for (var subrequestKey in subrequests) {
                var subrequest = subrequests[subrequestKey];
                var fields = subrequest.fields;
                if (this.hasFundDetailsFields(fields)) {
                    this.requestFundDetails(subrequest.instruments);
                }
                if (this.infront.isStreaming()) {
                    this.loadTGWFields(subrequest.instruments, subrequest.fields);
                    this.sendSubscribeRequest(subrequest.instruments, subrequest.fields, true, this.nextTradeNumReq);
                }
                else {
                    this.snapshot(subrequest.instruments, fields, true, this.instrumentUpdate, this.nextTradeNumReq);
                }
            }
        };
        InstrumentManager.prototype.sendSubscribeRequest = function (instruments, fields, guarantee, numTrades) {
            var _this = this;
            if (guarantee === void 0) { guarantee = false; }
            if (numTrades === void 0) { numTrades = InstrumentManager.kDefaultNumTrades; }
            fields = this.removeNonSubscriptionFields(fields);
            if (fields.length == 0) {
                fields.push(InfrontConstants.Fields.LAST);
            }
            var sReq = new Infront.InstrumentSubscriptionRequest();
            sReq.instruments = instruments;
            sReq.fields = fields;
            if (this.hasTradesField(fields)) {
                sReq.num_trades = numTrades || InstrumentManager.kDefaultNumTrades;
            }
            //If we subscribe to TRADES, we need to create an array in the cache to house them
            if (sReq.fields.indexOf(Infront.QuoteFields.TRADES) > -1) {
                this.createTradesCache(sReq.instruments);
            }
            //Same goes for orderbook
            if (sReq.fields.indexOf(Infront.QuoteFields.ORDERBOOK) > -1) {
                this.createOrderbookCache(sReq.instruments);
            }
            if (sReq.fields.length > 0) {
                var reqOpts = new Infront.RequestOptions();
                if (guarantee) {
                    reqOpts.onError = function (error_code, error_message) {
                        for (var i = 0; i < instruments.length; i++) {
                            _this.cache.updateCacheTimestamp(Infront.CacheKeyFactory.createInstrumentKey(instruments[i]));
                        }
                        InfrontUtil.errorLogCallback(error_code, error_message);
                    };
                }
                else {
                    reqOpts.onError = InfrontUtil.errorLogCallback;
                }
                reqOpts.onSuccess = function (data) {
                };
                this.messageService.instrumentSubscribe(sReq, reqOpts);
            }
        };
        InstrumentManager.prototype.subscribe = function (instruments, fields, guarantee, callback, numTrades) {
            var _this = this;
            if (guarantee === void 0) { guarantee = false; }
            if (numTrades === void 0) { numTrades = 0; }
            0;
            this.subscriptionCounter.addCountFor(instruments, fields, 1);
            this.nextTradeNumReq = Math.max(this.nextTradeNumReq, numTrades);
            if (this.delayedSubscribeTimerID) {
                clearTimeout(this.delayedSubscribeTimerID);
            }
            this.delayedSubscribeTimerID = setTimeout(function () {
                _this.delayedSubscribeExecute();
                _this.delayedSubscribeTimerID = null;
                _this.nextTradeNumReq = 0;
            }, InstrumentManager.kBunchingDelay);
            if (this.infront.isStreaming() && callback) {
                this.registerObserver(instruments, callback);
            }
        };
        InstrumentManager.prototype.createTradesCache = function (instruments) {
            for (var i = 0; i < instruments.length; i++) {
                var key = Infront.CacheKeyFactory.createTradesArrayKey(instruments[i]);
                this.cache.createArray(key, null, function (a, b) {
                    return b[Infront.MWSConstants.seq_id] - a[Infront.MWSConstants.seq_id];
                }, false);
            }
        };
        InstrumentManager.prototype.createOrderbookCache = function (instruments) {
            for (var i = 0; i < instruments.length; i++) {
                var bKey = Infront.CacheKeyFactory.createBidsArrayKey(instruments[i]);
                var aKey = Infront.CacheKeyFactory.createAsksArrayKey(instruments[i]);
                this.cache.createArray(bKey, null, compareOrderbookLevels);
                this.cache.createArray(aKey, null, compareOrderbookLevels);
                //this.cache.createArray(CacheKeyFactory.createMaxVolumesArrayKey(instruments[i]), null);
            }
        };
        InstrumentManager.prototype.requestFundDetails = function (instruments) {
            var _this = this;
            var _loop_1 = function () {
                var instrument = instruments[i];
                var key = Infront.CacheKeyFactory.createInstrumentKey(instrument);
                var request = new Infront.FundDetailsRequest();
                request.instrument = instrument;
                reqOpts = new Infront.RequestOptions();
                reqOpts.onError = InfrontUtil.errorLogCallback;
                reqOpts.onSuccess = function (result) {
                    var wrapper = {};
                    wrapper[InfrontConstants.FundDetailsMap.ROOT_OBJECT] = JSON.parse(result["details_as_json"]);
                    _this.cache.update(key, wrapper);
                };
                this_1.messageService.sendRequest(request, reqOpts);
            };
            var this_1 = this, reqOpts;
            for (var i = 0; i < instruments.length; i++) {
                _loop_1();
            }
        };
        InstrumentManager.prototype.resubscribeAllInstruments = function () {
            if (this.infront.isStreaming()) {
                var subrequests = this.subscriptionCounter.getSubscriptionRequests(false);
                for (var subrequestKey in subrequests) {
                    var subrequest = subrequests[subrequestKey];
                    var fields = subrequest.fields;
                    this.sendSubscribeRequest(subrequest.instruments, subrequest.fields, false, InstrumentManager.kDefaultNumTrades);
                }
            }
        };
        InstrumentManager.prototype.delayedUnsubscribeExecute = function () {
            var subrequests = this.subscriptionCounter.getUnsubscribeRequests();
            for (var subrequestKey in subrequests) {
                var subrequest = subrequests[subrequestKey];
                var fields = subrequest.fields;
                this.sendUnsubscribeRequest(subrequest.instruments, subrequest.fields);
            }
        };
        InstrumentManager.prototype.sendUnsubscribeRequest = function (instruments, fields) {
            if (this.infront.isStreaming()) {
                var tmpFields = this.removeNonSubscriptionFields(fields);
                if (tmpFields.length > 0) {
                    var usReq = new Infront.InstrumentUnsubscribeRequest();
                    usReq.instruments = instruments;
                    usReq.fields = fields;
                    var reqOpts = new Infront.RequestOptions();
                    reqOpts.onError = InfrontUtil.errorLogCallback;
                    reqOpts.onSuccess = function (data) {
                    };
                    this.messageService.sendRequest(usReq, reqOpts);
                }
            }
        };
        InstrumentManager.prototype.unsubscribe = function (instruments, fields, callback) {
            var _this = this;
            this.subscriptionCounter.addCountFor(instruments, fields, -1);
            if (this.infront.isStreaming()) {
                this.unregisterObserver(instruments, callback);
                if (this.delayedUnsubscribeTimerID) {
                    clearTimeout(this.delayedUnsubscribeTimerID);
                }
                this.delayedUnsubscribeTimerID = setTimeout(function () {
                    _this.delayedUnsubscribeExecute();
                    _this.delayedUnsubscribeTimerID = null;
                }, InstrumentManager.kLazyUnsubDelay);
            }
        };
        InstrumentManager.prototype.getIntradayTradesSnapshot = function (instrument, callback, numTrades) {
            var _this = this;
            if (numTrades === void 0) { numTrades = 0; }
            var options = new Infront.InstrumentIntradayTradesOptions();
            options.items = numTrades != 0 ? numTrades : InstrumentManager.kDefaultNumTrades;
            options.sortBy = "TIME_DESC";
            options.onData = function (data) {
                var key = Infront.CacheKeyFactory.createTradesArrayKey(instrument);
                _this.cache.createArray(key, null, function (a, b) {
                    return b[Infront.MWSConstants.seq_id] - a[Infront.MWSConstants.seq_id];
                }, false);
                var array = _this.cache.getArray(Infront.CacheKeyFactory.createTradesArrayKey(instrument));
                for (var i = 0; i < data.length; i++) {
                    array.push(data[i]);
                }
                if (callback) {
                    callback(instrument, data);
                }
            };
            options.onError = InfrontUtil.errorLogCallback;
            this.infront.instrumentIntradayTrades(instrument, options);
        };
        InstrumentManager.prototype.snapshot = function (instruments, fields, guarantee, callback, numTrades) {
            var _this = this;
            if (guarantee === void 0) { guarantee = false; }
            if (numTrades === void 0) { numTrades = 0; }
            this.loadTGWFields(instruments, fields);
            var req = new Infront.GetSnapshotRequest();
            req.instruments = instruments;
            req.fields = this.removeNonSnapshotFields(fields);
            if (this.hasTradesField(fields)) {
                for (var i = 0; i < instruments.length; i++) {
                    this.getIntradayTradesSnapshot(instruments[i], null, numTrades);
                }
            }
            if (req.fields.length > 0) {
                if (req.fields.indexOf(Infront.QuoteFields.ORDERBOOK) > -1) {
                    this.createOrderbookCache(req.instruments);
                }
                var reqOpts = new Infront.RequestOptions();
                //if (callback) {
                //    reqOpts.onSuccess = (result:Object):void => {
                //        if (!(result.hasOwnProperty("instruments")) || result["instruments"].length == 0) {
                //            callback({});
                //        } else {
                //            callback(result[MWSConstants.instruments]);
                //        }
                //    }
                //} else {
                reqOpts.onSuccess = function (result) {
                    if (!(result.hasOwnProperty("instruments")) || result["instruments"].length == 0) {
                        if (guarantee) {
                            for (var i = 0; i < instruments.length; i++) {
                                _this.cache.updateCacheTimestamp(Infront.CacheKeyFactory.createInstrumentKey(instruments[i]));
                            }
                            if (callback)
                                callback({});
                        }
                        InfrontUtil.errorLogCallback(404, "Unknown instrument(s)");
                    }
                    else {
                        var cb = _this.cache.getSnapshotOnDataCallback();
                        var arr = result[Infront.MWSConstants.instruments];
                        for (var i = 0; i < arr.length; i++) {
                            var update = result["instruments"][i];
                            _this.cache.getSnapshotOnDataCallback()(update);
                            if (update.hasOwnProperty(Infront.MWSConstants.bids) || update.hasOwnProperty(Infront.MWSConstants.asks)) {
                                _this.processOrderbook(update);
                            }
                        }
                        if (callback)
                            callback(result[Infront.MWSConstants.instruments]);
                    }
                };
                //}
                if (guarantee) {
                    reqOpts.onError = function (error_code, error_message) {
                        for (var i = 0; i < instruments.length; i++) {
                            _this.cache.updateCacheTimestamp(Infront.CacheKeyFactory.createInstrumentKey(instruments[i]));
                        }
                        InfrontUtil.errorLogCallback(error_code, error_message);
                        if (callback) {
                            callback({});
                        }
                    };
                }
                else {
                    reqOpts.onError = InfrontUtil.errorLogCallback;
                }
                this.messageService.sendRequest(req, reqOpts);
            }
        };
        InstrumentManager.prototype.removeNonSnapshotFields = function (fields) {
            var nonSnapshotFields = Array.prototype.concat.apply([], fields);
            var isShortableIdx = nonSnapshotFields.indexOf("IS_SHORTABLE");
            if (isShortableIdx > -1) {
                nonSnapshotFields.splice(isShortableIdx, 1);
            }
            var tradesIdx = fields.indexOf("TRADES");
            if (tradesIdx > -1) {
                nonSnapshotFields.splice(tradesIdx, 1);
            }
            var marginRateIdx = nonSnapshotFields.indexOf("MARGIN_RATE");
            if (marginRateIdx > -1) {
                nonSnapshotFields.splice(marginRateIdx, 1);
            }
            nonSnapshotFields = this.removeFundsDetailsFields(nonSnapshotFields);
            return nonSnapshotFields;
        };
        InstrumentManager.prototype.removeNonSubscriptionFields = function (fields) {
            var nonSubscriptionFields = Array.prototype.concat.apply([], fields);
            var isShortableIdx = nonSubscriptionFields.indexOf("IS_SHORTABLE");
            if (isShortableIdx > -1) {
                nonSubscriptionFields.splice(isShortableIdx, 1);
            }
            var marginRateIdx = nonSubscriptionFields.indexOf("MARGIN_RATE");
            if (marginRateIdx > -1) {
                nonSubscriptionFields.splice(marginRateIdx, 1);
            }
            nonSubscriptionFields = this.removeFundsDetailsFields(nonSubscriptionFields);
            return nonSubscriptionFields;
        };
        InstrumentManager.prototype.removeFundsDetailsFields = function (fields) {
            var nonFundsDetailsFields = Array.prototype.concat.apply([], fields);
            var fundDetailsIdx = nonFundsDetailsFields.indexOf(InfrontConstants.FieldGroups.FUND_DETAILS);
            if (fundDetailsIdx > -1) {
                nonFundsDetailsFields.splice(fundDetailsIdx, 1);
            }
            return nonFundsDetailsFields;
        };
        InstrumentManager.prototype.hasTradesField = function (fields) {
            return fields.indexOf(Infront.QuoteFields.TRADES) >= 0;
        };
        InstrumentManager.prototype.hasFundDetailsFields = function (fields) {
            return fields.indexOf(InfrontConstants.FieldGroups.FUND_DETAILS) >= 0;
        };
        InstrumentManager.prototype.loadTGWFields = function (instruments, fields) {
            if (this.infront.getTradingStatus() == Infront.InfrontStatus.Connected && (fields.indexOf("IS_SHORTABLE") > -1 || fields.indexOf("MARGIN_RATE") > -1)) {
                for (var i = 0; i < instruments.length; i++) {
                    this.singleLoadTGWFields(instruments[i]);
                }
            }
        };
        InstrumentManager.prototype.singleLoadTGWFields = function (instrument) {
            var _this = this;
            var key = Infront.CacheKeyFactory.createInstrumentKey(instrument);
            var tpOpts = new Infront.TradingPowerOptions();
            tpOpts.instrument = instrument;
            tpOpts.onData = function (data) {
                var tmpObj = {};
                tmpObj[Infront.MWSConstants.instrument] = instrument;
                if (data.hasOwnProperty(Infront.MWSConstants.is_shortable)) {
                    tmpObj[Infront.MWSConstants.is_shortable] = data[Infront.MWSConstants.is_shortable];
                }
                if (data.hasOwnProperty(Infront.MWSConstants.margin_rate)) {
                    tmpObj[Infront.MWSConstants.margin_rate] = data[Infront.MWSConstants.margin_rate];
                }
                _this.cache.update(key, tmpObj);
            };
            tpOpts.onError = InfrontUtil.errorLogCallback;
            this.infront.trGetTradingPower(this.infront.getTradingManager().getCurrentPortfolio().name, tpOpts);
        };
        InstrumentManager.prototype.instrumentUpdate = function (data) {
            if (data.hasOwnProperty(Infront.MWSConstants.md_instrument_update) && data[Infront.MWSConstants.md_instrument_update].hasOwnProperty(Infront.MWSConstants.instrument)) {
                var update = data[Infront.MWSConstants.md_instrument_update];
                var key = Infront.CacheKeyFactory.createInstrumentKey(update[Infront.MWSConstants.instrument]);
                this.cache.update(key, data[Infront.MWSConstants.md_instrument_update]);
                if (update.hasOwnProperty(Infront.MWSConstants.trades)) {
                    this.processTrades(update);
                }
                if (update.hasOwnProperty(Infront.MWSConstants.bids) || update.hasOwnProperty(Infront.MWSConstants.asks)) {
                    this.processOrderbook(update);
                }
                if (this.observers.hasOwnProperty(key)) {
                    var obs = this.observers[key];
                    for (var i = 0; i < obs.length; i++) {
                        obs[i](data[Infront.MWSConstants.md_instrument_update]);
                    }
                }
            }
        };
        InstrumentManager.prototype.processTrades = function (update) {
            var key = Infront.CacheKeyFactory.createTradesArrayKey(update[Infront.MWSConstants.instrument]);
            var array = this.cache.getArray(key);
            if (array) {
                var trades = update[Infront.MWSConstants.trades];
                if (array.length() == 0) {
                    array.replaceWith(trades);
                }
                else {
                    for (var i = trades.length - 1; i >= 0; i--) {
                        array.push(trades[i]);
                    }
                }
            }
        };
        InstrumentManager.prototype.processOrderbook = function (update) {
            var instrument = update[Infront.MWSConstants.instrument];
            var bKey = Infront.CacheKeyFactory.createBidsArrayKey(instrument);
            var bidsArray = this.cache.getArray(bKey);
            var aKey = Infront.CacheKeyFactory.createAsksArrayKey(instrument);
            var askArray = this.cache.getArray(aKey);
            var instrumentKey = Infront.CacheKeyFactory.createInstrumentKey(instrument);
            //var maxVolumes = this.cache.getArray(CacheKeyFactory.createMaxVolumesArrayKey(instrument));
            if (update.hasOwnProperty(Infront.MWSConstants.bids)) {
                if (bidsArray) {
                    var bids = update[Infront.MWSConstants.bids];
                    this.processOrderbookUpdate(instrument, OrderbookSide.BID, bids, bidsArray);
                }
            }
            if (update.hasOwnProperty(Infront.MWSConstants.asks)) {
                if (askArray) {
                    var asks = update[Infront.MWSConstants.asks];
                    this.processOrderbookUpdate(instrument, OrderbookSide.ASK, asks, askArray);
                }
            }
            //var allLevels:OrderbookRow[] = [].concat(askArray.get(), bidsArray.get());
            //var maxVolume = 0;
            //for( var i = 0; i < allLevels.length; i++ ) {
            //    maxVolume = allLevels[i].volume.get() > maxVolume ? allLevels[i].volume.get() : maxVolume;
            //}
            //this.getMaxOrderbookVolume(instrument).set(maxVolume);
            var maxVolume = 0;
            var allMaxVolumes = {};
            for (var i = 0; i < askArray.length() || i < bidsArray.length(); i++) {
                if (i < askArray.length()) {
                    maxVolume = askArray.data[i].volume.get() > maxVolume ? askArray.data[i].volume.get() : maxVolume;
                }
                if (i < bidsArray.length()) {
                    maxVolume = bidsArray.data[i].volume.get() > maxVolume ? bidsArray.data[i].volume.get() : maxVolume;
                }
                allMaxVolumes["max_volumes" + i] = maxVolume;
                //this.cache.update(instrumentKey, { : maxVolume);
            }
            allMaxVolumes["max_volumes"] = maxVolume;
            this.cache.update(instrumentKey, allMaxVolumes);
        };
        InstrumentManager.prototype.processOrderbookUpdate = function (instrument, side, orderLevels, targetArray) {
            for (var i = 0; i < orderLevels.length; i++) {
                var row = findLevel(targetArray, orderLevels[i][Infront.MWSConstants.level]);
                if (row == null) {
                    row = new OrderbookRow(side);
                    row.level = orderLevels[i][Infront.MWSConstants.level];
                    //row.barWidth = new ComputedObservable([row.volume, this.getMaxOrderbookVolume(instrument)], computeBarWidth);
                    targetArray.push(row);
                }
                if (orderLevels[i].hasOwnProperty(Infront.MWSConstants.price)) {
                    row.price.set(orderLevels[i][Infront.MWSConstants.price]);
                }
                if (orderLevels[i].hasOwnProperty(Infront.MWSConstants.volume)) {
                    row.volume.set(orderLevels[i][Infront.MWSConstants.volume]);
                }
                if (orderLevels[i].hasOwnProperty(Infront.MWSConstants.orders)) {
                    row.numOrders.set(orderLevels[i][Infront.MWSConstants.orders]);
                }
            }
            //Loop from the bottom and remove any trailing empty levels
            var lastRow = targetArray.lastItem();
            while (lastRow != null && lastRow.volume.get() < 1) {
                targetArray.removeItemAt(targetArray.length() - 1);
                lastRow = targetArray.lastItem();
            }
        };
        InstrumentManager.kDefaultNumTrades = 25; //Number of trades to request by default
        InstrumentManager.kBunchingDelay = 50; //Milliseconds to wait before sending subscribe
        InstrumentManager.kLazyUnsubDelay = 3000; //Milliseconds to wait before sending unsubscribe
        return InstrumentManager;
    }());
    Infront.InstrumentManager = InstrumentManager;
    //Orderbook-related functionality
    function findLevel(array, level) {
        return array.find(function (item) {
            return item.level == level;
        });
    }
    function compareOrderbookLevels(a, b) {
        return a.level - b.level;
    }
    //function computeBarWidth(args:any[]):number {
    //    var volume:number = args[0];
    //    var biggestVolume:number = args[1];
    //    return (volume/biggestVolume)*100;
    //}
    var OrderbookRow = (function () {
        // public barWidth:ComputedObservable;
        function OrderbookRow(side) {
            this.level = -1;
            this.price = new Infront.Observable();
            this.price.set(0);
            this.volume = new Infront.Observable();
            this.volume.set(0);
            this.numOrders = new Infront.Observable();
            this.numOrders.set(0);
            //  this.barWidth = null;
        }
        return OrderbookRow;
    }());
    Infront.OrderbookRow = OrderbookRow;
    var OrderbookSide;
    (function (OrderbookSide) {
        OrderbookSide[OrderbookSide["BID"] = 0] = "BID";
        OrderbookSide[OrderbookSide["ASK"] = 1] = "ASK";
    })(OrderbookSide = Infront.OrderbookSide || (Infront.OrderbookSide = {}));
})(Infront || (Infront = {}));
/**
 * Created by hage on 25.09.2015.
 */
/**
 * The InterLibraryLink is a set of interfaces describing a generic and extensible
 * link mechanism between objects.
 */
var InterLibraryLink;
/**
 * Created by hage on 25.09.2015.
 */
/**
 * The InterLibraryLink is a set of interfaces describing a generic and extensible
 * link mechanism between objects.
 */
(function (InterLibraryLink) {
    /**
     * All messages transmitted must contain a datatype specified in this enum.
     * Add datatypes when needed.
     */
    var DataType = (function () {
        function DataType(datatype) {
            this.dataType = datatype;
        }
        //# Infinancials.Visual DataType
        DataType.infinInstrument = "InfinInstrument";
        DataType.isin = "Isin";
        DataType.field = "Field";
        DataType.universeObject = "UniverseObject";
        DataType.symphonyApp = "SymphonyApp";
        //# Infront.Visual DataType
        DataType.infrontInstrument = "InfrontInstrument";
        DataType.removeInfrontInstrument = "RemoveInfrontInstrument";
        DataType.infrontNewsItem = "InfrontNewsItem";
        DataType.feed = "Feed";
        DataType.price = "Price";
        DataType.infrontAlert = "Alert";
        return DataType;
    }());
    InterLibraryLink.DataType = DataType;
    /**
     * A message contains a value and an associated type.
     */
    var Message = (function () {
        function Message(type, value) {
            this.type = type;
            this.value = value;
        }
        return Message;
    }());
    InterLibraryLink.Message = Message;
    function targetSupportsType(target, type) {
        return target.accepts().indexOf(type) > -1;
    }
    InterLibraryLink.targetSupportsType = targetSupportsType;
    /**
     * To better communication between libraries, we need to externalize the Controller linking system.
     * Using this "singleton" class we can easily add/remove targets and broadcast the message only to those that accept it.
     * This controller allow multi-directional links.
     */
    var ControllerLinkFactory = (function () {
        function ControllerLinkFactory() {
            this.targetsList = [];
        }
        ControllerLinkFactory.getInstance = function (channel) {
            if (channel === void 0) { channel = ControllerLinkFactory.DEFAULT_CHANNEL; }
            var retVal = null;
            if (InfrontUtil.isNumber(channel)) {
                if (!ControllerLinkFactory._channels[channel]) {
                    ControllerLinkFactory._channels[channel] = new ControllerLinkFactory();
                }
                retVal = ControllerLinkFactory._channels[channel];
            }
            return retVal;
        };
        ControllerLinkFactory.eachChannel = function (channels, cb) {
            var cArr = InfrontUtil.isArray(channels) ? channels : [channels];
            for (var i = 0; i < cArr.length; i++) {
                var clf = ControllerLinkFactory.getInstance(cArr[i]);
                if (clf) {
                    cb(clf);
                }
            }
        };
        ControllerLinkFactory.link = function (channels, target) {
            ControllerLinkFactory.eachChannel(channels, function (clf) {
                clf.link(target);
            });
        };
        ControllerLinkFactory.unlink = function (channels, target) {
            ControllerLinkFactory.eachChannel(channels, function (clf) {
                clf.unlink(target);
            });
        };
        ControllerLinkFactory.broadcastMessage = function (channels, msg) {
            var cArr = InfrontUtil.isArray(channels) ? channels : [channels];
            ControllerLinkFactory.eachChannel(channels, function (clf) {
                clf.broadcastMessage(msg);
            });
        };
        ControllerLinkFactory.prototype.link = function (target) {
            if (this.targetsList.indexOf(target) < 0)
                this.targetsList.push(target);
        };
        ControllerLinkFactory.prototype.unlink = function (target) {
            if (this.targetsList.indexOf(target) != -1)
                this.targetsList.splice(this.targetsList.indexOf(target), 1);
        };
        /** Handle action between widgets and broadcast the message. */
        ControllerLinkFactory.prototype.broadcastMessage = function (msg) {
            for (var i = 0; i < this.targetsList.length; i++) {
                if (InterLibraryLink.targetSupportsType(this.targetsList[i], msg.type))
                    this.targetsList[i].receiveMessage(msg);
            }
        };
        ControllerLinkFactory.DEFAULT_CHANNEL = 9007199254740991; //Number.MAX_SAFE_INTEGER not supported by IE
        ControllerLinkFactory._channels = {};
        return ControllerLinkFactory;
    }());
    InterLibraryLink.ControllerLinkFactory = ControllerLinkFactory;
})(InterLibraryLink || (InterLibraryLink = {}));
///<reference path='Bindings.ts' />
var Infront;
///<reference path='Bindings.ts' />
(function (Infront) {
    /**
     * An array that can be observed (typically by an ArrayBinding). Any manipulation of the array will cause callbacks to the observer(s).
     * Interaction mostly mirrors the way you interact with a normal javascript array (except for the [] index). Callbacks are sent synchronously.
     */
    var ObservableArray = (function () {
        function ObservableArray() {
            this.id = "ObservableArray" + ObservableArray.counter++;
            this.data = [];
            this.obs = [];
        }
        /**
         * Returns the javascript-array backing this ObservableArray. Be careful about interacting with this, since any manipulation of the
         * array will not be signaled to any observers.
         */
        ObservableArray.prototype.get = function () {
            return this.data;
        };
        /**
        Insert a value at the specified index.
        */
        ObservableArray.prototype.insert = function (obj, index) {
            this.data.splice(index, 0, obj);
            this.updateInserted(obj, index);
        };
        /**
         * Search for obj and remove it if found.
         */
        ObservableArray.prototype.remove = function (obj) {
            var index = this.data.indexOf(obj);
            if (index > -1) {
                this.data.splice(index, 1);
                this.updateRemoved(obj, index);
            }
        };
        /**
         * Remove the item at the given index and returns it.
         * @param index
         * @returns {any}
         */
        ObservableArray.prototype.removeItemAt = function (index) {
            var obj = null;
            if (index >= 0 && index < this.data.length) {
                obj = this.data[index];
                this.data.splice(index, 1);
                this.updateRemoved(obj, index);
            }
            return obj;
        };
        /**
         * Appends an item to the end (biggest index) of the array.
         * @param obj
         */
        ObservableArray.prototype.push = function (obj) {
            this.data.push(obj);
            this.updateInserted(obj, this.data.length - 1);
        };
        /**
         * Removes the item at the end (biggest index) of the array and returns it.
         * @returns {any}
         */
        ObservableArray.prototype.pop = function () {
            return this.removeItemAt(this.data.length - 1);
        };
        /**
         * Searches for an item and, if found, moves it to a new index.
         * @param obj
         * @param newIndex
         */
        ObservableArray.prototype.move = function (obj, newIndex) {
            var currentIndex = this.data.indexOf(obj);
            if (currentIndex > -1) {
                if (currentIndex != newIndex) {
                    //Remove object from array
                    this.data.splice(currentIndex, 1);
                    //Insert it back, compensating for the now missing object if necessary.
                    this.data.splice(newIndex > currentIndex ? newIndex - 1 : newIndex, 0, obj);
                }
                this.updateMoved(obj, currentIndex, newIndex);
            }
            else {
                0;
            }
        };
        /**
         * Removes all items in the array.
         */
        ObservableArray.prototype.clear = function () {
            while (this.data.length > 0) {
                this.removeItemAt(this.data.length - 1);
            }
        };
        /**
         * Sorts the array using the provided compare-function. After the sort the observers will receive one move-callback
         * for each element with the new position, even if it did not move in the array.
         * @param compareFunction
         */
        ObservableArray.prototype.sort = function (compareFunction) {
            var temp = [].concat(this.data);
            temp.sort(compareFunction);
            for (var i = 0; i < temp.length; i++) {
                this.move(temp[i], i);
            }
        };
        /**
         * Iterates over the array and executes func with each item as a parameter. You should NOT modify the array in this
         * process.
         * @param func
         */
        ObservableArray.prototype.foreach = function (func) {
            for (var i = 0; i < this.data.length; i++) {
                func(this.data[i]);
            }
        };
        /**
         * Searches through the array and returns the first (or last if reverse is true) item where the compare-function
         * returns true.
         * @param compare
         * @param reverse
         * @returns {any}
         */
        ObservableArray.prototype.find = function (compare, reverse) {
            if (reverse === void 0) { reverse = false; }
            var retVal = null;
            for (var i = reverse ? this.data.length - 1 : 0; reverse ? i >= 0 : i < this.data.length; reverse ? i-- : i++) {
                if (compare(this.data[i])) {
                    retVal = this.data[i];
                    break;
                }
            }
            return retVal;
        };
        /**
         * Replaces the data currently in the array with new data.
         * @param data
         */
        ObservableArray.prototype.replaceWith = function (data) {
            this.clear();
            for (var i = 0; i < data.length; i++) {
                this.data.push(data[i]);
            }
            for (var j = 0; j < this.obs.length; j++) {
                this.obs[j].reInit(this.data);
            }
        };
        ObservableArray.prototype.length = function () {
            return this.data.length;
        };
        ObservableArray.prototype.item = function (index) {
            return this.data[index];
        };
        ObservableArray.prototype.lastItem = function () {
            return this.data.length > 0 ? this.data[this.data.length - 1] : null;
        };
        ObservableArray.prototype.indexOf = function (obj) {
            return this.data.indexOf(obj);
        };
        ObservableArray.prototype.contains = function (obj) {
            return this.data.lastIndexOf(obj) > -1;
        };
        //Binding-related methods
        ObservableArray.prototype.observe = function (b) {
            this.obs.push(b);
            b.reInit(this.data);
        };
        ObservableArray.prototype.unbind = function (b) {
            var index = this.obs.indexOf(b);
            if (index >= 0) {
                this.obs.splice(index, 1);
            }
        };
        ObservableArray.prototype.updateInserted = function (obj, index) {
            for (var i = 0; i < this.obs.length; i++) {
                this.obs[i].itemAdded(obj, index);
            }
        };
        ObservableArray.prototype.updateRemoved = function (obj, index) {
            for (var i = 0; i < this.obs.length; i++) {
                this.obs[i].itemRemoved(obj, index);
            }
        };
        ObservableArray.prototype.updateMoved = function (obj, oldIndex, newIndex) {
            for (var i = 0; i < this.obs.length; i++) {
                this.obs[i].itemMoved(obj, oldIndex, newIndex);
            }
        };
        ObservableArray.counter = 0;
        return ObservableArray;
    }());
    Infront.ObservableArray = ObservableArray;
    /**
     * An observable array that will insert any object in sorted order, according to the provided
     * compare-function. This overrides the functionality of the push- and insert-functions.
     */
    var SortedObservableArray = (function (_super) {
        __extends(SortedObservableArray, _super);
        function SortedObservableArray(compareFunction, allowDuplicates) {
            if (allowDuplicates === void 0) { allowDuplicates = true; }
            var _this = _super.call(this) || this;
            _this.allowDuplicates = allowDuplicates;
            _this.compareFunction = compareFunction;
            return _this;
        }
        /**
         * Finds the correct insert-index for the given item using a binary search through the array.
         * @param obj
         * @param startIndex
         * @param endIndex
         * @returns {*}
         */
        SortedObservableArray.prototype.findInsertIndex = function (obj, startIndex, endIndex) {
            var range = endIndex - startIndex;
            if (range < 5) {
                var retVal = startIndex;
                for (var i = startIndex; i <= endIndex; i++) {
                    if (this.compareFunction(obj, this.data[i]) >= 0 && (i >= endIndex || this.compareFunction(obj, this.data[i + 1]) <= 0)) {
                        retVal = i + 1;
                        break;
                    }
                }
                return retVal;
            }
            else {
                var pivot = Math.floor(startIndex + ((endIndex - startIndex) / 2));
                if (this.compareFunction(obj, this.data[pivot]) > 0) {
                    return this.findInsertIndex(obj, pivot, endIndex);
                }
                else {
                    return this.findInsertIndex(obj, startIndex, pivot);
                }
            }
        };
        SortedObservableArray.prototype.replaceWith = function (newData) {
            var tmp = [].concat(newData);
            tmp.sort(this.compareFunction);
            _super.prototype.replaceWith.call(this, tmp);
        };
        /**
         * The push-function normally appends the item to the end of the array, but since this class maintains
         * a sorted order it will be inserted into it's proper place instead.
         * @param obj
         */
        SortedObservableArray.prototype.push = function (obj) {
            var insertIndex = this.findInsertIndex(obj, 0, this.data.length - 1);
            if (this.allowDuplicates || this.data.length <= insertIndex || (this.compareFunction(obj, this.data[insertIndex]) != 0 && (insertIndex == 0 || this.compareFunction(obj, this.data[insertIndex - 1]) != 0))) {
                _super.prototype.insert.call(this, obj, insertIndex);
            }
        };
        /**
         * See push().
         */
        SortedObservableArray.prototype.insert = function (obj, index) {
            this.push(obj);
        };
        /**
         * Changes the compare-function of this array. This causes the array to be re-sorted.
         * @param compareFunction
         */
        SortedObservableArray.prototype.setCompareFunction = function (compareFunction) {
            this.compareFunction = compareFunction;
            this.sort(this.compareFunction);
        };
        return SortedObservableArray;
    }(ObservableArray));
    Infront.SortedObservableArray = SortedObservableArray;
    /**
     * This class will intercept and pass on callbacks from the ObservableArray backing it, but act if only the top part of the array (as specified by the cap-parameter) is visible.
     */
    var ObservableArrayCap = (function () {
        function ObservableArrayCap(backing, cap, paddingItem) {
            if (paddingItem === void 0) { paddingItem = null; }
            this.cap = cap;
            this.obs = [];
            this.backing = backing;
            if (this.backing) {
                this.backing.observe(this);
            }
            this.paddingItem = paddingItem;
        }
        ObservableArrayCap.prototype.createTempDataArray = function () {
            var data = [];
            var backLen = this.backing.length();
            var n = this.paddingItem != null ? this.cap : Math.min(backLen, this.cap);
            for (var i = 0; i < n; i++) {
                if (i < backLen) {
                    data.push(this.backing.item(i));
                }
                else if (this.paddingItem != null) {
                    data.push(this.paddingItem);
                }
            }
            return data;
        };
        ObservableArrayCap.prototype.observe = function (b) {
            this.obs.push(b);
            if (this.backing) {
                b.reInit(this.createTempDataArray());
            }
        };
        ObservableArrayCap.prototype.unbind = function (b) {
            var index = this.obs.indexOf(b);
            if (index >= 0) {
                this.obs.splice(index, 1);
            }
        };
        //IArrayBinding-methods
        ObservableArrayCap.prototype.reInit = function (items) {
            var data = this.createTempDataArray();
            for (var j = 0; j < this.obs.length; j++) {
                this.obs[j].reInit(data);
            }
        };
        ObservableArrayCap.prototype.itemAdded = function (item, index) {
            if (index < this.cap) {
                this.updateInserted(item, index);
                if (this.backing.length() > this.cap) {
                    this.updateRemoved(this.backing.item(this.cap), this.cap);
                }
                else if (this.paddingItem != null) {
                    this.updateRemoved(this.paddingItem, this.cap);
                }
            }
        };
        ObservableArrayCap.prototype.itemRemoved = function (item, index) {
            if (index < this.cap) {
                this.updateRemoved(item, index);
                if (this.backing.length() >= this.cap) {
                    this.updateInserted(this.backing.item(this.cap - 1), this.cap - 1);
                }
                else if (this.paddingItem != null) {
                    this.updateInserted(this.paddingItem, this.cap - 1);
                }
            }
        };
        ObservableArrayCap.prototype.itemMoved = function (item, oldIndex, newIndex) {
            if (oldIndex < this.cap && newIndex < this.cap) {
                this.updateMoved(item, oldIndex, newIndex);
            }
            else if (oldIndex < this.cap) {
                this.itemRemoved(item, oldIndex);
            }
            else if (newIndex < this.cap) {
                this.itemAdded(item, newIndex);
            }
        };
        ObservableArrayCap.prototype.updateInserted = function (obj, index) {
            for (var i = 0; i < this.obs.length; i++) {
                this.obs[i].itemAdded(obj, index);
            }
        };
        ObservableArrayCap.prototype.updateRemoved = function (obj, index) {
            for (var i = 0; i < this.obs.length; i++) {
                this.obs[i].itemRemoved(obj, index);
            }
        };
        ObservableArrayCap.prototype.updateMoved = function (obj, oldIndex, newIndex) {
            for (var i = 0; i < this.obs.length; i++) {
                this.obs[i].itemMoved(obj, oldIndex, newIndex);
            }
        };
        return ObservableArrayCap;
    }());
    Infront.ObservableArrayCap = ObservableArrayCap;
    var CachedObservableArrayCap = (function (_super) {
        __extends(CachedObservableArrayCap, _super);
        function CachedObservableArrayCap(cache, cap, paddingItem) {
            if (paddingItem === void 0) { paddingItem = null; }
            var _this = _super.call(this, null, cap, paddingItem) || this;
            _this.cache = cache;
            _this.cacheKey = null;
            _this.bound = false;
            return _this;
        }
        CachedObservableArrayCap.prototype.restoreBacking = function () {
        };
        CachedObservableArrayCap.prototype.reInit = function (items) {
            if (this.bound && !this.backing) {
                this.backing = this.cache.getArray(this.cacheKey);
            }
            _super.prototype.reInit.call(this, items);
        };
        CachedObservableArrayCap.prototype.unbindFromCache = function () {
            this.backing = null;
            this.bound = false;
            if (this.cacheKey) {
                this.cache.unbindFromArray(this.cacheKey, this);
            }
        };
        CachedObservableArrayCap.prototype.bindToCache = function (cacheKey) {
            this.bound = true;
            this.cacheKey = cacheKey;
            this.cache.bindToArray(cacheKey, this);
        };
        return CachedObservableArrayCap;
    }(ObservableArrayCap));
    Infront.CachedObservableArrayCap = CachedObservableArrayCap;
    /**
     * Syncs the contents of a target array based on events from the source array.
     */
    var ObservableArraySync = (function () {
        function ObservableArraySync(target, filter) {
            if (filter === void 0) { filter = null; }
            this.target = target;
            this.filter = filter;
        }
        ObservableArraySync.prototype.setSource = function (source) {
            this.source.unbind(this);
            this.target.clear();
            this.source = source;
            this.source.observe(this);
        };
        ObservableArraySync.prototype.reInit = function (items) {
            var filteredItems = [];
            if (this.filter) {
                for (var i = 0; i < items.length; i++) {
                    if (this.filter(items[i])) {
                        filteredItems.push(items[i]);
                    }
                }
            }
            else {
                filteredItems = items;
            }
            this.target.replaceWith(filteredItems);
        };
        ObservableArraySync.prototype.itemAdded = function (item, index) {
            if (!InfrontUtil.isFunction(this.filter) || this.filter(item))
                this.target.insert(item, index);
        };
        ObservableArraySync.prototype.itemRemoved = function (item, index) {
            this.target.remove(item);
        };
        ObservableArraySync.prototype.itemMoved = function (item, oldIndex, newIndex) {
            //Empty. This class only concerns itself with contents, not order.
        };
        ObservableArraySync.prototype.destroy = function () {
            this.target = null;
            if (this.source) {
                this.source.unbind(this);
            }
            this.source = null;
            this.filter = null;
        };
        return ObservableArraySync;
    }());
    Infront.ObservableArraySync = ObservableArraySync;
    var ObservableArrayRowCounter = (function () {
        function ObservableArrayRowCounter(array) {
            this.array = array;
            this.rowCount = new Observable();
            this.rowCount.set(0);
            this.array.observe(this);
        }
        ObservableArrayRowCounter.prototype.unbind = function () {
            this.array.unbind(this);
        };
        ObservableArrayRowCounter.prototype.update = function () {
            this.rowCount.set(this.array.length());
        };
        ObservableArrayRowCounter.prototype.reInit = function (items) {
            this.update();
        };
        ObservableArrayRowCounter.prototype.itemAdded = function (item, index) {
            this.update();
        };
        ObservableArrayRowCounter.prototype.itemRemoved = function (item, index) {
            this.update();
        };
        ObservableArrayRowCounter.prototype.itemMoved = function (item, oldIndex, newIndex) {
            this.update();
        };
        return ObservableArrayRowCounter;
    }());
    Infront.ObservableArrayRowCounter = ObservableArrayRowCounter;
    /**
     * Lets you subscribe to a value (usually a primitive) and receive updates when it changes.
     */
    var Observable = (function () {
        function Observable() {
            this.bindings = [];
            this.value = null;
            this.infrontObservable = true;
        }
        Observable.prototype.observe = function (b) {
            var _this = this;
            this.bindings.push(b);
            if (this.value != null) {
                /* A timeout is used here to ensure that the initial callback comes after initialization is finished.
                 * Sending the callback immediately (without the callback) can cause symptoms similar to that of
                 * concurrent execution. This was observed in ContinouslySortedCachedInstrumentObservableArray, where
                 * the initial callback caused the array to be modified while in a loop.
                 */
                InfrontUtil.setZeroTimeout(function () {
                    if (_this.bindings.indexOf(b) > -1) {
                        b.valueUpdated(_this.value);
                    }
                });
            }
        };
        Observable.prototype.unbind = function (b) {
            var index = this.bindings.indexOf(b);
            if (index >= 0) {
                this.bindings.splice(index, 1);
                if (b.cleanup) {
                    b.cleanup();
                }
            }
        };
        Observable.prototype.updateBindings = function () {
            for (var i = 0; i < this.bindings.length; i++) {
                this.bindings[i].valueUpdated(this.value);
            }
        };
        Observable.prototype.set = function (val) {
            if (this.value != val) {
                this.value = val;
                this.updateBindings();
            }
        };
        Observable.prototype.get = function () {
            return this.value;
        };
        Observable.prototype.isNull = function () {
            return this.value === null;
        };
        return Observable;
    }());
    Infront.Observable = Observable;
    var ObservableBinding = (function (_super) {
        __extends(ObservableBinding, _super);
        function ObservableBinding(translate) {
            var _this = _super.call(this) || this;
            _this.translate = translate;
            return _this;
        }
        ObservableBinding.prototype.valueUpdated = function (val) {
            if (InfrontUtil.isFunction(this.translate)) {
                this.set(this.translate(val));
            }
            else {
                this.set(val);
            }
        };
        return ObservableBinding;
    }(Observable));
    Infront.ObservableBinding = ObservableBinding;
    /**
     * A bastardization of Observable that doesn't actually have its own value. Instead it subscribes to
     * one or more other Observables and runs the provided computation-function whenever one or more of them
     * changes their value (and then updates its own listeners).
     */
    var ComputedObservable = (function (_super) {
        __extends(ComputedObservable, _super);
        function ComputedObservable(args, computation) {
            var _this = _super.call(this) || this;
            _this.args = args;
            _this.computation = computation;
            for (var i = 0; i < _this.args.length; i++) {
                _this.args[i].observe(_this);
            }
            return _this;
        }
        ComputedObservable.prototype.set = function (val) {
            //Empty, this observable does not have it's own value and is computed on the fly from other observables.
        };
        ComputedObservable.prototype.valueUpdated = function (val) {
            var parameters = [];
            for (var i = 0; i < this.args.length; i++) {
                parameters.push(this.args[i].get());
            }
            this.value = this.computation(parameters);
            this.updateBindings();
        };
        ComputedObservable.prototype.unbindAll = function () {
            for (var i = 0; i < this.args.length; i++) {
                this.args[i].unbind(this);
            }
        };
        return ComputedObservable;
    }(Observable));
    Infront.ComputedObservable = ComputedObservable;
    var CacheComputedObservable = (function (_super) {
        __extends(CacheComputedObservable, _super);
        function CacheComputedObservable(cache, fields, computation) {
            var _this = _super.call(this) || this;
            _this.computation = computation;
            _this.cache = cache;
            _this.fields = fields;
            _this.unbinds = [];
            for (var i = 0; i < fields.length; i++) {
                _this.unbinds.push(cache.customBind(fields[i].cacheKey, fields[i].fieldSpec, _this));
            }
            _this.valueUpdated(null);
            return _this;
        }
        CacheComputedObservable.prototype.set = function (val) {
            //Empty, this observable does not have it's own value and is computed on the fly from other observables.
        };
        CacheComputedObservable.prototype.valueUpdated = function (val) {
            var parameters = [];
            for (var i = 0; i < this.fields.length; i++) {
                parameters.push(this.cache.get(this.fields[i].cacheKey, this.fields[i].fieldSpec));
            }
            var computedVal = this.computation(parameters);
            if (computedVal == NaN) {
                0;
            }
            if (computedVal != this.value) {
                this.value = computedVal;
                this.updateBindings();
            }
        };
        CacheComputedObservable.prototype.unbindAll = function () {
            for (var i = 0; i < this.unbinds.length; i++) {
                this.unbinds[i]();
            }
        };
        return CacheComputedObservable;
    }(Observable));
    Infront.CacheComputedObservable = CacheComputedObservable;
    /**
     * Observable that computes a average on a single field in all rows of a table, potentially weighted by the contents of a second column.
     */
    var CacheWeightedAverageObservable = (function (_super) {
        __extends(CacheWeightedAverageObservable, _super);
        function CacheWeightedAverageObservable(cache, avgField, weightField, avgFieldCacheKeyCreator, weightFieldCacheKeyCreator) {
            var _this = _super.call(this) || this;
            _this.cache = cache;
            _this.cacheKeyCreator = avgFieldCacheKeyCreator;
            _this.weightFieldCacheKeyCreator = weightFieldCacheKeyCreator;
            _this.avgField = avgField;
            _this.weightField = weightField;
            _this.valueMap = {};
            _this.weightMap = {};
            _this.unbindMap = {};
            return _this;
        }
        CacheWeightedAverageObservable.prototype.set = function (val) {
            //Empty, this observable does not have it's own value and is computed on the fly from other observables.
        };
        CacheWeightedAverageObservable.prototype.update = function () {
            var weightTotal = 0;
            var weightedSum = 0;
            for (var p in this.valueMap) {
                if (this.valueMap.hasOwnProperty(p)) {
                    weightTotal += this.weightMap[p];
                    weightedSum += (this.valueMap[p] * this.weightMap[p]);
                }
            }
            this.value = weightedSum / weightTotal;
            this.updateBindings();
        };
        /** IArrayBinding **/
        CacheWeightedAverageObservable.prototype.reInit = function (items) {
            for (var i = 0; i < items.length; i++) {
                this.itemAdded(items[i], i);
            }
        };
        CacheWeightedAverageObservable.prototype.itemAdded = function (item, index) {
            var _this = this;
            var valueKey = this.cacheKeyCreator(item);
            var unbinds = [];
            var binding = Infront.BindingFactory.createInlineBinding(function (value) {
                _this.valueMap[valueKey] = value;
                _this.update();
            });
            unbinds.push(this.cache.customBind(valueKey, this.avgField, binding));
            if (this.weightField) {
                var weightKey = this.weightFieldCacheKeyCreator(item);
                var weightBinding = Infront.BindingFactory.createInlineBinding(function (value) {
                    _this.weightMap[valueKey] = value;
                    _this.update();
                });
                unbinds.push(this.cache.customBind(weightKey, this.weightField, weightBinding));
            }
            else {
                this.weightMap[valueKey] = 1;
            }
            this.unbindMap[valueKey] = function () {
                for (var i = 0; i < unbinds.length; i++) {
                    unbinds[i]();
                }
            };
        };
        CacheWeightedAverageObservable.prototype.itemRemoved = function (item, index) {
            var key = this.cacheKeyCreator(item);
            this.unbindMap[key]();
            delete this.unbindMap[key];
            delete this.valueMap[key];
            delete this.weightMap[key];
            this.update();
        };
        CacheWeightedAverageObservable.prototype.itemMoved = function (item, oldIndex, newIndex) { };
        return CacheWeightedAverageObservable;
    }(Observable));
    Infront.CacheWeightedAverageObservable = CacheWeightedAverageObservable;
    /**
     * Observable that performs a computation on a single field in all rows of a table.
     */
    var CacheColumnComputedObservable = (function (_super) {
        __extends(CacheColumnComputedObservable, _super);
        function CacheColumnComputedObservable(cache, field, cacheKeyCreator, computation, strategy) {
            if (strategy === void 0) { strategy = Infront.ComputationStrategy.PASSIVE; }
            var _this = _super.call(this) || this;
            _this.field = InfrontUtil.isArray(field) ? field : [field];
            _this.cache = cache;
            _this.cacheKeyCreator = cacheKeyCreator;
            _this.computation = computation;
            _this.strategy = strategy;
            _this.valueMap = {};
            _this.unbindMap = {};
            _this.observedKeys = [];
            return _this;
        }
        CacheColumnComputedObservable.prototype.set = function (val) {
            //Empty, this observable does not have its own value and is computed on the fly from other observables.
        };
        CacheColumnComputedObservable.prototype.update = function () {
            var calculate = true;
            if (this.strategy == Infront.ComputationStrategy.PASSIVE) {
                for (var i = 0; i < this.observedKeys.length; i++) {
                    calculate = calculate && this.valueMap.hasOwnProperty(this.observedKeys[i]) && InfrontUtil.isNumber(this.valueMap[this.observedKeys[i]]); //typeof(this.valueMap[this.observedKeys[i]]) == "number";
                }
            }
            else if (this.strategy == Infront.ComputationStrategy.FIRST_RESULT) {
                for (var i = 0; i < this.observedKeys.length; i++) {
                    if (!this.valueMap.hasOwnProperty(this.observedKeys[i]) && !this.cache.has(this.observedKeys[i], Infront.BindingCache.kCacheTimestampField)) {
                        calculate = false;
                        break;
                    }
                }
            }
            if (calculate) {
                var values = [];
                for (var p in this.valueMap) {
                    if (this.valueMap.hasOwnProperty(p)) {
                        values.push(this.valueMap[p]);
                    }
                }
                this.value = this.computation(values);
                this.updateBindings();
            }
        };
        /** IArrayBinding **/
        CacheColumnComputedObservable.prototype.reInit = function (items) {
            for (var i = 0; i < items.length; i++) {
                this.itemAdded(items[i], i);
            }
        };
        CacheColumnComputedObservable.prototype.itemAdded = function (item, index) {
            var _this = this;
            var key = this.cacheKeyCreator(item);
            this.observedKeys.push(key);
            var binding = Infront.BindingFactory.createInlineBinding(function (value) {
                //Check if we are still subscribing to this. In case of large tables, unsubscribes can be performed while
                //messages are still in the processing queue, so we check to be sure.
                if (_this.unbindMap.hasOwnProperty(key)) {
                    _this.valueMap[key] = value;
                    _this.update();
                }
            });
            this.unbindMap[key] = this.cache.customBind(key, this.field, binding);
        };
        CacheColumnComputedObservable.prototype.itemRemoved = function (item, index) {
            var key = this.cacheKeyCreator(item);
            this.unbindMap[key]();
            delete this.unbindMap[key];
            delete this.valueMap[key];
            var idx = this.observedKeys.indexOf(key);
            if (idx > -1) {
                this.observedKeys.splice(idx, 1);
            }
            this.update();
        };
        CacheColumnComputedObservable.prototype.itemMoved = function (item, oldIndex, newIndex) { };
        return CacheColumnComputedObservable;
    }(Observable));
    Infront.CacheColumnComputedObservable = CacheColumnComputedObservable;
    /**
     * An implementation of Observable that has an override-method. This is intended to allow for forcing the value without
     * having if-statements everywhere.
     * It saves any changes to the value while the override is in effect so nothing will be lost.
     */
    var OverridableObservable = (function (_super) {
        __extends(OverridableObservable, _super);
        function OverridableObservable() {
            var _this = _super.call(this) || this;
            _this.overrideActive = false;
            return _this;
        }
        OverridableObservable.prototype.set = function (val) {
            if (!this.overrideActive) {
                _super.prototype.set.call(this, val);
            }
            else {
                this.actualValue = val;
            }
        };
        OverridableObservable.prototype.override = function (val) {
            this.set(val);
            this.overrideActive = true;
        };
        OverridableObservable.prototype.clearOverride = function () {
            this.overrideActive = false;
            _super.prototype.set.call(this, this.actualValue);
        };
        return OverridableObservable;
    }(Observable));
    Infront.OverridableObservable = OverridableObservable;
})(Infront || (Infront = {}));
var Infront;
(function (Infront) {
    var TradingPower = (function () {
        function TradingPower() {
        }
        return TradingPower;
    }());
    Infront.TradingPower = TradingPower;
    var Portfolio = (function () {
        function Portfolio(name, description) {
            this.name = name;
            this.description = description;
        }
        return Portfolio;
    }());
    Infront.Portfolio = Portfolio;
    var TradingManager = (function () {
        function TradingManager(infront) {
            var _this = this;
            this.infront = infront;
            this.observerManager = new InfrontUtil.ObserverManager();
            this.currencyConverter = new Infront.CurrencyConverter(this.infront, false);
            this.baseCurrency = this.infront.getBaseCurrency();
            this.started = false;
            this.initialized = false;
            this.portfoliosLoaded = false;
            this.subscribeToAll = 0;
            this.currentPortfolioReady = false;
            this.readyPortfolios = [];
            this.todaysPLEnabled = false;
            this.strategyManager = new StrategyManager(this.infront);
            this.infront.registerEventObserver(Infront.TradingConnectedEvent.kEventName, function (event) { return _this.didTradingLogin(); });
            this.infront.registerEventObserver(Infront.TradingDisconnectedEvent.kEventName, function (event) { return _this.didTradingLogout(); });
        }
        TradingManager.prototype.init = function () {
            this.initialized = true;
            this.getPortfolios();
        };
        TradingManager.prototype.didTradingLogin = function () {
            this.init();
        };
        TradingManager.prototype.didTradingLogout = function () {
            var cache = this.infront.getCache();
            if (this.portfolios) {
                for (var i = 0; i < this.portfolios.length; i++) {
                    this.clearDatatstructuresForPortfolio(this.portfolios[i]);
                }
            }
            this.portfolios = null;
            this.initialized = false;
            this.portfoliosLoaded = false;
            this.setCurrentPortfolio(null);
            this.subscribeToAll = 0;
        };
        TradingManager.prototype.doSubscribeToAllPortfolios = function () {
            var unsubs = [];
            for (var i = 0; i < this.portfolios.length; i++) {
                unsubs.push(this.subscribe(this.portfolios[i]));
            }
            this.unsubscribeAll = function () {
                while (unsubs.length > 0) {
                    unsubs.pop()();
                }
            };
        };
        TradingManager.prototype.subscribeToAllPortfolios = function () {
            if (this.subscribeToAll == 0) {
                if (this.portfoliosLoaded) {
                    this.unsubscribe();
                    this.doSubscribeToAllPortfolios();
                }
            }
            this.subscribeToAll++;
        };
        TradingManager.prototype.unsubscribeFromAllPortfolios = function () {
            this.subscribeToAll--;
            if (this.subscribeToAll == 0) {
                this.unsubscribe();
                this.subscribe(this.currentPortfolio);
            }
        };
        TradingManager.prototype.getPortfolios = function () {
            var _this = this;
            if (this.initialized && this.started) {
                this.portfolios = null;
                var opts = new Infront.DataOptions();
                opts.onData = function (result) {
                    if (result.hasOwnProperty(Infront.MWSConstants.portfolios)) {
                        _this.updatePortfolios(result[Infront.MWSConstants.portfolios]);
                    }
                };
                opts.onError = function (error_code, error_message) {
                    0;
                };
                this.infront.trGetPortfolioNames(opts);
            }
        };
        TradingManager.prototype.getPortfolio = function (portfolioName) {
            for (var i = 0; i < this.portfolios.length; i++) {
                if (this.portfolios[i].name == portfolioName) {
                    return this.portfolios[i];
                }
            }
            return null;
        };
        TradingManager.prototype.createDatastructuresForPortfolio = function (p) {
            this.infront.getCache().createArray(Infront.CacheKeyFactory.createPortfolioPositionsKey(p), Infront.CacheKeyFactory.createPositionKey);
            this.infront.getCache().createArray(Infront.CacheKeyFactory.createPortfolioCashPositionsKey(p), Infront.CacheKeyFactory.createCashPositionKey);
            this.infront.getCache().createArray(Infront.CacheKeyFactory.createPortfolioOrdersKey(p), Infront.CacheKeyFactory.createOrderKey);
            this.infront.getCache().createArray(Infront.CacheKeyFactory.createPortfolioTradesKey(p), Infront.CacheKeyFactory.createTradeKey);
            this.infront.getCache().createArray(Infront.CacheKeyFactory.createPortfolioValuesKey(p), Infront.CacheKeyFactory.createValueKey);
        };
        TradingManager.prototype.clearDatatstructuresForPortfolio = function (p) {
            this.infront.getCache().clearArray(Infront.CacheKeyFactory.createPortfolioPositionsKey(p));
            this.infront.getCache().clearArray(Infront.CacheKeyFactory.createPortfolioCashPositionsKey(p));
            this.infront.getCache().clearArray(Infront.CacheKeyFactory.createPortfolioOrdersKey(p));
            this.infront.getCache().clearArray(Infront.CacheKeyFactory.createPortfolioTradesKey(p));
            this.infront.getCache().clearArray(Infront.CacheKeyFactory.createPortfolioValuesKey(p));
        };
        TradingManager.prototype.updatePortfolios = function (newPortfolios) {
            var _this = this;
            if (newPortfolios && newPortfolios.length > 0) {
                this.portfolios = [];
                for (var i = 0; i < newPortfolios.length; i++) {
                    var p = new Portfolio(newPortfolios[i]['name'], newPortfolios[i]['description']);
                    this.portfolios.push(p);
                    this.createDatastructuresForPortfolio(p);
                }
                if (this.externallySetActivePortfolioName) {
                    this.setCurrentPortfolioName(this.externallySetActivePortfolioName);
                }
                else {
                    this.setCurrentPortfolio(this.portfolios[0]);
                }
                if (this.subscribeToAll > 0) {
                    this.doSubscribeToAllPortfolios();
                }
                this.observerManager.foreach(function (obs) {
                    obs.availablePortfoliosChanged(_this.portfolios);
                });
                this.portfoliosLoaded = true;
                this.processDeferredCalls();
            }
            else {
                this.portfolios = null;
                this.setCurrentPortfolio(null);
            }
        };
        TradingManager.prototype.setCurrentPortfolioName = function (portfolioName) {
            if (this.portfolios && this.portfolios.length > 0) {
                for (var i = 0; i < this.portfolios.length; i++) {
                    if (this.portfolios[i].name == portfolioName) {
                        this.setCurrentPortfolio(this.portfolios[i]);
                        break;
                    }
                }
                //Unknown portfolio, so we go with it and create the necessary datastructures
                var p = new Portfolio(portfolioName, "");
                this.createDatastructuresForPortfolio(p);
                this.setCurrentPortfolio(p);
            }
            else {
                this.externallySetActivePortfolioName = portfolioName;
            }
        };
        TradingManager.prototype.setCurrentPortfolio = function (portfolio) {
            var _this = this;
            var newPortfolioName = portfolio ? portfolio.name : "";
            var currentPortfolioName = this.currentPortfolio ? this.currentPortfolio.name : "";
            if (newPortfolioName != currentPortfolioName) {
                this.externallySetActivePortfolioName = null;
                this.currentPortfolio = portfolio;
                if (portfolio != null) {
                    this.updateTradingPower(portfolio);
                }
                if (this.subscribeToAll == 0) {
                    this.unsubscribe();
                    this.unsubscribeAll = this.subscribe(portfolio);
                }
                this.currentPortfolioReady = false; //= this.readyPortfolios.indexOf(portfolio.name) > -1;
                this.observerManager.foreach(function (obs) {
                    obs.currentPortfolioChanged(_this.currentPortfolio);
                    if (_this.currentPortfolioReady || portfolio == null) {
                        obs.currentPortfolioReady(_this.currentPortfolio);
                    }
                });
            }
        };
        TradingManager.prototype.getCurrentPortfolio = function () {
            return this.currentPortfolio;
        };
        TradingManager.prototype.portfolioReady = function (portfolio) {
            if (portfolio && this.readyPortfolios.indexOf(portfolio.name) == -1) {
                this.readyPortfolios.push(portfolio.name);
            }
            if (!this.currentPortfolioReady && portfolio.name == this.currentPortfolio.name) {
                this.currentPortfolioReady = true;
                this.observerManager.foreach(function (observer) {
                    observer.currentPortfolioReady(portfolio);
                });
            }
        };
        TradingManager.prototype.observe = function (obs) {
            var _this = this;
            if (!this.started) {
                this.started = true;
                this.getPortfolios();
            }
            this.observerManager.addObserver(obs);
            if (this.portfolios != null) {
                InfrontUtil.setZeroTimeout(function () {
                    obs.currentPortfolioChanged(_this.currentPortfolio);
                    obs.availablePortfoliosChanged(_this.portfolios);
                    if (_this.readyPortfolios.indexOf(_this.currentPortfolio.name) > -1) {
                        obs.currentPortfolioReady(_this.currentPortfolio);
                    }
                });
            }
        };
        TradingManager.prototype.removeObserver = function (obs) {
            this.observerManager.removeObserver(obs);
        };
        TradingManager.prototype.updateTradingPower = function (portfolio) {
            var _this = this;
            var opts = new Infront.TradingPowerOptions();
            opts.onData = function (result) {
                _this.infront.getCache().update(Infront.CacheKeyFactory.createPortfolioDataKey(portfolio), result);
                //if (result.hasOwnProperty("trading_power")) {
                //    this.infront.getCache().update(CacheKeyFactory.createPotentialValueKey(portfolio.name, "trading_power"), {
                //        "value": result["trading_power"]
                //    });
                //}
            };
            opts.onError = function (error_code, error_message) {
                0;
            };
            this.infront.trGetTradingPower(portfolio.name, opts);
        };
        TradingManager.prototype.updatePositionInstrument = function (position) {
            var _this = this;
            var instrument = position[Infront.MWSConstants.instrument];
            var posKey = Infront.CacheKeyFactory.createPositionKey(position);
            var opts = new Infront.DataOptions();
            opts.onData = function (data) {
                var cacheObj = {};
                cacheObj[Infront.MWSConstants.instrument] = data[Infront.MWSConstants.instrument];
                _this.infront.getCache().update(posKey, cacheObj);
            };
            opts.onError = InfrontUtil.errorLogCallback;
            this.infront.instrumentInfo(instrument.feed, instrument.ticker, opts);
        };
        TradingManager.prototype.setUpMarketValueField = function (position) {
            var cacheKey = Infront.CacheKeyFactory.createPositionKey(position);
            var instrument = Infront.Instrument.inf_clone(position[Infront.MWSConstants.instrument]);
            var validInstrument = instrument.isValid();
            var instrCacheKey = Infront.CacheKeyFactory.createInstrumentKey(instrument);
            //Set up Hybrid currency field
            var instrCurrRef = new Infront.FieldReference(instrCacheKey, [Infront.MWSConstants.instrument, Infront.MWSConstants.currency]);
            var posCurrRef = new Infront.FieldReference(cacheKey, [Infront.MWSConstants.instrument, Infront.MWSConstants.currency]);
            var compCurrRef = new Infront.FieldReference(cacheKey, TradingManager.kCompositeCurrency);
            this.infront.getCache().createComputedField(cacheKey, TradingManager.kCompositeCurrency, [posCurrRef], function (args) {
                return args[0] || args[1];
            });
            var volumeRef = new Infront.FieldReference(cacheKey, Infront.MWSConstants.volume);
            if (validInstrument) {
                //Set up market value
                var priceRef = new Infront.FieldReference(instrCacheKey, Infront.MWSConstants.last_valid);
                var tsRef = new Infront.FieldReference(instrCacheKey, Infront.BindingCache.kCacheTimestampField);
                var mpRef = new Infront.FieldReference(cacheKey, Infront.MWSConstants.multiplier);
                var marketPriceRef = new Infront.FieldReference(cacheKey, Infront.MWSConstants.market_price);
                this.infront.getCache().createComputedField(cacheKey, TradingManager.kMarketValueField, [volumeRef, priceRef, tsRef, mpRef, marketPriceRef], function (args) {
                    if (InfrontUtil.isNumber(args[0]) && InfrontUtil.isNumber(args[1])) {
                        //We have numeric values for both arguments, great!
                        return args[0] * args[1] * args[3]; //volume * price * multiplier
                    }
                    else if (InfrontUtil.isNumber(0) && InfrontUtil.isNumber(args[4]) && InfrontUtil.isNumber(args[2])) {
                        //We don't have a price (and we do have a timestamp) but we do have market price from the position object. Calculate using this instead of Price.
                        return args[0] * args[4] * args[3];
                    }
                    else if (InfrontUtil.isNumber(args[2])) {
                        //We don't have numeric values, but we do have a timestamp. This means we have gotten an update and still don't have data, so we return 0.
                        return 0;
                    }
                    else {
                        //We are still waiting for data.
                        return undefined;
                    }
                });
            }
            else {
                //If we don't have a valid instrument, we just set market-value based on what we get from the TGW
                var tmpObj = {};
                tmpObj[TradingManager.kMarketValueField] = position[Infront.MWSConstants.volume] * position[Infront.MWSConstants.market_price] * position[Infront.MWSConstants.multiplier];
                this.infront.getCache().update(cacheKey, tmpObj);
            }
            //Convert to Base market-value
            var mktValRef = new Infront.FieldReference(cacheKey, TradingManager.kMarketValueField);
            var baseMktValRef = new Infront.FieldReference(cacheKey, TradingManager.kBaseMarketValueField);
            // var srcCurrencyRef = new FieldReference(cacheKey, [MWSConstants.instrument, MWSConstants.currency]);
            this.currencyConverter.createConvertedFieldFromCache(mktValRef, compCurrRef, this.baseCurrency, baseMktValRef);
            //Set up todays change
            // var changeField:string = this.infront.isStreaming() ? "change" : "lv_change";
            // var changeRef = new FieldReference(instrCacheKey, changeField);
            // this.infront.getCache().createComputedField(cacheKey, TradingManager.kChangeValueField, [volumeRef, changeRef], (args:any[])=>{
            //     return args[0] * args[1];
            // });
            //Set up converted base invested field
            var baseInvestedRef = new Infront.FieldReference(cacheKey, Infront.MWSConstants.base_invested);
            var baseCurrencyRef = new Infront.FieldReference(cacheKey, Infront.MWSConstants.base_currency);
            var convertedBaseInvestedRef = new Infront.FieldReference(cacheKey, TradingManager.kConvertedBaseInvestedField);
            this.currencyConverter.createConvertedFieldFromCache(baseInvestedRef, baseCurrencyRef, this.baseCurrency, convertedBaseInvestedRef);
            // var changeValref = new FieldReference(cacheKey, TradingManager.kChangeValueField);
            var todaysPLRef = new Infront.FieldReference(cacheKey, TradingManager.kTodaysPL);
            var baseChangeValRef = new Infront.FieldReference(cacheKey, TradingManager.kBaseChangeValueField);
            this.currencyConverter.createConvertedFieldFromCache(todaysPLRef, compCurrRef, this.baseCurrency, baseChangeValRef);
            //Subscribe to change/lv_change, last, last-valid
            if (instrument.isValid()) {
                this.infront.subscribe(instrument, ["LAST", "LAST_VALID", "CHANGE", "PREVIOUS_CLOSE"], true);
            }
            //Set up total change
            // var baseInvestedRef = new FieldReference(cacheKey, MWSConstants.base_invested);
            // if( !position.hasOwnProperty(MWSConstants.base_invested) ) {
            //     //Hack base invested with today's currency.
            //     var investedRef = new FieldReference(cacheKey, MWSConstants.invested);
            //     this.currencyConverter.createConvertedFieldFromCache(investedRef, srcCurrencyRef, this.baseCurrency, baseInvestedRef);
            // }
            var investedRef = new Infront.FieldReference(cacheKey, Infront.MWSConstants.invested);
            this.infront.getCache().createComputedField(cacheKey, TradingManager.kResultValueField, [investedRef, mktValRef], function (args) {
                if (InfrontUtil.noElementsUndefined(args)) {
                    return args[1] - args[0];
                }
                else {
                    return 0;
                }
            });
            //Convert to Base result-value
            var resultValueRef = new Infront.FieldReference(cacheKey, TradingManager.kResultValueField);
            var baseResultValueRef = new Infront.FieldReference(cacheKey, TradingManager.kBaseResultValueField);
            this.currencyConverter.createConvertedFieldFromCache(resultValueRef, compCurrRef, this.baseCurrency, baseResultValueRef);
        };
        /**
         * Unsubscribes from fields required to calcuate market-value and related fields ( see setUpMarketValueField() )
         * @param position
         */
        TradingManager.prototype.unsubscribeFromMarketvalueData = function (position) {
            var instrument = position[Infront.MWSConstants.instrument];
            this.infront.unsubscribe(instrument, ["LAST", "LAST_VALID", "CHANGE", "PREVIOUS_CLOSE"]);
        };
        TradingManager.prototype.setUpTodaysPLField = function (portfolio, position) {
            var _this = this;
            var key = Infront.CacheKeyFactory.createPositionKey(position);
            var instr = new Infront.Instrument(position[Infront.MWSConstants.instrument][Infront.MWSConstants.feed], position[Infront.MWSConstants.instrument][Infront.MWSConstants.ticker]);
            this.infront.getCache().customBind(key, [TradingManager.kBaseMarketValueField], Infront.BindingFactory.createInlineBinding(function (val) {
                _this.calculatePositionPL(portfolio, instr);
            }));
        };
        TradingManager.prototype.setUpCashPositionMarketValueField = function (cashPosition) {
            var cacheKey = Infront.CacheKeyFactory.createCashPositionKey(cashPosition);
            var srcValueref = new Infront.FieldReference(cacheKey, Infront.MWSConstants.amount);
            var srcCurrencyRef = new Infront.FieldReference(cacheKey, [Infront.MWSConstants.cash, Infront.MWSConstants.currency]);
            var baseMktValRef = new Infront.FieldReference(cacheKey, TradingManager.kBaseMarketValueField);
            this.currencyConverter.createConvertedFieldFromCache(srcValueref, srcCurrencyRef, this.baseCurrency, baseMktValRef);
        };
        /**
         * Modifies an order-object for insertion into cache. It extracts AlgoParams from an array and makes them into normal cache-values.
         * It also adds the read only field (default value of false) if it is missing.
         * @param order
         */
        TradingManager.prototype.getStrategy = function (feed, strategyID, callback) {
            this.strategyManager.getStrategy(feed, strategyID, callback);
        };
        TradingManager.prototype.processOrderObject = function (order) {
            var _this = this;
            if (order.hasOwnProperty(Infront.MWSConstants.algo_params) && order.hasOwnProperty(Infront.MWSConstants.algo_id) && order[Infront.MWSConstants.algo_id].length > 0) {
                var params = order[Infront.MWSConstants.algo_params];
                this.strategyManager.getStrategy(order[Infront.MWSConstants.instrument][Infront.MWSConstants.feed], order[Infront.MWSConstants.algo_id], function (strategy) {
                    if (strategy) {
                        var newParams = {};
                        for (var i = 0; i < params.length; i++) {
                            var id = params[i][Infront.MWSConstants.id].toLowerCase();
                            newParams[id] = _this.processAlgoParam(id, params[i][Infront.MWSConstants.value], strategy);
                        }
                        var orderProxy = {};
                        //orderProxy[MWSConstants.instrument] = order[MWSConstants.instrument];
                        //orderProxy[MWSConstants.order_id] = order[MWSConstants.order_id];
                        orderProxy[Infront.MWSConstants.algo_params] = newParams;
                        _this.infront.getCache().update(Infront.CacheKeyFactory.createOrderKey(order), orderProxy);
                    }
                });
                delete order[Infront.MWSConstants.algo_params];
            }
            if (order.hasOwnProperty(Infront.MWSConstants.custom_tags)) {
                var tags = order[Infront.MWSConstants.custom_tags];
                var newTags = {};
                for (var i = 0; i < tags.length; i++) {
                    newTags[tags[i][Infront.MWSConstants.id].toString().toLowerCase()] = tags[i][Infront.MWSConstants.value];
                }
                order[Infront.MWSConstants.custom_tags] = newTags;
            }
            if (!order.hasOwnProperty(Infront.MWSConstants.read_only)) {
                order[Infront.MWSConstants.read_only] = false;
            }
            //Remove any error-status from cache if they are not present in an update.
            if (!order.hasOwnProperty(Infront.MWSConstants.fail_code)) {
                order[Infront.MWSConstants.fail_code] = null;
            }
            if (!order.hasOwnProperty(Infront.MWSConstants.error_message)) {
                order[Infront.MWSConstants.error_message] = null;
            }
            var instrCacheKey = Infront.CacheKeyFactory.createInstrumentKey(new Infront.Instrument(order[Infront.MWSConstants.instrument][Infront.MWSConstants.feed], order[Infront.MWSConstants.instrument][Infront.MWSConstants.ticker]));
            var orderCacheKey = Infront.CacheKeyFactory.createOrderKey(order);
            var lastRef = new Infront.FieldReference(instrCacheKey, [this.infront.isStreaming() ? "last" : "last_valid"]);
            var priceRef = new Infront.FieldReference(orderCacheKey, ["price"]);
            this.infront.getCache().createComputedField(orderCacheKey, TradingManager.kOrderHealthKey, [lastRef, priceRef], function (args) {
                if (InfrontUtil.isNumber(args[0]) && InfrontUtil.isNumber(args[1])) {
                    var diff = args[1] - args[0];
                    var diffPct = Math.abs(diff / args[1]) * 100;
                    //console.log(order["instrument"]["ticker"] + ": " + diffPct);
                    if (diffPct > 2) {
                        return "BAD";
                    }
                    else if (diffPct < 0.5) {
                        return "GOOD";
                    }
                    else {
                        return "NEUTRAL";
                    }
                }
                else {
                    return null;
                }
            });
            //Subscribe to last, last-valid
            // var opts = new QuoteOptions();
            // opts.fields = ["LAST", "LAST_VALID"];
            // opts.onError = this.infront.getCache().getGuaranteedSnapshotOnErrorCallback(order["instrument"], true);
            // opts.onData = this.infront.getCache().getGuaranteedSnapshotOnDataCallback(order["instrument"]);
            // this.infront.quote(order["instrument"], opts);
            this.infront.subscribe(order[Infront.MWSConstants.instrument], ["LAST", "LAST_VALID"], true);
            return order;
        };
        TradingManager.prototype.processAlgoParam = function (id, value, strategy) {
            var retVal;
            var paramDef = strategy.findParam(id);
            if (paramDef) {
                switch (paramDef.type) {
                    case Infront.AlgoParam.PRICE:
                    case Infront.AlgoParam.INT:
                    case Infront.AlgoParam.DOUBLE:
                    case Infront.AlgoParam.STOP_PRICE:
                        retVal = parseFloat(value);
                        break;
                    case Infront.AlgoParam.DATE:
                        var dateStr = value;
                        retVal = dateStr.substr(0, 4) + "-" + dateStr.substr(4, 2) + "-" + dateStr.substr(6, 2);
                        break;
                    case Infront.AlgoParam.TIME:
                    case Infront.AlgoParam.DATETIME:
                    case Infront.AlgoParam.FREE_TEXT:
                    case Infront.AlgoParam.INIT_CODE:
                    case Infront.AlgoParam.PARENT:
                    case Infront.AlgoParam.DONE_CODE:
                    case Infront.AlgoParam.VALID_SESSION:
                    case Infront.AlgoParam.DROPDOWN:
                    case Infront.AlgoParam.BOOL:
                    case Infront.AlgoParam.INSTRUMENT:
                    case Infront.AlgoParam.VOLUME_LOTS:
                    case Infront.AlgoParam.MULTI_LINE:
                    default:
                        retVal = value;
                        break;
                }
            }
            else {
                retVal = value;
            }
            return retVal;
        };
        TradingManager.prototype.processPosition = function (portfolio, position) {
            if (!position.hasOwnProperty(Infront.MWSConstants.portfolio))
                position[Infront.MWSConstants.portfolio] = portfolio.name;
            if (position.hasOwnProperty(Infront.MWSConstants.cash)) {
                position[Infront.MWSConstants.instrument] = {
                    "instrument_type": "CASH",
                    "ticker": position[Infront.MWSConstants.cash][Infront.MWSConstants.currency]
                };
                if (position[Infront.MWSConstants.amount] < 0) {
                    position[Infront.MWSConstants.amount] = position[Infront.MWSConstants.amount];
                }
                this.setUpCashPositionMarketValueField(position);
                this.infront.getCache().updateArrayItem(Infront.CacheKeyFactory.createPortfolioCashPositionsKey(portfolio), position);
            }
            else if (position.hasOwnProperty(Infront.MWSConstants.volume) && position[Infront.MWSConstants.volume] != 0) {
                //Add multiplier (1) if not present. This makes computations later easier, as we can rely on this field being present.
                if (!position.hasOwnProperty(Infront.MWSConstants.multiplier)) {
                    position[Infront.MWSConstants.multiplier] = 1;
                }
                //So. if we don't have market-price, we set it to avg-price (invested/volume). We do this to minimize the effect
                //positions with no data has on totals. If we get market-data, we use that anyway, but if we have no market-data and no
                //market_price from TGW, we set value = invested.
                if (!position.hasOwnProperty(Infront.MWSConstants.market_price) || !InfrontUtil.isNumber(position[Infront.MWSConstants.market_price])) {
                    if (position[Infront.MWSConstants.invested]) {
                        position[Infront.MWSConstants.market_price] = position[Infront.MWSConstants.invested] / position[Infront.MWSConstants.volume];
                    }
                    else {
                        position[Infront.MWSConstants.market_price] = 0;
                    }
                }
                this.infront.getCache().updateArrayItem(Infront.CacheKeyFactory.createPortfolioPositionsKey(portfolio), position);
                this.setUpMarketValueField(position);
                this.setUpTodaysPLField(portfolio, position);
                this.updatePositionInstrument(position);
            }
            else if (position.hasOwnProperty(Infront.MWSConstants.volume) && position[Infront.MWSConstants.volume] == 0) {
                this.infront.getCache().deleteArrayItem(Infront.CacheKeyFactory.createPortfolioPositionsKey(portfolio), position);
                //this.unsubscribeFromMarketvalueData(position); TODO: We have to make sure we actually subscibed before putting this back in
            }
        };
        TradingManager.prototype.calculatePositionPLForPortfolio = function (portfolio) {
            var _this = this;
            var cache = this.infront.getCache();
            var positions = cache.getArray(Infront.CacheKeyFactory.createPortfolioPositionsKey(portfolio));
            positions.foreach(function (posKey) {
                var feed = cache.get(posKey, [Infront.MWSConstants.instrument, Infront.MWSConstants.feed]);
                var ticker = cache.get(posKey, [Infront.MWSConstants.instrument, Infront.MWSConstants.ticker]);
                var instrument = new Infront.Instrument(feed, ticker);
                _this.calculatePositionPL(portfolio, instrument);
            });
        };
        TradingManager.prototype.calculatePositionPL = function (portfolio, instrument) {
            if (!this.todaysPLEnabled) {
                return;
            }
            var cache = this.infront.getCache();
            var posKey = Infront.CacheKeyFactory.createPotentialPositionKey(portfolio.name, instrument);
            var instrKey = Infront.CacheKeyFactory.createInstrumentKey(instrument);
            var currentVolume;
            var prvCls;
            var last;
            // var change:number;
            var marketPrice;
            var trades = [];
            var cachedTrades = cache.getArray(Infront.CacheKeyFactory.createPortfolioTradesKey(portfolio));
            cachedTrades.foreach(function (item) {
                var feed = cache.get(item, [Infront.MWSConstants.instrument, Infront.MWSConstants.feed]);
                var ticker = cache.get(item, [Infront.MWSConstants.instrument, Infront.MWSConstants.ticker]);
                if (instrument.feed == feed && instrument.ticker == ticker) {
                    var obj = {};
                    obj.volume = cache.get(item, [Infront.MWSConstants.volume]);
                    obj.price = cache.get(item, [Infront.MWSConstants.price]);
                    obj.buySell = cache.get(item, [Infront.MWSConstants.buy_or_sell]);
                    obj.tradeTime = Date.parse(cache.get(item, [Infront.MWSConstants.trade_time]));
                    obj.signedVolume = (obj.buySell == "SELL" ? -1 : 1) * obj.volume;
                    trades.push(obj);
                }
            });
            trades = trades.sort(function (a, b) {
                return a - b;
            });
            var cm = new InfrontUtil.MultiCallbackHandler(function () {
                marketPrice = cache.get(posKey, [Infront.MWSConstants.market_price]);
                if (last == -1) {
                    last = marketPrice;
                    prvCls = marketPrice;
                }
                var contractSize = cache.get(instrKey, Infront.MWSConstants.contract_size);
                if (!InfrontUtil.isNumber(contractSize)) {
                    contractSize = 1;
                }
                var cleanChange = last - prvCls;
                if (!InfrontUtil.isNumber(cleanChange))
                    cleanChange = 0;
                var pl = TradingManager.doCalculatePositionPL(currentVolume, prvCls, last, cleanChange, contractSize, trades);
                var obj = {};
                obj[TradingManager.kTodaysPL] = pl;
                cache.update(posKey, obj);
            });
            cache.asyncGet(cm.addCallback(function (val) {
                currentVolume = val;
            }), posKey, Infront.MWSConstants.volume);
            if (instrument.isValid()) {
                cache.asyncGet(cm.addCallback(function (val) {
                    prvCls = val;
                }), instrKey, Infront.MWSConstants.previous_close);
                cache.asyncGet(cm.addCallback(function (val) {
                    last = val;
                }), instrKey, Infront.MWSConstants.last_valid);
            }
            else {
                prvCls = -1;
                last = -1;
            }
        };
        TradingManager.doCalculatePositionPL = function (actualPortfolioVolume, prvCls, marketPrice, change, contractSize, trades) {
            var currentVolume; //The volume we are modifying throughout the function
            var realizedValueGain;
            var newPotentialValue;
            var newPotentialVolume;
            var oldPotentialVolume;
            function applyLogicalTrade(signedVolume, signedValue, price, isRealizing) {
                var initialPrice;
                var priceDelta;
                var realizeVolume;
                if (isRealizing) {
                    realizeVolume = signedVolume;
                    // First "eat" any position we have been building today
                    if (newPotentialVolume != 0) {
                        // Entry price = avg price of existing today's position building
                        initialPrice = newPotentialValue / (newPotentialVolume * contractSize);
                        // Split trades spanning the today's pos and historical pos border into two logical trades
                        realizeVolume = InfrontUtil.sign(signedVolume) * Math.min(Math.abs(signedVolume), Math.abs(newPotentialVolume));
                        // Realize until NewPotentialVolume is zero
                        priceDelta = initialPrice - price;
                        realizedValueGain = realizedValueGain + (realizeVolume * priceDelta);
                        // Note: Bugfix, we cannot set these to 0, must decrease as we're reducing the position,
                        // but in a way that keeps the correct avg position price
                        newPotentialVolume = newPotentialVolume + realizeVolume;
                        newPotentialValue = newPotentialValue + (realizeVolume * initialPrice);
                        // If there is anything left of the trade, eat from the historical position instead
                        realizeVolume = signedVolume - realizeVolume;
                    }
                    if (realizeVolume != 0) {
                        initialPrice = prvCls;
                        priceDelta = initialPrice - price;
                        realizedValueGain = realizedValueGain + (realizeVolume * priceDelta);
                        // Reduce historical posision towards zero
                        if (oldPotentialVolume != 0) {
                            if (Math.abs(oldPotentialVolume) > Math.abs(realizeVolume)) {
                                oldPotentialVolume = oldPotentialVolume + realizeVolume;
                            }
                            else {
                                oldPotentialVolume = 0;
                            }
                        }
                    }
                }
                else {
                    // The trade is "building" the current posistion stronger (buys for long position)
                    // Keep a tally of how much we have bought for today
                    newPotentialVolume = newPotentialVolume + signedVolume;
                    newPotentialValue = newPotentialValue + signedValue;
                }
                currentVolume = currentVolume + signedVolume;
            }
            function calcSignedValue(trade, volume) {
                if (volume === void 0) { volume = null; }
                var useVolume = volume != null ? volume : trade.volume;
                return trade.price * useVolume;
            }
            var realizeVolume;
            var oldPotentialValueGain;
            var newPotentialValueGain;
            var priceDelta;
            var avgPriceToday;
            currentVolume = actualPortfolioVolume;
            realizedValueGain = 0;
            newPotentialValue = 0;
            newPotentialVolume = 0;
            // First adjust volume to the value it had before trading today
            for (var i = 0; i < trades.length; i++) {
                currentVolume = currentVolume - trades[i].signedVolume;
            }
            // Three value gains that must be calculated / maintained
            // - OldPotentialValueGain: Unrealized gain on remaining opening position volume (MktPrice - PrevClose)
            // - RealizedValueGain: Relized gain on day-trading (SellTrade.Price - BuyTrade.Price) and on position reducing trades (SellTrade.Price - PrevClose)
            // - NewPotentialValueGain: Unrealized gain on new "building" volume (MktPrice - Trade.Price)
            oldPotentialVolume = currentVolume; // This is the position volume at the beginning of the day
            // Now loop through all the trades again
            for (var i = 0; i < trades.length; i++) {
                var trade = trades[i];
                var isRealizing = (currentVolume != 0) && (InfrontUtil.sign(trade.signedVolume) != InfrontUtil.sign(currentVolume));
                if (isRealizing) {
                    // Realizing (part of) a long/short position
                    realizeVolume = InfrontUtil.sign(trade.signedVolume) * Math.min(trade.volume, Math.abs(currentVolume));
                    // Split trades spanning the long/short border into two logical trades
                    // Realize until CurrentVolume is zero
                    applyLogicalTrade(realizeVolume, calcSignedValue(trade, Math.abs(realizeVolume)), trade.price, isRealizing);
                    // Continue building the opposite position (long <-> short)
                    realizeVolume = trade.signedVolume - realizeVolume;
                    if (realizeVolume != 0) {
                        applyLogicalTrade(realizeVolume, calcSignedValue(trade, Math.abs(realizeVolume)), trade.price, isRealizing);
                    }
                }
                else {
                    applyLogicalTrade(trade.signedVolume, calcSignedValue(trade), trade.price, isRealizing);
                }
            }
            oldPotentialValueGain = change * oldPotentialVolume;
            if (newPotentialVolume != 0) {
                avgPriceToday = newPotentialValue / (Math.abs(newPotentialVolume) * contractSize);
                priceDelta = marketPrice - avgPriceToday;
                newPotentialValueGain = newPotentialVolume * priceDelta;
            }
            else {
                newPotentialValueGain = 0;
            }
            return (oldPotentialValueGain + newPotentialValueGain + realizedValueGain) * contractSize;
        };
        TradingManager.prototype.subscribePortfolio = function (portfolio) {
            var _this = this;
            //Portfolio Data
            var opts = new Infront.DataOptions();
            opts.onError = function (error_code, error_message) {
                0;
                //It isn't exactly ready, but we have all the information we can about this portfolio, so we send the ready callback anyway.
                _this.portfolioReady(portfolio);
            };
            opts.onData = function (data) {
                if (data.hasOwnProperty(Infront.MWSConstants.positions)) {
                    var positions = data[Infront.MWSConstants.positions];
                    for (var i = 0; i < positions.length; i++) {
                        _this.processPosition(portfolio, positions[i]);
                    }
                }
                if (data.hasOwnProperty(Infront.MWSConstants.position)) {
                    _this.processPosition(portfolio, data[Infront.MWSConstants.position]);
                }
                if (data.hasOwnProperty(Infront.MWSConstants.values)) {
                    var values = data[Infront.MWSConstants.values];
                    for (var i = 0; i < values.length; i++) {
                        values[i]["key"] = TradingManager.createPortfolioValueId(values[i]["name"]); //values[i]["name"].replace(" ", "_").toLowerCase();
                    }
                    _this.infront.getCache().updateArrayItems(Infront.CacheKeyFactory.createPortfolioValuesKey(portfolio), data[Infront.MWSConstants.values]);
                }
                if (data.hasOwnProperty(Infront.MWSConstants.value)) {
                    var value = data[Infront.MWSConstants.value];
                    value["key"] = TradingManager.createPortfolioValueId(value["name"]); // value["name"].replace(" ", "_").toLowerCase();
                    _this.infront.getCache().updateArrayItem(Infront.CacheKeyFactory.createPortfolioValuesKey(portfolio), value);
                }
                if (data.hasOwnProperty(Infront.MWSConstants.alerts)) {
                    _this.infront.getCache().updateArrayItems(Infront.CacheKeyFactory.createPortfolioAlertsKey(portfolio), data[Infront.MWSConstants.alerts]);
                }
                if (data.hasOwnProperty(Infront.MWSConstants.alert)) {
                    _this.infront.getCache().updateArrayItem(Infront.CacheKeyFactory.createPortfolioAlertsKey(portfolio), data[Infront.MWSConstants.alert]);
                }
                if (data.hasOwnProperty(Infront.MWSConstants.total_invested)) {
                    var obj = {};
                    obj[Infront.MWSConstants.total_invested] = data[Infront.MWSConstants.total_invested];
                    _this.infront.getCache().update(Infront.CacheKeyFactory.createPortfolioDataKey(portfolio), obj);
                }
                //At this point the portfolio has data, so we send the portfolio ready event if this is the current portfolio.
                _this.portfolioReady(portfolio);
                //var values:Object[] = data.hasOwnProperty(MWSConstants.value) ? [data[MWSConstants.value]] : data.hasOwnProperty(MWSConstants.values) ? data[MWSConstants.values] : null;
                //if( values ) {
                //    for( var i = 0; i < values.length; i++ ) {
                //        this.infront.getCache().updateArrayItem(CacheKeyFactory.createPortfolioValuesKey(portfolio), values[i]);
                //    }
                //}
            };
            return this.infront.trPortfolio(portfolio.name, opts);
        };
        TradingManager.prototype.subscribeOrders = function (portfolio) {
            var _this = this;
            //Orders
            var orOpts = new Infront.DataOptions();
            orOpts.onError = InfrontUtil.errorLogCallback;
            orOpts.onData = function (data) {
                var initializedObj = {};
                initializedObj[TradingManager.kInitializedField] = true;
                if (data.hasOwnProperty(Infront.MWSConstants.orders)) {
                    var ordersCacheKey = Infront.CacheKeyFactory.createPortfolioOrdersKey(portfolio);
                    var orders = data[Infront.MWSConstants.orders];
                    for (var i = 0; i < orders.length; i++) {
                        _this.infront.getCache().updateArrayItem(ordersCacheKey, _this.processOrderObject(orders[i]));
                        //We use this field so that clients can listen to a single field when wating for the cache to populate.
                        _this.infront.getCache().update(Infront.CacheKeyFactory.createOrderIdKey(portfolio.name, orders[i][Infront.MWSConstants.order_id]), initializedObj);
                    }
                }
                if (data.hasOwnProperty(Infront.MWSConstants.order)) {
                    _this.infront.getCache().updateArrayItem(Infront.CacheKeyFactory.createPortfolioOrdersKey(portfolio), _this.processOrderObject(data[Infront.MWSConstants.order]));
                    //We use this field so that clients can listen to a single field when wating for the cache to populate.
                    _this.infront.getCache().update(Infront.CacheKeyFactory.createOrderIdKey(portfolio.name, data[Infront.MWSConstants.order][Infront.MWSConstants.order_id]), initializedObj);
                    // TODO: consider updating trading power only on certain order states
                    _this.updateTradingPower(portfolio);
                }
            };
            return this.infront.trOrders(portfolio.name, orOpts);
        };
        TradingManager.prototype.subscribeTrades = function (portfolio) {
            var _this = this;
            //Trades
            var trOpts = new Infront.DataOptions();
            trOpts.onError = InfrontUtil.errorLogCallback;
            trOpts.onData = function (data) {
                if (data.hasOwnProperty(Infront.MWSConstants.trades)) {
                    _this.infront.getCache().updateArrayItems(Infront.CacheKeyFactory.createPortfolioTradesKey(portfolio), data[Infront.MWSConstants.trades]);
                    _this.calculatePositionPLForPortfolio(portfolio);
                }
                if (data.hasOwnProperty(Infront.MWSConstants.trade)) {
                    var trade = data[Infront.MWSConstants.trade];
                    _this.infront.getCache().updateArrayItem(Infront.CacheKeyFactory.createPortfolioTradesKey(portfolio), trade);
                    var instr = new Infront.Instrument(trade[Infront.MWSConstants.instrument][Infront.MWSConstants.feed], trade[Infront.MWSConstants.instrument][Infront.MWSConstants.ticker]);
                    _this.calculatePositionPL(portfolio, instr);
                }
            };
            return this.infront.trTrades(portfolio.name, trOpts);
        };
        TradingManager.prototype.unsubscribe = function () {
            if (this.unsubscribeAll) {
                this.unsubscribeAll();
                this.unsubscribeAll = null;
            }
        };
        TradingManager.prototype.setComputedCacheValues = function (portfolio) {
            ////Total Market Value
            var valuesArrayKey = Infront.CacheKeyFactory.createPortfolioValuesKey(portfolio);
            var positionsArrayKey = Infront.CacheKeyFactory.createPortfolioPositionsKey(portfolio);
            var valueName = "Market value";
            var valueKey = TradingManager.createPortfolioValueId(valueName); // valueName.replace(" ", "_").toLowerCase();
            var portfolioMarketValueKey = Infront.CacheKeyFactory.createPotentialValueKey(portfolio.name, valueName);
            this.infront.getCache().createInCacheFieldComputation(portfolioMarketValueKey, "value", positionsArrayKey, TradingManager.kBaseMarketValueField, Infront.ComputationStrategy.PASSIVE, function (item) { return item; }, InfrontUtil.sumArray);
            this.infront.getCache().update(portfolioMarketValueKey, {
                name: valueName,
                key: valueKey,
                portfolio: portfolio.name
            });
            this.infront.getCache().insertKeyIntoArray(valuesArrayKey, portfolioMarketValueKey);
            //Sum Cash value
            var cashPositionsArrayKey = Infront.CacheKeyFactory.createPortfolioCashPositionsKey(portfolio);
            var cpValueName = "Sum Cash";
            var cpValueKey = TradingManager.createPortfolioValueId(cpValueName);
            var portfolioSumCashValueKey = Infront.CacheKeyFactory.createPotentialValueKey(portfolio.name, cpValueName);
            this.infront.getCache().createInCacheFieldComputation(portfolioSumCashValueKey, "value", cashPositionsArrayKey, TradingManager.kBaseMarketValueField, Infront.ComputationStrategy.PASSIVE, function (item) { return item; }, InfrontUtil.sumArray);
            this.infront.getCache().update(portfolioSumCashValueKey, {
                name: cpValueName,
                key: cpValueKey,
                portfolio: portfolio.name
            });
            this.infront.getCache().insertKeyIntoArray(valuesArrayKey, portfolioSumCashValueKey);
            //Portfolio Value
            var totalValueName = "Portfolio value";
            var totalValueKey = TradingManager.createPortfolioValueId(totalValueName);
            var portfolioTotalValueKey = Infront.CacheKeyFactory.createPotentialValueKey(portfolio.name, totalValueName);
            var marketValueRef = new Infront.FieldReference(portfolioMarketValueKey, "value");
            var sumCashValueRef = new Infront.FieldReference(portfolioSumCashValueKey, "value");
            //var totalCashRef = new FieldReference(CacheKeyFactory.createPotentialValueKey(portfolio.name, TradingManager.kTotalCashName), "value");
            this.infront.getCache().createComputedField(portfolioTotalValueKey, "value", [marketValueRef, sumCashValueRef], function (args) {
                return (typeof (args[0]) == "number" ? args[0] : 0) + (typeof (args[1]) == "number" ? args[1] : 0);
            });
            this.infront.getCache().update(portfolioTotalValueKey, {
                name: totalValueName,
                key: totalValueKey,
                portfolio: portfolio.name
            });
            this.infront.getCache().insertKeyIntoArray(valuesArrayKey, portfolioTotalValueKey);
        };
        TradingManager.prototype.subscribe = function (portfolio) {
            var unsubs = [];
            if (portfolio) {
                unsubs.push(this.subscribePortfolio(portfolio));
                unsubs.push(this.subscribeOrders(portfolio));
                unsubs.push(this.subscribeTrades(portfolio));
                this.setComputedCacheValues(portfolio);
            }
            return function () {
                while (unsubs.length > 0) {
                    unsubs.pop()();
                }
            };
        };
        TradingManager.prototype.processDeferredCalls = function () {
            if (this.deferredCalls) {
                for (var i = 0; i < this.deferredCalls.length; i++) {
                    this.deferredCalls[i]();
                }
                this.deferredCalls = null;
            }
        };
        TradingManager.prototype.deferToPostPortfolioLoad = function (runner) {
            if (this.portfoliosLoaded) {
                runner();
            }
            else {
                if (!this.deferredCalls) {
                    this.deferredCalls = [];
                }
                this.deferredCalls.push(runner);
            }
        };
        TradingManager.prototype.enableTodaysPL = function () {
            this.todaysPLEnabled = true;
        };
        TradingManager.prototype.needValue = function (valueName) {
            var _this = this;
            var func = null;
            var lcName = valueName.toLowerCase();
            switch (lcName) {
                case TradingManager.kTotalChangeTodayName:
                    func = function () {
                        _this.needTodayTotalChange();
                    };
                    break;
                case TradingManager.kTotalResultName:
                    func = function () {
                        _this.needTotalResult();
                    };
                    break;
                case TradingManager.kBaseTotalResultName:
                    func = function () {
                        _this.needBaseTotalResult();
                    };
                    break;
                case TradingManager.kTotalChangeTodayPercentName:
                    func = function () {
                        _this.needTodayChangePercentValue();
                    };
                    break;
                case TradingManager.kTotalResultPercentName:
                    func = function () {
                        _this.needTotalChangePercentValue();
                    };
                    break;
            }
            if (func != null) {
                this.deferToPostPortfolioLoad(func);
            }
        };
        TradingManager.prototype.needTodayTotalChange = function () {
            this.createSummedValueForAllPortfolios(TradingManager.kBaseChangeValueField, TradingManager.kTotalChangeTodayName);
        };
        TradingManager.prototype.needTotalResult = function () {
            this.createSummedValueForAllPortfolios(TradingManager.kResultValueField, TradingManager.kTotalResultName);
        };
        TradingManager.prototype.needBaseTotalResult = function () {
            this.createSummedValueForAllPortfolios(TradingManager.kBaseResultValueField, TradingManager.kBaseTotalResultName);
        };
        TradingManager.prototype.needTodayChangePercentValue = function () {
            for (var i = 0; i < this.portfolios.length; i++) {
                var portfolio = this.portfolios[i];
                var valuesArrayKey = Infront.CacheKeyFactory.createPortfolioValuesKey(portfolio);
                var portfolioChangeTodayPercentKey = Infront.CacheKeyFactory.createPotentialValueKey(portfolio.name, TradingManager.kTotalChangeTodayPercentName);
                var todayChangeRef = new Infront.FieldReference(Infront.CacheKeyFactory.createPotentialValueKey(portfolio.name, TradingManager.kTotalChangeTodayName), "value");
                var portfolioValueRef = new Infront.FieldReference(Infront.CacheKeyFactory.createPotentialValueKey(portfolio.name, "portfolio value"), "value");
                this.infront.getCache().createComputedField(portfolioChangeTodayPercentKey, "value", [portfolioValueRef, todayChangeRef], function (args) {
                    if (InfrontUtil.isNumber(args[0]) && InfrontUtil.isNumber(args[1])) {
                        var prevValue = args[0] - args[1];
                        return (args[1] / prevValue) * 100;
                    }
                    else {
                        return null;
                    }
                });
            }
        };
        TradingManager.prototype.needTotalChangePercentValue = function () {
            this.needBaseTotalResult();
            this.createSummedValueForAllPortfolios(TradingManager.kConvertedBaseInvestedField, TradingManager.kConvertedTotalBaseInvestedName);
            for (var i = 0; i < this.portfolios.length; i++) {
                var portfolio = this.portfolios[i];
                var valuesArrayKey = Infront.CacheKeyFactory.createPortfolioValuesKey(portfolio);
                var portfolioTotalChangePctKey = Infront.CacheKeyFactory.createPotentialValueKey(portfolio.name, TradingManager.kTotalResultPercentName);
                var totalResultRef = new Infront.FieldReference(Infront.CacheKeyFactory.createPotentialValueKey(portfolio.name, TradingManager.kBaseTotalResultName), "value");
                var cTotalInvestedRef = new Infront.FieldReference(Infront.CacheKeyFactory.createPotentialValueKey(portfolio.name, TradingManager.kConvertedTotalBaseInvestedName), "value");
                // var totalInvestedRef = new FieldReference(CacheKeyFactory.createPotentialValueKey(portfolio.name, TradingManager.kTotalInvestedName), "value");
                this.infront.getCache().createComputedField(portfolioTotalChangePctKey, "value", [totalResultRef, cTotalInvestedRef], function (args) {
                    if (InfrontUtil.isNumber(args[0]) && InfrontUtil.isNumber(args[1])) {
                        return (args[0] / args[1]) * 100;
                    }
                    else {
                        return null;
                    }
                });
            }
        };
        TradingManager.prototype.createSummedValueForAllPortfolios = function (sourceField, targetField) {
            for (var i = 0; i < this.portfolios.length; i++) {
                this.createSummedPortfolioValue(this.portfolios[i], sourceField, targetField);
            }
        };
        TradingManager.prototype.createSummedPortfolioValue = function (portfolio, sourceField, targetField) {
            var portfolioMarketValueKey = Infront.CacheKeyFactory.createPotentialValueKey(portfolio.name, targetField);
            if (!this.infront.getCache().has(portfolioMarketValueKey, "value")) {
                var valuesArrayKey = Infront.CacheKeyFactory.createPortfolioValuesKey(portfolio);
                var positionsArrayKey = Infront.CacheKeyFactory.createPortfolioPositionsKey(portfolio);
                var valueKey = TradingManager.createPortfolioValueId(targetField);
                this.infront.getCache().createInCacheFieldComputation(portfolioMarketValueKey, "value", positionsArrayKey, sourceField, Infront.ComputationStrategy.AGGRESSIVE, function (item) {
                    return item;
                }, InfrontUtil.sumArray);
                this.infront.getCache().update(portfolioMarketValueKey, {
                    name: targetField,
                    key: valueKey,
                    portfolio: portfolio.name
                });
                this.infront.getCache().insertKeyIntoArray(valuesArrayKey, portfolioMarketValueKey);
            }
        };
        TradingManager.createPortfolioValueId = function (valueName) {
            return valueName.replace(new RegExp("\ ", "g"), "_").toLowerCase();
        };
        TradingManager.kInitializedField = "_initialized";
        TradingManager.kCompositeCurrency = "s_pos_currency";
        TradingManager.kConvertedBaseInvestedField = "c_base_invested";
        TradingManager.kMarketValueField = "market_value";
        TradingManager.kBaseMarketValueField = "base_market_value";
        TradingManager.kChangeValueField = "change_value";
        TradingManager.kBaseChangeValueField = "base_change_value";
        TradingManager.kResultValueField = "result_value";
        TradingManager.kBaseResultValueField = "base_result_value";
        TradingManager.kSumCashField = "sum_cash";
        TradingManager.kTodaysPL = "pl_today";
        TradingManager.kOrderHealthKey = "_OrderHealth";
        TradingManager.kTotalChangeTodayName = "change today";
        TradingManager.kTotalChangeTodayPercentName = "pct_change_today";
        TradingManager.kTotalResultName = "total result";
        TradingManager.kTotalResultPercentName = "total_result_pct";
        TradingManager.kBaseTotalResultName = "base_total_result";
        TradingManager.kTotalInvestedName = "total_invested";
        TradingManager.kConvertedTotalBaseInvestedName = "c_total_invested";
        TradingManager.kTotalCashName = "Total cash";
        return TradingManager;
    }());
    Infront.TradingManager = TradingManager;
    var StrategyManager = (function () {
        function StrategyManager(infront) {
            this.infront = infront;
            this.strategies = {};
            this.feedsInRequest = [];
            this.pendingRequests = [];
        }
        StrategyManager.prototype.getStrategy = function (feed, stragetyId, callback) {
            var _this = this;
            if (this.strategies[feed]) {
                InfrontUtil.setZeroTimeout(function () {
                    _this.findAndSendStrategy(_this.strategies[feed], stragetyId, callback);
                });
            }
            else if (this.feedsInRequest.indexOf(feed) > -1) {
                this.pendingRequests.push({
                    feed: feed,
                    strategyId: stragetyId,
                    callback: callback
                });
            }
            else {
                this.feedsInRequest.push(feed);
                var opts = new Infront.GetAlgosOptions();
                opts.feed = feed;
                opts.onData = function (data) {
                    _this.removeFeedInRequest(feed);
                    var array = data["algos"];
                    var strats = [];
                    for (var i = 0; i < array.length; i++) {
                        strats.push(Infront.Strategy.parseFromResultObject(array[i]));
                    }
                    _this.strategies[feed] = strats;
                    _this.findAndSendStrategy(strats, stragetyId, callback);
                    _this.processPendingRequests();
                };
                opts.onError = function (error_code, error_message) {
                    _this.removeFeedInRequest(feed);
                    callback(null);
                };
                this.infront.trGetAlgos(opts);
            }
        };
        StrategyManager.prototype.processPendingRequests = function () {
            for (var i = this.pendingRequests.length - 1; i >= 0; i--) {
                if (this.strategies.hasOwnProperty(this.pendingRequests[i].feed.toString())) {
                    var req = this.pendingRequests[i];
                    this.findAndSendStrategy(this.strategies[req.feed], req.strategyId, req.callback);
                    this.pendingRequests.splice(i, 1);
                }
            }
        };
        StrategyManager.prototype.findAndSendStrategy = function (strats, strategyId, callback) {
            var strategy = null;
            for (var i = 0; i < strats.length; i++) {
                if (strats[i].id == strategyId) {
                    strategy = strats[i];
                    break;
                }
            }
            callback(strategy);
        };
        StrategyManager.prototype.removeFeedInRequest = function (feed) {
            var idx = this.feedsInRequest.indexOf(feed);
            if (idx > -1) {
                this.feedsInRequest.splice(idx, 1);
            }
        };
        return StrategyManager;
    }());
})(Infront || (Infront = {}));
//# sourceMappingURL=CommonFramework-1.2.36.c.js.map
