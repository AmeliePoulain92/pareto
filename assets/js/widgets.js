/* Instantiate Infront web toolkit and run it when document is ready */
var infrontUI;

var chartWidget = null;
var chartWidgetOptions = null;

function setChart(feed, ticker) {
    chartWidgetOptions.instruments = [{
        "feed": feed,
        "ticker": ticker
    }];
    chartWidget.modify(chartWidgetOptions);
}

$(document).on('ready', infrontInit);

function infrontInit() {
    $.getJSON('https://online.paretosec.com/accesstoken/anonymous', function(data) {
        if (!data) {
            return;
        }

        var config = {
            language: 'no',
            routingUrl: 'https://pareto-mws1.infrontservices.com/mws',
            signed_token: data,
            token_type: 'JWT',
            client_application: 'WEB',
        };

        infrontUI = new Infront.UI(config);
        infrontUI.registerEventObserver('onReady', infrontReady);
        infrontUI.init();
    });
}

function infrontReady() {
    headerWidgetsInit();
    mainIndexChartWidgetInit();

    brokerstatsWidgetInit();
    overviewWidgetInit();
    chartWidgetInit();
    // quoteListWidgetInit();
    rankingWinnersWidgetInit();
    rankingLosersWidgetInit();
    highestTurnoverWidgetInit();
    newsListWidgetInit();
    financialCalendarWidgetInit();

    smallHistChartWigetInit();

    fundMorningstarRatingWidgetInit();

    marketRankingsWinnersWidgetInit();
    marketRankingsLosersWidgetInit();
    marketRankingsHighestTurnoverWidgetInit();
    marketRankingsHighestTurnoverParetoWidgetInit();

    marketListOverviewInstrumentInfoWidgetInit();
    marketListOverviewComplexChartWidgetInit();
    marketListOverviewHistoricalWidgetInit();
    marketListOverviewStockInfoWidgetInit();

    marketOrderBookWidgetInit();
    marketLatestTradesWidgetInit();
    marketBrokerStatisticsWidgetInit();
    marketNewsWidgetInit();
    marketQuotelistWidgetInit();
    marketFinancialCalendarWidgetInit();


    companyNewsWidgetInit();
    marketIntradayChartWidgetInit();
    marketNetBoughtWidgetInit();
    marketNetSoldWidgetInit();
    companyBrokerStatisticsWidgetInit();
    companyHistoricalPricesWidgetInit();
    companyNewsAltWidgetInit();
    companyFinancialCalendarWidgetInit();
}

function headerWidgetsInit() {
    var svOpts = new Infront.InstrumentSingleValueWidgetOptions();
    svOpts.name = "LV_PCT_CHANGE";
    // svOpts.rootElement = "span";
    svOpts.enableChangeStatusColors = true;
    svOpts.instrument = new Infront.Instrument(18177, "OSEBX");
    headerItem1Widget = infrontUI.instrumentSingleValueWidget("#headerItem1Widget", svOpts);
    svOpts.instrument = new Infront.Instrument(17921, "OMXS30");
    headerItem2Widget = infrontUI.instrumentSingleValueWidget("#headerItem2Widget", svOpts);
    svOpts.instrument = new Infront.Instrument(12, "USDNOK");
    headerItem3Widget = infrontUI.instrumentSingleValueWidget("#headerItem3Widget", svOpts);
    svOpts.instrument = new Infront.Instrument(12, "EURUSD");
    headerItem4Widget = infrontUI.instrumentSingleValueWidget("#headerItem4Widget", svOpts);
    svOpts.instrument = new Infront.Instrument(2098, "DAX");
    headerItem4Widget = infrontUI.instrumentSingleValueWidget("#headerItem5Widget", svOpts);

    svOpts.name = "LAST_VALID";
    svOpts.instrument = new Infront.Instrument(18177, "OSEBX");
    headerItem1Widget = infrontUI.instrumentSingleValueWidget("#headerItem12Widget", svOpts);
    svOpts.instrument = new Infront.Instrument(17921, "OMXS30");
    headerItem2Widget = infrontUI.instrumentSingleValueWidget("#headerItem22Widget", svOpts);
    svOpts.instrument = new Infront.Instrument(12, "USDNOK");
    headerItem3Widget = infrontUI.instrumentSingleValueWidget("#headerItem32Widget", svOpts);
    svOpts.instrument = new Infront.Instrument(12, "EURUSD");
    headerItem4Widget = infrontUI.instrumentSingleValueWidget("#headerItem42Widget", svOpts);
    svOpts.instrument = new Infront.Instrument(2098, "DAX");
    headerItem4Widget = infrontUI.instrumentSingleValueWidget("#headerItem52Widget", svOpts);
}

function mainIndexChartWidgetInit() {
    var chartOpts = new Infront.ChartWidgetOptions2();
    chartOpts.defaultPeriod = "3M";
    chartOpts.instruments = [new Infront.Instrument(18177, 'STL')];
    //chartOpts.showVolume = true;
    chartOpts.zoom = false;
    chartOpts.streaming = true;
    chartOpts.chartUI = {
        tooltipVersion: "advanced",
        periodMenu: false,
        indicatorMenu: false,
        chartTypeMenu: false,
        expandButton: false,
        searchBox: false
    };

    indexChart = infrontUI.chartWidget2("#mainIndexChart", chartOpts);

    if(!indexChart)
        return;

    window.addEventListener('resize', resizeThrottler, false);
    $('.ps-header__sandwich').on('click', resizeThrottler);

    var resizeTimeout;
    function resizeThrottler() {
        // Ignore resize events as long as an chart.resizWidget execution is in the queue
        if ( !resizeTimeout ) {
            resizeTimeout = setTimeout(function() {
                resizeTimeout = null;
                indexChart.resizeWidget();

                // The actualResizeHandler will execute at a rate of 15fps
            }, 300);
        }
    }
}

function brokerstatsWidgetInit() {
    var opts = new Infront.BrokerstatsWidgetOptions();
    opts.columns = ['TICKER', 'NET_BUY_VALUE', 'TOTAL_VALUE', 'TOTAL_VALUE_PCT'];
    opts.period = InfrontConstants.BrokerStatsPeriodes.INTRADAY;
    opts.feed = 17921;
    opts.defaultSortedColumn = 3;
    opts.broker = 'PAS';
    opts.widgetTitle = 'Highest Turnover at Pareto today (OSEBX)';
    opts.maxItems = 10;

    infrontUI.brokerstatsWidget("#brokerStats", opts);
}

function overviewWidgetInit() {
    // Market overview
    var overviewWidget = infrontUI.indexOverviewWidget('#index-overview0', {
        instrument: { feed: 18177, ticker: 'OSEBX' },
        primaryValue: 'PCT_CHANGE'
    });
}

function chartWidgetInit() {
    var chartWidget = null;
    var chartWidgetOptions = null;

    function setChart(feed, ticker) {
        chartWidgetOptions.instruments = [{
            "feed": feed,
            "ticker": ticker
        }];
        chartWidgetOptions.widgetTitle = 'Market overview';
        chartWidget.modify(chartWidgetOptions);
    }

    // Chart
    chartWidgetOptions = new Infront.ChartWidgetOptions();
    chartWidgetOptions.instruments = [new Infront.Instrument(18177, "OSEBX")];
    chartWidgetOptions.showVolume = false;
    chartWidgetOptions.type = Infront.ChartWidget.kIntraday;
    chartWidgetOptions.zoom = false;
    chartWidgetOptions.showComplexChartInColorbox = true;
    chartWidgetOptions.yAxisOpposite = true;
    chartWidgetOptions.chartUI = {
        tooltipVersion: "advanced"
    };
    chartWidget = infrontUI.paretoChartWidget("#chart", chartWidgetOptions, true);

    var quoteListOptions = new Infront.QuoteListOptions();
    quoteListOptions.columns = [
        {
            name: "TICKER",
            heading: "Major indices"
        },
        {
            name: "LAST_VALID",
            heading: "Last"
        },
        "LV_PCT_CHANGE",
        "TIME"
    ];
    quoteListOptions.sortable = false;
    quoteListOptions.enableChangeStatusColors = true;
    quoteListOptions.interactionHighlight = true;
    quoteListOptions.rowSelectable = true;
    quoteListOptions.selectedRow = 0;
    quoteListOptions.widgetTitle = 'Market overview';
    quoteListOptions.instruments = [
        { ticker: "OSEBX", feed: 18177 },
        { ticker: "OMXS30", feed: 17921 },
        { ticker: "OMXC20", feed: 17665 },
        { ticker: "OMXH25", feed: 100 },
        { ticker: "USDNOK", feed: 12 }
    ];
    quoteListOptions.onInstrumentSelected = function(instrument) {
        setChart(instrument.feed, instrument.ticker);
    };
    quoteListWidget = infrontUI.quoteList("#indicatorList", quoteListOptions);

}


function rankingWinnersWidgetInit() {
    // Winners
    var rankingOpts = new Infront.RankingWidgetOptions();
    rankingOpts.feed = 18177;
    rankingOpts.sortOrder = Infront.SortOrder.Desc;
    rankingOpts.rankingField = Infront.RankingField.LV_PCT_CHANGE;
    rankingOpts.widgetTitle = null;
    rankingOpts.columns = [
        {
            name: "TICKER",
            heading: "Ticker"
        },
        "LV_PCT_CHANGE",
        "LAST_VALID",
        {
            name: "TURNOVER",
            heading: "Net vol."
        }
    ];
    rankingOpts.enableChangeStatusColors = true;
    rankingOpts.enablePeriodeSelector = false;
    rankingOpts.rows = 5;
    rankingOpts.instrumentTypes = ["STOCK"];
    rankingOpts.interactionHighlight = true;
    rankingOpts.streaming = false; // TEMP bugfix 2017-06-27
    infrontUI.rankingWidget("#ranking0", rankingOpts);
}

function rankingLosersWidgetInit() {
    // Losers
    var rankingOpts = new Infront.RankingWidgetOptions();
    rankingOpts.feed = 18177;
    rankingOpts.rankingField = Infront.RankingField.LV_PCT_CHANGE;
    rankingOpts.columns = [
        {
            name: "TICKER",
            heading: "Ticker"
        },
        "LV_PCT_CHANGE",
        "LAST_VALID",
        "TURNOVER"
    ];
    rankingOpts.sortOrder = Infront.SortOrder.Asc;
    rankingOpts.enableChangeStatusColors = true;
    rankingOpts.enablePeriodeSelector = false;
    rankingOpts.rows = 5;
    rankingOpts.instrumentTypes = ["STOCK"];
    rankingOpts.interactionHighlight = true;
    rankingOpts.streaming = false; // TEMP bugfix 2017-06-27
    infrontUI.rankingWidget("#ranking1", rankingOpts);
}

function highestTurnoverWidgetInit() {
    // Highest turnover
    var rankingOpts = new Infront.RankingWidgetOptions();
    rankingOpts.feed = 18177;
    rankingOpts.columns = [
        {
            name: "TICKER",
            heading: "Ticker"
        },
        "LV_PCT_CHANGE",
        "LAST_VALID",
        "TURNOVER"
    ];
    rankingOpts.sortOrder = Infront.SortOrder.Desc;
    rankingOpts.rankingField = Infront.RankingField.TURNOVER;
    rankingOpts.enableChangeStatusColors = true;
    rankingOpts.enablePeriodeSelector = false;
    rankingOpts.rows = 5;
    rankingOpts.widgetTitle = "Highest turnovers";
    rankingOpts.instrumentTypes = ["STOCK"];
    rankingOpts.interactionHighlight = true;
    infrontUI.rankingWidget("#ranking2", rankingOpts);
}

function newsListWidgetInit() {
    // News
    var nlOpts = new Infront.NewsListWidgetOptions();
    nlOpts.feeds = [1189, 1160, 1018, 1123, 1023, 1014, 1161, 1033, 1020];
    nlOpts.navButton = Infront.WidgetNavButtonType.NAVIGATE;
    nlOpts.navButtonClicked = function () {
        window.location.assign("/Market/News/");
    };
    nlOpts.maxItems = 5;
    nlOpts.paging = true;
    nlOpts.pageItems = 5;
    nlOpts.enableSourceSelector = false;
    infrontUI.newsListWidget("newslist0", nlOpts);
}

function financialCalendarWidgetInit() {
    // Financial calendar
    var opts = new Infront.FinancialCalendarWidgetOptions();
    opts.countryCodes = ['NO'];
    opts.endDate = InfrontUtil.addMonths(new Date(), 6);
    opts.layout = Infront.FinancialCalendarLayout.EXPANDED;
    opts.paging = false;
    opts.pageItems = 5;
    infrontUI.financialCalendarWidget("#calendar", opts);
}

function smallHistChartWigetInit() {
    var chartWidgetOptions = new Infront.ChartWidgetOptions();
    chartWidgetOptions.instruments = [{
        "feed": 18177,
        "ticker": "SCHA"
    }, {
        "feed": 17921,
        "ticker": 'OMXS30'
    }];
    chartWidgetOptions.showVolume = false;
    chartWidgetOptions.type = Infront.ChartWidget.kHistorical
    chartWidgetOptions.defaultPeriod = "3M";
    chartWidgetOptions.zoom = false;
    chartWidgetOptions.showComplexChartInColorbox = true;
    chartWidgetOptions.yAxisOpposite = true;
    chartWidget = infrontUI.paretoChartWidget("#smallHistChart", chartWidgetOptions, true);
}

function fundMorningstarRatingWidgetInit() {
    var opts = new Infront.FundMorningstarRatingWidgetOptions();
    opts.instrument = new Infront.Instrument(4241, "0P00000HCU");
    morningstarRatingWidget = infrontUI.fundMorningstarRatingWidget("#fundMorningstar", opts);
}

function marketRankingsWinnersWidgetInit() {
    var rankingOpts = new Infront.RankingWidgetOptions();
    rankingOpts.feed = 18177;
    rankingOpts.columns = [
        "TICKER",
        "LAST_VALID",
        "CHANGE",
        "LV_PCT_CHANGE",
        "ACC_VOLUME",
        "TURNOVER",
            // {
                // name: "tradeLink",
                // type: "custom",
                // className: "cell-text-right",
                // content: function(rowId) {
                //     return "<a href='" + PARETO.instrumentTradeLink(rowId.ticker, rowId.feed) + "'><img title='Trade this' alt='H' src='/gfx/tradebtn_mini_en.png' class='btnTrade'></a>";
                // }
            // },
        {
            name: "PCT_CHANGE",
            hidden: true
        }
    ];
    rankingOpts.rankingField = Infront.RankingField.PCT_CHANGE;
    //rankingOpts.sortOrder = Infront.SortOrder.Desc;
    rankingOpts.enablePeriodSelector = true;
    rankingOpts.enableChangeStatusColors = true;
    rankingOpts.interactionHighlight = true;
    rankingOpts.instrumentTypes = ["STOCK"];
    rankingOpts.streaming = false; // TEMP bugfix 2017-06-27
    // rankingOpts.onInstrumentSelected = function(instrument) {
    //     window.location.assign("/Ticker/" + instrument.ticker + "/" + instrument.feed);
    // };
    rankingOpts.rows = 20;
    winnersWidget = infrontUI.rankingWidget("#marketRankingsWinners", rankingOpts);
}

function marketRankingsLosersWidgetInit() {
    var rankingOpts = new Infront.RankingWidgetOptions();
    rankingOpts.feed = 18177;
    rankingOpts.columns = [
        "TICKER",
        "LAST_VALID",
        "CHANGE",
        "LV_PCT_CHANGE",
        "ACC_VOLUME",
        "TURNOVER",
        // {
        //     name: "tradeLink",
        //     type: "custom",
        //     className: "cell-text-right",
        //     content: function(rowId) {
        //         return "<a href='" + PARETO.instrumentTradeLink(rowId.ticker, rowId.feed) + "'><img title='Trade this' alt='H' src='/gfx/tradebtn_mini_en.png' class='btnTrade'></a>";
        //     }
        // }
        , {
            name: "PCT_CHANGE",
            hidden: true
        }
    ];
    rankingOpts.rankingField = Infront.RankingField.PCT_CHANGE;
    rankingOpts.sortOrder = Infront.SortOrder.Asc;
    rankingOpts.enablePeriodSelector = true;
    rankingOpts.enableChangeStatusColors = true;
    rankingOpts.interactionHighlight = true;
    rankingOpts.streaming = false; // TEMP bugfix 2017-06-27
    rankingOpts.instrumentTypes = ["STOCK"];
    // rankingOpts.onInstrumentSelected = function(instrument) {
    //     window.location.assign("/Ticker/" + instrument.ticker + "/" + instrument.feed);
    // };
    rankingOpts.rows = 20;
    losersWidget = infrontUI.rankingWidget("#marketRankingsLosers", rankingOpts);
}

function marketRankingsHighestTurnoverWidgetInit() {
    var rankingOpts = new Infront.RankingWidgetOptions();
    rankingOpts.feed = 18177;
    rankingOpts.columns = [
        "TICKER",
        "LAST_VALID",
        "CHANGE",
        "LV_PCT_CHANGE",
        "BID",
        "ASK",
        "HIGH",
        "LOW",
        "OPEN",
        "PREV_CLOSE",
        "ACC_VOLUME",
        "TURNOVER",
        "S_DATETIME",
        // {
        //     name: "tradeLink",
        //     type: "custom",
        //     className: "cell-text-right",
        //     content: function (rowId) {
        //         return "<a href='" + PARETO.instrumentTradeLink(rowId.ticker, rowId.feed) + "'><img title='Trade this' alt='H' src='/gfx/tradebtn_mini_en.png' class='btnTrade'></a>";
        //     }
        // }
    ];
    rankingOpts.rankingField = Infront.RankingField.TURNOVER;
    rankingOpts.sortOrder = Infront.SortOrder.Desc;
    rankingOpts.enablePeriodSelector = false;
    rankingOpts.enableChangeStatusColors = true;
    rankingOpts.widgetTitle = "Highest turnover";
    rankingOpts.interactionHighlight = true;
    rankingOpts.instrumentTypes = ["STOCK"];
    // rankingOpts.onInstrumentSelected = function (instrument) {
    //     window.location.assign("/Ticker/" + instrument.ticker + "/" + instrument.feed);
    // };
    rankingOpts.rows = 20;
    highestTurnoverWidget = infrontUI.rankingWidget("#marketRankingsHighestTurnover", rankingOpts);
}

function marketRankingsHighestTurnoverParetoWidgetInit() {
    var opts = new Infront.BrokerstatsWidgetOptions();
    opts.period = InfrontConstants.BrokerStatsPeriodes.INTRADAY;
    opts.sortable = false;
    opts.instrument = null;
    opts.widgetTitle = "Highest turnover at Pareto";
    opts.feed = 18177;
    opts.broker = "PAS";
    opts.columns = [
        "TICKER",
        "FULL_NAME",
        "TOTAL_VALUE",
        "TOTAL_VALUE_PCT",
        {
            name: "SYM_TURN",
            heading: "SymTurn"
        },
        {
            name: "SYM_TURN_PCT",
            heading: "SymTurn%"
        },
        "NET_VOLUME",
        "INT_VOLUME",
        "AVG_BUY",
        "AVG_SELL",
        "TRADES"
        // "TIME"
        //,"S_DATETIME"
    ];
    opts.defaultSortedColumn = 2;
    opts.interactionHighlight = true;
    // opts.onInstrumentSelected = function (instrument) {
    //     window.location.assign("/Ticker/" + instrument.ticker + "/" + instrument.feed);
    // };

    //opts.enablePeriodSelector = true;
    highestTurnoverParetoWidget = infrontUI.brokerstatsWidget("#marketRankingsHighestTurnoverPareto", opts);
}

function marketListOverviewInstrumentInfoWidgetInit() {
    var quoteListOptions = new Infront.QuoteListOptions();
    quoteListOptions.columns = ["LAST_VALID", "LV_CHANGE", "LV_PCT_CHANGE", "BID", "ASK", "HIGH", "LOW", "PREVIOUS_CLOSE", "TURNOVER", "ACC_VOLUME", "S_DATETIME"];
    quoteListOptions.sortable = false;
    quoteListOptions.enableChangeStatusColors = true;
    quoteListOptions.instruments = [{
        ticker: 'STL',
        feed: 18177
    }];
    overviewInstrumentInfoWidget = infrontUI.quoteList("#marketListOverviewInstrumentInfo", quoteListOptions);
}

function marketListOverviewComplexChartWidgetInit() {
    var instrument = new Infront.Instrument(18177, 'STL');
    var opts = new Infront.ChartWidgetOptions2();
    opts.defaultPeriod = "6M";
    opts.selectablePeriods = ['1D', '5D', '10D', '1M', '6M', '1Y', '5Y', 'YTD'];
    // opts.type = Infront.ChartWidgetConstants.kInteractive;
    opts.periodSelectorPosition = "CENTER";
    opts.instruments = instrument;
    opts.showVolume = true;
    opts.zoom = true;
    opts.chartUI = {
        tooltipVersion: "advanced",
        searchBox: false,
        expandButton: false,
    };
    overviewComplexChartWidget = infrontUI.chartWidget2("#marketListOverviewComplexChart", opts);

    if(!overviewComplexChartWidget)
        return;

    window.addEventListener('resize', resizeThrottler, false);
    $('.ps-header__sandwich').on('click', resizeThrottler);

    var resizeTimeout;
    function resizeThrottler() {
        // Ignore resize events as long as an chart.resizWidget execution is in the queue
        if ( !resizeTimeout ) {
            resizeTimeout = setTimeout(function() {
                resizeTimeout = null;
                overviewComplexChartWidget.resizeWidget();

                // The actualResizeHandler will execute at a rate of 15fps
            }, 300);
        }
    }
}

function marketListOverviewHistoricalWidgetInit() {
    var hOpts = new Infront.HistoricalOverviewWidgetOptions();
    hOpts.instrument = { ticker: 'STL', feed: 18177 };
    hOpts.historicFields = ["1W", "1M", "3M", "6M", "1Y"];
    overviewHistoricalWidget = infrontUI.historicalOverviewWidget("#marketListOverviewHistorical", hOpts);
}

function marketListOverviewStockInfoWidgetInit() {
    var opts = new Infront.InstrumentValuesWidgetOptions();
    opts.instrument = { ticker: 'STL', feed: 18177 };
    opts.fields = [
        {
            name: "ISIN",
            className: "cell-text-right"
        },
        "MARGIN_RATE",
        {
            name: "IS_SHORTABLE",
            heading: "Available for shorting",
            className: "cell-text-right"
        },
        {
            name: "MIN_ICEBERG_SIZE",
            heading: "Minimum open volume"
        },
        {
            name: "COMPANY_URL",
            heading: ' '
        }
    ];
    opts.layout = Infront.MultipleValuesWidgetLayout.VERTICAL;
    overviewStockInfoWidget = infrontUI.instrumentValuesWidget("#marketListOverviewStockInfo", opts);
}

function marketOrderBookWidgetInit() {
    var orderBookOpts = new Infront.OrderbookWidgetOptions();
    orderBookOpts.instrument = { feed: 18177, ticker: 'STL' };
    orderBookOpts.levels = 5;
    orderBookOpts.layout = Infront.OrderbookRowLayout.WIDE;
    marketOrderBookWidget = infrontUI.orderbookWidget("#marketOrderBook", orderBookOpts);
}

function marketLatestTradesWidgetInit() {
    var itOpts = new Infront.IntradayTradesWidgetOptions();
    itOpts.instrument = { feed: 18177, ticker: 'STL' };
    itOpts.widgetTitle = "Latest trades";
    itOpts.pageItems = 5;
    // itOpts.navButton = Infront.WidgetNavButtonType.EXPAND;
    // itOpts.navButtonClicked = function () {
    //     expandLatestTrades();
    // };
    itOpts.paging = false;
    itOpts.columns = ["TIME", "LAST", "VOLUME", "BUYER", "SELLER", "MARKET"];
    marketLatestTradesWidget = infrontUI.intradayTradesWidget("#marketLatestTrades", itOpts);
}

function marketBrokerStatisticsWidgetInit() {
    var brokerStatsOpts = new Infront.BrokerstatsWidgetOptions();
    brokerStatsOpts.feed = 17921;
    brokerStatsOpts.columns = ["NAME",
        "FULLNAME",
        "TOTAL_VALUE",
        "TOTAL_VALUE_PCT",
        "NET_BUY_VALUE",
        "INT_VALUE",
        "INT_TRADES",
        "TRADES",
        "TRADES_PCT",
        "HIT_VALUE_PCT",
        "TAKE_VALUE_PCT"
        // "S_DATETIME"
    ];

    brokerStatsOpts.period = InfrontConstants.BrokerStatsPeriodes.INTRADAY;
    brokerStatsOpts.sortable = true;
    brokerStatsOpts.interactionHighlight = true;
    brokerStatsOpts.defaultSortedColumn = 2;
    brokerStatsOpts.enablePeriodSelector = true;
    brokerStatsOpts.widgetTitle = "Broker statistics " + ('SE' == 'SE' ? 'STO' : 'OSE');
    // brokerStatsOpts.onBrokerSelected = function(broker) {
    //     window.location.href = "/Market/BrokerInstrumentStatistics/" + broker;
    // };

    brokerStatsWidget = infrontUI.brokerstatsWidget('#marketBrokerStats', brokerStatsOpts);
}

function marketNewsWidgetInit() {
    opts = new Infront.NewsListWidgetOptions();
    opts.maxItems = 200;
    opts.paging = true;
    opts.pageItems = 20;
    opts.customFilters = [{
        name: "Norway",
        region: 47
    }, {
        name: "Oslo Børs",
        feeds: [1189, 18182, 1160]
    }, {
        name: "OTC",
        feeds: [1380]
    }, {
        name: "Sweden",
        region: 46
    }, {
        name: "Nasdaq, Stockholm",
        feeds: [17929, 1000, 17930]
    }, {
        name: "Global",
        feeds: [1336, 1134, 1132, 1067, 1133, 1117, 1374]
    }];
    //opts.defaultRegion = defaultRegion;
    opts.preSelectedRegions = ["Norway"];
    opts.enableSourceSelector = true;
    opts.enableRegionSelector = true;
    opts.enableQuickFilter = true;
    newsWidget = infrontUI.newsListWidget("#newsWidget", opts);
}

function marketQuotelistWidgetInit() {
    var cvOpts = new Infront.ChainViewerWidgetOptions();
    cvOpts.columns = [
        {
            "name": "TICKER",
            //"alwaysVisible": true,
            "hover": "FULL_NAME"
        },
        "FULL_NAME",

        "LAST_VALID",

        "LV_CHANGE",
        "LV_PCT_CHANGE",
        "BID",
        "ASK",
        "HIGH",
        "LOW",
        "OPEN",
        "PREVIOUS_CLOSE",
        "TURNOVER",
        {
            name: "ONEXCH_VOLUME",
            heading: "Turnover"
        },
        {
            name: "MCAP",
            heading: "MCap",
            type: "computed",
            shorten: true,
            dataType: Infront.DataType.Integer,
            computeFields: ["NUM_SHARES", "LAST_VALID"],
            compute: function(rowId, args) {
                return args[0] * args[1];
            }
        },
        "YTD_PCT_CHANGE",
        "ONE_W_PCT_CHANGE",
        "ONE_M_PCT_CHANGE",
        "THREE_M_PCT_CHANGE",
        "SIX_M_PCT_CHANGE",
        "ONE_Y_PCT_CHANGE",
        "TWO_Y_PCT_CHANGE",
        "THREE_Y_PCT_CHANGE",
        "FIVE_Y_PCT_CHANGE",
        "S_DATETIME",
        // {
        //     name: "tradeLink",
        //     type: "custom",
        //     className: "cell-text-right",
        //     content: function (rowId) {
        //         var result = "<a href='" + PARETO.instrumentTradeLink(rowId.ticker, rowId.feed) + "'><img title='Trade this' alt='H' src='/gfx/tradebtn_mini_en.png' class='btnTrade'></a>";
        //         return result;
        //     }
        // }
    ];

    var overviewColumns = [
        "TICKER",
        "FULL_NAME",
        "LAST_VALID",
        "LV_CHANGE",
        "LV_PCT_CHANGE",
        "BID",
        "ASK",
        "HIGH",
        "LOW",
        "OPEN",
        "PREVIOUS_CLOSE",
        "TURNOVER",
        "MCAP",
        "S_DATETIME",
        "tradeLink"
    ];

    var performanceColumns = [
        "TICKER",
        "LV_CHANGE",
        "LV_PCT_CHANGE",
        "YTD_PCT_CHANGE",
        "ONE_W_PCT_CHANGE",
        "ONE_M_PCT_CHANGE",
        "THREE_M_PCT_CHANGE",
        "SIX_M_PCT_CHANGE",
        "ONE_Y_PCT_CHANGE",
        "TWO_Y_PCT_CHANGE",
        "THREE_Y_PCT_CHANGE",
        "FIVE_Y_PCT_CHANGE",
        "tradeLink"
    ];
    cvOpts.sortable = true;
    cvOpts.enableChangeStatusColors = true;

    cvOpts.tabs = [
        {
            id: "overview",
            label: "Overview",
            columns: overviewColumns
        },
        {
            id: "performance",
            label: "Performance",
            columns: performanceColumns
        }
    ];

    cvOpts.interactionHighlight = true;
    // cvOpts.onInstrumentSelected = function(instrument) {
    //     window.location.assign("/Ticker/" + instrument.ticker + "/" + instrument.feed);
    // };

    cvOpts.chains = [
        { feed: 965, provider: 31, name: "OBX", description: "OBX" },
        { feed: 965, provider: 31, name: "Standard", description: "OB Standard" },
        { feed: 965, provider: 31, name: "Match", description: "OB Match" },
        { feed: 18177, provider: 31, name: "MERK", description: "Merkur Market" },
        { feed: 965, provider: 31, name: "New", description: "OB Nye" },
        { feed: 965, provider: 31, name: "OBPCC", description: "Egenkapitalbevis" },
        { feed: 965, provider: 31, name: "ETF", description: "ETF" },
        { feed: 965, provider: 31, name: "Axess", description: "Oslo Axess" },
        { feed: 965, provider: 31, name: "ETN", description: "ETN" },
        { feed: 965, provider: 31, name: "NorwegianStocks", description: "Norwegian stocks" }
    ];
    cvOpts.widgetTitle = 'OBX';

    listWidget = infrontUI.chainViewer("#quotelist", cvOpts);
}

function marketFinancialCalendarWidgetInit() {
    var opts = new Infront.FinancialCalendarWidgetOptions();
    opts.countryCodes = ['SE'];
    opts.endDate = InfrontUtil.addMonths(new Date(), 12);
    opts.layout = Infront.FinancialCalendarLayout.EXPANDED;
    opts.paging = true;
    opts.pageItems = 20;
    opts.maxItems = 200;
    finCalWidget = infrontUI.financialCalendarWidget("#financialCalendar", opts);
}

function companyNewsWidgetInit() {
    var allNewsSources = [1336, 1134, 1132, 1067, 1133, 1117, 1189, 1160, 1018, 1123, 1023, 1014, 1161, 1033, 1020, 1333, 1327, 17939, 17930, 17929, 1158, 1114, 1116, 1235, 1038, 1122, 17933, 1206, 1000];
    var opts = new Infront.NewsListWidgetOptions();
    opts.instrument = new Infront.Instrument(18177, 'STL');
    opts.enableRegionSelector = false;
    opts.defaultRegion = 47; // Norge
    opts.maxItems = 8;
    opts.feeds = allNewsSources;
    marketNewsHeadlinesWidget = infrontUI.newsListWidget("#companyNews", opts);
}

function marketIntradayChartWidgetInit() {
    chartWidgetOptions = new Infront.ChartWidgetOptions();
    chartWidgetOptions.instruments = [{
        ticker: 'STL',
        feed: 18177
    }];
    chartWidgetOptions.showVolume = false;
    chartWidgetOptions.type = Infront.ChartWidget.kIntraday;
    chartWidgetOptions.zoom = false;
    chartWidgetOptions.widgetTitle = 'Today';
    chartWidgetOptions.showComplexChartInColorbox = true;
    chartWidgetOptions.yAxisOpposite = true;
    marketIntradayChart = infrontUI.chartWidget("#marketIntradayChart", chartWidgetOptions);
}

function marketNetBoughtWidgetInit() {
    var bsOpts = new Infront.BrokerstatsWidgetOptions();
    bsOpts.instrument = {
        "feed": 18177,
        "ticker": 'STL'
    };
    bsOpts.columns = ["NAME", "TOTAL_VOLUME", {
        name: "TOTAL_VOLUME_PCT",
        heading: "TVolume%"
    }, "INT_VOLUME", "NET_VOLUME"];
    bsOpts.period = InfrontConstants.BrokerStatsPeriodes.INTRADAY;
    bsOpts.sortable = false;
    bsOpts.maxItems = 5;
    bsOpts.defaultSortedColumn = 4;
    bsOpts.defaultSortOrder = Infront.SortOrder.Desc;
    bsOpts.widgetTitle = "Net bought (vol.)";
    marketNetBoughtWidget = infrontUI.brokerstatsWidget("#marketNetBought", bsOpts);
}

function marketNetSoldWidgetInit() {
    var bsOpts = new Infront.BrokerstatsWidgetOptions();
    bsOpts.instrument = {
        "feed": 18177,
        "ticker": 'STL'
    };
    bsOpts.columns = ["NAME", "TOTAL_VOLUME", {
        name: "TOTAL_VOLUME_PCT",
        heading: "TVolume%"
    }, "INT_VOLUME", "NET_VOLUME"];
    bsOpts.period = InfrontConstants.BrokerStatsPeriodes.INTRADAY;
    bsOpts.sortable = false;
    bsOpts.maxItems = 5;
    bsOpts.defaultSortedColumn = 4;
    bsOpts.defaultSortOrder = Infront.SortOrder.Asc;
    bsOpts.widgetTitle = "Net sold (vol.)";
    marketNetSoldWidget = infrontUI.brokerstatsWidget("#marketNetSold", bsOpts);
}

function companyBrokerStatisticsWidgetInit() {
    var bsOpts = new Infront.BrokerstatsWidgetOptions();
    bsOpts.instrument = {
        "feed": 18177,
        "ticker": 'STL'
    };
    bsOpts.columns = [
        "NAME",
        "FULLNAME",
        "TOTAL_VALUE",
        "TOTAL_VALUE_PCT",
        "NET_VOLUME",
        "INT_VOLUME",
        "AVG_BUY",
        "AVG_SELL",
        "BUY_VALUE",
        "TRADES",
        "HIT_VALUE_PCT",
        "TAKE_VALUE_PCT"
        //"TIME"
    ];
    bsOpts.period = InfrontConstants.BrokerStatsPeriodes.INTRADAY;
    bsOpts.sortable = true;
    bsOpts.defaultSortedColumn = 4;
    bsOpts.widgetTitle = "Broker statistics STL";
    bsOpts.enablePeriodSelector = true;
    brokerstatsWidget = infrontUI.brokerstatsWidget("#companyBrokerStatistics", bsOpts);
}

function companyHistoricalPricesWidgetInit() {
    var opts = new Infront.HistoryWidgetOptions();
    opts.instrument = new Infront.Instrument(18177, 'STL');
    opts.startYear = 1999;
    var todayDate = new Date();
    var yearNow = todayDate.getFullYear();
    opts.endYear = yearNow;
    opts.columns = ["OPEN", "HIGH", "LOW", "LAST_VALID"];
    historicalPricesWidget = infrontUI.historyWidget("#companyHistoricalPrices", opts);
}

function companyNewsAltWidgetInit() {
    var nlOpts = new Infront.NewsListWidgetOptions();
    nlOpts.instrument = { feed: 18177, ticker: 'STL' };
    nlOpts.navButton = Infront.WidgetNavButtonType.NAVIGATE;
    nlOpts.maxItems = 100;
    nlOpts.pageItems = 15;
    nlOpts.paging = true;
    var fromDate = new Date();
    fromDate.setDate(fromDate.getDate() - 3*30);
    nlOpts.startTime = fromDate;

    nlOpts.enableSourceSelector = false;
    newsNewListWidget = infrontUI.newsListWidget("#companyNewsWidget", nlOpts);
}

function companyFinancialCalendarWidgetInit() {
    var opts = new Infront.FinancialCalendarWidgetOptions();
    opts.endDate = InfrontUtil.addMonths(new Date(), 8);
    opts.layout = Infront.FinancialCalendarLayout.EXPANDED;
    opts.paging = false;
    opts.pageElements = 2;
    opts.instrument = { "feed": 18177, "ticker": 'STL' };
    newsFinCalWidget = infrontUI.financialCalendarWidget("#companyFinancialCalendar", opts);
}
