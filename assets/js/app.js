var $window = $(window);
var $body = $('body');

var sidebarToggleClass = 'has-sidebar-toggle';
var accountsActiveClass = 'has-accounts-open';

var $overlay = $('.ps-overlay');
var $sidebar = $('.ps-sidebar');

var $toggleAccountsButton = $('[data-toggle-accounts]');
var $toggleSidebarButton = $('[data-toggle-sidebar]');

$window.on('load', function() {
    $body.removeClass('is-preload');
});

$(function () {
    // Sometimes the same is different, but mostly it's the same
    $toggleAccountsButton.on('click', function() {
        $body.toggleClass(accountsActiveClass);
    });

    $toggleSidebarButton.on('click', function() {
        if ($body.hasClass(accountsActiveClass)) {
            $body.removeClass(accountsActiveClass);
        } else {
            $body.toggleClass(sidebarToggleClass);
        }

        deactivateSubmenu();
    });

    $sidebar.on('mouseleave', function () {
        if ($body.hasClass(sidebarToggleClass)) {
            deactivateSubmenu();
        }
    });

    $overlay.on('click', function(e) {
        $body.removeClass(sidebarToggleClass);
        $body.removeClass(accountsActiveClass);
    });

    $window.on('resize', function() {
        var mq = window.matchMedia('(max-width: 1200px)').matches;
        if (!mq) {
            return;
        }
        $body.removeClass(sidebarToggleClass);
    });

    $('[data-toggle-submenu]').on('click', function(e) {
        e.preventDefault();

        var $parent = $(this).parent();
        var $parentSiblings = $parent.siblings();
        var $parentSubmenu = $parent.find('.ps-sidebar__submenu');
        var $siblingsSubmenu = $parentSiblings.find('.ps-sidebar__submenu');

        $parent.toggleClass('is-active');
        $parentSubmenu.slideToggle('fast');
        $parentSiblings.removeClass('is-active');
        $siblingsSubmenu.slideUp('fast');
    });

    function deactivateSubmenu() {
        $('.ps-sidebar__menu-item').removeClass('is-active');
        $('.ps-sidebar__submenu').slideUp('fast');
    }
});
