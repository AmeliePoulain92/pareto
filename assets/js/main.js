import Vue from 'vue';
import $ from 'jquery';
import axios from 'axios';

import ResearchAnalysts from './components/ResearchAnalysts.vue';
import ResearchReports from './components/ResearchReports.vue';
import InfrontWidget from './components/InfrontWidget.vue';

new Vue({
    el: '#app',
    components: {
        ResearchAnalysts,
        ResearchReports,
        InfrontWidget,
    }
});
